# ![alt text](http://image.eveonline.com/Corporation/698393874_32.png "Explode. Now. Please.") Captain Thunks Eve-Online Tournament Tools

These tools are written using the Laravel Framework v5.2 and as such has the following requirements:

* PHP >= 5.5.9
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* [Composer](https://getcomposer.org/ "Composer Information")
* [MySQL Version of Eve DB](https://www.fuzzwork.co.uk/dump/)

Any reasonably uptodate Linux installation will have the required PHP Version and extensions, if you wish to check use the following commands:

* php -v
* php -m

## Installation

Once the repo is downloaded to your server directory you need to do the following:

* Setup the Eve DB linked above
* Add the mapPlanetPyRnd.sql table to the DB (I couldn't find a PHP method matching Pythons Random.random() function - the Mersene Twister implementation appears different)
* copy '.env.example' to '.env' and edit the file changing settings as necessary
* run 'composer update' 
* run 'php artisan key:generate'
* Have your webserver point to the repo's 'public' directory
* Ensure the 'ccptourney' and child folders are owned by the web user (chown -R <user>:<group> .)
* Make sure the 'storage' and 'bootstrap/cache' folders are writeable (chmod -R 0755 storage / chmod -R 0755 bootstrap/cache)
* Setup a CREST app with CCP. https://developers.eveonline.com/applications. Fill in the values in the .env file. Callback URL is YOURWEBSITE/auth by default.
* run 'php artisan migrate' from commandline. This updates DB with new tables. Tables not included in the Eve DB are added to the database using Laravels database migration, this should keep the schema accurate
* run 'php artisan prepare:skilltree' - This prepares the Eve SkillTree table
* run 'php artisan prepare:conqstations' - This prepares the Conquerable Stations table
* run 'php artisan prepare:lpstore' - Prepares LP Store tables
* run 'php artisan prepare:supers' - (long execution) Pulls Supercap Information from zKillboard to form the basis of the supertracker

## About

The repository consists of two Alliance Tournament tools that have been used by Pandemic Legion in the last few years. They are written using the Laravel PHP Framework. These tools use CCP provided data in a legitimate and legal fashion.

### TeamBuilder

This tool is deceptively simple and is designed to allow a team of ships to be constructed that obeys the rules for the tournament. Currently the tool is using the 2015 ATXIII Ruleset. It lists the available ships listed by points value and has searching and filtering capabilities. The URLs can be copy/pasted to share or save Teams.

### Match Analyzer

This tool was first written within hours of the CREST match feed going public. It's purpose is to scrutinize the data given by CREST to determine as much information about the participating teams as possible. It is able to determine active modules and some inactive modules through logical deduction. Initially the CREST feed had a small delay of 1 minute, this was increased as CCP became aware of this tool which could provide an advantage while matches were playing, now it is only effective with matches already played.

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).