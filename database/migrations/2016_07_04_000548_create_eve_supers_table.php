<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEveSupersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eveSupercaps', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('characterID')->unsigned();
            $table->string('characterName', 255);
            $table->integer('shipTypeID')->unsigned();
            $table->string('shipTypeName', 255);
            $table->dateTime('firstSeen')->nullable();
            $table->integer('firstSeenSystemID')->unsigned()->nullable();
            $table->string('firstSeenSystemName', 255)->nullable();
            $table->string('firstSeenRegionName', 255)->nullable();
            $table->dateTime('lastSeen')->nullable();
            $table->integer('lastSeenSystemID')->unsigned()->nullable();
            $table->string('lastSeenSystemName', 255)->nullable();
            $table->string('lastSeenRegionName', 255)->nullable();
            $table->integer('totalKills')->unsigned()->nullable();
            $table->integer('corporationID')->unsigned()->nullable();
            $table->string('corporationName', 255)->nullable();
            $table->integer('allianceID')->unsigned()->nullable();
            $table->string('allianceName', 255)->nullable();
            $table->integer('factionID')->unsigned()->nullable();
            $table->string('factionName', 255)->nullable();
            $table->double('lastMailX')->nullable();
            $table->double('lastMailY')->nullable();
            $table->double('lastMailZ')->nullable();
            $table->string('mailClosestCelestial', 255)->nullable();
            $table->double('mailClosestCelestialDistance')->nullable();
            $table->enum('isDestroyed', ['true', 'false'])->nullable();
            $table->integer('firstKillID')->unsigned()->nullable();
            $table->integer('lastKillID')->unsigned()->nullable();
            $table->timestamps();

            $table->primary('characterID');
        });

        Schema::create('eveSupercapUsers', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('ownerID')->unsigned();
            $table->integer('characterID')->unsigned();
            $table->text('notes')->nullable();
            $table->text('associatedChars')->nullable();
            $table->string('apiKey', 65)->nullable();
            $table->integer('apiID')->unsigned()->nullable();
            $table->bigInteger('shipUniqueID')->unsigned()->nullable();
            $table->double('shipPositionX')->nullable();
            $table->double('shipPositionY')->nullable();
            $table->double('shipPositionZ')->nullable();
            $table->string('shipClosestCelestial', 255)->nullable();
            $table->double('shipClosestCelestialDistance')->nullable();
            $table->timestamps();

            $table->primary(['ownerID', 'characterID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eveSupercaps');
        Schema::drop('eveSupercapUsers');
    }
}
