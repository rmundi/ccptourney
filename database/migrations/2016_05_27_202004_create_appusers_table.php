<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appUsers', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->string('characterHash', 128);
            $table->string('characterName', 128);
            $table->string('accessToken', 128);
            $table->string('refreshToken', 256)->nullable();
            $table->integer('expires');
            $table->ipAddress('ip');
            $table->string('userAgent', 512);
            $table->timestamps();

            $table->primary(['characterID', 'characterHash']);  // unique key that will differentiate between transfered characters 
        });

        Schema::create('apiCharacterInfo', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->string('characterName', 128);
            $table->string('race', 64);
            $table->integer('bloodLineID')->unsigned();
            $table->string('bloodLine', 64);
            $table->integer('ancestryID')->unsigned();
            $table->string('ancestry', 64);
            $table->decimal('accountBalance', 20, 2)->nullable();
            //$table->integer('skillPoints')->nullable()->unsigned();
            $table->string('shipName', 128)->nullable();
            $table->integer('shipTypeID')->nullable()->unsigned();
            $table->string('shipTypeName', 128)->nullable();
            $table->integer('corporationID')->unsigned();
            $table->string('corporationName', 128);
            $table->dateTime('corporationDate');
            $table->integer('allianceID')->unsigned()->nullable();
            $table->string('allianceName', 128)->nullable();
            $table->dateTime('allianceDate')->nullable();
            $table->string('lastKnownLocation', 128)->nullable();
            $table->integer('lastKnownLocationID')->nullable();
            $table->double('securityStatus');
            $table->timestamps();

            $table->primary('characterID');
            //$table->foreign('shipTypeID')->references('typeID')->on('invTypes');
            //$table->foreign('bloodlineID')->references('bloodlineID')->on('chrBloodlines');
            //$table->foreign('ancestryID')->references('ancestryID')->on('chrAncestries');
        });

        Schema::create('apiEmploymentHistory', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->integer('recordID')->unsigned();
            $table->integer('corporationID')->unsigned();
            $table->string('corporationName', 64);
            $table->dateTime('startDate')->nullable();
            //$table->timestamps();

            $table->primary('recordID');
            $table->index('characterID');
            //$table->foreign('characterID')->references('characterID')->on('crestCharacter');
        });

        Schema::create('crestCharacter', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->string('characterName', 128);
            $table->dateTime('DoB');
            $table->string('race', 64);
            $table->integer('bloodLineID')->unsigned();
            $table->string('bloodLine', 64);
            $table->integer('ancestryID')->unsigned();
            $table->string('ancestry', 64);
            $table->string('gender', 64);
            $table->integer('skillPoints')->nullable()->unsigned();
            $table->integer('freeRespecs')->unsigned();
            $table->integer('freeSkillPoints')->unsigned();
            $table->dateTime('cloneJumpDate');
            $table->dateTime('lastRespecDate');
            $table->dateTime('lastTimedRespec');
            $table->dateTime('remoteStationDate');
            $table->integer('corporationID')->unsigned();
            $table->string('corporationName', 128);
            $table->dateTime('corporationDate');
            $table->integer('allianceID')->unsigned()->nullable();
            $table->string('allianceName', 128)->nullable();
            $table->dateTime('allianceDate')->nullable();
            $table->integer('charisma')->unsigned();
            $table->integer('intelligence')->unsigned();
            $table->integer('memory')->unsigned();
            $table->integer('perception')->unsigned();
            $table->integer('willpower')->unsigned();
            $table->dateTime('jumpActivation');
            $table->dateTime('jumpFatigue');
            $table->dateTime('jumpLastUpdate');
            $table->string('lastLocationName', 128)->nullable();
            $table->integer('lastLocationID')->nullable();
            $table->mediumText('description');
            $table->timestamps();

            $table->primary('characterID');
            //$table->foreign('bloodlineID')->references('bloodlineID')->on('chrBloodlines');
            //$table->foreign('ancestryID')->references('ancestryID')->on('chrAncestries');
        });

        Schema::create('apiJumpClones', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->integer('jumpCloneID')->unsigned()->nullable();
            $table->integer('typeID')->unsigned();
            $table->integer('locationID')->unsigned();
            $table->string('cloneName', 128);
            //$table->timestamps();

            $table->primary('jumpCloneID'); // ? may cause problems using null for current clones in crestJumpCloneImplants
            $table->index('characterID');
            //$table->foreign('typeID')->references('typeID')->on('invTypes');
            //$table->foreign('characterID')->references('characterID')->on('crestCharacter');
        });

        Schema::create('apiJumpCloneImplants', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->integer('jumpCloneID')->unsigned()->nullable();
            $table->integer('typeID')->unsigned();
            $table->string('typeName', 128);
            //$table->timestamps();

            $table->index('jumpCloneID');
            $table->index('characterID');
            //$table->foreign('characterID')->references('characterID')->on('crestCharacter');
            //$table->foreign('jumpCloneID')->references('jumpCloneID')->on('crestJumpClones');
            //$table->foreign('typeID')->references('typeID')->on('invTypes');
        });

        Schema::create('apiSkills', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->integer('typeID')->unsigned();
            $table->string('typeName', 128);
            $table->integer('skillPoints')->unsigned();
            $table->integer('level')->unsigned();
            $table->integer('published')->unsigned();
            //$table->timestamps();

            $table->index('characterID');
            //$table->foreign('typeID')->references('typeID')->on('invTypes');
            //$table->foreign('characterID')->references('characterID')->on('crestCharacter');
        });

        Schema::create('eveSkillTree', function (Blueprint $table) {
            $table->integer('typeID')->unsigned();
            $table->string('typeName', 128);
            $table->integer('groupID')->unsigned();
            $table->string('groupName', 128);
            $table->string('description', 512);
            $table->integer('rank')->unsigned();
            $table->string('primaryAttribute', 64)->nullable();
            $table->string('secondaryAttribute', 64)->nullable();
            $table->integer('canNotBeTrainedOnTrial')->unsigned();
            $table->integer('published')->unsigned();
            
            $table->primary('typeID');
        });

        Schema::create('eveRequiredSkills', function (Blueprint $table) {
            $table->integer('typeID')->unsigned();
            $table->integer('requiredSkillID')->unsigned();
            $table->integer('skillLevel')->unsigned();
            
            $table->index('typeID');
        });

        Schema::create('apiSkillQueue', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->integer('queuePosition')->unsigned();
            $table->integer('typeID')->unsigned();
            $table->integer('level')->unsigned();
            $table->integer('startSP')->unsigned();
            $table->integer('endSP')->unsigned();
            $table->dateTime('startTime');
            $table->dateTime('endTime');
            
            $table->index('characterID');
        });

        Schema::create('eveConquerableStations', function (Blueprint $table) {
            $table->integer('stationID')->unsigned();
            $table->string('stationName', 128);
            $table->integer('stationTypeID')->unsigned();
            $table->integer('solarSystemID')->unsigned();
            $table->integer('corporationID')->unsigned();
            $table->string('corporationName', 128);
            $table->double('x');
            $table->double('y');
            $table->double('z');

            $table->index('stationID');
        });

        Schema::create('apiMedals', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->integer('medalID')->unsigned();
            $table->string('reason', 256)->nullable();
            $table->enum('status', ['private', 'public']);
            $table->integer('issuerID')->unsigned();
            $table->string('issuerName', 128);
            $table->dateTime('issued');
            $table->integer('corporationID')->unsigned();
            $table->string('corporationName', 128);
            $table->string('title', 256)->nullable();
            $table->string('description', 512)->nullable();

            $table->primary(['characterID', 'medalID']);  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appUsers');
        Schema::drop('apiCharacterInfo');
        Schema::drop('apiEmploymentHistory');
        Schema::drop('apiJumpCloneImplants');
        Schema::drop('apiJumpClones');
        Schema::drop('apiSkills');
        Schema::drop('crestCharacter');
        Schema::drop('eveSkillTree');
        Schema::drop('eveRequiredSkills');
        Schema::drop('apiSkillQueue');
        Schema::drop('eveConquerableStations');
        Schema::drop('apiMedals');
    }
}
