<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountstatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apiAccountStatus', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->dateTime('paidUntil');
            $table->dateTime('createDate');
            $table->integer('logonCount')->unsigned();
            $table->integer('logonMinutes')->unsigned();
            //$table->timestamps();

            $table->primary('characterID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apiAccountStatus');
    }
}
