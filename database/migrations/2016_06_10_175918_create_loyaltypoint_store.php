<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoyaltypointStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('npcLPStore', function (Blueprint $table) {
            $table->integer('corporationID')->unsigned();
            $table->integer('storeItemID')->unsigned();

            $table->index('corporationID');
        });

        Schema::create('npcLPStoreItems', function (Blueprint $table) {
            $table->integer('storeItemID')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->decimal('iskCost', 20, 2)->nullable();
            $table->integer('lpCost')->unsigned();
            $table->integer('typeID')->unsigned();
            $table->string('typeName', 128);

            $table->primary('storeItemID');
        });

        Schema::create('npcLPStoreRequiredItems', function (Blueprint $table) {
            $table->integer('storeItemID')->unsigned();
            $table->integer('typeID')->unsigned();
            $table->string('typeName', 128);
            $table->integer('quantity')->unsigned();

            $table->index('storeItemID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('npcLPStore');
        Schema::drop('npcLPStoreItems');
        Schema::drop('npcLPStoreRequiredItems');
    }
}
