<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apiStandings', function (Blueprint $table) {
            $table->integer('ownerID')->unsigned();
            $table->enum('type', ['agents', 'NPCCorporations', 'factions']);
            $table->integer('fromID')->unsigned();
            $table->string('fromName', 128);
            $table->decimal('standing', 4, 2)->nullable();

            $table->index('ownerID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apiStandings');
    }
}
