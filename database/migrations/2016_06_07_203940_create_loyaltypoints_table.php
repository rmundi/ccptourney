<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoyaltypointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crestLoyaltyPoints', function (Blueprint $table) {
            $table->integer('characterID')->unsigned();
            $table->integer('corporationID')->unsigned();
            $table->string('corporationName', 64);
            $table->integer('quantity')->unsigned();
            //$table->timestamps();

            $table->index('characterID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crestLoyaltyPoints');
    }
}
