// define table
var table;

Number.prototype.formatNumber = function(c, d, t){
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

function addAlert(type, message, clear) {

  if(clear === true) {
    $('#alert-messages').children('div').each( function ( index ) {
      if($(this).hasClass('alert-'+type))
        $(this).remove();
    });
  }

  $('#alert-messages').append('<div class="alert alert-'+type+' fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><p class="text-center">'+message+'</p></div>');

}

function setWaypoints() {
    // check for ap links # aplocationid
    if(crest_loggedin === true) {
        $('[data-apsolarsystemid]').each(function(){

            /* places link before name of first td
            var $tr = $(this).closest('tr');
            var $tds = $tr.find("td:nth-child(1)");

            var text = $tds.text();
            text = '<img class="aplink" src="https://thunkage.ddns.net/images/WindowIcons/map.png">'+'<span class="inline-block">'+text+'</span>';
            $tds.html(text);
            */

            /* places link after station/system name of location td */
            var text = $(this).text();
            text = '<span>'+text+'</span>'+'<img class="aplink" src="'+BASEURL+'/images/WindowIcons/map.png">';
            $(this).html(text);

        });
    }
}

function getSystemCSS(system) {
    var num;
    num = Number(system).formatNumber(1);

    return "system"+num.replace('.', '-');
}
$(function($) {
    // CLEARABLE INPUT
    function tog(v) {
        return v?'addClass':'removeClass';
    } 

    $(document).on('input', '.clearable', function(){
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function( e ){
        $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');   
    }).on('click', '.onX', function(){
        $(this).removeClass('x onX').val('').change().trigger( "keyup" );
        if(typeof table !== undefined && table !== undefined) {
            table.draw();
        }
    });
});

$(document).ready(function() {
    // SSO Login button
    $('.header-nav').on('click', '#ssologin', function (){
        var link = crest_auth_url+"/?response_type=code&redirect_uri="+crest_app_url+"&client_id="+crest_app_id+"&scope="+crest_scope+"&state="+crest_app_state;
        window.location.href = link;
        return false;
    });

    // toplink
    $('#back-top').hide();
    $(window).scroll(function(){
        if($(window).scrollTop() >= 600)
        {
            $('#back-top').fadeIn(500);
        }
        else
        {
            $('#back-top').fadeOut(500);
        }
    });

    setWaypoints();

    $('#contents').on('click', '.aplink', function () {
        //console.log($(this).parent().data('apsolarsystemid'));
        var apsolarsystemid = $(this).parent().data('apsolarsystemid');
        var apsolarsystemname = $(this).parent().data('apsolarsystemname');
        var aplocationid = $(this).parent().data('aplocationid');
        var aplocationname = $(this).parent().data('aplocationname');

        // aplocationid can be a station, but we can't set that as an AP destination yet so just stick with apsolarsystemid for now
        var jqxhr = $.ajax({
            headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
            async: true,
            type: 'POST',
            url: BASEURL+'/waypoint',
            data: { solarSystemID: apsolarsystemid, solarSystemName: apsolarsystemname, locationID: aplocationid, locationName: aplocationname },
            dataType: "text"
        }).done(function(msg) {
            addAlert('success', 'AP Destination set to '+apsolarsystemname, true);
        }).fail(function() {
            addAlert('danger', 'Unspecified Error setting AP waypoint', true);
        }).always(function() {
            //console.log( "complete" );
        });

    } );

    $('.toggle').click(function(e) {
        if (typeof e.target.nextElementSibling != "undefined") {
            var collapseID = e.target.nextElementSibling.id;
            
            if($( '#'+collapseID ).hasClass('in')) {
                $(this).html('<span class="glyphicon glyphicon-chevron-up"></span>')
            } else {
                $(this).html('<span class="glyphicon glyphicon-chevron-down"></span>')
            }
        }
    });

    
});