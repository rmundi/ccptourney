var precisionCheck = 0;
var ships = [];
var isShipCalculated = {};
var replayURL = '';
var FRAMEURL = '';
var maxDefense = '';
var maxAttack = '';
var maxControl = '';

var tourneyapp = {

	playButton: "Stop",
	//ships: [],
	replayFrame: 0,
	nextFrame: 0,
	reset: false,
	droneBay: [],  // droneBay[shipID][droneID]

	redFleetBonuses: {},
	blueFleetBonuses: {},

	// replace this with a db call to select commandships
	/*
	SELECT *
	FROM ccptourney.invTypes AS t
	LEFT JOIN invGroups AS g ON t.groupID = g.groupID
	WHERE g.groupName LIKE "%Command%"
	AND g.categoryID = 6

	or groupID 540 = command ship, 1534 = command destroyer

	-- better version, gets what shipgroups warfare links can be fitted to
	SELECT GROUP_CONCAT(DISTINCT COALESCE(a.valueInt,a.valueFloat)) AS VALUE
	FROM ccptourney.invTypes AS t
	LEFT JOIN dgmTypeAttributes AS a ON a.typeID = t.`typeID`
	LEFT JOIN dgmAttributeTypes AS ta ON ta.`attributeID` = a.`attributeID`
	WHERE t.groupID = 316
	AND ta.`unitID` = 115

	/*
	$commandShips = DB::connection(DB_CCPDB)
				->table('invTypes AS t')
                ->select('t.*')
                ->whereIn('t.groupID', function($query)
				    {
				        $query->select('DISTINCT COALESCE(a.valueInt,a.valueFloat) AS VALUE')
				              ->from('invTypes AS t')
				              ->leftjoin('dgmTypeAttributes AS ta', 'ta.typeID', '=', 't.typeID')
				              ->leftjoin('dgmAttributeTypes AS at', 'at.attributeID', '=', 'ta.attributeID')
				              ->where('t.groupID', '=', 316)
				              ->where('at.unitID', '=', 115);
				    })
                ->remember(CPPDB_REMEMBER_TIME)
                ->get();


    -- should = SELECT * 
FROM invTypes AS t
WHERE t.groupID IN(540,963,547,659,30,419,941,883,1534)

*/
	commandShips: ['Tengu', 'Loki', 'Legion', 'Proteus', 'Harbinger', 'Oracle', 'Prophecy', 'Prophecy Blood Raiders Edition', 'Drake', 'Ferox', 'Ferox Guristas Edition', 'Naga', 'Brutix', 'Brutix Serpentis Edition',
						'Myrmidon', 'Talos', 'Cyclone', 'Cyclone Thukker Tribe Edition', 'Hurricane', 'Tornado', 'Brutix Navy Issue', 'Drake Navy Issue', 'Harbinger Navy Issue', 'Hurricane Fleet Issue',
						'Absolution', 'Damnation', 'Nighthawk', 'Vulture', 'Astarte', 'Eos', 'Claymore', 'Sleipnir', 'Gnosis'],

	init: function () {
        this.writeConsole('Initialised...');
        this.monitorStart();
    },

	monitorStart: function() {
		this.getReplayFrame();
	},

	getReplayFrame: function() {
		// reset dead ships
		if(this.replayFrame == 0)
			this.resetDead();

		if(this.playButton == 'Play')
			return;

		this.writeConsole('Fetching Frame '+this.replayFrame);
		//console.log(replayURL);

		// send PHP which ships are ROFcalculated
		for (var shipIDX in ships) {
			var shipHiROFChecked = true;
			for (var highSlots in ships[shipIDX]['hiSlot']) {
				 if (ships[shipIDX]['hiSlot'].hasOwnProperty(highSlots)) {
				 	if(ships[shipIDX]['hiSlot'][highSlots]['calculatedROF'] !== true)
				 		shipHiROFChecked = false;
					//console.log(ships[shipIDX]['hiSlot'][highSlots]);
				}
			}

			//if( typeof ships[shipIDX]['calculatedROF'] !== 'undefined' && ships[shipIDX]['calculatedROF'] === true) {
			if( shipHiROFChecked === true) {
				isShipCalculated[shipIDX] = {ROFChecked: true};
			} else {
				isShipCalculated[shipIDX] = {ROFChecked: false, hiSlot: ships[shipIDX]['hiSlot'], shipROFBonus: Number(ships[shipIDX]['shipROFBonus']), shipType: ships[shipIDX]['type']};
				// needs other stuff like the Ships ROF bonus (if any)
			}
		}
		//console.log(isShipCalculated);

		var jqxhr = $.ajax({
						headers: {
				            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				        },
						async: true,
						type: 'POST',
						url: BASEURL+'/ajax',
						data: {	url: replayURL,
								ROFchecks: JSON.stringify(isShipCalculated)}
					})
		.done(function( data ) {
				if(tourneyapp.reset == true) {
					tourneyapp.resetMatch();
					return;
				}

				data = JSON.parse(data);
				
				// clear lit icons
				tourneyapp.clearIcons();
//console.log(data);
				//console.log(data);
				for (var shipID in data) {
				  if (data.hasOwnProperty(shipID)) {
				    //console.log(shipID + " -> " + data[shipID]);
				    var moduleData = data[shipID];
				    if (typeof data[shipID]['physicsData'] !== 'undefined')  {
				    	ships[shipID]['physicsData'] = data[shipID]['physicsData'];
				    	//console.log(tourneyapp.calcShipVelocity(ships[shipID]['physicsData']));
				    	var speed = Number(tourneyapp.calcShipVelocity(ships[shipID]['physicsData']));
				    	var oldSpeed = ships[shipID]['maxSpeed'];

				    	$('#'+shipID+'_speed').html(speed + " m/s");
				    	if(speed > oldSpeed) {
				    		//console.log("old max: "+oldSpeed+" - " + speed);
				    		ships[shipID]['maxSpeed'] = speed;

				    	}
				    }
				    for(var moduleID in moduleData) {
				    	if(moduleData.hasOwnProperty(moduleID)) {
				    		//console.log(moduleID + " ---> " + moduleData[moduleID]);
				    		
				    		if(moduleID == 'armor') {
				    			ships[shipID]['armor'] = moduleData[moduleID];
				    		}else if(moduleID == 'shield') {
				    			ships[shipID]['shield'] = moduleData[moduleID];
				    		}else if(moduleID == 'structure') {
				    			ships[shipID]['structure'] = moduleData[moduleID];
				    		} else {
				    			var effectData = moduleData[moduleID];

				    			var targetName = null;
					    		var targetShip = null;
					    		//if(moduleData[moduleID].hasOwnProperty(targetID)) {
					    		if(typeof moduleData[moduleID]['targetID'] !== 'undefined' &&  typeof ships[moduleData[moduleID]['targetID']] !== 'undefined') {
					    			targetName = ships[moduleData[moduleID]['targetID']]['pilotName'];
					    			targetShip = ships[moduleData[moduleID]['targetID']]['type'];
					    		}

					    		// extra information for tool tip: modname/r/nduration/r/ntargetName (targetShip)

					    		if(typeof moduleData[moduleID]['name'] !== 'undefined') {
						    		

						    		if(typeof moduleData[moduleID]['duration'] !== 'undefined') {
						    			var toolTipDetails = moduleData[moduleID]['name'];
						    			

						    			if(moduleData[moduleID]['slot'] == 'mid' && moduleData[moduleID]['guid'] == 'effects.ShieldBoosting') {
						    				var shieldObj = tourneyapp.calculateShieldBooster(moduleData[moduleID]['duration'], shipID);

						    				if(shieldObj !== null && typeof shieldObj === 'object') {
												// update details for team if it's not been done
										    	if(typeof shieldObj['linkName'] !== 'undefined') {
											    	if(ships[shipID]['team'] == 'Blue' && typeof tourneyapp.blueFleetBonuses['ActiveShielding'] === 'undefined') {
											    		tourneyapp.blueFleetBonuses['ActiveShielding'] = {};
											    		tourneyapp.blueFleetBonuses['ActiveShielding']['warfareLinkLevel'] = shieldObj['warfareLinkLevel'];
											    		tourneyapp.blueFleetBonuses['ActiveShielding']['siegeWarfareLevel'] = shieldObj['siegeWarfareLevel'];
											    		tourneyapp.blueFleetBonuses['ActiveShielding']['shipLevel'] = shieldObj['shipLevel'];
											    		tourneyapp.blueFleetBonuses['ActiveShielding']['linkName'] = shieldObj['linkName'];
											    		tourneyapp.blueFleetBonuses['ActiveShielding']['implant'] = shieldObj['implant'];

											    		tourneyapp.writeBlueConsole('Active Shield Ganglink detected');
											    		tourneyapp.writeBlueConsole('Link Name: '+ shieldObj['linkName']);
											    		tourneyapp.writeBlueConsole('Warfare Link Level: '+ shieldObj['warfareLinkLevel']);
											    		tourneyapp.writeBlueConsole('Siege Warfare Link Level: '+ shieldObj['siegeWarfareLevel']);
											    		tourneyapp.writeBlueConsole('Ship Level: '+ shieldObj['shipLevel']);
											    		tourneyapp.writeBlueConsole('Warfare Implant: '+ shieldObj['implant']);
											    	}
											    	if(ships[shipID]['team'] == 'Red' && typeof tourneyapp.redFleetBonuses['ActiveShielding'] === 'undefined') {
											    		tourneyapp.redFleetBonuses['ActiveShielding'] = {};
											    		tourneyapp.redFleetBonuses['ActiveShielding']['warfareLinkLevel'] = shieldObj['warfareLinkLevel'];
											    		tourneyapp.redFleetBonuses['ActiveShielding']['siegeWarfareLevel'] = shieldObj['siegeWarfareLevel'];
											    		tourneyapp.redFleetBonuses['ActiveShielding']['shipLevel'] = shieldObj['shipLevel'];
											    		tourneyapp.redFleetBonuses['ActiveShielding']['linkName'] = shieldObj['linkName'];
											    		tourneyapp.redFleetBonuses['ActiveShielding']['implant'] = shieldObj['implant'];

											    		tourneyapp.writeRedConsole('Active Shield Ganglink detected');
											    		tourneyapp.writeRedConsole('Link Name: '+ shieldObj['linkName']);
											    		tourneyapp.writeRedConsole('Warfare Link Level: '+ shieldObj['warfareLinkLevel']);
											    		tourneyapp.writeRedConsole('Siege Warfare Link Level: '+ shieldObj['siegeWarfareLevel']);
											    		tourneyapp.writeRedConsole('Ship Level: '+ shieldObj['shipLevel']);
											    		tourneyapp.writeRedConsole('Warfare Implant: '+ shieldObj['implant']);
											    	}
											    }

							    				toolTipDetails = shieldObj['modName']+ ' '+moduleData[moduleID]['name'];

							    				if(moduleData[moduleID]['duration'] != shieldObj['baseCycle'] && moduleData[moduleID]['duration'] != shieldObj['baseCycleSiege']) {
							    					$( "#"+moduleData[moduleID]['id'] ).parent('div').append( '<div class="module-overheat"></div>' );
							    				}
							    			}
						    			}

						    			if(moduleData[moduleID]['slot'] == 'low' && moduleData[moduleID]['guid'] == 'effects.ArmorRepair') {
						    				var armourObj = tourneyapp.calculateArmourBooster(moduleData[moduleID]['duration'], shipID);

						    				if(armourObj !== null && typeof armourObj === 'object') {
												// update details for team if it's not been done
										    	if(typeof armourObj['linkName'] !== 'undefined') {
											    	if(ships[shipID]['team'] == 'Blue' && typeof tourneyapp.blueFleetBonuses['RapidRepair'] === 'undefined') {
											    		tourneyapp.blueFleetBonuses['RapidRepair'] = {};
											    		tourneyapp.blueFleetBonuses['RapidRepair']['warfareLinkLevel'] = armourObj['warfareLinkLevel'];
											    		tourneyapp.blueFleetBonuses['RapidRepair']['armourWarfareLevel'] = armourObj['armourWarfareLevel'];
											    		tourneyapp.blueFleetBonuses['RapidRepair']['shipLevel'] = armourObj['shipLevel'];
											    		tourneyapp.blueFleetBonuses['RapidRepair']['linkName'] = armourObj['linkName'];
											    		tourneyapp.blueFleetBonuses['RapidRepair']['implant'] = armourObj['implant'];

											    		tourneyapp.writeBlueConsole('Rapid Repair Ganglink detected');
											    		tourneyapp.writeBlueConsole('Link Name: '+ armourObj['linkName']);
											    		tourneyapp.writeBlueConsole('Warfare Link Level: '+ armourObj['warfareLinkLevel']);
											    		tourneyapp.writeBlueConsole('Siege Warfare Link Level: '+ armourObj['armourWarfareLevel']);
											    		tourneyapp.writeBlueConsole('Ship Level: '+ armourObj['shipLevel']);
											    		tourneyapp.writeBlueConsole('Warfare Implant: '+ armourObj['implant']);
											    	}
											    	if(ships[shipID]['team'] == 'Red' && typeof tourneyapp.redFleetBonuses['RapidRepair'] === 'undefined') {
											    		tourneyapp.redFleetBonuses['RapidRepair'] = {};
											    		tourneyapp.redFleetBonuses['RapidRepair']['warfareLinkLevel'] = armourObj['warfareLinkLevel'];
											    		tourneyapp.redFleetBonuses['RapidRepair']['armourWarfareLevel'] = armourObj['armourWarfareLevel'];
											    		tourneyapp.redFleetBonuses['RapidRepair']['shipLevel'] = armourObj['shipLevel'];
											    		tourneyapp.redFleetBonuses['RapidRepair']['linkName'] = armourObj['linkName'];
											    		tourneyapp.redFleetBonuses['RapidRepair']['implant'] = armourObj['implant'];

											    		tourneyapp.writeRedConsole('Rapid Repair Ganglink detected');
											    		tourneyapp.writeRedConsole('Link Name: '+ armourObj['linkName']);
											    		tourneyapp.writeRedConsole('Warfare Link Level: '+ armourObj['warfareLinkLevel']);
											    		tourneyapp.writeRedConsole('Armoured Warfare Link Level: '+ armourObj['armourWarfareLevel']);
											    		tourneyapp.writeRedConsole('Ship Level: '+ armourObj['shipLevel']);
											    		tourneyapp.writeRedConsole('Warfare Implant: '+ armourObj['implant']);
											    	}
											    }

							    				toolTipDetails = armourObj['modName']+ ' '+moduleData[moduleID]['name'];

							    				if(moduleData[moduleID]['duration'] != armourObj['baseCycle'] && moduleData[moduleID]['duration'] != armourObj['baseCycleSiege']) {
							    					$( "#"+moduleData[moduleID]['id'] ).parent('div').append( '<div class="module-overheat"></div>' );
							    				}
							    			}
						    			}
						    			toolTipDetails = toolTipDetails+'\r\nDuration: '+moduleData[moduleID]['duration']+'ms';
						    			
						    		}

						    		if(targetName !== null) {
						    			// TODO: Can get range from ship to ship here
						    			toolTipDetails = toolTipDetails+'\r\n'+targetName+' ('+targetShip+')';
						    		}
						    	}

				    			//tourneyapp.clearIcon(moduleData[moduleID]);

				    			if(moduleData[moduleID]['name'] == 'Unknown') {
				    				//console.log('Unknown Module: ' + moduleData[moduleID]['guid'] + ' / ' + moduleData[moduleID]['effectName'])
				    				tourneyapp.writeConsole('Unknown Module: ' + moduleData[moduleID]['guid'] + ' / ' + moduleData[moduleID]['effectName'] + ' / ' + moduleData[moduleID]['duration'] + ' on ' +
				    					ships[shipID]['pilotName'] + '\'s ' + ships[shipID]['type']);
				    			} else {

				    			}
				    			if(moduleData[moduleID]['slot'] == 'high') {
				    				var moduleExists = false;
				    				var slotData = ships[shipID]['hiSlot'];
				    				for (var index in slotData) {
				    					if (slotData.hasOwnProperty(index)) {
				    						//console.log('SlotData: ' + slotData[index]['id'] + ' - Module Data: ' + moduleData[moduleID]['id'])
				    						if(slotData[index]['id'] == moduleData[moduleID]['id']) {
				    							moduleExists = true;
				    						}
				    					}
				    				}

				    				if(moduleExists == false) {
				    					ships[shipID]['hiSlot'].push({id: moduleData[moduleID]['id'], name: moduleData[moduleID]['name']});
				    					var once = false;
				    					$('#'+shipID+'_highs > div > img').each(function( indexImg ) {
											//console.log( index + ": " + $( this ).prop('id') );
											if($( this ).prop('id') == "" && once == false) {
												//console.log("Free slot found");
												$( this ).attr('id', moduleData[moduleID]['id'] );
												$( this ).attr('src', moduleData[moduleID]['icon'] ); 
												$( this ).attr('title', toolTipDetails ); 
												once = true;
											}
										});
				    					//console.log(ships[shipID]['hiSlot']);
				    				}

				    			} else if(moduleData[moduleID]['slot'] == 'mid') {
				    				var moduleExists = false;
				    				var slotData = ships[shipID]['medSlot'];
				    				for (var index in slotData) {
				    					if (slotData.hasOwnProperty(index)) {
				    						//console.log('High Slot: ' + slotData[index]['id'] + ' / ' +slotData[index]['name'])
				    						if(slotData[index]['id'] == moduleData[moduleID]['id']) {
				    							moduleExists = true;
				    						}
				    					}
				    				}

				    				if(moduleExists == false) {
				    					if(moduleData[moduleID]['name'] == 'Sensor Booster') {
				    						if($.inArray(ships[shipID]['type'], tourneyapp.commandShips) == -1) {
				    							ships[shipID]['medSlot'].push({id: moduleData[moduleID]['id'], name: moduleData[moduleID]['name']});
					    						//console.log(ships[shipID]['medSlot']);
				    						} else {
				    							//console.log('Sensor Booster/Ganglink detected, could not determine which on ' + ships[shipID]['pilotName'] + '\'s ' + ships[shipID]['type']);
				    							

				    							var moduleExists = false;
				    							var slotData = ships[shipID]['unknowns'];
				    							for (var index in slotData) {
							    					if (slotData.hasOwnProperty(index)) {
							    						//console.log('High Slot: ' + slotData[index]['id'] + ' / ' +slotData[index]['name'])
							    						if(slotData[index]['id'] == moduleData[moduleID]['id']) {
							    							moduleExists = true;
							    						}
							    					}
							    				}
							    				if(moduleExists == false) {
							    					tourneyapp.writeConsole('Sensor Booster/Ganglink detected, could not determine which on ' + ships[shipID]['pilotName'] + '\'s ' + ships[shipID]['type']);
								    				ships[shipID]['unknowns'].push({id: moduleData[moduleID]['id'], name: moduleData[moduleID]['name']});
					    							$('#'+shipID+'_unknowns').append( '<div class="module-image-container"><image id="'+moduleData[moduleID]['id']+'" class="slot-icon" src="'+BASEURL+'/images/Icons/items/5_64_10.png" title="Sensor Booster or Ganglink"/></div>' );
					    							$('#'+shipID+'_unknowns').css("width", Math.ceil($('#'+shipID+'_unknowns > div > img').length/2)*35);
					    						}
				    						}
				    					} else {
					    					ships[shipID]['medSlot'].push({id: moduleData[moduleID]['id'], name: moduleData[moduleID]['name']});
					    					var once = false;
					    					$('#'+shipID+'_mids > div > img').each(function( indexImg ) {
												//console.log( index + ": " + $( this ).prop('id') );
												if($( this ).prop('id') == "" && once == false) {
													//console.log("Free slot found");
													$( this ).attr('id', moduleData[moduleID]['id'] );
													$( this ).attr('src', moduleData[moduleID]['icon'] ); 
													$( this ).attr('title', toolTipDetails ); 
													once = true;
												}
											});
					    					//console.log(ships[shipID]['medSlot']);
					    				}
				    				}
				    			} else if(moduleData[moduleID]['slot'] == 'low') {
				    				var moduleExists = false;
				    				var slotData = ships[shipID]['loSlot'];
				    				for (var index in slotData) {
				    					if (slotData.hasOwnProperty(index)) {
				    						//console.log('High Slot: ' + slotData[index]['id'] + ' / ' +slotData[index]['name'])
				    						if(slotData[index]['id'] == moduleData[moduleID]['id']) {
				    							moduleExists = true;
				    						}
				    					}
				    				}

				    				if(moduleExists == false) {
				    					ships[shipID]['loSlot'].push({id: moduleData[moduleID]['id'], name: moduleData[moduleID]['name']});
				    					var once = false;
				    					$('#'+shipID+'_lows > div > img').each(function( indexImg ) {
											//console.log( index + ": " + $( this ).prop('id') );
											if($( this ).prop('id') == "" && once == false) {
												//console.log("Free slot found");
												$( this ).attr('id', moduleData[moduleID]['id'] );
												$( this ).attr('src', moduleData[moduleID]['icon'] ); 
												$( this ).attr('title', toolTipDetails ); 
												once = true;
											}
										});
				    					//console.log(ships[shipID]['hiSlot']);
				    				}
				    			}
				    			
				    		}

				    		if(typeof moduleData[moduleID]['duration'] !== 'undefined') {
				    			var slotData = ships[shipID]['hiSlot'];
			    				for (var myindex in slotData) {
			    					if (slotData.hasOwnProperty(myindex)) {
			    						if(ships[shipID]['hiSlot'][myindex]['id'] == moduleData[moduleID]['id']) {
							    			ships[shipID]['hiSlot'][myindex]['currentROF'] = moduleData[moduleID]['duration'];
					    					//console.log(moduleData[moduleID]['duration']);
					    				}
				    				}
				    			}
				    		}

				    		// louis litt the module
				    		$( '#'+moduleData[moduleID]['id']).parent( "div" ).addClass( 'module-highlight');
				    		$( '#'+moduleData[moduleID]['id']).attr('title', toolTipDetails);
				    		if(moduleData[moduleID].hasOwnProperty('ammoType')) {
				    			var slotData = ships[shipID]['hiSlot'];
			    				for (var index in slotData) {
			    					if (slotData.hasOwnProperty(index)) {
			    						if(slotData[index]['id'] == moduleData[moduleID]['id']) {
			    							$( '#'+moduleData[moduleID]['id']).attr('title', slotData[index]['name']+'\r\nDuration: '+moduleData[moduleID]['duration']+'ms\r\n'+targetName+' ('+targetShip+')'+'\r\n'+moduleData[moduleID]['ammoType']['typeName']);
			    						

				    						// check if we've calculated the ROF yet
				    						//if( typeof moduleData[moduleID]['calculatedROF'] !== 'undefined' && moduleData[moduleID]['calculatedROF'] === true && typeof moduleData[moduleID]['info'] !== 'undefined') {
				    						if( typeof moduleData[moduleID]['calculatedROF'] !== 'undefined' && moduleData[moduleID]['calculatedROF'] === true ) {
				    							ships[shipID]['hiSlot'][index]['calculatedROF'] = true;
				    							if(typeof moduleData[moduleID]['info'] !== 'undefined') {
					    							ships[shipID]['hiSlot'][index]['info'] = moduleData[moduleID]['info'];
					    							ships[shipID]['hiSlot'][index]['name'] = moduleData[moduleID]['info']['variant']['typeName'];
					    							ships[shipID]['hiSlot'][index]['meta'] = moduleData[moduleID]['info']['variant']['meta'];
					    							ships[shipID]['hiSlot'][index]['typeID'] = moduleData[moduleID]['info']['variant']['typeID'];
					    							$('#'+ships[shipID]['hiSlot'][index]['id']).attr('src', 'https://image.eveonline.com/Type/'+moduleData[moduleID]['info']['variant']['typeID']+'_32.png');
					    						}
				    							//ships[shipID]['calculatedROF'] = true;
				    						} else {
				    							if( typeof moduleData[moduleID]['calculatedROF'] !== 'undefined') {
				    								ships[shipID]['hiSlot'][index]['calculatedROF'] = moduleData[moduleID]['calculatedROF'];
				    							}
				    						}
				    					}
/* MOVED TO PHP
			    						if( typeof ships[shipID]['hiSlot'][index]['calculatedROF'] !== 'undefined') {
			    							if(ships[shipID]['hiSlot'][index]['calculatedROF'] == false) {
			    								
			    								var roleBonus = 1;
			    								if(ships[shipID]['type'] == 'Machariel' || ships[shipID]['type'] == 'Cynabal')
			    									roleBonus = 0.75;
			    								if(ships[shipID]['type'] == 'Vangel')
			    									roleBonus = 0.5;

			    								var testrof = tourneyapp.calculateROF(ships[shipID]['hiSlot'][index]['turretVariants'], ships[shipID]['hiSlot'][index]['groupName'], moduleData[moduleID]['duration'], roleBonus, Number(ships[shipID]['shipROFBonus']), shipID);

			    								if(testrof !== null && typeof testrof === 'object') {
			    									ships[shipID]['hiSlot'][index]['calculatedROF'] = true;
			    									//ships[shipID]['hiSlot'][index].push({info:testrof});
			    									ships[shipID]['hiSlot'][index]['info'] = testrof;

			    									// set stuff from ['variant']
			    									ships[shipID]['hiSlot'][index]['name'] = testrof['variant']['typeName'];
			    									ships[shipID]['hiSlot'][index]['meta'] = testrof['variant']['meta'];
			    									ships[shipID]['hiSlot'][index]['typeID'] = testrof['variant']['typeID'];
			    									$('#'+ships[shipID]['hiSlot'][index]['id']).attr('src', 'https://image.eveonline.com/Type/'+testrof['variant']['typeID']+'_32.png')
			    								} else {
			    									ships[shipID]['hiSlot'][index]['calculatedROF'] = testrof;
			    								}
			    							}
			    						}
/**/
			    					}
			    				}
				    			//$( '#'+moduleData[moduleID]['id']).attr('title', )
				    		}
				    		//$( '#'+moduleData[moduleID]['id']).parent( "div" ).addClass( 'module-highlight').fadeTo("slow", '0.0');
				    	}
				    }

				    // missiles/projectiles and stuff (todo)

				    // check hiSlots - see if all turrents are calculated - if so add those mods/rigs and skillz
				    if(typeof ships[shipID] !== 'undefined' && ships[shipID]['calculatedROF'] === false) {
					    var slotData = ships[shipID]['hiSlot'];
					    var isThereAMissingCalc = false;
					    var damageMods = [];
						for (var index in slotData) {
							if (slotData.hasOwnProperty(index)) {
				
								// check if we've calculated the ROF yet
								if( typeof ships[shipID]['hiSlot'][index]['calculatedROF'] !== 'undefined') {
									if(ships[shipID]['hiSlot'][index]['calculatedROF'] === false) {
										isThereAMissingCalc = true;
									}
								}

								if(isThereAMissingCalc == false && typeof ships[shipID]['hiSlot'][index]['info'] !== 'undefined') {
									// set the chars skills and set any discovered mods
									if(typeof ships[shipID]['hiSlot'][index]['info']['polarized'] !== 'undefined')
										ships[shipID]['hiSlot'][index]['name'] += ' (Polarized)';
									if(typeof ships[shipID]['hiSlot'][index]['info']['gunneryLvl'] !== 'undefined')
										ships[shipID]['gunneryLevel'] = ships[shipID]['hiSlot'][index]['info']['gunneryLvl'];
									if(typeof ships[shipID]['hiSlot'][index]['info']['rfLvl'] !== 'undefined')
										ships[shipID]['rapidFiringLevel'] = ships[shipID]['hiSlot'][index]['info']['rfLvl'];
									if(typeof ships[shipID]['hiSlot'][index]['info']['misLvl'] !== 'undefined')
										ships[shipID]['missileLevel'] = ships[shipID]['hiSlot'][index]['info']['misLvl'];
									if(typeof ships[shipID]['hiSlot'][index]['info']['rlLvl'] !== 'undefined')
										ships[shipID]['rapidLaunchLevel'] = ships[shipID]['hiSlot'][index]['info']['rlLvl'];
									if(typeof ships[shipID]['hiSlot'][index]['info']['spLvl'] !== 'undefined')
										ships[shipID]['missileSpecLevel'] = ships[shipID]['hiSlot'][index]['info']['spLvl'];
									if(typeof ships[shipID]['hiSlot'][index]['info']['shSkill'] !== 'undefined')
										ships[shipID]['shSkill'] = ships[shipID]['hiSlot'][index]['info']['shSkill'];
									if(typeof ships[shipID]['hiSlot'][index]['info']['implant'] !== 'undefined')
										ships[shipID]['implant'] = ships[shipID]['hiSlot'][index]['info']['implant'];
									damageMods = [];
									if(ships[shipID]['hiSlot'][index]['info']['mod1'] != 0)
										damageMods.push(ships[shipID]['hiSlot'][index]['info']['mod1']);
									if(ships[shipID]['hiSlot'][index]['info']['mod2'] != 0)
										damageMods.push(ships[shipID]['hiSlot'][index]['info']['mod2']);
									if(ships[shipID]['hiSlot'][index]['info']['mod3'] != 0)
										damageMods.push(ships[shipID]['hiSlot'][index]['info']['mod3']);
									if(ships[shipID]['hiSlot'][index]['info']['mod4'] != 0)
										damageMods.push(ships[shipID]['hiSlot'][index]['info']['mod4']);

									ships[shipID]['damageMods'] = damageMods;
									ships[shipID]['rofRigs'] = ships[shipID]['hiSlot'][index]['info']['rigs'];

								}
							}
						}

						if(isThereAMissingCalc == false && typeof ships[shipID]['rofRigs'] !== 'undefined') {
							
							var toolMsg = '';
							if(typeof ships[shipID]['shSkill'] !== 'undefined')
								toolMsg += '\r\nShip Level: '+ships[shipID]['shSkill'];
							if(typeof ships[shipID]['gunneryLevel'] !== 'undefined')
								toolMsg += '\r\nGunnery Level: '+ships[shipID]['gunneryLevel'];
							if(typeof ships[shipID]['rapidFiringLevel'] !== 'undefined')
								toolMsg += '\r\nRapid Firing Level: '+ships[shipID]['rapidFiringLevel'];
							if(typeof ships[shipID]['missileLevel'] !== 'undefined')
								toolMsg += '\r\nMissile Level: '+ships[shipID]['missileLevel'];
							if(typeof ships[shipID]['rapidLaunchLevel'] !== 'undefined')
								toolMsg += '\r\nRapid Launch Level: '+ships[shipID]['rapidLaunchLevel'];
							if(typeof ships[shipID]['missileSpecLevel'] !== 'undefined')
								toolMsg += '\r\nMissile Spec Level: '+ships[shipID]['missileSpecLevel'];
							if(typeof ships[shipID]['rofRigs'] !== 'undefined')
								toolMsg += '\r\n# ROF Rigs: '+ships[shipID]['rofRigs'];
							if(typeof ships[shipID]['implant'] !== 'undefined')
								toolMsg += '\r\nImplant: '+ships[shipID]['implant'];

							$('#'+shipID+'_pilot').attr('title', ships[shipID]['pilotName']+toolMsg);
							if(damageMods.length > 0) {
								var modNum;

								for(modNum = 0; modNum < damageMods.length; modNum++) {
									var thisMod = damageMods[modNum];
									var thisMeta = '0';
									switch(thisMod) {
    									case 10.5:
    										thisMeta = 'T2 / Meta 4 or Meta > 6';
    										break;
    									case 10:
    										thisMeta = 'Meta 6 (Really unlikely!)';
    										break;
    									case 9.75:
    										thisMeta = 'Meta 3';
    										break;
    									case 9:
    										thisMeta = 'Meta 2';
    										break;
    									case 8.25:
    										thisMeta = 'Meta 1';
    										break;
    									case 7.5:
    										thisMeta = 'Meta 0';
    										break;
    									default:
    										thisMeta = 'unknown meta';
    								}
									ships[shipID]['loSlot'].push({id: 0-modNum, name: 'Damage Mod'});
			    					var once = false;
			    					$('#'+shipID+'_lows > div > img').each(function( indexImg ) {
										//console.log( index + ": " + $( this ).prop('id') );
										if($( this ).prop('id') == "" && once == false) {
											//console.log("Free slot found");
											$( this ).attr('id', 0-modNum );
											$( this ).attr('src', BASEURL+'/images/Icons/items/5_64_12.png' ); 
											$( this ).attr('title', thisMeta ); 
											once = true;
										}
									});
								}
							}

							ships[shipID]['calculatedROF'] = true;

							// update portrait
							$('#'+shipID+'_pilot').parent('div').addClass('module-rof-checked');
							$('#'+shipID+'_pilot').parent('div').attr('title', ships[shipID]['pilotName']+toolMsg);
						} else if(isThereAMissingCalc == false && typeof ships[shipID]['rofRigs'] === 'undefined') {
							// update portrait
							$('#'+shipID+'_pilot').parent('div').addClass('module-rof-failed');

							// set to calculated to prevent rechecks
							ships[shipID]['calculatedROF'] = true;
						}
					}

					// mark overheated mods
					if(typeof ships[shipID] !== 'undefined' && ships[shipID]['calculatedROF'] === true) {
					    var slotData = ships[shipID]['hiSlot'];

					    for (var index in slotData) {
							if (slotData.hasOwnProperty(index)) {
								if(typeof slotData[index]['info'] !== 'undefined' && typeof ships[shipID]['hiSlot'][index]['currentROF'] !== 'undefined') {
									var overheat = slotData[index]['info']['normalDamage']*0.85 ;
									var current = ships[shipID]['hiSlot'][index]['currentROF'];

									if(overheat == current || overheat.toFixed(precisionCheck) == current) {
										//console.log('SUCCESS!');
										//console.log(ships[shipID]['hiSlot'][index]['id']);
										//$( "#"+ships[shipID]['hiSlot'][index]['id'] ).remove();
										$( "#"+ships[shipID]['hiSlot'][index]['id'] ).parent('div').append( '<div class="module-overheat"></div>' );
									}
								}
							}
						}

					}

				    // drones
				    if(typeof ships[shipID] !== 'undefined' && typeof data[shipID]['drones'] !== 'undefined') {
				    	var droneData = ships[shipID]['drones'];

				    	for(var droneID in droneData) {
				    		if(droneData.hasOwnProperty(droneID)) {
				    			//tourneyapp.droneBay[shipID][droneID] = droneData;
				    			ships[shipID]['droneBay'] = droneData;
/*				    			ships[shipID]['droneBay'][droneData[droneID]['itemID']].push({id: droneData[droneID]['itemID'], 
				    											name: droneData[droneID]['name'],
				    											duration: droneData[droneID]['duration'],
				    											icon: droneData[droneID]['icon']});
/**/
				    		}
				    	}
					}

				    if(typeof ships[shipID] !== 'undefined') {
					    // update ship health bars
					    $('#'+shipID+'_hull').css('height', ships[shipID]['structure']*100+'%');
					    $('#'+shipID+'_armour').css('height', ships[shipID]['armor']*100+'%');
					    $('#'+shipID+'_shield').css('height', ships[shipID]['shield']*100+'%');

					    if(ships[shipID]['structure'] == 0) {
					    	if(!$( "#"+shipID ).hasClass( "tourney-dead" )) {
					    		$( "#"+shipID ).addClass( "tourney-dead");
					    		$('#'+shipID+'_speed').html("Max Speed: "+ships[shipID]['maxSpeed']+" m/s");
					    	}
					    }
					}
				  }
				};

				// end of ships

				//console.log(ships);
				if(data.hasOwnProperty('framesProcessed')) {
					if(data['framesProcessed'] > 1) {
						tourneyapp.writeConsole(data['framesProcessed'] + ' Frames Processed.');
					}
				}
				if(data.hasOwnProperty('nextFrame')) {
					replayURL = data['nextFrame'];
					tourneyapp.replayFrame = data['currentFrame'];
					//alert(replayURL);
					//console.log(data['currentFrame']);
					tourneyapp.getReplayFrame();
				} else {
					for (var x in ships) {
						$('#'+x+'_speed').html("Max Speed: "+ships[x]['maxSpeed']+" m/s");
					}
					tourneyapp.writeConsole('All Frames Processed.');

					tourneyapp.togglePlay();

					//ships = [];
					tourneyapp.replayFrame = 0;
					tourneyapp.nextFrame = 0;
					replayURL = FRAMEURL+'0/';
				}

				// update stats
				if(data.hasOwnProperty('blueTeamFleetAttributes')) {
					var blueEHP = (data['blueTeamFleetAttributes']['totalEHP'] / maxDefense) * 100;
					$('#blueteam-fleet-attributes-defense-bar').css('width', blueEHP+'%' );
					$('#blueteam-fleet-attributes-defense-fill').css('width', (((data['blueTeamFleetAttributes']['totalReps'] / data['blueTeamFleetAttributes']['totalEHP']) *100) * 1)+'%' );
					$('#blueteam-fleet-attributes-defense-bar').attr('title', data['blueTeamFleetAttributes']['totalEHP']+' / '+data['blueTeamFleetAttributes']['totalReps']+' EHP');

					var blueAttack = (data['blueTeamFleetAttributes']['maxDPS'] / maxAttack) * 100;
					$('#blueteam-fleet-attributes-attack-bar').css('width', blueAttack+'%' );
					$('#blueteam-fleet-attributes-attack-fill').css('width', ((data['blueTeamFleetAttributes']['appliedDPS'] / data['blueTeamFleetAttributes']['maxDPS']) *100)+'%' );
					$('#blueteam-fleet-attributes-attack-bar').attr('title', data['blueTeamFleetAttributes']['appliedDPS']+' / '+data['blueTeamFleetAttributes']['maxDPS']+' DPS');

					var blueControl = (data['blueTeamFleetAttributes']['maxControl'] / maxControl) * 100;
					$('#blueteam-fleet-attributes-control-bar').css('width', blueControl+'%' );
					$('#blueteam-fleet-attributes-control-fill').css('width', ((data['blueTeamFleetAttributes']['appliedControl'] / data['blueTeamFleetAttributes']['maxControl']) *100)+'%' );
					$('#blueteam-fleet-attributes-control-bar').attr('title', data['blueTeamFleetAttributes']['appliedControl']+' / '+data['blueTeamFleetAttributes']['maxControl']+' Control');
				}

				if(data.hasOwnProperty('redTeamFleetAttributes')) {
					var redEHP = (data['redTeamFleetAttributes']['totalEHP'] / maxDefense) * 100;
					$('#redteam-fleet-attributes-defense-bar').css('width', redEHP+'%' );
					$('#redteam-fleet-attributes-defense-fill').css('width', (((data['redTeamFleetAttributes']['totalReps'] / data['redTeamFleetAttributes']['totalEHP']) * 100) *1)+'%' );
					$('#redteam-fleet-attributes-defense-bar').attr('title', data['redTeamFleetAttributes']['totalEHP']+' / '+data['redTeamFleetAttributes']['totalReps']+' EHP');

					var redAttack = (data['redTeamFleetAttributes']['maxDPS'] / maxAttack) * 100;
					$('#redteam-fleet-attributes-attack-bar').css('width', redAttack+'%' );
					$('#redteam-fleet-attributes-attack-fill').css('width', ((data['redTeamFleetAttributes']['appliedDPS'] / data['redTeamFleetAttributes']['maxDPS']) *100)+'%' );
					$('#redteam-fleet-attributes-attack-bar').attr('title', data['redTeamFleetAttributes']['appliedDPS']+' / '+data['redTeamFleetAttributes']['maxDPS']+' DPS');

					var redControl = (data['redTeamFleetAttributes']['maxControl'] / maxControl) * 100;
					$('#redteam-fleet-attributes-control-bar').css('width', redControl+'%' );
					$('#redteam-fleet-attributes-control-fill').css('width', ((data['redTeamFleetAttributes']['appliedControl'] / data['redTeamFleetAttributes']['maxControl']) *100)+'%' );
					$('#redteam-fleet-attributes-control-bar').attr('title', data['redTeamFleetAttributes']['appliedControl']+' / '+data['redTeamFleetAttributes']['maxControl']+' Control');
				}
				if(data.hasOwnProperty('nextFrame')) {
					var seconds = data['currentFrame'] % 60;
					if (seconds.toString().length == 1) {
			            seconds = "0" + seconds;
			        }
					$('#time-display').html(Math.floor(data['currentFrame'] / 60)+':'+seconds+' (x'+data['tidiFactor']+')');
				}
				//tourneyapp.replayFrame++;
				//replayURL = FRAMEURL+replayFrame+'/';
				//tourneyapp.getReplayFrame();
		})
		.fail(function() {
			tourneyapp.writeConsole('error');
			setTimeout(function() {
				tourneyapp.getReplayFrame();
			},3000);
		})
		.always(function() {
			//alert( "complete" );
		});
	},

	clearIcons: function() {
		$('.slot-layout img').each(function(){
			//$(this).css( 'backgroundColor', 'transparent');
			$(this).parent( "div" ).removeClass( 'module-highlight');
		});

		$('.module-overheat').each(function(){
			$(this).remove();
		});
	},
	clearIcon: function(module) {
		//console.log(module['id']);

		//$('#'+module['id']).css( 'backgroundColor', 'transparent');
		$('#'+module['id']).removeClass( 'module-highlight');
	},

	writeConsole: function(message) {
		$('#console').prepend(message+'<br>');
	},

	writeRedConsole: function(message) {
		$('#red-console').append(message+'<br>');
	},

	writeBlueConsole: function(message) {
		$('#blue-console').append(message+'<br>');
	},

	togglePlay: function() {
		if(this.playButton == 'Stop')
        	this.playButton = 'Play';
        else
        	this.playButton = 'Stop';

        $('#control-button').html(this.playButton);
	},

	resetDead: function() {
		$( '.tournament-row > .tourney-dead' ).removeClass('tourney-dead');
	},

	resetMatch: function() {
		this.playButton = 'Play';
		$('#control-button').html(this.playButton);
		$('#time-display').html('0:00 (x1)');

		this.replayFrame = 0;
		this.nextFrame = 0;
		replayURL = FRAMEURL;

		this.resetDead();

		this.reset = false;

		$('.module-image-container').removeClass( 'module-highlight');
	},

	calculateROF: function(turretVariants, weaponType, duration, roleBonus, shipROFBonus, shipID) {
		//var shipBonuses = [5, 7.5, 10]; // do this by invTraits work out the bonus and send for check.
		var maxShipSkills = 0;
		if(shipROFBonus != 0) {
			maxShipSkills = 5;
		}
		// also rolebonus of 25 and 50 (don't forget them (Large Proj, Med Proj and Missiles))
		// and overheat 15%
		// rigs upto 3 10% (because no T2 allowed)
		var gunneryBonus = 2;
		var rapidFireBonus = 4;

		var missilesBonus = 2;
		var rapidLaunchBonus = 3;
		var missileSpecBonus = 2;

		var damageTurretModBonus = [10.5, 9.75, 9, 8.25, 7.5, 0]; // highly unlikely to be 10 unless Missile - will be rig if turret based (so removed)
		var damageMissileModBonus = [10.5, 9.75, 9, 8.25, 7.5, 0]; 

		var dmNum1, dmNum2, dmNum3, dmNum4, rigNum, rigs, gSkill, rfSkill, spSkill, shSkill;
		var isMatch = false;
		var numMatches = 0;
		var tempBonus;
		var myMatches = [];
		var maxStackingBonus = 8;

		switch(weaponType) {
		    case 'Energy Weapon':
		    case 'Projectile Weapon':
		    case 'Hybrid Weapon':
		    	
		    	for (var baseRofCount = 0; baseRofCount < turretVariants.length; ++baseRofCount) {
		    		var baseRof = turretVariants[baseRofCount]['baseROF'];
			        for (dmNum1 = 0; dmNum1 < damageTurretModBonus.length; ++dmNum1) {
			    		var dmBonus1 = damageTurretModBonus[dmNum1];
			    		for (dmNum2 = 0; dmNum2 < damageTurretModBonus.length; ++dmNum2) {
			    			var dmBonus2 = damageTurretModBonus[dmNum2];
			    			for (dmNum3 = 0; dmNum3 < damageTurretModBonus.length; ++dmNum3) {
			    				var dmBonus3 = damageTurretModBonus[dmNum3];
							    for(rigNum = 0; rigNum < 4; ++rigNum) {
								    if(dmNum1 >= dmNum2 && dmNum2 >= dmNum3) {
									    var bonuses = [];

									    if(dmBonus1 != 0) {
									    	bonuses.push(dmBonus1);
									    }
									    if(dmBonus2 != 0) {
									    	bonuses.push(dmBonus2);
									    }
									    if(dmBonus3 != 0) {
									    	bonuses.push(dmBonus3);
									    }
									    
								    	for (rigs = 0; rigs <= rigNum; rigs++) {
								    		if(rigs != 0) {
								    			bonuses.push(10.5);
								    		}
								    	}

								    	bonuses.sort(function(a, b){return b-a});

								    	var stackingBonus = 1;

								    	if(bonuses.length > 0)
								    		stackingBonus = this.calculateStackingBonus(bonuses);  	
									
										var shipSkillStart = 0;
										if(shipROFBonus > 0)
								  			shipSkillStart = 0;
								  
								    	for(gSkill = 1; gSkill < 6; gSkill++) {
								    		for(rfSkill = 1; rfSkill < 6; rfSkill++) {
								    			for(shSkill = shipSkillStart; shSkill <= maxShipSkills; shSkill++) {
									    			if(!(gSkill < 3 && rfSkill > 0) && (gSkill >= rfSkill)) {
										    			tempBonus = baseRof;
										    			tempBonus *= 1-((gSkill * gunneryBonus)/100);
										    			tempBonus *= 1-((rfSkill * rapidFireBonus)/100);
										    			tempBonus *= 1-((shSkill * shipROFBonus)/100);
										    			if(weaponType == 'Projectile Weapon') {
										    				tempBonus *= roleBonus;
										    			}
										    			tempBonus *= stackingBonus;

										    			if(tempBonus.toFixed(precisionCheck) == duration || tempBonus == duration) {
										    				isMatch = true;
										    				numMatches++;

										    				var myObject = {};
										    				myObject['gunneryLvl'] = gSkill;
										    				myObject['rfLvl'] = rfSkill;
										    				if(shSkill > 0)
												    			myObject['shSkill'] = shSkill;
										    				myObject['mod1'] = dmBonus1;
										    				myObject['mod2'] = dmBonus2;
										    				myObject['mod3'] = dmBonus3;
										    				myObject['rigs'] = rigs-1;
										 					myObject['normalDamage'] = tempBonus;
										 					myObject['variant'] = turretVariants[baseRofCount];

										 					myMatches.push(myObject);
										    				//console.log(myObject);
										    			}

										    			// implant
									    				var tempBonus2 = tempBonus * 0.97; 
									    				if(tempBonus2.toFixed(precisionCheck) == duration || tempBonus2 == duration) {
										    				isMatch = true;
										    				numMatches++;

										    				var myObject = {};
										    				myObject['gunneryLvl'] = gSkill;
										    				myObject['rfLvl'] = rfSkill;
										    				if(shSkill > 0)
											    				myObject['shSkill'] = shSkill;
										    				myObject['mod1'] = dmBonus1;
										    				myObject['mod2'] = dmBonus2;
										    				myObject['mod3'] = dmBonus3;
										    				myObject['rigs'] = rigs-1;
										 					myObject['implant'] = '3%';
										 					myObject['normalDamage'] = tempBonus2;
										 					myObject['variant'] = turretVariants[baseRofCount];

										 					myMatches.push(myObject);
										    				//console.log(myObject);
											    		}

											    		// overheating
									    				var tempBonus2 = tempBonus * 0.85; 
									    				if(tempBonus2.toFixed(precisionCheck) == duration || tempBonus2 == duration) {
										    				isMatch = true;
										    				numMatches++;

										    				var myObject = {};
										    				myObject['gunneryLvl'] = gSkill;
										    				myObject['rfLvl'] = rfSkill;
										    				if(shSkill > 0)
										    					myObject['shSkill'] = shSkill;
										    				myObject['mod1'] = dmBonus1;
										    				myObject['mod2'] = dmBonus2;
										    				myObject['mod3'] = dmBonus3;
										    				myObject['rigs'] = rigs-1;
										    				myObject['overheated'] = true;
										 					myObject['normalDamage'] = tempBonus;
										 					myObject['variant'] = turretVariants[baseRofCount];

										 					myMatches.push(myObject);
										    				//console.log(myObject);
										    			}

										    			// overheating ++ implant
									    				var tempBonus2 = (tempBonus * 0.85) * 0.97; // overheating ++ implant
									    				if(tempBonus2.toFixed(precisionCheck) == duration || tempBonus2 == duration) {
										    				isMatch = true;
										    				numMatches++;

										    				var myObject = {};
										    				myObject['gunneryLvl'] = gSkill;
										    				myObject['rfLvl'] = rfSkill;
										    				if(shSkill > 0)
									    						myObject['shSkill'] = shSkill;
										    				myObject['mod1'] = dmBonus1;
										    				myObject['mod2'] = dmBonus2;
										    				myObject['mod3'] = dmBonus3;
										    				myObject['rigs'] = rigs-1;
										    				myObject['overheated'] = true;
										    				myObject['implant'] = '3%';
										 					myObject['normalDamage'] = (tempBonus*0.97);
										 					myObject['variant'] = turretVariants[baseRofCount];

										 					myMatches.push(myObject);
										    				//console.log(myObject);
										    			}
										    		}
										    	}
								    		}
								    	}
									}
								}
							}
						}
					}
				}

				if(isMatch == true && numMatches == 1) {
					return myObject;
				}

				if(numMatches > 1) {
					var misL = myMatches[0]['gunneryLvl'];
					var rlL = myMatches[0]['rfLvl'];
					//var spL = myMatches[0]['spLvl'];
					
					var myDam1 = myMatches[0]['mod1'];
					var myDam2 = myMatches[0]['mod2'];
					var myDam3 = myMatches[0]['mod3'];
					var rg = myMatches[0]['rigs'];
					var oh = myMatches[0]['overheated'];
					var imp = myMatches[0]['implant'];
					var nd = myMatches[0]['normalDamage'];
					var shL = null; //var shL = myMatches[0][shSkill];

					var variants = [];
					var isVariant = true;
					if(typeof myMatches[0]['shSkill'] !== 'undefined') {
						shL = myMatches[0]['shSkill'];
					}

//	myObject['variant'] = turretVariants[baseRofCount];

					for (var objCount = 1; objCount < myMatches.length; ++objCount) {
			    		var obj = myMatches[objCount];

			    		if( misL != myMatches[objCount]['gunneryLvl'] ||
			    			rlL != myMatches[objCount]['rfLvl'] ||
			    			//spL != myMatches[objCount]['spLvl'] ||
			    			myDam1 != myMatches[objCount]['mod1'] ||
			    			myDam2 != myMatches[objCount]['mod2'] ||
			    			myDam3 != myMatches[objCount]['mod3'] ||
			    			rg != myMatches[objCount]['rigs'] ||
			    			oh != myMatches[objCount]['overheated'] ||
			    			imp != myMatches[objCount]['implant'] ||
			    			nd != myMatches[objCount]['normalDamage']) {

			    			isVariant = false;
			    		}

			    		if(typeof myMatches[objCount]['shSkill'] !== 'undefined') {
			    			if(shL != myMatches[objCount]['shSkill'])
			    				isVariant = false;
			    		}
			    	}

			    	// if it's just a variant
			    	if(isVariant == true) {
			    		myMatches[myMatches.length-1]['variant']['typeName'] += '\r\n(or Variant with same ROF)';
			    		return myMatches[myMatches.length-1];
			    	} else {
						console.log('Multi-Matches ('+shipID+') : '+numMatches);
						console.log(myMatches);
						//console.log(shipROFBonus);
						//console.log(maxShipSkills);
						return true;
					}
				}

		        return false;
		        break;

		    case 'Missile Launcher Torpedo':
		    case 'Missile Launcher Heavy':
		    case 'Missile Launcher Rapid Heavy':
		    case 'Missile Launcher Cruise':
		    case 'Missile Launcher Rocket':
		    case 'Missile Launcher Light':
		    case 'Missile Launcher Rapid Light':
		    case 'Missile Launcher Defender':
		    case 'Missile Launcher Citadel':
		    case 'Missile Launcher Heavy Assault':
		    	for (var baseRofCount = 0; baseRofCount < turretVariants.length; ++baseRofCount) {
		    		var baseRof = turretVariants[baseRofCount]['baseROF'];
			    	for (dmNum1 = 0; dmNum1 < damageMissileModBonus.length; ++dmNum1) {
			    		var dmBonus1 = damageMissileModBonus[dmNum1];
			    		for (dmNum2 = 0; dmNum2 < damageMissileModBonus.length; ++dmNum2) {
			    			var dmBonus2 = damageMissileModBonus[dmNum2];
			    			for (dmNum3 = 0; dmNum3 < damageMissileModBonus.length; ++dmNum3) {
			    				var dmBonus3 = damageMissileModBonus[dmNum3];
			    				for (dmNum4 = 0; dmNum4 < damageMissileModBonus.length; ++dmNum4) {
				    				var dmBonus4 = damageMissileModBonus[dmNum4];
								    for(rigNum = 0; rigNum < 4; ++rigNum) {
									    if(dmNum1 >= dmNum2 && dmNum2 >= dmNum3 && dmNum3 >= dmNum4) {
										    var bonuses = [];

										    if(dmBonus1 != 0) {
										    	bonuses.push(dmBonus1);
										    }
										    if(dmBonus2 != 0) {
										    	bonuses.push(dmBonus2);
										    }
										    if(dmBonus3 != 0) {
										    	bonuses.push(dmBonus3);
										    }
										    if(dmBonus4 != 0) {
										    	bonuses.push(dmBonus4);
										    }

									    	for (rigs = 0; rigs <= rigNum; rigs++) {
									    		if(rigs != 0) {
									    			bonuses.push(10);
									    		}
									    	}

									    	bonuses.sort(function(a, b){return b-a});

									    	var stackingBonus = 1;

									    	if(bonuses.length > 0)
									    		stackingBonus = this.calculateStackingBonus(bonuses);  	
										
											var shipSkillStart = 0;
											if(shipROFBonus > 0)
									  			shipSkillStart = 0;

									    	for(gSkill = 5; gSkill < 6; gSkill++) {
									    		for(rfSkill = 1; rfSkill < 6; rfSkill++) {
									    			for(spSkill = 0; spSkill < 6; spSkill++) {
									    				for(shSkill = shipSkillStart; shSkill <= maxShipSkills; shSkill++) {
									    					if(!(gSkill < 3 && rfSkill > 0) && (gSkill >= spSkill) && (rfSkill >= spSkill) && bonuses.length <= maxStackingBonus && !(rfSkill > 0 && rfSkill < 3) && !(gSkill > 0 && gSkill < 3)) {
										    				//if(!(gSkill < 3 && rfSkill > 0) && (gSkill >= spSkill) && (rfSkill >= spSkill) && bonuses.length < 4) {

										    					//shipBonusModifier = 1-((shSkill * shipROFBonus))
												    			tempBonus = baseRof;
												    			tempBonus *= 1-((gSkill * missilesBonus)/100);
												    			tempBonus *= 1-((rfSkill * rapidLaunchBonus)/100);
												    			tempBonus *= 1-((spSkill * missileSpecBonus)/100);
												    			tempBonus *= 1-((shSkill * shipROFBonus)/100);
												    			//if(weaponType == 'Projectile Weapon') {  // VANGEL
												    			//	tempBonus *= roleBonus;
												    			//}
												    			tempBonus *= stackingBonus;

												    			if(tempBonus.toFixed(precisionCheck) == duration || tempBonus == duration) {
												    				isMatch = true;
												    				numMatches++;

												    				var myObject = {};
												    				myObject['misLvl'] = gSkill;
												    				myObject['rlLvl'] = rfSkill;
												    				myObject['spLvl'] = spSkill;
												    				if(shSkill > 0)
												    					myObject['shSkill'] = shSkill;
												    				myObject['mod1'] = dmBonus1;
												    				myObject['mod2'] = dmBonus2;
												    				myObject['mod3'] = dmBonus3;
												    				myObject['mod4'] = dmBonus4;
												    				myObject['rigs'] = rigs-1;
												 					myObject['normalDamage'] = tempBonus;
												 					myObject['variant'] = turretVariants[baseRofCount];

												 					myMatches.push(myObject);
												    				//console.log(myObject);
												    			} 

											    				// implant
											    				var tempBonus2 = tempBonus * 0.97; // implant
											    				if(tempBonus2.toFixed(precisionCheck) == duration || tempBonus2 == duration) {
												    				isMatch = true;
												    				numMatches++;

												    				var myObject = {};
												    				myObject['misLvl'] = gSkill;
												    				myObject['rlLvl'] = rfSkill;
												    				myObject['spLvl'] = spSkill;
												    				if(shSkill > 0)
											    						myObject['shSkill'] = shSkill;
												    				myObject['mod1'] = dmBonus1;
												    				myObject['mod2'] = dmBonus2;
												    				myObject['mod3'] = dmBonus3;
												    				myObject['mod4'] = dmBonus4;
												    				myObject['rigs'] = rigs-1;
												 					myObject['implant'] = '3%';
												 					myObject['normalDamage'] = tempBonus2;
												 					myObject['variant'] = turretVariants[baseRofCount];

												 					myMatches.push(myObject);
												    				//console.log(myObject);
												    			} 

											    				// overheating
											    				var tempBonus2 = tempBonus * 0.85; // implant
											    				if(tempBonus2.toFixed(precisionCheck) == duration || tempBonus2 == duration) {
												    				isMatch = true;
												    				numMatches++;

												    				var myObject = {};
												    				myObject['misLvl'] = gSkill;
												    				myObject['rlLvl'] = rfSkill;
												    				myObject['spLvl'] = spSkill;
												    				if(shSkill > 0)
										    							myObject['shSkill'] = shSkill;
												    				myObject['mod1'] = dmBonus1;
												    				myObject['mod2'] = dmBonus2;
												    				myObject['mod3'] = dmBonus3;
												    				myObject['mod4'] = dmBonus4;
												    				myObject['rigs'] = rigs-1;
												    				myObject['overheated'] = true;
												 					myObject['normalDamage'] = tempBonus;
												 					myObject['variant'] = turretVariants[baseRofCount];

												 					myMatches.push(myObject);
												    				//console.log(myObject);
												    			} 

												    			// overheating ++ implant
											    				var tempBonus2 = (tempBonus * 0.85) *0.97; // implant
											    				if(tempBonus2.toFixed(precisionCheck) == duration || tempBonus2 == duration) {
												    				isMatch = true;
												    				numMatches++;

												    				var myObject = {};
												    				myObject['misLvl'] = gSkill;
												    				myObject['rlLvl'] = rfSkill;
												    				myObject['spLvl'] = spSkill;
												    				if(shSkill > 0)
									    								myObject['shSkill'] = shSkill;
												    				myObject['mod1'] = dmBonus1;
												    				myObject['mod2'] = dmBonus2;
												    				myObject['mod3'] = dmBonus3;
												    				myObject['mod4'] = dmBonus4;
												    				myObject['rigs'] = rigs-1;
												    				myObject['overheated'] = true;
												    				myObject['implant'] = '3%';
												 					myObject['normalDamage'] = tempBonus * 0.97;
												 					myObject['variant'] = turretVariants[baseRofCount];

												 					myMatches.push(myObject);
												    				//console.log(myObject);
												    			}
												    		}
												    	}
										    		}
									    		}
									    	}
									    }
									}
								}
							}
						}
					}
				}

				if(isMatch == true && numMatches == 1) {
					return myObject;
				}

				if(numMatches > 1) {
					var misL = myMatches[0]['misLvl'];
					var rlL = myMatches[0]['rlLvl'];
					var spL = myMatches[0]['spLvl'];
					
					var myDam1 = myMatches[0]['mod1'];
					var myDam2 = myMatches[0]['mod2'];
					var myDam3 = myMatches[0]['mod3'];
					var myDam4 = myMatches[0]['mod4'];
					var rg = myMatches[0]['rigs'];
					var oh = myMatches[0]['overheated'];
					var imp = myMatches[0]['implant'];
					var nd = myMatches[0]['normalDamage'];
					var shL = null; //var shL = myMatches[0][shSkill];

					var variants = [];
					var isVariant = true;
					if(typeof myMatches[0]['shSkill'] !== 'undefined') {
						shL = myMatches[0]['shSkill'];
					}

//	myObject['variant'] = turretVariants[baseRofCount];

					for (var objCount = 1; objCount < myMatches.length; ++objCount) {
			    		var obj = myMatches[objCount];

			    		if( misL != myMatches[objCount]['misLvl'] ||
			    			rlL != myMatches[objCount]['rlLvl'] ||
			    			spL != myMatches[objCount]['spLvl'] ||
			    			myDam1 != myMatches[objCount]['mod1'] ||
			    			myDam2 != myMatches[objCount]['mod2'] ||
			    			myDam3 != myMatches[objCount]['mod3'] ||
			    			myDam4 != myMatches[objCount]['mod4'] ||
			    			rg != myMatches[objCount]['rigs'] ||
			    			oh != myMatches[objCount]['overheated'] ||
			    			imp != myMatches[objCount]['implant'] ||
			    			nd != myMatches[objCount]['normalDamage']) {

			    			isVariant = false;
			    		}

			    		if(typeof myMatches[objCount]['shSkill'] !== 'undefined') {
			    			if(shL != myMatches[objCount]['shSkill'])
			    				isVariant = false;
			    		}
			    	}

			    	// if it's just a variant
			    	if(isVariant == true) {
			    		myMatches[myMatches.length-1]['variant']['typeName'] += '\r\n(or Variant with same ROF)';
			    		return myMatches[myMatches.length-1];
			    	} else {
						console.log('Multi-Matches ('+shipID+') : '+numMatches);
						console.log(myMatches);
						//console.log(shipROFBonus);
						//console.log(maxShipSkills);
						return true;
					}
				}
		        return false;
		        break;

		    case 'Mining Laser':
		    case 'Festival Launcher':
		    case 'Missile Launcher Bomb':
		    	return true;
		    	break;
		    default:
		        console.log(weaponType+' is not recognized');
		        return false; 
		} 
	},

	calculateArmourBooster: function(rof, shipID) {
		// Armour Booster / Ancillary Booster - maybe hull repairers
		var sizeBase = [15000, 12000, 6000]; // x-l, l, m, s
		var armourModBonus = [0, 4.8, 6];
		var myMatches = [];
		var numMatches = 0;
		var isNoBonus = false;

		for (var repairSkill = 1; repairSkill < 6; ++repairSkill) { // this should usually be 5
			for (var warfareLinkLevel = 0; warfareLinkLevel < 6; ++warfareLinkLevel) {
	    		for (var armourWarfareLevel = 0; armourWarfareLevel < 6; ++armourWarfareLevel) {
	    			for (var shipLevel = 0; shipLevel < 6; ++shipLevel) { // if this is ever 0 then surely the mod is on the wrong ship (without bonus to armour warfare)		
		    			for (var numRigs = 0; numRigs < 4; ++numRigs) {
		    				for (var sizeNum = 0; sizeNum < sizeBase.length; ++sizeNum) {

		    					var bonuses = [];
	    						var stackingBonus = 1;

						    	for (rigs = 0; rigs <= numRigs; rigs++) {
						    		if(rigs != 0) {
						    			bonuses.push(15);
						    		}
						    	}

				    			if(numRigs > 0) {
				    				bonuses.sort(function(a, b){return b-a});
				    				stackingBonus = this.calculateStackingBonus(bonuses);
				    			}

					    		var sizeROF = sizeBase[sizeNum];
					    		sizeROF *= 1-(repairSkill * 0.05);
					    		sizeROFImp = sizeROF * 0.97; // 3% personal implant

					    		//sizeROF *= stackingBonus;
					    		//sizeROFImp *= stackingBonus;
					    		for (var modNum = 0; modNum < armourModBonus.length; ++modNum) {
						    		var baseBonus = armourModBonus[modNum];

						    		var calcBonus = (baseBonus / 100);

						    		//calcBonus *= (1+(repairSkill * 0.05));
						    		calcBonus *= (1+(warfareLinkLevel * 0.1)); // warfare link specialist
						    		calcBonus *= (1+(armourWarfareLevel * 0.2)); // armour warfare specialist
						    		calcBonus *= (1+(shipLevel * 0.03));
						    		//calcBonus *= stackingBonus;
						    		//calcBonus *= 1.03;
						    		var calcImplant = calcBonus * 1.25;
	
						    		if(	(sizeROF * (1 - calcBonus)) == rof || 
						    			(sizeROF * (1 - calcImplant)) == rof || 
						    			((sizeROF * (1 - calcBonus))*0.85) == rof || 
						    			((sizeROF * (1 - calcImplant))*0.85) == rof ||
						    			(sizeROFImp * (1 - calcBonus)) == rof || 
						    			(sizeROFImp * (1 - calcImplant)) == rof || 
						    			((sizeROFImp * (1 - calcBonus))*0.85) == rof || 
						    			((sizeROFImp * (1 - calcImplant))*0.85) == rof ) {

						    			var myObject = {};
					    				myObject['warfareLinkLevel'] = warfareLinkLevel;
					    				myObject['armourWarfareLevel'] = armourWarfareLevel;
					    				myObject['shipLevel'] = shipLevel;

					    				
					    				if(	(sizeROFImp * (1 - calcBonus)) == rof || 
							    			(sizeROFImp * (1 - calcImplant)) == rof || 
							    			((sizeROFImp * (1 - calcBonus))*0.85) == rof || 
							    			((sizeROFImp * (1 - calcImplant))*0.85) == rof )	{
											myObject['pilotImplant'] = true;
											myObject['baseCycle'] = sizeROFImp;
										} else {
											myObject['pilotImplant'] = false;
					    					myObject['baseCycle'] = sizeROF;
										}


					    				if(baseBonus == 4.8) {
					    					myObject['tech'] = 1;
					    					myObject['linkName'] = 'Armoured Warfare Link - Rapid Repair I';
					    				} else {
					    					if(baseBonus == 6) {
						    					myObject['tech'] = 2;
						    					myObject['linkName'] = 'Armoured Warfare Link - Rapid Repair II';
						    				}
					    				}

					    				switch(sizeBase[sizeNum]) {
					    					case 15000:
					    						myObject['modName'] = 'Large ';
					    						break;
					    					case 12000:
					    						myObject['modName'] = 'Medium ';
					    						break;
					    					case 6000:
					    						myObject['modName'] = 'Small ';
					    						break;
					    					case 'Default':
					    						console.log('unspecified problem with Armoured Warfare Link - Rapid Repair detection');
					    				}

					    				
					    				myObject['implant'] = false;	
					    				if((sizeROF * (1 - calcImplant)) == rof || ((sizeROF * (1 - calcImplant))*0.85) == rof || 
					    					sizeROFImp * (1 - calcImplant) == rof || ((sizeROFImp * (1 - calcImplant))*0.85) == rof) {
											myObject['implant'] = true;
											myObject['baseCycleSiege'] = (sizeROF * (1 - calcImplant));
										} else {
											myObject['implant'] = false;
											myObject['baseCycleSiege'] = (sizeROF * (1 - calcBonus));
										}

										if( myObject['shipLevel'] == 0 && myObject['armourWarfareLevel'] == 0 && myObject['warfareLinkLevel'] == 0) {
											myMatches.push(myObject);
											numMatches++;
											isNoBonus = true;

											return myObject;
										} else {
											// assume they put the link on a sip with the bonus
											//if(myObject['shipLevel'] > 0) {
												myMatches.push(myObject);
												numMatches++;
											//}
										}
										//console.log(myObject);
						    		}
						    	}
					    	}
					    }
	    			}
	    		}
	    	}
    	}

    	if(numMatches == 0) {
    		console.log('fail');
    		return false;
    	}
    	if(numMatches > 1) {
    		console.log('Armoured Warfare Link Multi-Matches ('+shipID+') : '+numMatches);
			console.log(myMatches);
    	}

    	return myObject;
	},

	calculateShieldBooster: function(rof, shipID) {
		// Shield Booster / Ancillary Booster
		var sizeBase = [5000, 4000, 3000, 2000]; // x-l, l, m, s
		var siegeModBonus = [0, 4.6, 6];
		var myMatches = [];
		var numMatches = 0;
		var isNoBonus = false;

		for (var warfareLinkLevel = 0; warfareLinkLevel < 6; ++warfareLinkLevel) {
    		for (var siegeWarfareLevel = 0; siegeWarfareLevel < 6; ++siegeWarfareLevel) {
    			for (var shipLevel = 0; shipLevel < 6; ++shipLevel) { // if this is ever 0 then surely the mod is on the wrong ship (without bonus to siege warfare)		
    				for (var sizeNum = 0; sizeNum < sizeBase.length; ++sizeNum) {
			    		var sizeROF = sizeBase[sizeNum];

			    		for (var modNum = 0; modNum < siegeModBonus.length; ++modNum) {
				    		var baseBonus = siegeModBonus[modNum];

				    		var calcBonus = (baseBonus / 100);

				    		calcBonus *= (1+(warfareLinkLevel * 0.1)); // warfare link specialist
				    		calcBonus *= (1+(siegeWarfareLevel * 0.2)); // siege warfare specialist
				    		calcBonus *= (1+(shipLevel * 0.03));
				    		var calcImplant = calcBonus * 1.25;

				    		if(	(sizeROF * (1 - calcBonus)) == rof || 
				    			(sizeROF * (1 - calcImplant)) == rof || 
				    			((sizeROF * (1 - calcBonus))*0.85) == rof || 
				    			((sizeROF * (1 - calcImplant))*0.85) == rof) {

				    			var myObject = {};
			    				myObject['warfareLinkLevel'] = warfareLinkLevel;
			    				myObject['siegeWarfareLevel'] = siegeWarfareLevel;
			    				myObject['shipLevel'] = shipLevel;

			    				if(baseBonus == 4.6) {
			    					myObject['tech'] = 1;
			    					myObject['linkName'] = 'Siege Warfare Link - Active Shielding I';
			    				} else {
			    					if(baseBonus == 6) {
				    					myObject['tech'] = 2;
				    					myObject['linkName'] = 'Siege Warfare Link - Active Shielding II';
				    				}
			    				}

			    				switch(sizeROF) {
			    					case 5000:
			    						myObject['modName'] = 'X-Large ';
			    						break;
			    					case 4000:
			    						myObject['modName'] = 'Large ';
			    						break;
			    					case 3000:
			    						myObject['modName'] = 'Medium ';
			    						break;
			    					case 2000:
			    						myObject['modName'] = 'Small ';
			    						break;
			    					case 'Default':
			    						console.log('unspecified problem with Siege Warfare Link - Active Shielding detection');
			    				}

			    				myObject['baseCycle'] = sizeROF;
			    				myObject['implant'] = false;	
			    				if((sizeROF * (1 - calcImplant)) == rof || ((sizeROF * (1 - calcImplant))*0.85) == rof) {
									myObject['implant'] = true;
									myObject['baseCycleSiege'] = (sizeROF * (1 - calcImplant));
								} else {
									myObject['implant'] = false;
									myObject['baseCycleSiege'] = (sizeROF * (1 - calcBonus));
								}

								if( myObject['shipLevel'] == 0 && myObject['siegeWarfareLevel'] == 0 && myObject['warfareLinkLevel'] == 0) {
									myMatches.push(myObject);
									numMatches++;
									isNoBonus = true;

									return myObject;
								} else {
									// assume they put the link on a sip with the bonus
									if(myObject['shipLevel'] > 0) {
										myMatches.push(myObject);
										numMatches++;
									}
								}
								//console.log(myObject);
				    		}
				    	}
			    	}
    			}
    		}
    	}

    	if(numMatches == 0)
    		return false;

    	if(numMatches > 1) {
    		console.log('Siege Warfare Link Multi-Matches ('+shipID+') : '+numMatches);
			console.log(myMatches);
    	}

    	return myObject;
	},

	calculateStackingBonus: function(bonuses) {
		var magicModifier = 1-(bonuses[0]/100);
		var index;

		for (index = 1; index < bonuses.length; ++index) {
		    magicModifier *= 1-(bonuses[index] * Math.exp(-Math.pow(index,2)/7.1289)/100);
		}

		return magicModifier;
	},

	calcShipVelocity: function(physicsData) {
        return physicsData ? Math.sqrt(physicsData.vx * physicsData.vx + physicsData.vy * physicsData.vy + physicsData.vz * physicsData.vz).toFixed(0) : "-";
    }

	
}