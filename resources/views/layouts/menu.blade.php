<div class="navbar">
	<div class="navbar-inner">
		<!-- start: Header Menu -->
		<div class="navbar-collapse collapse header-nav">
			<a class="navbar-left brand" href="{{ GetURLPath('/') }}">
				<image src="{{secure_asset('https://image.eveonline.com/alliance/386292982_32.png') }}" width="32" height="32" />
				Pandemic Legion
			</a>
			<?php
			/*
			<ul class="nav navbar-left">
				<li class="dropdown"><a class="btn" href="{{ GetURLPath('/') }}">Tournaments</a></li>
			</ul>
			<ul class="nav navbar-left">
				<li class="dropdown"><a class="btn" href="{{ GetURLPath('/teambuilder') }}">Team Builder</a></li>
			</ul>
			*/
			?>
			@if(session()->has('sso_characterid'))
			<ul class="nav navbar-left">
				<li class="dropdown"><a class="btn" href="{{ GetURLPath('/supertracker') }}">SuperCap Tracker</a></li>
			</ul>
			@endif
			<ul class="nav navbar-left">
				<li class="dropdown"><a class="btn" href="https://bitbucket.org/rmundi/ccptourney">BitBucket</a></li>
			</ul>
			@if(env('CREST_APP_ID') != '' && env('CREST_APP_KEY') != '' && env('CREST_APP_URL') != '')
				<ul class="nav navbar-right navbar-right-end">
				@if(!session()->has('sso_characterid'))
					<li class="dropdown"><a id="ssologin" class="btn loginsso" href=""></a></li>
				@else
					<li class="dropdown">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
							<image class="loginportrait" src="{{{secure_asset('https://image.eveonline.com/Character/'.session('sso_characterid').'_32.jpg')}}}" width="32" height="32" />
							{{{session('sso_charactername')}}}
							<span class="caret"></span>
							@if(session()->has('sso_isloggedin') && session('sso_isloggedin') === true)
								<br />
								<span class="nav-subscript">
								@if(session()->has('sso_stationname'))
									{{{session('sso_stationname')}}}
								@else
									{{{session('sso_solarsystemname')}}}
								@endif
								</span>
							@endif
						</a>
						<ul class="dropdown-menu notifications">
							<li class="dropdown-menu-title">
								<span>Character</span>
							</li>
							<li><a href="{{ GetURLPath('/character/'.urlencode(session('sso_charactername'))) }}"><span class="message">Character Sheet</span></a></li>
							<li><a href="{{ GetURLPath('/assets') }}"><span class="message">Assets</span></a></li>
							<li class="dropdown-menu-title">
								<span>Settings</span>
							</li>
							<li>
								<a href="{{ GetURLPath('/logout') }}">
									<span class="message"> Logout</span>
								</a>
							</li>
						</ul>
	                </li>
				@endif
				</ul>
			@endif           
		</div>
		<!-- end: Header Menu -->
	</div>
</div>
<div class="menu-spacer"></div>
@if(env('GOOGLE_AD_CODE') != '' && env('GOOGLE_AD_SLOT') != '')
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Generic Leaderboard ad -->
	<ins class="adsbygoogle"
	     style="display:block;width:728px;height:90px;margin-left:auto;margin-right:auto;"
	     data-ad-client="{{{env('GOOGLE_AD_CODE')}}}"
	     data-ad-slot="{{{env('GOOGLE_AD_SLOT')}}}"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
@endif