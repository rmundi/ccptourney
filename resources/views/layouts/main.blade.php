<!doctype html>
<html lang="en">
	<head>
		<!-- start: Meta -->
		<meta charset="utf8">
		<meta name="description" content="Captain Thunk's Eve Online Tool Kit">
		<meta name="author" content="Captain Horatio Thunk">
		<meta name="keyword" content="Thunk, Eve-Online, Tournament, Match, Fitting, Ships, CREST, CCP, AT XI, ATXII, ATXIII, ATXIV, ATXV, Alliance, EveTools, CharacterSheet">
		<!-- end: Meta -->

		<!-- start: Mobile Specific -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<!-- end: Mobile Specific -->

		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- start: CSS -->
		<link id="bootstrap-style" href="{{ GetURLPath('') }}/css/bootstrap.min.css?v={{{config('tools.app-ver')}}}" rel="stylesheet">
		<link id="base-style" href="{{ GetURLPath('') }}/css/bootstrap-theme.min.css?v={{{config('tools.app-ver')}}}" rel="stylesheet">
		<link id="base-style-custom" href="{{ GetURLPath('') }}/css/bootstrap-theme-custom.min.css?v={{{config('tools.app-ver')}}}" rel="stylesheet">
		<link id="datatables-style" href="{{ GetURLPath('') }}/css/datatables.min.css?v={{{config('tools.app-ver')}}}" rel="stylesheet">
		<!-- end: CSS -->

		<!-- start: js -->
		@if(config('tools.sso-id') != '' && config('tools.sso-key') != '' && config('tools.sso-url') != '')
		<script>
			var crest_app_url = "{{{config('tools.sso-url')}}}";
			var crest_app_id = "{{{config('tools.sso-id')}}}";
			var crest_app_state = "{{{Thunk\Classes\Crest::setSSOState()}}}";
			var crest_auth_url = "{{{config('tools.sso-auth-url')}}}";
			var crest_scope = "{{{Thunk\Classes\Crest::setSSOScope()}}}";
			@if(session()->has('sso_isloggedin') && session('sso_isloggedin') === true)
				var crest_loggedin = true;
			@else
				var crest_loggedin = false;
			@endif
		</script>
		@endif

		<script type="text/javascript" src="{{ GetURLPath('') }}/js/jquery-1.12.3.min.js?v={{{config('tools.app-ver')}}}"></script>
		<script type="text/javascript" src="{{ GetURLPath('') }}/js/jquery-ui/jquery-ui.min.js?v={{{config('tools.app-ver')}}}"></script>
		<script type="text/javascript" src="{{ GetURLPath('') }}/js/bootstrap.min.js?v={{{config('tools.app-ver')}}}"></script>
		<script type="text/javascript" src="{{ GetURLPath('') }}/js/custom.min.js?v={{{config('tools.app-ver')}}}"></script>
		<script type="text/javascript" src="{{ GetURLPath('') }}/js/datatables.min.js?v={{{config('tools.app-ver')}}}"></script>
		@yield('load_additional_js')
		<!-- end: js -->

		<!-- start: Favicon -->
		<link rel="shortcut icon" href="{{GetURLPath('')}}/favicon.png">
		<!-- end: Favicon -->

		<title>@yield('page_title')</title>
	</head>
	<body class="@yield('body_class')">

		@include('layouts.menu')

		<div class="container">
			@include('layouts.messages')

			@yield('content')
		</div>
		@include('layouts.footer')
		
		
		
		<script>var BASEURL = '{{ GetURLPath('') }}';</script>
		@if(env('GOOGLE_TRACK_CODE') != '')
			<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', '{{{env("GOOGLE_TRACK_CODE")}}}']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			</script>
		@endif

		@yield('additional_js')
		
	</body>
</html>
