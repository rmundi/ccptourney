<div class="footer">
	<div class="pull-left hidden-xs hidden-sm">
		All EVE related materials are property of <a href="http://www.ccpgames.com">CCP Games</a><br>
		<a href="{{ GetURLPath('ccpcopyright') }}">© CCP hf. All rights reserved. Used with permission.</a>
	</div>
    <div class="pull-right" style="text-align:right">
		© Captain Thunk<br />
		<a href="{{ GetURLPath('legal') }}">Copyright Notice</a>
	</div>
</div>
<!-- Back To Top -->
<div class="hidden-xs hidden-sm">
	<div id="back-top">
		<a href="#top">
			<span class="glyphicon glyphicon-arrow-up"></span><br />
			Back to the Top
		</a>
	</div>
</div>