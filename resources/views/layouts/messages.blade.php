<div id='alert-messages'>
    @if(Session::has('errormessage'))
    <div class="alert alert-danger fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p class="text-center">{{Session::get('errormessage')}}</p>
    </div>
    @endif
    @if(Session::has('warningmessage'))
    <div class="alert alert-warning fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p class="text-center">{{Session::get('warningmessage')}}</p>
    </div>
    @endif
    @if(Session::has('infomessage'))
    <div class="alert alert-info fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p class="text-center">{{Session::get('infomessage')}}</p>
    </div>
    @endif
    @if(Session::has('successmessage'))
    <div class="alert alert-success fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p class="text-center">{{Session::get('successmessage')}}</p>
    </div>
    @endif
</div>
<?php 
    Session::forget('errormessage');
    Session::forget('warningmessage');
    Session::forget('infomessage');
    Session::forget('successmessage');
?>