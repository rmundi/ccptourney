@extends('layouts.main')

@section('page_title')Pandemic Legion - Character 
@stop

@section('body_class')body_class 
@stop

@section('content')

<div class="page">
	<div id="contents">
		@if(isset($APIPAGE))
			@include('api.'.$APIPAGE)
		@endif
	</div>
	<div style="clear:both;">&nbsp;</div>
</div>
@stop

