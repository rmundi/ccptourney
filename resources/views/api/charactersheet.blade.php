<?php

	// not good practice to put so much PHP into a view, but damn!, it's a lot easier this way. Thanks, I'm here all night, try the long-limbed roes.
	// logic for it, better to send simple $data object to the view and do necessary processing here than repeat it constantly everytime this view is used.

	// check CrestController->getCharacter to see what it expects to receive

	$raceImg = getIcon(Thunk\chrRace::getRaceImg($DATA['character']->race), 128);
	$bloodLineImg = getIcon(Thunk\chrBloodline::getBloodlineImg($DATA['character']->bloodLine), 128);

	// get Implant info
	$implantArray = array();
	$charImplants = array();
	$maxClones = 0;

	$intBonus = 0;
	$memBonus = 0;
	$chaBonus = 0;
	$percBonus = 0;
	$wilBonus = 0;
	$extraImplants = false;

	$intBonusTemp = 0;
	$memBonusTemp = 0;
	$chaBonusTemp = 0;
	$percBonusTemp = 0;
	$wilBonusTemp = 0;

	foreach($DATA['cloneImplants'] as $tempImplant) {
		if($tempImplant->jumpCloneID === null) { 
			
			if($tempImplant->slotNum > 10)
				$extraImplants = true;
	
			$intBonus += $tempImplant->intBonus;
			$memBonus += $tempImplant->memBonus;
			$chaBonus += $tempImplant->chaBonus;
			$percBonus += $tempImplant->percBonus;
			$wilBonus += $tempImplant->willBonus;

			$implantArray[$tempImplant->slotNum] = array('Int' => $tempImplant->intBonus, 'Mem' => $tempImplant->memBonus, 'Cha' => $tempImplant->chaBonus, 'Per' => $tempImplant->percBonus, 'Wil' => $tempImplant->willBonus, 'typeName' => $tempImplant->typeName);
		}
	}

	$intBase = $DATA['character']->intelligence;
	$memBase = $DATA['character']->memory;
	$chaBase = $DATA['character']->charisma;
	$percBase = $DATA['character']->perception;
	$wilBase = $DATA['character']->willpower;

	$DATA['character']->intelligence += $intBonus;
	$DATA['character']->memory += $memBonus;
	$DATA['character']->charisma += $chaBonus;
	$DATA['character']->perception += $percBonus;
	$DATA['character']->willpower += $wilBonus;

	$skillPoints = 0;
	$skillGroupName = '';
	$skillGroups = array();
	$cloneJumpModifier = 0;


//$skillPoints = $DATA['skills']->sum('skillPoints');
// TODO: Can this be unified?
	foreach($DATA['skills'] as $s) {
		$skillPoints += $s->skillPoints;
		if($s->groupName != $skillGroupName) {
			$skillGroupName = $s->groupName;
			$skillGroups[$skillGroupName] = 0;
		}
		if($s->typeID == 33399) 
			$cloneJumpModifier = $s->level * 3600;
	
		if($s->typeID == 24242 || $s->typeID == 33407)
			$maxClones += $s->level;

		$skillGroups[$skillGroupName]++;
	}
?>
<div class="row">
	<div class="col-md-3">
		<div class="row text-center">
			<div class="portrait-img">
				<img src="https://image.eveonline.com/Character/{{$DATA['character']->characterID}}_256.jpg" alt="{{$DATA['character']->name}}" title="{{$DATA['character']->name}}" border="0" height="256" width="256">
			</div>
			<div style="clear:both;">&nbsp;</div>
			<div class="row">
				<div class="col-xs-4">
					<img src="https://image.eveonline.com/Corporation/{{$DATA['character']->corporationID}}_64.png" alt="{{$DATA['character']->corporationName}} Logo" title="{{$DATA['character']->corporationName}}" height="64" width="64">
				</div>
				<div class="col-xs-4">
					<img src="/images/Icons/items/{{$raceImg}}.png" alt="{{$DATA['character']->race}}" title="{{$DATA['character']->race}}" height="64" width="64">
				</div>
				<div class="col-xs-4">
					<img src="/images/Icons/items/{{$bloodLineImg}}.png" alt="{{$DATA['character']->bloodLine}}" title="{{$DATA['character']->bloodLine}}" height="64" width="64">
				</div>
			</div>
			<div style="clear:both;">&nbsp;</div>
			<div class="text-italic">Character updated {{$DATA['character']->updated_at}}</div>
			<div style="clear:both;">&nbsp;</div>
			@if($DATA['accountStatus'])
			<div class="text-left">
				<div class="row api-page-header">
					<div>
						Account
					</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-5">Created</div>
					<div class="col-xs-7 api-page-grid-cell">{{$DATA['accountStatus']->createDate}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-5">Paid Until</div>
					<div class="col-xs-7 api-page-grid-cell">{{$DATA['accountStatus']->paidUntil}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-5">Logon Count</div>
					<div class="col-xs-7 api-page-grid-cell">{{number_format($DATA['accountStatus']->logonCount,0,".",",")}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-5">Logon Minutes</div>
					<div class="col-xs-7 api-page-grid-cell">{{number_format($DATA['accountStatus']->logonMinutes,0,".",",")}}</div>
				</div>
			</div>
			@endif
		</div>
	</div>
	<div class="hidden-lg hidden-md" style="clear:both;">&nbsp;</div>
	<div class="col-md-9">
		<div class="row">
			<div class="col-md-8">
				<div class="row api-page-header api-main-header">
					<div class="api-page-title">
						{{$DATA['character']->characterName}}
					</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Corporation</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->corporationName}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Alliance</div>
					<div class="col-xs-6 api-page-grid-cell">{{($DATA['character']->allianceName != null ? $DATA['character']->allianceName : '-')}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Date of Birth</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->DoB}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">R / B / A</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->race}} / {{$DATA['character']->bloodLine}} / {{$DATA['character']->ancestry}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Skill Points</div>
					<div class="col-xs-6 api-page-grid-cell">{{number_format($skillPoints,0,".",",")}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">ISK</div>
					<div class="col-xs-6 api-page-grid-cell">{{number_format($DATA['characterInfo']->accountBalance,2,".",",")}}</div>
				</div>
				@if($DATA['characterInfo'] && $DATA['characterInfo']->securityStatus)
				<div class="row api-page-grid">
					<div class="col-xs-6">Security Status</div>
					<div class="col-xs-6 api-page-grid-cell">{{number_format($DATA['characterInfo']->securityStatus,1,".",",")}}</div>
				</div>
				@endif
				<div class="row api-page-grid">
					<div class="col-xs-6">Home Station</div>
					<?php $loc = resolveID($DATA['character']->homeStationID); ?>
					<div class="col-xs-6 api-page-grid-cell">{{$loc[0]['name']}}</div>
				</div>
			</div>
			<div class="hidden-lg hidden-md" style="clear:both;">&nbsp;</div>
			<div class="col-md-4">
				<div class="row api-page-header api-main-header">
					<div>Attributes</div>
					<div class="api-page-subtext">(Total - Implant - Base)</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Intelligence</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->intelligence}} (+{{(int)$intBonus}}) - {{$intBase}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Perception</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->perception}} (+{{(int)$percBonus}}) - {{$percBase}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Charisma</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->charisma}} (+{{(int)$chaBonus}}) - {{$chaBase}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Willpower</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->willpower}} (+{{(int)$wilBonus}}) - {{$wilBase}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-6">Memory</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->memory}} (+{{(int)$memBonus}}) - {{$memBase}}</div>
				</div>
				@if($DATA['character']->DoB != "0000-00-00 00:00:00" && $DATA['character']->updated_at && $skillPoints != 0)
				<div class="row api-page-grid">
					<div class="col-xs-6">Avg. SP/hour</div>
					<div class="col-xs-6 api-page-grid-cell">{{number_format($skillPoints / (( (strtotime($DATA['character']->updated_at) - strtotime($DATA['character']->DoB)) / 60) /60),2,".",",")}}</div>
				</div>
				@endif
				@if($DATA['character']->freeSkillPoints != 0)
				<div class="row api-page-grid">
					<div class="col-xs-6">Bonus SP</div>
					<div class="col-xs-6 api-page-grid-cell">{{number_format($DATA['character']->freeSkillPoints,0,".",",")}}</div>
				</div>
				@endif
				@if($DATA['character']->freeRespecs != 0)
				<div class="row api-page-grid">
					<div class="col-xs-6"># Remaps</div>
					<div class="col-xs-6 api-page-grid-cell">{{$DATA['character']->freeRespecs}}</div>
				</div>
				@endif
				@if($maxClones > count($DATA['clones']))
				<div class="row api-page-grid">
					<div class="col-xs-6"># Free JC Slots</div>
					<div class="col-xs-6 api-page-grid-cell">{{($maxClones - count($DATA['clones']))}}</div>
				</div>
				@endif
			</div>
		</div>
		<div style="clear:both;">&nbsp;</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row api-page-header">
					<div>Implants</div>
				</div>
				@if($implantArray != null)
				<div class="row api-page-grid">
					<div class="col-sm-6">
					@for($x=1;$x<=5;$x++)
						<div class="row">
							<div class="col-xs-1">{{$x}}.</div>
							@if(isset($implantArray[$x]['typeName']))
							<div class="col-xs-11 clip-text">{{$implantArray[$x]['typeName']}}</div>
							@else
							<div class="col-xs-11 text-italic">No Implant</div>
							@endif
						</div>
					@endfor
					</div>
					<div class="col-sm-6">
					@for($x=6;$x<=10;$x++)
						<div class="row">
							<div class="col-xs-1">{{$x}}.</div>
							@if(isset($implantArray[$x]['typeName']))
							<div class="col-xs-11 clip-text">{{$implantArray[$x]['typeName']}}</div>
							@else
							<div class="col-xs-11 text-italic">No Implant</div>
							@endif
						</div>
					@endfor
					</div>
					@if($extraImplants === true)
					<div class="col-sm-6">
						@foreach($implantArray as $idx => $xtraplant)
							@if($idx > 10)
							<div class="row">
								<div class="col-xs-1">{{$idx}}.</div>
								<div class="col-xs-11 clip-text">{{$xtraplant['typeName']}}</div>
							</div>
							@endif
						@endforeach
					</div>
					@endif
				</div>
				@else
					<div class="row api-page-grid">
						<div class="col-xs-12 text-italic">No Implants</div>
					</div>
				@endif
			</div>
		</div>
		<div style="clear:both;">&nbsp;</div>

		<div class="row">
			<div class="col-md-12">
				<div class="row api-page-header">
					<div>Training</div>
				</div>
				<div class="row api-page-grid">
					@if($DATA['skillQueue']->count() > 0)
					<div class="col-xs-4">Currently training</div>
					<div class="col-xs-8 api-page-grid-cell">{{($DATA['skillQueue'] ? $DATA['skillQueue'][0]->typeName.' training to level '.$DATA['skillQueue'][0]->level : 'None')}}</div>
					@else
					<div class="col-xs-12 text-italic">No skill in Training</div>
					@endif
				</div>
				@if($DATA['skillQueue']->count() > 0)
				<div class="row api-page-grid">
					<div class="col-xs-4">Time remaining</div>
					<div class="col-xs-8 api-page-grid-cell">{{($DATA['skillQueue'][0]->endTime != '' ? rel_time(strtotime($DATA['skillQueue'][0]->endTime),gmdate("Y-m-d H:i:s")) : '<span class="text-italic">Training has been paused</span>')}}</div>
				</div>
				<div class="row api-page-grid">
					<div class="col-xs-4">ETA Completed</div>
					<div class="col-xs-8 api-page-grid-cell">{{($DATA['skillQueue'][0]->endTime != '' ? $DATA['skillQueue'][0]->endTime.' (EVE-Time)' : '<span class="text-italic">N/A</span>')}}</div>
				</div>	
				@endif
			</div>
		</div>	
	</div>
</div>
<div style="clear:both;">&nbsp;</div>
	<div class="row">
		<div class="col-md-12">
			<div class="row api-page-header">
				<div>Timers</div>
			</div>
			<div class="row api-page-grid">
				<div class="col-xs-6 col-sm-3 api-page-grid-cell">Home Station Date</div>
				<div class="col-xs-6 col-sm-3 api-page-grid-cell clip-text">
					<?php
					if(strtotime($DATA['character']->remoteStationDate) < strtotime(gmdate("Y-m-d H:i:s")))
						echo '<span class="text-green text-italic">Now</span>';
					else
						echo rel_time($DATA['character']->remoteStationDate, gmdate("Y-m-d H:i:s"), true);
					?>
				</div>
				<div class="col-xs-6 col-sm-3 api-page-grid-cell">Next Jump</div>
				<div class="col-xs-6 col-sm-3 api-page-grid-cell clip-text">
					<?php
					if(strtotime($DATA['character']->jumpActivation) < strtotime(gmdate("Y-m-d H:i:s")))
						echo '<span class="text-green text-italic">Now</span>';
					else
						echo rel_time($DATA['character']->jumpActivation, gmdate("Y-m-d H:i:s"), true);
					?>
				</div>
			</div>
			<div class="row api-page-grid">
				<div class="col-xs-6 col-sm-3 api-page-grid-cell">Next Remap</div>
				<div class="col-xs-6 col-sm-3 api-page-grid-cell clip-text">
					<?php
					$datetime = new DateTime(null, new DateTimeZone('UTC'));
					$datetime->modify('-1 years');
					if(strtotime($DATA['character']->lastTimedRespec) < strtotime($datetime->format("Y-m-d H:i:s")))
						echo '<span class="text-green text-italic">Now</span>';
					else
						echo rel_time($DATA['character']->lastTimedRespec, $datetime->format("Y-m-d H:i:s"), true);
					?>
				</div>
				<div class="col-xs-6 col-sm-3 api-page-grid-cell">Jump Fatigue</div>
				<div class="col-xs-6 col-sm-3 api-page-grid-cell clip-text">
					<?php
					if(strtotime($DATA['character']->jumpFatigue) < strtotime(gmdate("Y-m-d H:i:s")))
						echo '<span class="text-green text-italic">None</span>';
					else
						echo rel_time($DATA['character']->jumpFatigue, gmdate("Y-m-d H:i:s"), true);
					?>
				</div>
			</div>
			<div class="row api-page-grid">
				<div class="col-xs-6 col-sm-3 api-page-grid-cell">Next Clone Jump</div>
				<div class="col-xs-6 col-sm-3 api-page-grid-cell clip-text">
					<?php
					if((strtotime($DATA['character']->cloneJumpDate)+(86400-$cloneJumpModifier)) < strtotime(gmdate("Y-m-d H:i:s")))
						echo '<span class="text-green text-italic">Now</span>';
					else
						echo rel_time((strtotime($DATA['character']->cloneJumpDate)+(86400-$cloneJumpModifier)), gmdate("Y-m-d H:i:s"), true);
					?>
				</div>
			</div>
		</div>
	</div>
<div style="clear:both;">&nbsp;</div>
<ul class="col-md-12 nav nav-pills nav-justified" role="pilllist">
	<li class="active" id="Skillspill">
		<a href="#Skills" data-toggle="pill">Skills</a>
	</li>
	@if(isset($DATA['skillQueue']) && $DATA['skillQueue']->count() > 1)
	<li id="SkillQueuepill">
		<a href="#SkillQueue" data-toggle="pill">Skill Queue</a>
	</li>
	@endif
	<li id="EmploymentHistorypill">
	  	<a href="#EmploymentHistory" data-toggle="pill">Employment History</a>
	</li>
	@if(isset($DATA['medals']) && count($DATA['medals']) > 0)
	<li id="Decorationspill" class="hidden-xs">
		<a href="#Decorations" data-toggle="pill">Decorations</a>
	</li>
	@endif
	@if(isset($DATA['lp']) && count($DATA['lp']) > 0)
	<li id="LoyaltyPointspill" class="hidden-xs">
		<a href="#LoyaltyPoints" data-toggle="pill">Loyalty Points</a>
	</li>
	@endif
	@if(isset($DATA['clones']) && count($DATA['clones']) > 0)
	<li id="JumpClonesspill">
		<a href="#JumpClones" data-toggle="pill">Jump Clones ({{count($DATA['clones'])}})</a>
	</li>
	@endif
	@if(isset($DATA['standings']) && count($DATA['standings']) > 0)
	<li id="Standingspill">
		<a href="#Standings" data-toggle="pill">Standings</a>
	</li>
	@endif
</ul>
<div style="clear:both;">&nbsp;</div>
<div class="tab-content">
	<div class="tab-pane fade in active" id="Skills">
		<div class="row">
			<div class="btn-group pull-right" data-toggle="buttons">
			  <label class="btn btn-xs btn-primary">
			    <input type="radio" name="options" id="allskills" autocomplete="off" >All Skills
			  </label>
			  <label class="btn btn-xs btn-primary active">
			    <input type="radio" name="options" id="trainedskills" autocomplete="off" checked>Injected Skills
			  </label>
			</div>
		</div>
		<div class="row">
			<?php
				$lastGroupName = '';
				$requiredSP[0] = 0;
				$requiredSP[1] = 250;
				$requiredSP[2] = 1415;
				$requiredSP[3] = 8000;
				$requiredSP[4] = 45255;
				$requiredSP[5] = 256000;

				$skillCounter = 0;
				$SPCounter = 0;
				$l5SkillCounter = 0;
				$l5SPCounter = 0;
				$totalSkillCounter = 0;
				$totalSPCounter = 0;
				$l5TotalSkillCounter = 0;
				$l5TotalSPCounter = 0;

				$connectionsSkill = 0;
				$diplomacySkill = 0;
				$criminalConnectionsSkill = 0;

				if(isset($DATA['skills'])) {
					foreach($DATA['skills'] as $skillT) {
						if($skillT->groupName != $lastGroupName) {
							if($lastGroupName != '') {
								//echo '<div style="clear:both;">&nbsp;</div>';
								echo '<div class="skills-total'.($skillGroups[$lastGroupName] == 0 ? ' hidden' : '').'" '.($skillGroups[$lastGroupName] == 0 ? 'data-train="untrained"' : '').' style="clear:both;">&middot; '.$skillCounter.' '.$lastGroupName.' skills trained, for a total of '.number_format($SPCounter,0,".",",").' skillpoints.</div>';
								if($l5SkillCounter > 0)
									echo '<div class="skills-total'.($skillGroups[$lastGroupName] == 0 ? ' hidden' : '').'" '.($skillGroups[$lastGroupName] == 0 ? 'data-train="untrained"' : '').'>&middot; '.$l5SkillCounter.' skills trained to level 5, for a total of '.number_format($l5SPCounter,0,".",",").' skillpoints.</div>';
								echo '</div>';

								$totalSkillCounter += $skillCounter;
								$totalSPCounter += $SPCounter;
								$l5TotalSkillCounter += $l5SkillCounter;
								$l5TotalSPCounter += $l5SPCounter;
								$skillCounter = 0;
								$SPCounter = 0;
								$l5SkillCounter = 0;
								$l5SPCounter = 0;
							}
							echo '<h2 class="clip-text '.strtolower(str_replace(' ','-',$skillT->groupName)).'-groupicon'.($skillGroups[$skillT->groupName] == 0 ? ' hidden' : '').'" '.($skillGroups[$skillT->groupName] == 0 ? 'data-train="untrained"' : '').'>'.$skillT->groupName.'</h2>';
							echo '<div class="row">';
							$lastGroupName = $skillT->groupName;
						}

						$skill = null;

						if($skillT->skillPoints !== null) {
							// This is an injected skill

							// determine social skills that modify standings
							if($skillT->typeID == 3359)
								$connectionsSkill = $skillT->level;

							if($skillT->typeID == 3357)
								$diplomacySkill = $skillT->level;

							if($skillT->typeID == 3361)
								$criminalConnectionsSkill = $skillT->level;

							$skill = $skillT;
							$SPCounter += $skill->skillPoints;
							$skillCounter++;

							// calcs	
							if($skill->level == 5) {
								$l5SkillCounter++;
								$l5SPCounter += $skill->skillPoints;
							}

							$maxSP = $requiredSP[5] * $skill->rank;

							//check if training/queued - get train duration while we're here
							$trainClass = '';
							$queueLevel = 0;
							if($DATA['skillQueue']->count() > 0)
								$baseTrain = strtotime($DATA['skillQueue'][0]->startTime);
							else
								$baseTrain = 0;

							$totalTrain = 0;
							if($DATA['skillQueue']->count() > 0) {
								foreach($DATA['skillQueue'] as $queue) {
									$totalTrain += (strtotime($queue->endTime)-strtotime($queue->startTime));
									if($queue->typeName == $skill->typeName) {
										if($DATA['skillQueue'][0]->typeName == $skill->typeName) {
											$trainClass = "train-training";
											break;
										} else {
											$trainClass = "train-queued";
											$queueLevel = $queue->level;
										}
									}
								}
							}

							// calculate which SkillBook Icon
							if($skill->level == 5) {
								$skillBook = 'fullytrained-trainskill';
							} elseif($skill->skillPoints == ($requiredSP[$skill->level] * $skill->rank) || $skill->skillPoints == ($requiredSP[$skill->level] * $skill->rank)-1) { // fix for rank 7 level 4
								// determin if trained to a level or partially trained
								$skillBook = 'leveltrained-trainskill';
							} else {
								$skillBook = 'levelpartiallytrained-trainskill';
							}
							?>
							<div class="row dotted-border {{$trainClass}} {{($skill->level == 5 && $trainClass == '' ? 'level5' : '')}}">
								<div class="col-xs-9 clip-text">
									<div class="{{$skillBook}}"></div>
									{{$skill->typeName}}
									<span class="hidden-xs"> / Rank {{$skill->rank}} / Level: {{$skill->level}} / SP: {{number_format($skill->skillPoints,0,".",",")}} of {{number_format($maxSP,0,".",",")}}
									@if($trainClass == "train-training")
										<span class="text-italic hidden-xs"> - training to Level {{{$skill->level+1}}}</span>
									@endif
									</span>
								</div>
								<div class="col-xs-3">
									@if($trainClass == '')
										<div class="level{{$skill->level}}-trainicon"></div>
									@elseif($trainClass == 'train-training')
										<div class="level{{($skill->level+1)}}-act-trainicon"></div>
									@elseif($trainClass == 'train-queued')
										<div class="level{{{$skill->level}}}-train-{{$queueLevel}}-queued-trainicon"></div>
									@endif
								</div>
							</div>
							<?php
						} else {
							// This is a skill not injected
							$skillBook = 'donothave-trainskill';
							$canTrain = true;

/* This works locally, but not on pl.com. $testSkill is empty. So have done a standard foreach as below. Reasons are unknown, but some laravel collections have a problem with the pl setup
							$reqSkills = $DATA['requiredSkills']->where('typeID', $skillT->typeID);
							if($reqSkills->count() > 0) {
								foreach($reqSkills as $req) {
									$testSkill = $DATA['skills']->where('typeID', $req->requiredSkillID);
									if($testSkill->count() > 0) {
										foreach($testSkill as $testS) {
											if($req->skillLevel > $testS->level  )
												$canTrain = false;
										}
									}
								}
							} else {
								$canTrain = false;
							}
*/
							$reqSkills = array();

							if($DATA['requiredSkills']->count() > 0) {
								foreach($DATA['requiredSkills'] as $reqS) {
									if($reqS->typeID == $skillT->typeID)
										$reqSkills[] = $reqS;
								}

								if(count($reqSkills > 0)) {
									foreach($reqSkills as $req) {
										$testSkill = array();
										foreach($DATA['skills'] as $skillS) 
											if($skillS->typeID == $req->requiredSkillID)
												$testSkill[] = $skillS;
										
										if(count($testSkill > 0)) 
											foreach($testSkill as $testS) 
												if($req->skillLevel > $testS->level)
													$canTrain = false;
									}
								}
							} else {
								$canTrain = false;
							}

							if($canTrain === true)
								$trainClass = 'can-train';
							else
								$trainClass = 'cannot-train';

							?>
							<div class="row dotted-border untrained hidden" data-train="untrained">
								<div class="col-xs-9 clip-text">
									<div class="{{$skillBook}}"></div>{{$skillT->typeName}}<span class="hidden-xs"> / Rank {{$skillT->rank}} </span>
								</div>
								<div class="col-xs-3">
									<div class="{{$trainClass}}"></div>
								</div>
							</div>
							<?php
							
						}
					}
				}

				echo '<div class="skills-total" style="clear:both;">&middot; '.$skillCounter.' '.$lastGroupName.' skills trained, for a total of '.number_format($SPCounter,0,".",",").' skillpoints.</div>';
				if($l5SkillCounter > 0)
					echo '<div class="skills-total">&middot; '.$l5SkillCounter.' skills trained to level 5, for a total of '.number_format($l5SPCounter,0,".",",").' skillpoints.</div>';
				echo '</div>';
				$totalSkillCounter += $skillCounter;
				$totalSPCounter += $SPCounter;
			?>
			<div style="clear:both;">&nbsp;</div>
			<div class="total-skills-total">&middot; {{$totalSkillCounter}} skills trained, for a total of {{number_format($totalSPCounter,0,".",",")}} skillpoints.</div>
		</div>
	</div>
	<div class="tab-pane fade" id="SkillQueue">
		@if($DATA['skillQueue']->count() > 0)
		<div class="row">
			<div>Completes on {{str_replace(' ',' at ',$DATA['skillQueue'][(count($DATA['skillQueue'])-1)]->endTime)}}<span class="pull-right hidden-xs">{{rel_time(($totalTrain+$baseTrain), gmdate("Y-m-d H:i:s"), true)}}</span></div>
			<table id="settingstable" class="table table-striped table-condensed">
				<thead>
        			<tr>
		        		<th class="hidden-xs">#</th>
		        		<th>Skill</th>
		        		<th>Start</th>
		        		<th>End</th>
		        		<th class="hidden-xs">Duration</th>
		        	</tr>
		        </thead>
		        <tbody>
		        <?php $counter = 0; ?>
				@foreach($DATA['skillQueue'] AS $skillRow)
					@if($counter != 0)
					<tr>
						<td class="hidden-xs">{{$counter}}</td>
						<td>{{$skillRow->typeName}} level: {{$skillRow->level}}</td>
						<td>{{$skillRow->startTime}}</td>
						<td>{{$skillRow->endTime}}</td>
						<td class="hidden-xs">{{rel_time($skillRow->endTime, $skillRow->startTime, false)}}</td>
					</tr>
					@endif
					<?php $counter++; ?>
				@endforeach
				</tbody>
			</table>
		</div>
		@endif
	</div>
	<div class="tab-pane fade" id="EmploymentHistory">
		<div class="row">
			<table id="settingstable" class="table table-striped table-condensed">
				<thead>
        			<tr>
		        		<th>Corporation</th>
		        		<th>Joined</th>
		        		<th>Left</th>
		        	</tr>
		        </thead>
		        <tbody>
		        <?php $counter = -1; ?>
				@foreach($DATA['corpHistory'] AS $corpRow)
					<tr>
						<td><img class="hidden-xs" src="https://image.eveonline.com/Corporation/{{$corpRow->corporationID}}_64.png" alt="{{$corpRow->corporationName}} Logo" title="{{$corpRow->corporationName}}" height="32" width="32"> {{$corpRow->corporationName}}</td>
						<td>{{$corpRow->startDate}}</td>
						<td>{{($counter >= 0 ? $DATA['corpHistory'][$counter]->startDate : '-')}}</td>
					</tr>
					<?php $counter++; ?>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="tab-pane fade hidden-xs" id="Decorations">
		<div class="row">
			<table id="settingstable" class="table table-striped table-condensed">
				<thead>
        			<tr>
		        		<th>Title</th>
		        		<th>Reason</th>
		        		<th>Desription</th>
		        		<th>Issued</th>
		        		<th>Corporation</th>
		        	</tr>
		        </thead>
		        <tbody>
				@foreach($DATA['medals'] AS $medal)
					<tr>
						<td>{{$medal->title}}</td>
						<td>{{$medal->reason}}</td>
						<td>{{$medal->description}}</td>
						<td><img class="hidden-xs" src="{{{config('tools.ccp-image-url')}}}Character/{{$medal->issuerID}}_64.jpg" alt="{{$medal->issuerName}} Logo" title="{{$medal->issuerName}}" height="32" width="32"> {{$medal->issuerName}} {{$medal->issued}}</td>
						<td><img class="hidden-xs" src="{{{config('tools.ccp-image-url')}}}Corporation/{{$medal->corporationID}}_64.png" alt="{{$medal->corporationName}} Logo" title="{{$medal->corporationName}}" height="32" width="32"><br />{{$medal->corporationName}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			
		</div>
	</div>
	<div class="tab-pane fade hidden-xs" id="LoyaltyPoints">
		<div class="row">
			<table id="settingstable" class="table table-striped table-condensed">
				<thead>
        			<tr>
		        		<th>Corporation</th>
		        		<th>Loyalty Points</th>
		        		<th class="lpcolumn"></th>
		        	</tr>
		        </thead>
		        <tbody>
				@foreach($DATA['lp'] AS $lp)
					<tr>
						<td><img class="hidden-xs" src="{{{config('tools.ccp-image-url')}}}/Corporation/{{$lp->corporationID}}_64.png" alt="{{$lp->corporationName}} Logo" title="{{$lp->corporationName}}" height="32" width="32"> {{$lp->corporationName}}</td>
						<td>{{number_format($lp->quantity, 0,'.',',')}}</td>
						<td>
							<a class="lplink" data-corpid="{{$lp->corporationID}}" data-lp="{{$lp->quantity}}" data-corpname="{{$lp->corporationName}}" data-toggle="modal" data-target="#Modal" href="#">
								<image class="loyaltypointstore" src="{{GetAssetPath('images/WindowIcons/lpstore.png') }}" width="32" height="32" />
							</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			
		</div>
	</div>
	<div class="tab-pane fade" id="JumpClones">
		<div class="row">
			@foreach($DATA['clones'] as $clone)
				<?php 
					$cloneImplants = $DATA['cloneImplants']->where('jumpCloneID', $clone->jumpCloneID)->toArray();
				?>
				@if(count($cloneImplants) > 0)
				<h3 class="lineheader clip-text line-button">{{(!$clone->locationName ? 'Unknown' : $clone->locationName)}} {{($clone->cloneName ? ' - '.$clone->cloneName : '')}}</h3>
			    <button type="button" class="btn btn-black btn-sm pull-right toggle" data-toggle="collapse" data-target="#{{$clone->jumpCloneID}}"><span class="glyphicon glyphicon-chevron-up"></span></button>
				<div id="{{$clone->jumpCloneID}}" class="collapse">
					<div class="row api-page-grid">
						<div class="col-sm-6">
						@for($x=1;$x<=5;$x++)
							<div class="row">
								<div class="col-xs-1">{{$x}}.</div>
								<?php
									$testImp = null;
									foreach($cloneImplants as $isSlotPlant) 
										if(intval($isSlotPlant['slotNum']) == $x)
											$testImp = $isSlotPlant;
								?>
								@if(isset($testImp))
								<div class="col-xs-11 clip-text">{{$testImp['typeName']}}</div>
								@else
								<div class="col-xs-11 text-italic">No Implant</div>
								@endif
							</div>
						@endfor
						</div>
						<div class="col-sm-6">
						@for($x=6;$x<=10;$x++)
							<div class="row">
								<div class="col-xs-1">{{$x}}.</div>
								<?php
									$testImp = null;
									foreach($cloneImplants as $isSlotPlant) 
										if(intval($isSlotPlant['slotNum']) == $x)
											$testImp = $isSlotPlant;
								?>
								@if(isset($testImp))
								<div class="col-xs-11 clip-text">{{$testImp['typeName']}}</div>
								@else
								<div class="col-xs-11 text-italic">No Implant</div>
								@endif
							</div>
						@endfor
						</div>
					</div>
					<div style="clear:both;">&nbsp;</div>
				</div>
				@else
				<h3 class="lineheader clip-text line-button">{{(!$clone->locationName ? 'Unknown' : $clone->locationName)}}</h3>
				@endif
			@endforeach
		</div>
	</div>
	<div class="tab-pane fade hidden-xs" id="Standings">
		@if(isset($DATA['standings']['factions']))
		<div class="row">
			<h2 class="clip-text header-underline">Factions</h2>
			<table id="agentstable" class="table table-striped table-condensed">
				<thead>
        			<tr>
		        		<th>Faction</th>
		        		<th>Standing</th>
		        		<th class="afcolumn"></th>
		        	</tr>
		        </thead>
		        <tbody>
				@foreach($DATA['standings']['factions'] AS $standing)
					<tr>
						<td><img class="hidden-xs" src="{{{config('tools.ccp-image-url')}}}/Alliance/{{$standing->fromID}}_64.png" alt="{{$standing->fromName}} Logo" title="{{$standing->fromName}}" height="16" width="16"> {{$standing->fromName}}</td>
						<td>{{$standing->adjStanding}}
							@if($standing->adjStanding != $standing->standing)
							 ({{$standing->adjSkill}} {{$standing->adjSkillLevel}} raises your effective standing from {{$standing->standing}})
							@endif
						</td>
						<td>
							<a class="aflink" data-facid="{{$standing->fromID}}" data-facname="{{$standing->fromName}}" data-toggle="modal" data-target="#Modal" href="#">
								<image class="agentfinder" src="{{GetAssetPath('images/WindowIcons/agentfinder.png') }}" width="16" height="16" />
							</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		@endif
		@if(isset($DATA['standings']['NPCCorporations']))
		<div class="row">
			<h2 class="clip-text header-underline">NPC Corporations</h2>
			<table id="agentstable" class="table table-striped table-condensed">
				<thead>
        			<tr>
		        		<th>Corporation</th>
		        		<th>Faction</th>
		        		<th>Standing</th>
		        		<th class="afcolumn"></th>
		        	</tr>
		        </thead>
		        <tbody>
				@foreach($DATA['standings']['NPCCorporations'] AS $standing)
					<tr>
						<td><img class="hidden-xs" src="{{{config('tools.ccp-image-url')}}}/Corporation/{{$standing->fromID}}_64.png" alt="{{$standing->fromName}} Logo" title="{{$standing->fromName}}" height="16" width="16"> {{$standing->fromName}}</td>
						<td><img class="hidden-xs" src="{{{config('tools.ccp-image-url')}}}/Alliance/{{$standing->factionID}}_64.png" alt="{{$standing->factionName}} Logo" title="{{$standing->factionName}}" height="16" width="16"> {{$standing->factionName}}</td>
						<td>{{$standing->adjStanding}}
							@if($standing->adjStanding != $standing->standing)
							 ({{$standing->adjSkill}} {{$standing->adjSkillLevel}} raises your effective standing from {{$standing->standing}})
							@endif
						</td>
						<td>
							<a class="aflink" data-corpid="{{$standing->fromID}}" data-corpname="{{$standing->fromName}}" data-toggle="modal" data-target="#Modal" href="#">
								<image class="agentfinder" src="{{GetAssetPath('images/WindowIcons/agentfinder.png') }}" width="16" height="16" />
							</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		@endif
		@if(isset($DATA['standings']['agents']))
		<div class="row">
			<h2 class="clip-text header-underline">Agents</h2>
			<table id="agentstable" class="table table-striped table-condensed">
				<thead>
        			<tr>
		        		<th>Name</th>
		        		<th>Corporation</th>
		        		<th>Faction</th>
		        		<th>Type</th>
		        		<th>Division</th>
		        		<th class="text-center">Level</th>
		        		<th>Location</th>
		        		<th class="text-center">Security</th>
		        		<th class="text-center">Can Locate</th>
		        		<th>Standing</th>
		        	</tr>
		        </thead>
		        <tbody>
				@foreach($DATA['standings']['agents'] AS $standing)
					<tr>
						<td>{{$standing->fromName}}</td>
						<td>{{$standing->itemName}}</td>
						<td>{{$standing->factionName}}</td>
						<td>{{str_replace("GenericStoryline","Generic Storyline",str_replace("Mission","",str_replace("Agent","",$standing->agentType)))}}</td>
						<td>{{$standing->divisionName}}</td>
						<td class="text-center">{{$standing->level}}</td>
						<td data-apsolarsystemid="{{$standing->systemID}}" data-apsolarsystemname="{{$standing->systemName}}" data-aplocationid="{{$standing->locationID}}" data-aplocationname="{{$standing->locationName}}">{{$standing->locationName}}</td>
						<td class="text-center"><span class="{{ getSystemCSS($standing->security) }}">{{number_format($standing->security,1)}}</span></td>
						<td class="text-center">
							@if($standing->isLocator == 1)
							Yes
							@else
							No
							@endif
						</td>
						<td>{{$standing->adjStanding}}
							@if($standing->adjStanding != $standing->standing)
							 ({{$standing->adjSkill}} {{$standing->adjSkillLevel}} raises your effective standing from {{$standing->standing}})
							@endif
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		@endif
	</div>
</div>

<!-- Loyalty Store Modal -->
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Loyalty Point Store" aria-hidden="true">
    <div class="modal-dialog modal-lpstore">
        <div id="modalcontent" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 id="modaltitle" class="modal-title">Loyalty Store</h4>
            </div>
            <div class="modal-body">
            	Loading...
	        </div>
	        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
	    </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="preparing-file-modal" title="Download Excel Sheet" style="display: none;">
    Producing the Excel file, please wait...
 
    <!--Throw what you'd like for a progress indicator below-->
    <div class="ui-progressbar-value ui-corner-left ui-corner-right" style="width: 100%; height:22px; margin-top: 20px;"></div>
</div>
 
<div id="error-modal" title="Error" style="display: none;">
    There was a problem generating the Excel document, you can try again if you like but I bet it still won't work. #AGMARMONEYTEAM
</div>
@section('additional_js')
<script>
var table;
var lp;
var maxLP = 999999999;
var modaltype = 'lpstore';
var offset;

$.extend( $.fn.dataTableExt.oStdClasses, {
    "sFilterInput": "clearable"
});

$(function () {

	$("#allskills, #trainedskills").change(function () {
	  	if ($("#allskills").is(":checked")) {
	    	$('[data-train]').each(function() {
				$( this ).removeClass( "hidden" );
			});
	    } else {
	    	$('[data-train]').each(function() {
				$( this ).addClass( "hidden" );
			});
	    }
	});

	//$("#allItems, #affordableItems").change(function () {
	$('#Modal .modal-body').on('change', '#allItems, #affordableItems', function() {
	  	if ($("#allItems").is(":checked")) {
	    	// no filter
	    	maxLP = 999999999;
	    } else {
	    	//filter by affordability
	    	maxLP = lp;
	    }
	    table.draw();
	});

	$('#Modal .modal-body').on('change', '#allAgents, #availableAgents', function() {
		/*
	  	if ($("#allAgents").is(":checked")) {
	    	// no filter
	    	$.fn.dataTable.ext.search = [];
	    } else {
	    	//filter by affordability
	    	$.fn.dataTable.ext.search = [];
	    	$.fn.dataTable.ext.search.push(
			    function( settings, data, dataIndex ) {
			    	console.log(data);
			        
			        if(Number(data[11+offset]) < Number(data[23+offset]) && Number(data[16+offset]) < Number(data[23+offset]) && Number(data[20+offset]) < Number(data[23+offset])) {
		        		return false;
		        	}
		        	return true;
			    }
			);
	    }
	    */
	    table.draw();
	});

	$('#Standings').on('click', 'a.aflink', function () {
        var btn = $(this);
        offset = 0;

        modaltype = 'agentfinder';

    	if(typeof btn.data('facid') === 'undefined') {
        	
	        $('#Modal .modal-body').html('<div>'+
				'<div>'+
					'<img id="modalcorpimage" class="hidden-xs" src="" alt="" title="" height="64" width="64">'+
					'<h1 id="modalcorpname" class="inline"></h1>'+
				'</div>'+
				'<div class="row agentfinderbuttons">'+
					'<div class="btn-group pull-right" data-toggle="buttons">'+
					  '<label class="btn btn-xs btn-primary active">'+
					    '<input type="radio" name="options" id="allAgents" autocomplete="off" checked>All'+
					  '</label>'+
					  '<label class="btn btn-xs btn-primary">'+
					    '<input type="radio" name="options" id="availableAgents" autocomplete="off">Available'+
					  '</label>'+
					'</div>'+
				'</div>'+
				'<table id="aftable" class="table table-striped table-condensed-padding dataTable no-footer" cellspacing="0" width="100%">'+
			        '<thead>'+
			            '<tr>'+
			            	'<th>Name</th>'+
			                '<th>Type</th>'+
				        	'<th>Division</th>'+
				        	'<th>Level</th>'+
				        	'<th>Location</th>'+
				        	'<th>Security</th>'+
				        	'<th>Standing</th>'+
				        	'<th></th>'+
				        	'<th></th>'+
			            '</tr>'+
			        '</thead>'+
			    '</table>'+
			'</div>');

	        $('#modalcorpname').html(btn.data('corpname'));
			$('#modalcorpimage').attr('src', '{{config("tools.ccp-image-url")}}'+'Corporation/'+btn.data('corpid')+'_64.png');
	        $('#modalcorpimage').attr('title', btn.data('corpname'));
	        $('#modalcorpimage').attr('alt', btn.data('corpname'));
	        $('#modaltitle').html('Agent Finder');
	    } else {
	    	offset = 1;
	        $('#Modal .modal-body').html('<div>'+
				'<div>'+
					'<img id="modalcorpimage" class="hidden-xs" src="" alt="" title="" height="64" width="64">'+
					'<h1 id="modalcorpname" class="inline"></h1>'+
				'</div>'+
				'<div class="row agentfinderbuttons">'+
					'<div class="btn-group pull-right" data-toggle="buttons">'+
					  '<label class="btn btn-xs btn-primary active">'+
					    '<input type="radio" name="options" id="allAgents" autocomplete="off" checked>All'+
					  '</label>'+
					  '<label class="btn btn-xs btn-primary">'+
					    '<input type="radio" name="options" id="availableAgents" autocomplete="off">Available'+
					  '</label>'+
					'</div>'+
				'</div>'+
				'<table id="aftable" class="table table-striped table-condensed-padding dataTable no-footer" cellspacing="0" width="100%">'+
			        '<thead>'+
			            '<tr>'+
			            	'<th>Name</th>'+
			            	'<th>Corporation</th>'+
			                '<th>Type</th>'+
				        	'<th>Division</th>'+
				        	'<th>Level</th>'+
				        	'<th>Location</th>'+
				        	'<th>Security</th>'+
				        	'<th>Standing</th>'+
				        	'<th></th>'+
				        	'<th></th>'+
			            '</tr>'+
			        '</thead>'+
			    '</table>'+
			'</div>');

			$('#modalcorpimage').attr('src', '{{config("tools.ccp-image-url")}}'+'Alliance/'+btn.data('facid')+'_64.png');
	        $('#modalcorpimage').attr('title', btn.data('facname'));
	        $('#modalcorpimage').attr('alt', btn.data('facname'));
	        $('#modalcorpname').html(btn.data('facname'));
	        $('#modaltitle').html('Agent Finder');
	    }

        //$('#Modal .modal-body').html('Loading...');
        var systemCSS;

        $.fn.dataTable.ext.search = [];
/*		$.fn.dataTable.ext.search.push(
		    function( settings, data, dataIndex ) {
		        
		        // Agent
		        var agentBase = row[6+offset];
		        var agentAdj = row[11+offset];

		        var highestBase = agentBase;
		        var highestAdj = agentAdj;

				// Corp	
				var corpBase = row[19+offset];
		        var corpAdj = row[20+offset];

		        if(corpAdj > highestAdj) {
		        	highestBase = corpBase;
		        	highestAdj = corpAdj;
		        }

				// Faction
				var facBase = row[15+offset];
		        var facAdj = row[16+offset];

				if(facAdj > highestAdj) {
		        	highestBase = facBase;
		        	highestAdj = facAdj;
		        }


		        if ( num <= maxLP  )
		        {
		            return true;
		        }
		        return false;
		    }
		);
*/
        table = $('#aftable').DataTable( {
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	            "url": BASEURL+'/agentfinder',
	            "type": "POST",
	            "data": function ( d ) {
	                d.corpID = btn.data('corpid');
	                d.facID = btn.data('facid');
	                d.corpName = btn.data('corpname');
	                d.facName = btn.data('facname');
	                d.charID = {{$DATA['character']->characterID}};
	                d.filterAgents = $("#availableAgents").is(':checked');
	            },
	            "beforeSend": function (request) {
			        request.setRequestHeader( "X-CSRF-TOKEN",  $('meta[name="csrf-token"]').attr('content'));
			    }
	        },
	        "stateSave": false,
	        "deferRender": true,
	        "info": true,
	        "ordering": true,
	        "paging": true,
	        "searching": true,
	        "autoWidth": true,
	        "pageLength": 25,
	        "order": [[ 3+offset, 'desc' ]],
	        "createdRow": function ( row, data, index ) {
	        	if(Number(data[11+offset]) < Number(data[23+offset]) && Number(data[16+offset]) < Number(data[23+offset]) && Number(data[20+offset]) < Number(data[23+offset])) {
	        		$(row).addClass('masked');
	        	}
	        },
	        "drawCallback": function( settings ) {
	        	setWaypoints();
		        //var api = this.api();
		 
		        // Output the data for the visible rows to the browser's console
		        //console.log( api.rows( {page:'current'} ).data() );
		    },
	        "columnDefs": [
	        	{
	        		"createdCell": function (td, cellData, rowData, row, col) {
						$(td).attr("data-apsolarsystemid",rowData[26+offset]).attr("data-apsolarsystemname",rowData[10+offset]).attr("data-aplocationid",rowData[25+offset]).attr("data-aplocationname",rowData[4+offset]);
						
					},
					"searchable": false,
	                "orderable": true,
	                //"data": {
					//    "_": "",
					//    "filter": "systemName",
					//    "display": 4+offset
					//},
					//"data": function ( row, type, val, meta ) {
						//console.log(row);
						//console.log(type);
						//console.log(val);
						//console.log(meta);
				      
				      // 'sort', 'type' and undefined all just use the integer
				    //  return row[4+offset];
				    //},
				    "data": 4+offset,
					"targets": 4+offset 
				},
				{
					"render": function ( data, type, row ) {
						systemCSS = getSystemCSS(Number(row[5+offset]).formatNumber(1)); 
	                	return Number(row[5+offset]).formatNumber(1);
	                },
	                "createdCell": function (td, cellData, rowData, row, col) {
						$(td).addClass(getSystemCSS(Number(cellData).formatNumber(1)))
					},
					"searchable": false,
	                "orderable": true,
	                "data": 5+offset,
					"targets": 5+offset 
				},
				{
					"render": function ( data, type, row ) {
						var highestBase = -20;
						var highestAdj = -20;

						// Agent
						if(Number(row[9+offset]) > Number(highestBase) && row[9+offset] !== null) {
							highestAdj = row[11+offset];
							highestBase = row[9+offset];
						}

						// Corp		
						if(Number(row[19+offset]) > Number(highestBase) && row[19+offset] !== null) {
							highestAdj = row[20+offset];
							highestBase = row[19+offset];
						}

						// Faction
						if(Number(row[15+offset]) > Number(highestBase) && row[15+offset] !== null) {
							highestAdj = row[16+offset];
							highestBase = row[15+offset];
						}

						if(highestAdj == -20) {
							highestAdj = 0.00;
							highestBase = 0.00;
						}

	                	return highestAdj+' ('+highestBase+')';
	                },
					"searchable": false,
	                "orderable": true,
	                //"orderData": [ 11, 20, 16 ],
	                "data": 26+offset,
					"targets": 6+offset 
				},
				{
					"searchable": true,
	                "orderable": false,
		            "visible": false,
				    "data": 7+offset,
					"targets": 7+offset 
				},
				{
					"searchable": true,
	                "orderable": false,
		            "visible": false,
				    "data": 8+offset,
					"targets": 8+offset 
				},
		  	],
	    });

        $('div.dataTables_filter input').attr('placeholder', 'Search');
	   // $('#Modal').modal('show');
	});

	$('#LoyaltyPoints').on('click', 'a.lplink', function () {
        var btn = $(this);
        lp = btn.data('lp');

        modaltype = 'lpstore';

        $.fn.dataTable.ext.search = [];
		$.fn.dataTable.ext.search.push(
		    function( settings, data, dataIndex ) {
		        var num = parseFloat( data[2].replace(/\,/g,'') ) || 0; 

		        if ( num <= maxLP  )
		        {
		            return true;
		        }
		        return false;
		    }
		);

        $('#Modal .modal-body').html('Loading...');

        var jqxhr = $.ajax({
			headers: {
				            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				        },
			async: true,
			type: 'POST',
			url: BASEURL+'/loyaltypointstore',
			data: { corpID: btn.data('corpid'), lp: btn.data('lp'), corpName: btn.data('corpname') },
			dataType: "json"
		}).done(function(msg) {
			if(msg != "false") {
                $('#Modal .modal-body').html('<div>'+
					'<div>'+
						'<img id="modalcorpimage" class="hidden-xs" src="" alt="" title="" height="64" width="64">'+
						'<h1 id="modalcorpname" class="inline"></h1>'+
					'</div>'+
					'<div class="row lpsearchbuttons">'+
						'<div class="btn-group pull-right" data-toggle="buttons">'+
						  '<label class="btn btn-xs btn-primary active">'+
						    '<input type="radio" name="options" id="allItems" autocomplete="off" checked>All'+
						  '</label>'+
						  '<label class="btn btn-xs btn-primary">'+
						    '<input type="radio" name="options" id="affordableItems" autocomplete="off">Affordable'+
						  '</label>'+
						'</div>'+
					'</div>'+
					'<table id="lptable" class="table table-striped table-condensed-padding dataTable no-footer" cellspacing="0" width="100%">'+
				        '<thead>'+
				            '<tr>'+
				            	'<th class="imgcolumn"></th>'+
				                '<th>Reward</th>'+
					        	'<th>LP Cost</th>'+
					        	'<th>ISK Cost</th>'+
					        	'<th>Required Items</th>'+
					        	'<th>Category</th>'+
				            '</tr>'+
				        '</thead>'+
				    '</table>'+
            	'</div>');

                table = $('#lptable').DataTable( {
                	"destroy": true,
					"autoWidth": false,
					"data": msg['data'],
					"columnDefs": [
						{ 
							"render": function ( data, type, row ) {
			                	return '<img class="hidden-xs" src="{{config('tools.ccp-image-url')}}Type/'+row.typeID+'_64.png" alt="'+row.typeName+' Logo" title="'+row.typeName+'" height="32" width="32">';
			                },
							"searchable": false,
							"className": 'imgcolumn',
			                "orderable": false,
			                "data": null,
							"targets": 0 
						},
						{
							"render": function ( data, type, row ) {
			                	return row.itemQuantity+' x '+row.typeName;
			                },
							"searchable": true,
			                "orderable": true,
			                "data": "typeName",
							"targets": 1 
						},
						{
							"render": function ( data, type, row ) {
			                	return Number(row.lpCost).formatNumber(0);
			                },
							"searchable": true,
			                "orderable": true,
			                "data": "lpCost",
							"targets": 2 
						},
						{
							"render": function ( data, type, row ) {
			                	return Number(row.iskCost).formatNumber(0);
			                },
							"searchable": true,
			                "orderable": true,
			                "data": "iskCost",
							"targets": 3 
						},
						{
							"render": function ( data, type, row ) {
								if(row.reqItems === null) {
									return '<span class="text-italic">No Items Required</span>';
			                	} else {
			                		var reqItems = '';
			                		var reqs = JSON.parse(row.reqItems);

									for(var i=0;i<reqs.length;i++){
										var obj = reqs[i];
										for(var key in obj){
											if(key == 'reqQuantity')
												var reqQuantity = obj[key];

											if(key == 'reqTypeName')
												var reqTypeName = obj[key];

											if(key == 'reqTypeID')
												var reqTypeID = obj[key];
										}
										if(typeof key !== undefined) {
											reqItems += '<span class="block">'+reqQuantity+' x '+reqTypeName+'</span>';
										}
    								}

			                		return reqItems;
			                	}
			                },
							"searchable": true,
			                "orderable": false,
			                "data": "reqItems",
							"targets": 4 
						},
						{
							"visible": false,
							"searchable": true,
			                "orderable": false,
			                "data": "categoryName",
							"targets": 5 
						}
				  	],
				  	"order": [[ 2, 'asc' ]],
				} );

		        maxLP = 999999999;

		        $("#allItems").prop("checked", true);
		        $("#affordableItems").prop("checked", false);
		        $("#allItems").parent().addClass("active");
		        $("#affordableItems").parent().removeClass("active");

				$('div.dataTables_filter input').attr('placeholder', 'Search');

				$('#Modal').modal('show');

				$('#modaltitle').html('Loyalty Store');
				$('#modalcorpimage').attr('src', '{{config("tools.ccp-image-url")}}'+'Corporation/'+btn.data('corpid')+'_64.png');
		        $('#modalcorpimage').attr('title', btn.data('corpname'));
		        $('#modalcorpimage').attr('alt', btn.data('corpname'));
		        $('#modalcorpname').html(btn.data('corpname'));

			}	
		}).fail(function() {
	    	addAlert('danger', 'Unspecified Error retrieving LP store', true);
		}).always(function() {
			//console.log( "complete" );
		});
        return false;
    });

    $('#Modal').on('show.bs.modal', function (e) {
    	//$('#modalcorpimage').attr('src', '');
    	//$('#modalcorpimage').attr('title', '');
    	//$('#modalcorpimage').attr('alt', '');
       // $('#modalcorpname').html('');

		//$('#Modal .modal-body').html('Loading...');
	    //$('#Modal').removeData('bs.modal');
	})
});
</script>
@stop