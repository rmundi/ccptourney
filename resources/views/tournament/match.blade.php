<?php
	define('PRESCISION_CHECK', 10);

	$REDTEAM = (json_decode($REDTEAM));
	$BLUETEAM = (json_decode($BLUETEAM));

	//dd($REDTEAM);
?>

@extends('layouts.main')

@section('load_additional_js')
	<script src="{{ GetURLPath('') }}/js/tournament.min.js?v={{{config('tools.AppVer')}}}"></script>
@stop

@section('page_title')Pandemic Legion - {{$REDTEAM->name}} vs. {{$BLUETEAM->name}} @stop

@section('body_class')body_class @stop

@section('content')

<div class="page">
	<div id="contents">
		<h1 class="pagination-centered">{{$REDTEAM->name}} vs. {{$BLUETEAM->name}}</h1>
		<div class="tournament-timer">
			<span id="time-display">0:00 (x1)</span> 
			<button id="control-button" class="btn btn-primary btn-xs">Stop</button> 
			<button id="reset-button" class="btn btn-danger btn-xs">Reset</button>
		</div>
		<div class="row">
			<div class="col-md-6 tournament-row">
				<div class="row">
					<div class="pagination-centered">
						<a class="pen_secondary1" href="{{$REDTEAM->teamURL}}">{{$REDTEAM->name}}</a>
						@if($REDTEAM->score !== false)
							 ({{$REDTEAM->score}})
						@endif
					</div>
				</div>
				@if(isset($REDTEAM->ships))
					<div class="tourament-stats-box">
						<div id="redteam-fleet-attributes-defense-bar" class="tournament-fleet-attributes-bar">
							<div id="redteam-fleet-attributes-defense-fill" class="tournament-fleet-attributes-fill">
							</div>
						</div>
						<div id="redteam-fleet-attributes-attack-bar" class="tournament-fleet-attributes-bar">
							<div id="redteam-fleet-attributes-attack-fill" class="tournament-fleet-attributes-fill">
							</div>
						</div>
						<div id="redteam-fleet-attributes-control-bar" class="tournament-fleet-attributes-bar">
							<div id="redteam-fleet-attributes-control-fill" class="tournament-fleet-attributes-fill">
							</div>
						</div>
					</div>
					<?php $even = true; ?>
					@foreach($REDTEAM->ships as $item)
						<?php 
							if($even == false)
								$even = true;
							else
								$even = false; 
						?>
						<div class="row <?php echo ($even == true ? 'tournament-even-row' : '');?>" id="{{$item->id}}">
							<div class="inline-block">
								@if(($item->type == 'Tengu' || $item->type == 'Loki' || $item->type == 'Proteus' || $item->type == 'Legion') && $item->subs == 5)
									<div class="module-image-container"><image class="img kill-killer-alliance block" src="{{$item->icon}}" title="{{$item->type}}&#013;{{$item->subSystems[0]->name}}&#013;{{$item->subSystems[1]->name}}
									&#013;{{$item->subSystems[2]->name}}&#013;{{$item->subSystems[3]->name}}&#013;{{$item->subSystems[4]->name}}&#013;{{$item->hiSlots}}/{{$item->medSlots}}/{{$item->loSlots}}/{{$item->rigs}}/{{$item->subs}}" /></div>
								@else
									<div class="module-image-container"><image class="img kill-killer-alliance block" src="{{$item->icon}}" title="{{$item->type}}&#013;{{$item->hiSlots}}/{{$item->medSlots}}/{{$item->loSlots}}/{{$item->rigs}}/{{$item->subs}}" /></div>
								@endif
								<div class="module-image-container"><image class="img kill-killer-alliance block" src="{{$item->pilotIcon}}" title="{{$item->pilotName}}" id="{{$item->id}}_pilot" /></div>
							</div>
							<div class="inline-block tournament-ship-health">
								<div id="{{$item->id}}_hull" class="inline-block tournament-ship-bar tournament-ship-hull" title="Hull">
								</div>
								<div id="{{$item->id}}_armour" class="inline-block tournament-ship-bar tournament-ship-armour" title="Armour">
								</div>
								<div id="{{$item->id}}_shield" class="inline-block tournament-ship-bar tournament-ship-shield" title="Shield">
								</div>
							</div>
							<div class="inline-block">
								<div class="block">
									{{$item->pilotName}}
								</div>
								<div class="block">
									{{$item->type}}
								</div>
								<div id="{{$item->id}}_speed"class="block">
									0 m/s
								</div>
							</div>
							<div class="inline">
								<div class="slot-layout" style="width: {{ceil($item->hiSlots/2)*35}}px;" id="{{$item->id}}_highs">
									<?php $x = 0; ?>
									@for($X = 0;$x < $item->hiSlots;$x++)
										@if(isset($item->hiSlot[$x]))
											<div class="module-image-container"><image class="slot-icon" id="{{$item->hiSlot[$x]->id}}" src="https://image.eveonline.com/Type/{{$item->hiSlot[$x]->typeID}}_32.png" title="{{$item->hiSlot[$x]->name}}" /></div>
										@else
											<div class="module-image-container"><image class="slot-icon" src="{{asset('images/Icons/items/8_64_11.png')}}" /></div>
										@endif
									@endfor
								</div>
								<div class="slot-layout" style="width: {{ceil($item->medSlots/2)*35}}px;" id="{{$item->id}}_mids">
									<?php $x = 0; ?>
									@for($X = 0;$x < $item->medSlots;$x++)
										@if(isset($item->medSlot[$x]))
											<div class="module-image-container"><image class="slot-icon" id="{{$item->hiSlot[$x]->id}}" src="https://image.eveonline.com/Type/{{$item->medSlot[$x]->typeID}}_32.png" title="{{$item->medSlot[$x]->name}}" /></div>
										@else
											<div class="module-image-container"><image class="slot-icon" src="{{asset('images/Icons/items/8_64_10.png')}}" /></div>
										@endif
									@endfor
								</div>
								<div class="slot-layout" style="width: {{ceil($item->loSlots/2)*35}}px;" id="{{$item->id}}_lows">
									<?php $x = 0; ?>
									@for($X = 0;$x < $item->loSlots;$x++)
										@if(isset($item->loSlot[$x]))
											<div class="module-image-container"><image class="slot-icon" id="{{$item->hiSlot[$x]->id}}" src="https://image.eveonline.com/Type/{{$item->loSlot[$x]->typeID}}_32.png" title="{{$item->loSlot[$x]->name}}" /></div>
										@else
											<div class="module-image-container"><image class="slot-icon" src="{{asset('images/Icons/items/8_64_9.png')}}" /></div>
										@endif
									@endfor
								</div>
								<div class="slot-layout" id="{{$item->id}}_unknowns">
							</div>
							</div>
						</div>
					@endforeach
				@endif
			</div>
    		<div class="col-md-6 tournament-row">
    			<div class="row">
    				<div class="pagination-centered">
						<a class="pen_primary1" href="{{$BLUETEAM->teamURL}}">{{$BLUETEAM->name}}</a>
						@if($BLUETEAM->score !== false)
							 ({{$BLUETEAM->score}})
						@endif
					</div>
				</div>
				@if(isset($BLUETEAM->ships))
					<div class="tourament-stats-box">
						<div id="blueteam-fleet-attributes-defense-bar" class="tournament-fleet-attributes-bar">
							<div id="blueteam-fleet-attributes-defense-fill" class="tournament-fleet-attributes-fill">
							</div>
						</div>
						<div id="blueteam-fleet-attributes-attack-bar" class="tournament-fleet-attributes-bar">
							<div id="blueteam-fleet-attributes-attack-fill" class="tournament-fleet-attributes-fill">
							</div>
						</div>
						<div id="blueteam-fleet-attributes-control-bar" class="tournament-fleet-attributes-bar">
							<div id="blueteam-fleet-attributes-control-fill" class="tournament-fleet-attributes-fill">
							</div>
						</div>
					</div>
					<?php $even = true; ?>
					@foreach($BLUETEAM->ships as $item)
						<?php 
							if($even == false)
								$even = true;
							else
								$even = false; 
						?>
						<div class="row <?php echo ($even == true ? 'tournament-even-row' : '');?>" id="{{$item->id}}">
							<div class="inline-block">
								@if(($item->type == 'Tengu' || $item->type == 'Loki' || $item->type == 'Proteus' || $item->type == 'Legion') && $item->subs == 5)
									<div class="module-image-container"><image class="img kill-killer-alliance block" src="{{$item->icon}}" title="{{$item->type}}&#013;{{$item->subSystems[0]->name}}&#013;{{$item->subSystems[1]->name}}
									&#013;{{$item->subSystems[2]->name}}&#013;{{$item->subSystems[3]->name}}&#013;{{$item->subSystems[4]->name}}&#013;{{$item->hiSlots}}/{{$item->medSlots}}/{{$item->loSlots}}/{{$item->rigs}}/{{$item->subs}}" /></div>
								@else
									<div class="module-image-container"><image class="img kill-killer-alliance block" src="{{$item->icon}}" title="{{$item->type}}&#013;{{$item->hiSlots}}/{{$item->medSlots}}/{{$item->loSlots}}/{{$item->rigs}}/{{$item->subs}}" /></div>
								@endif
								<div class="module-image-container"><image class="img kill-killer-alliance block" src="{{$item->pilotIcon}}" title="{{$item->pilotName}}" id="{{$item->id}}_pilot" /></div>
							</div>
							<div class="inline-block tournament-ship-health">
								<div id="{{$item->id}}_hull" class="inline-block tournament-ship-bar tournament-ship-hull" title="Hull">
								</div>
								<div id="{{$item->id}}_armour" class="inline-block tournament-ship-bar tournament-ship-armour" title="Armour">
								</div>
								<div id="{{$item->id}}_shield" class="inline-block tournament-ship-bar tournament-ship-shield" title="Shield">
								</div>
							</div>
							<div class="inline-block">
								<div class="block">
									{{$item->pilotName}}
								</div>
								<div class="block">
									{{$item->type}}
								</div>
								<div id="{{$item->id}}_speed"class="block">
									0 m/s
								</div>
							</div>
							<div class="slot-layout" style="width: {{ceil($item->hiSlots/2)*35}}px;" id="{{$item->id}}_highs">
								<?php $x = 0; ?>
								@for($X = 0;$x < $item->hiSlots;$x++)
									@if(isset($item->hiSlot[$x]))
										<div class="module-image-container"><image class="slot-icon" id="{{$item->hiSlot[$x]->id}}" src="https://image.eveonline.com/Type/{{$item->hiSlot[$x]->typeID}}_32.png" title="{{$item->hiSlot[$x]->name}}" /></div>
									@else
										<div class="module-image-container"><image class="slot-icon" src="{{asset('images/Icons/items/8_64_11.png')}}" /></div>
									@endif
								@endfor
							</div>
							<div class="slot-layout" style="width: {{ceil($item->medSlots/2)*35}}px;" id="{{$item->id}}_mids">
								<?php $x = 0; ?>
								@for($X = 0;$x < $item->medSlots;$x++)
									@if(isset($item->medSlot[$x]))
										<div class="module-image-container"><image class="slot-icon" id="{{$item->hiSlot[$x]->id}}" src="https://image.eveonline.com/Type/{{$item->medSlot[$x]->typeID}}_32.png" title="{{$item->medSlot[$x]->name}}" /></div>
									@else
										<div class="module-image-container"><image class="slot-icon" src="{{asset('images/Icons/items/8_64_10.png')}}" /></div>
									@endif
								@endfor
							</div>
							<div class="slot-layout" style="width: {{ceil($item->loSlots/2)*35}}px;" id="{{$item->id}}_lows">
								<?php $x = 0; ?>
								@for($X = 0;$x < $item->loSlots;$x++)
									@if(isset($item->loSlot[$x]))
										<div class="module-image-container"><image class="slot-icon" id="{{$item->hiSlot[$x]->id}}" src="https://image.eveonline.com/Type/{{$item->loSlot[$x]->typeID}}_32.png" title="{{$item->loSlot[$x]->name}}" /></div>
									@else
										<div class="module-image-container"><image class="slot-icon" src="{{asset('images/Icons/items/8_64_9.png')}}" /></div>
									@endif
								@endfor
							</div>
							<div class="slot-layout" id="{{$item->id}}_unknowns">
							</div>
						</div>
					@endforeach
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 tournament-row">
				<div id="red-console" class="well well-lg">
				</div>
			</div>
			<div class="col-md-6 tournament-row">
				<div id="blue-console" class="well well-lg">
				</div>
			</div>
		</div>
		@if(isset($BLUETEAM->ships))
			<div class="row">
				<div id="console" class="well well-lg">
				</div>
			</div>
		@endif
	</div>
	<div style="clear:both;">&nbsp;</div>
</div>

@stop
@section('additional_js')
@if(isset($BLUETEAM->ships))
	<script>
		$(document).ready(function() {
			precisionCheck = {{PRESCISION_CHECK}};
			

			@if(isset($FRAMEURL))
				replayURL = '{{$FRAMEURL}}'+'0/';
				FRAMEURL = '{{$FRAMEURL}}'+'0/';
			@endif

			@if(isset($MAXDEFENSE))
				maxDefense = '{{$MAXDEFENSE}}';
			@endif

			@if(isset($MAXATTACK))
				maxAttack = '{{$MAXATTACK}}';
			@endif

			@if(isset($MAXCONTROL))
				maxControl = '{{$MAXCONTROL}}';
			@endif

			@if(isset($REDTEAM) && !empty($REDTEAM))
				@if(isset($REDTEAM->ships) && !empty($REDTEAM->ships))
					@foreach($REDTEAM->ships as $ship)
						ships[{{$ship->id}}] = {
						    pilotName: '{{addslashes($ship->pilotName)}}',
						    pilotIcon: '{{$ship->pilotIcon}}',
						    type: '{{$ship->type}}',
						    icon: '{{$ship->icon}}',
						    points: '{{$ship->points}}',
						    loSlots: '{{$ship->loSlots}}',
						    medSlots: '{{$ship->medSlots}}',
						    hiSlots: '{{$ship->hiSlots}}',
						    rigs: '{{$ship->rigs}}',
						    subs: '{{$ship->subs}}',
						    shipROFBonus: '{{$ship->shipROFBonus}}',
						    physicsData: null,
						    calculatedROF: false,
						    team: 'Red',
						    maxSpeed: 0,
						    hiSlot: [],
						    medSlot: [],
						    loSlot: [],
						    droneBay: {},
						    unknowns: [],
						    armor: 1,
						    shield: 1,
						    structure: 1
						};
						@if(isset($ship->hiSlot) && !empty($ship->hiSlot))
							var x = 0;

							@foreach($ship->hiSlot as $slot)
								rofArray = [];
								@foreach($slot->turretVariants as $rof)
									rofArray.push({typeName: "{{$rof->typeName}}", typeID:{{$rof->typeID}}, baseROF:{{(isset($rof->baseROF) ? $rof->baseROF : 0)}}, meta:{{$rof->meta}} });
								@endforeach
								ships[{{$ship->id}}]['hiSlot'][x] = {id: '{{$slot->id}}', graphicID: '{{$slot->graphicID}}', name: '{{addslashes($slot->name)}}', typeID: '{{$slot->typeID}}', groupName: '{{$slot->groupName}}', turretVariants: rofArray, calculatedROF: false };
								x++;
							@endforeach
						@endif
					@endforeach
				@endif
			@endif

			@if(isset($BLUETEAM) && !empty($BLUETEAM))
				@if(isset($BLUETEAM->ships) && !empty($BLUETEAM->ships))
					@foreach($BLUETEAM->ships as $ship)
						ships[{{$ship->id}}] = {
						    pilotName: '{{addslashes($ship->pilotName)}}',
						    pilotIcon: '{{$ship->pilotIcon}}',
						    type: '{{$ship->type}}',
						    icon: '{{$ship->icon}}',
						    points: '{{$ship->points}}',
						    loSlots: '{{$ship->loSlots}}',
						    medSlots: '{{$ship->medSlots}}',
						    hiSlots: '{{$ship->hiSlots}}',
						    rigs: '{{$ship->rigs}}',
						    subs: '{{$ship->subs}}',
						    shipROFBonus: '{{$ship->shipROFBonus}}',
						    physicsData: null,
						    calculatedROF: false,
						    team: 'Blue',
						    maxSpeed: 0,
						    hiSlot: [],
						    medSlot: [],
						    loSlot: [],
						    droneBay: {},
						    unknowns: [],
						    armor: 1,
						    shield: 1,
						    structure: 1
						};
						@if(isset($ship->hiSlot) && !empty($ship->hiSlot))
							var x = 0;

							@foreach($ship->hiSlot as $slot)
								rofArray = [];
								@foreach($slot->turretVariants as $rof)
									rofArray.push({typeName: "{{$rof->typeName}}", typeID:{{$rof->typeID}}, baseROF:{{(isset($rof->baseROF) ? $rof->baseROF : 0)}}, meta:{{$rof->meta}} });
								@endforeach
								ships[{{$ship->id}}]['hiSlot'][x] = {id: '{{$slot->id}}', graphicID: '{{$slot->graphicID}}', name: '{{addslashes($slot->name)}}', typeID: '{{$slot->typeID}}', groupName: '{{$slot->groupName}}', turretVariants: rofArray, calculatedROF: false };
								x++;
							@endforeach
						@endif
					@endforeach
				@endif
			@endif

		
			$('.tournament-timer').on('click', '#control-button', function (){
				tourneyapp.togglePlay();

		        if(tourneyapp.playButton == 'Stop') {
		        	tourneyapp.getReplayFrame();
		        }
		    });

		    $('.tournament-timer').on('click', '#reset-button', function (){
				tourneyapp.reset = true;

				if(tourneyapp.playButton == 'Play')
					tourneyapp.resetMatch();
		    });
			tourneyapp.init();
		});
	</script>
@endif
@stop