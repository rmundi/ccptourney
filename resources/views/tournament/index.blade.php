@extends('layouts.main')

@section('page_title')Pandemic Legion - Tournaments Index @stop

@section('body_class')body_class @stop

@section('content')

<div class="page">
	<div id="contents">
		<div class="row text-center">
			<h1 class="pagination-centered">CCP ALLIANCE TOURNAMENTS</h1>
			
			<table class="table table-striped">
				<tr>
	        		<th class="col-md-12">Tournament</th>
	        	</tr>
				@foreach($INFO as $item)
				<tr>
	        		<td class="col-md-12"><a href="{{$item['href']}}">{{$item['name']}}</a></td>
	        	</tr>
	    		@endforeach
    		</table>
		</div>
	</div>
	<div style="clear:both;">&nbsp;</div>
</div>

@stop
