@extends('layouts.main')

@section('page_title')Pandemic Legion - {{$DETAILS['name']}} @stop

@section('body_class')body_class @stop

@section('content')

<div class="page">
	<div id="contents">
		<div class="row text-center">
			<h1 class="pagination-centered">{{$DETAILS['name']}}</h1>
			<h3 class="pagination-centered">{{$DETAILS['type']}} - {{$DETAILS['membershipCutoff']}}<h3>
			<h4 class="pagination-centered"><a href="{{$DETAILS['series']}}">Matches</a></h4>

			@foreach($TEAMS as $item)
	        	<div class="col-md-3">{{$item['name']}}</div>
	        	<div class="col-md-1"><a href="{{$item['href']}}">Team Info</a></div>
	        	<div class="col-md-2"><a href="{{$item['teamStats']}}">Tournament Stats</a></div>
	    	@endforeach
    		
		</div>
	</div>
	<div style="clear:both;">&nbsp;</div>
</div>

@stop
