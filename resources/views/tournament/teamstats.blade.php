@extends('layouts.main')

@section('page_title')Pandemic Legion - {{$DETAILS['name']}} @stop

@section('body_class')body_class @stop

@section('content')

<div class="page">
	<div id="contents">
		<div class="row">
			<h1 class="pagination-centered">{{$DETAILS['name']}}</h1>
			
			<h3 class="lineheader">Team Info:</h3>
			<div class="row">
				<div class="col-md-4">
					Team Captain: 
					{{$DETAILS['captain']['name']}}
					<image class="kill-killer-alliance img" src="{{$DETAILS['captain']['icon']}}" />	
				</div>
				<div class="col-md-4">
					Ships Destroyed: 
					{{number_format($DETAILS['shipsKilled'],0)}}	
				</div>
				<div class="col-md-4">
					ISK Destroyed: 
					{{number_format($DETAILS['iskKilled'])}}	
				</div>
			</div>
			<div style="clear:both;">&nbsp;</div>
			@if(isset($DETAILS['flagshipType']['name']) && $DETAILS['flagshipType']['name'] != "")
				<h3 class="lineheader">Flagship:</h3>
				<div class="row">
					<image class="kill-killer-alliance img" src="{{$DETAILS['flagshipType']['icon']}}" />
		        	{{$DETAILS['flagshipType']['name']}}
				</div>
				<div style="clear:both;">&nbsp;</div>
			@endif
			<h3 class="lineheader">Team Members:</h3>
			@foreach($TEAMMEMBERS as $item)
			<div class="col-md-4">
				<image class="kill-killer-alliance img" src="{{$item['icon']}}" />
	        	{{$item['name']}}
	        </div>
	        @endforeach
			<div style="clear:both;">&nbsp;</div>
			<h3 class="lineheader">Pilots:</h3>
			@foreach($PILOTS as $item)
			<div class="col-md-4">
				<image class="kill-killer-alliance img" src="{{$item['icon']}}" />
	        	{{$item['name']}}
	        </div>
	        @endforeach
	        <div style="clear:both;">&nbsp;</div>
	        <h3 class="lineheader">Matches:</h3>
			@foreach($MATCHES as $item)
			<div class="col-md-4">
	        	<a href="{{$item['url']}}">{{$item['seriesData']->redTeam->team->teamName}} vs. {{$item['seriesData']->blueTeam->team->teamName}}</a> 
	        	@if(isset($item['seriesData']->winner))
	        		@if(isset($item['seriesData']->winner->team->teamName) && $item['seriesData']->winner->team->href == $item['matchData']->winner->href && $item['seriesData']->winner->team->teamName == $DETAILS['name'])
	        			(won)
	        		@elseif(isset($item['seriesData']->loser->team->teamName) && $item['seriesData']->winner->team->href != $item['matchData']->winner->href && $item['seriesData']->loser->team->teamName == $DETAILS['name'])
	        			(won)
	        		@else
	        			(lost)
	        		@endif
	        	@endif
	        </div>
	        @endforeach	
	        <div style="clear:both;">&nbsp;</div>
	        <h3 class="lineheader">Ban Frequency:</h3>
			@foreach($BANS as $item)
			<div class="col-md-4">
				<image class="kill-killer-alliance img" src="{{$item['shipType']['icon']}}" />
	        	{{$item['shipType']['name']}}
	        	Bans: {{$item['numBans']}}
	        </div>
	        @endforeach
	        <div style="clear:both;">&nbsp;</div>
			<h3 class="lineheader">Bans Against:</h3>
			@foreach($BANSAGAINST as $item)
			<div class="col-md-4">
				<image class="kill-killer-alliance img" src="{{$item['shipType']['icon']}}" />
	        	{{$item['shipType']['name']}}
	        	Bans: {{$item['numBans']}}
	        </div>
	    	@endforeach
    		
		</div>
	</div>
	<div style="clear:both;">&nbsp;</div>
</div>

@stop
