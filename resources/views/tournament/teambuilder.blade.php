@extends('layouts.main')

@section('page_title')Pandemic Legion - Tournament Team Builder @stop

@section('body_class')body_class @stop

@section('content')

<div class="page">
	<div id="contents" style="height:900px;">
		<h1 class="pagination-centered">PL Tournament Team Builder</h1>
		<div class="row shipbuilder-row lineheader">
			<!-- Single button -->
			<div class="btn-group">
			 	<button id="ship-selector" type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
			    	Ships <span class="caret"></span>
			  	</button>
			  	<ul id="ship-menu" class="dropdown-menu-compact dropdown-menu" role="menu">
		  		</ul>
			</div>
			<div class="inline">
				<input id="shipsearch" class="tournament-searchbox" name="shipsearchbox" autocomplete="off" placeholder="Search" type="text">
			</div>
			<div class="checkbox inline">
    			<label>
      				<input id="filtertoggle" type="checkbox"> Apply Filters
    			</label>
  			</div>
  			<div class="radio inline">
				<label>
					<input type="radio" name="menuradio" id="menuradio1" value="shipclass" checked>
					Show Ship Class
				</label>
			</div>
			<div class="radio inline">
				<label>
			 		<input type="radio" name="menuradio" id="menuradio2" value="shipslots">
					Show Slot Layout
			 	</label>
			</div>
			<div class="pull-right display-tourny-info">
				Ships: <span id="ships-number">0</span>/12
				Points: <span id="points-number">0</span>/100
			</div>
		</div>
		<div id="filterlist" class="collapse row">
			<div class="well">
				<label class="filters">
      				<input id="projectiletoggle" type="checkbox" class="togglecheckbox"> Projectile Weapons
    			</label>
    			<label class="filters">
      				<input id="energytoggle" type="checkbox" class="togglecheckbox"> Energy Weapons
    			</label>
    			<label class="filters">
      				<input id="hybridtoggle" type="checkbox" class="togglecheckbox"> Hybrid Weapons
    			</label>
    			<label class="filters">
      				<input id="missiletoggle" type="checkbox" class="togglecheckbox"> Missile Weapons
    			</label>
    			<label class="filters">
      				<input id="dronetoggle" type="checkbox" class="togglecheckbox"> Drone Bonus
    			</label>
    			<label class="filters">
      				<input id="webstoggle" type="checkbox" class="togglecheckbox"> Webifier Bonus
    			</label>
    			<label class="filters">
      				<input id="scramstoggle" type="checkbox" class="togglecheckbox"> Scrambler Bonus
    			</label>
    			<label class="filters">
      				<input id="ecmtoggle" type="checkbox" class="togglecheckbox"> ECM Bonus
    			</label>
    			<label class="filters">
      				<input id="tdtoggle" type="checkbox" class="togglecheckbox"> Weapon Disruption Bonus
    			</label>
    			<label class="filters">
      				<input id="dampstoggle" type="checkbox" class="togglecheckbox"> Sensor Dampening Bonus
    			</label>
    			<label class="filters">
      				<input id="tptoggle" type="checkbox" class="togglecheckbox"> Target Painting Bonus
    			</label>
    			<label class="filters">
      				<input id="energydraintoggle" type="checkbox" class="togglecheckbox"> Energy Drain Bonus
    			</label>
    			<label class="filters">
      				<input id="warfaretoggle" type="checkbox" class="togglecheckbox"> Warfare Link Bonus
    			</label>
    			<label class="filters">
      				<input id="armourtoggle" type="checkbox" class="togglecheckbox"> Armour Bonus
    			</label>
    			<label class="filters">
      				<input id="shieldtoggle" type="checkbox" class="togglecheckbox"> Shield Bonus
    			</label>
    			<label class="filters">
      				<input id="remotearmourtoggle" type="checkbox" class="togglecheckbox"> Remote Armour Bonus
    			</label>
    			<label class="filters">
      				<input id="shieldemissiontoggle" type="checkbox" class="togglecheckbox"> Remote Shield Bonus
    			</label>
    			<label class="filters">
      				<input id="capemissiontoggle" type="checkbox" class="togglecheckbox"> Capacitor Emission Bonus
    			</label>
    			<label class="filters">
      				<input id="repdronetoggle" type="checkbox" class="togglecheckbox"> Repair Drone Bonus
    			</label>
			</div>
		</div>
		<div id="shipList" class="row tournament-row">
			
		</div>
	</div>
	<div style="clear:both;">&nbsp;</div>
</div>

@stop
@section('additional_js')
<script>
var ships = [];
var traits = [];
var teamList = [];
var points = 100;
var pointsUsed = 0;
var menuType = "points";
var subSubMenu = false;
var groupMenu = false;
//var canMarauders = true;
var canLogistics = true;
var canLogisticsFrigates = true;
var selectedShips = {};
var results = {};
var isFiltered = false;
var maxShips = 2;

$(document).ready(function() {
	$('#shipsearch').tourny_search( function() { $('#shipsearch').val(''); } );
	loadData();
	
});
$('input[type=radio][name=menuradio]').change(function() {
    createMenuList();
});

$('#filtertoggle').change(function() {
    if($(this).is(":checked")) {
        $('#filterlist').collapse('show');
        isFiltered = true;
    } else {
    	$('#filterlist').collapse('hide');
    	isFiltered = false;
    }
    createMenuList();       
});

$('.togglecheckbox').change(function() {
	createMenuList();
});

(function( $ ) {

	var tourny_search = function(element, callback) {

		//create our objects and things
		this.data = {}, this.data['element'] = element, this.data['menu'] = $('<ul class="dropdown-menu" style="max-width: 500px;"></ul>').appendTo('body'), this.callback = callback;

		//bind our primary search event
		this.data['element'].on('keyup', $.proxy(function(event) { if (!event.isDefaultPrevented() && event.keyCode != 9 && event.keyCode != 38 && event.keyCode != 40) { return $.proxy(this.do_search(event), this); } }, this));
		
		//bind our utility keypress stuff events
		this.data['element'].on('keydown', $.proxy(function(event) { 
			event.stopPropagation();
			switch(event.keyCode) {
				case 38: $.proxy(this.move_prev(event), this); break;
				case 40: $.proxy(this.move_next(event), this); break;
				case 27: $.proxy(this.hide_menu(event), this); break;
			}
		}, this));
		
		//handle any enter key presses intelligently
		this.data['element'].on('keypress', $.proxy(function(event) { event.stopPropagation(); if (event.keyCode == 13 && this.data['menu'].find('.plactive').length == 1) { $.proxy(this.run_callback(event), this); } }, this));
		
		//handle a couple of other types of event
		this.data['element'].on('blur', $.proxy(function(){ $.proxy(this.hide_menu(), this); }, this));
		this.data['menu'].on('click', 'a', $.proxy(function(event){ $.proxy(this.run_callback(event), this); }, this));	
		this.data['menu'].on('mouseenter', 'li', $.proxy(function(event){ this.data['menu'].find('.plactive').removeClass('plactive'); $(event.currentTarget).addClass('plactive').addClass('plactive'); }, this));
	}

	//add all the functions we need
	tourny_search.prototype = {
		
		constructor: tourny_search,
		
		//get the position of the input so we can correctly offset the search window
		get_position: function() {
			var pos = $.extend({}, this.data['element'].offset(), { height: this.data['element'][0].offsetHeight });
			return { top: (pos.top + pos.height), left: pos.left };
		}, 
				
		//move the selection around
		move_prev: function(event) { event.preventDefault(); this.data['menu'].find('.plactive').removeClass('plactive').prev().addClass('plactive'); if ( this.data['menu'].find('.plactive').length == 0) { this.data['menu'].find('li').last().addClass('plactive'); } },
		move_next: function(event) { event.preventDefault(); this.data['menu'].find('.plactive').removeClass('plactive').next().addClass('plactive'); if ( this.data['menu'].find('.plactive').length == 0) { this.data['menu'].find('li').first().addClass('plactive'); } },
	
		//goto the selected items seach page
		run_callback: function(event) { $.proxy(this.callback(this.data['menu'].find('.plactive').data('value'), event), this); },
	
		//hide the drop down
		hide_menu: function(event) { this.data['menu'].fadeOut(200); },
		
		//the main event - perfom an lookup to see what there is to see
		do_search: function(event) { 
			//clear any throttled searched now now
			clearTimeout(this.data['throttle']);
			var searchString = this.data['element'].val();

			if ( this.data['element'].val() == '' || this.data['element'].val().length < 3) { this.hide_menu(event); return; }

			this.data['throttle'] = setTimeout($.proxy(function() {
				this.data['menu'].empty();
				for(var searchID in ships) {
					if(ships.hasOwnProperty(searchID)) {
						if(ships[searchID]['typeName'].toLowerCase().indexOf(searchString.toLowerCase()) >= 0 || ships[searchID]['groupName'].toLowerCase().indexOf(searchString.toLowerCase()) >= 0) {
							var canProcessShip = canSelectThisShip(ships[searchID]);

							if(canProcessShip == true) {
								if(ships[searchID]['points'] <= (points-pointsUsed)) {
									this.data['menu'].append('<li><a onClick="selectShip('+ships[searchID]['typeID']+');">'+ ships[searchID]['typeName'] + ' ('+ships[searchID]['points']+' pts)</a></li>');
								}
							}
						}
					}
				}
				this.data['menu'].not(':visible').css(this.get_position()).fadeIn(200);
				this.data['menu'].find('li').first().addClass('');
			}, this), 600);
		}
	};

	//define the search method
	$.fn.tourny_search = function(callback) {
		return this.each(function() {
			var $this = $(this), data = $this.data('tourny_search');
			if (!data) { $this.data('tourny_search', (data = new tourny_search($this, callback))); }
		});
	}
})( jQuery );

function loadData() {
	var jqxhr = $.ajax({
					headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        },
					type: 'POST',
					url: BASEURL+'/shipdata'
				})
	.done(function( data ) {
		data = JSON.parse(data);
		ships = data['ships'];
		traits = data['traits'];

		var urlShips = getUrlParameter('ships');
		if(urlShips !== undefined) {
			var urlIDs = urlShips.split(',');
			window.history.pushState("object or string", "Title", BASEURL+'/teambuilder');
			console.log(urlIDs);
			for(var x=0;x<urlIDs.length;x++) {
				selectShip(urlIDs[x]);
			}
		}
		createMenuList();
	});
}

function getTraits(typeID) {
	var traitData = [];
	for(var traitID in traits) {
		if(traits.hasOwnProperty(traitID)) {
			if(traits[traitID]['typeID'] == typeID) {
				traitData.push(traits[traitID]);
			}
		}
	}

	return traitData;
}

function getShip(typeID) {
	for(var shipID in ships) {
		if(ships.hasOwnProperty(shipID)) {
			if(ships[shipID]['typeID'] == typeID) {
				return ships[shipID];
			}
		}
	}
}

function canSelectThisShip(thisShip) {
	for(var selID in selectedShips) {
		if(selectedShips.hasOwnProperty(selID)) {
			if(selID == thisShip['typeID'] && selectedShips[selID] >= maxShips) {
				return false;
			}
		}
	}
	//if(thisShip['groupName'] == 'Marauder' && canMarauders == false) {
	//	return false;
	//}

	if((thisShip['groupName'] == 'Logistics' ||  thisShip['typeName'] == 'Augoror' ||  thisShip['typeName'] == 'Scythe' ||  thisShip['typeName'] == 'Exequror' ||  thisShip['typeName'] == 'Osprey') && canLogistics == false) {
		return false;
	}

	if((thisShip['typeName'] == 'Inquisitor' ||  thisShip['typeName'] == 'Bantam' ||  thisShip['typeName'] == 'Navitas' ||  thisShip['typeName'] == 'Burst') && canLogisticsFrigates == false) {
		return false;
	}

	if(isFiltered == true) {
		var thisTraits = getTraits(thisShip['typeID']);

		for(var thisTraitID in thisTraits) {
			if(thisTraits.hasOwnProperty(thisTraitID)) {
				if($('#projectiletoggle').is(":checked") && thisTraits[thisTraitID]['groupName'] == 'Gunnery' && thisTraits[thisTraitID]['typeName'].indexOf('Projectile Turret') >= 0) {
					return true;
				}
				if($('#energytoggle').is(":checked") && thisTraits[thisTraitID]['groupName'] == 'Gunnery' && thisTraits[thisTraitID]['typeName'].indexOf('Energy Turret') >= 0) {
					return true;
				}
				if($('#hybridtoggle').is(":checked") && thisTraits[thisTraitID]['groupName'] == 'Gunnery' && thisTraits[thisTraitID]['typeName'].indexOf('Hybrid Turret') >= 0) {
					return true;
				}
				if($('#missiletoggle').is(":checked") && thisTraits[thisTraitID]['groupName'] == 'Missiles') {
					return true;
				}
				if($('#dronetoggle').is(":checked") && thisTraits[thisTraitID]['groupName'] == 'Drones') {
					return true;
				}
				if($('#webstoggle').is(":checked") && thisTraits[thisTraitID]['groupName'] == 'Stasis Web') {
					return true;
				}
				if($('#scramstoggle').is(":checked") && thisTraits[thisTraitID]['groupName'] == 'Warp Scrambler') {
					return true;
				}
				if($('#ecmtoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Electronic Warfare') {
					return true;
				}
				if($('#tdtoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Weapon Disruption') {
					return true;
				}
				if($('#dampstoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Sensor Linking') {
					return true;
				}
				if($('#tptoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Target Painting') {
					return true;
				}
				if($('#energydraintoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Capacitor Emission Systems'  && 
					(thisTraits[thisTraitID]['bonusText'].indexOf('Energy Vampire') >= 0 || thisTraits[thisTraitID]['bonusText'].indexOf('Energy Neutralizer') >= 0)) {
					return true;
				}
				if($('#warfaretoggle').is(":checked") && thisTraits[thisTraitID]['groupName'] == 'Leadership') {
					return true;
				}
				if($('#armourtoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Repair Systems') {
					return true;
				}
				if($('#shieldtoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Shield Operation') {
					return true;
				}
				if($('#remotearmourtoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Remote Armor Repair Systems') {
					return true;
				}
				if($('#shieldemissiontoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Shield Emission Systems') {
					return true;
				}
				if($('#capemissiontoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Capacitor Emission Systems' && thisTraits[thisTraitID]['bonusText'].indexOf('Remote Capacitor') >= 0) {
					return true;
				}
				if($('#repdronetoggle').is(":checked") && thisTraits[thisTraitID]['typeName'] == 'Repair Drone Operation') {
					return true;
				}
			}
		}
		return false;
	}
	return true;
}

function createMenuList() {
	var menuData = "";
	if(menuType == "points") {
		var currentPoints = 0;
		var currentGroup = "";
		var currentMarketGroup = "";
		
		for(var shipID in ships) {
			if(ships.hasOwnProperty(shipID)) {
				var canProcessShip = canSelectThisShip(ships[shipID]);

				if(canProcessShip == true) {
					if(ships[shipID]['points'] <= (points-pointsUsed)) {
						if(currentMarketGroup != "" && currentMarketGroup != ships[shipID]['marketGroupName'] && groupMenu == true) {
							menuData += "</ul></li>";
						}

						if(currentGroup != "" && (currentGroup != ships[shipID]['groupName'] || ships[shipID]['points'] != currentPoints) && subSubMenu == true) {
							menuData += "</ul></li>";
						}

						if(ships[shipID]['points'] != currentPoints) {

							if(currentPoints != 0) {
								menuData += "</ul></li>";
							}
							currentPoints = ships[shipID]['points'];
							menuData += '<li class="dropdown-submenu"><a>'+ships[shipID]['points']+' Points</a><ul class="dropdown-menu-compact dropdown-menu">';
							var listCount = 0;
							var groupCount = 0;
							var tempGroup = "";
							for(var subID in ships) {
								if(ships.hasOwnProperty(subID)) {
									if(ships[subID]['points'] == ships[shipID]['points']) {
										listCount++;
										if(ships[subID]['groupName'] != tempGroup) {
											tempGroup = ships[subID]['groupName'];
											groupCount++;
										}
									}
								}
							}
							
							if(listCount > 20 && groupCount > 1) {
								subSubMenu = true;
								groupMenu = false;
							} else {
								subSubMenu = false;
								groupMenu = false;
							}

							if(listCount > 20 && groupCount == 1) {
								groupMenu = true;
							}
							
						}

						if(subSubMenu == true && ships[shipID]['groupName'] != currentGroup) {
							menuData += '<li class="dropdown-submenu"><a>'+ships[shipID]['groupName']+'</a><ul class="dropdown-menu-compact dropdown-menu">';
							currentGroup = ships[shipID]['groupName'];
						}

						if(groupMenu == true && ships[shipID]['marketGroupName'] != currentMarketGroup) {
							menuData += '<li class="dropdown-submenu"><a>'+ships[shipID]['marketGroupName']+'</a><ul class="dropdown-menu-compact dropdown-menu">';
							currentMarketGroup = ships[shipID]['marketGroupName'];
						}
						//if($('input[type=radio][name=menuradio]').value == '')
						if($("input[name='menuradio']:checked").val() == 'shipclass') {
							menuData += '<li><a id="'+ships[shipID]['typeID']+'" onClick="selectShip('+ships[shipID]['typeID']+');">'+ships[shipID]['typeName']+' ('+ships[shipID]['groupName']+')</a></li>';
						} else {
							menuData += '<li><a id="'+ships[shipID]['typeID']+'" onClick="selectShip('+ships[shipID]['typeID']+');">'+ships[shipID]['typeName']+' ('+ships[shipID]['hiSlots']+'/'+ships[shipID]['midSlots']+'/'+ships[shipID]['loSlots']+'/'+ships[shipID]['rigSlots']+')</a></li>';
						}
					}
				}
			}
		}
	}
	
	$( '#ship-menu' ).html( menuData );
}
function removeRow(row) {

	var parent = row.parentElement;
	var grandParent = parent.parentElement;
	var removeID = row.previousSibling.src.replace('https://image.eveonline.com/Type/','').replace('_64.png','');
	
	grandParent.parentNode.removeChild(grandParent);
	for(var teamID in teamList) {
		if(teamList.hasOwnProperty(teamID)) {
			if(teamList[teamID]['typeID'] == removeID) {
				teamList.splice(teamID, 1);
				break;
			}
		}
	}
	
	checkShips();
}

function selectShip(id) {
	var shipInfo = getShip(id);
	var shipTraits = getTraits(id);
	var pageData = '';

	addShip(id);

	pageData = '<div class="col-md-12">';
	pageData += '			<div class="inline-block">';
	pageData += '				<image class="img block" src="'+'https://image.eveonline.com/Type/'+id+'_64.png" title="'+shipInfo['typeName']+'" />';
	pageData += '<button class="btn btn-default btn-xs remove-row-icon" onClick="removeRow(this);" type="button">';
    pageData += '<span class="glyphicon glyphicon-remove"></span>';
	pageData += '</button>';
	pageData += '			</div>';
	pageData += '			<div class="inline-block">';
	pageData += '				<div class="block">';
	pageData += shipInfo['typeName'];
	pageData += '				</div>';
	pageData += '				<div class="block">';
	pageData += shipInfo['groupName'];
	pageData += '				</div>';
	pageData += '				<div class="block">';
	pageData += shipInfo['marketGroupName'];
	pageData += '				</div>';
	pageData += '			</div>';
	pageData += '			<div class="inline">';
	pageData += '				<div class="inline-block" style="width: '+(Math.ceil(shipInfo['hiSlots']/2)*32)+'px;">';
					for(var x=0;x < shipInfo['hiSlots'];x++) {
						pageData += '<image class="slot-icon" src="'+BASEURL+'/images/Icons/items/8_64_11.png" />';
					}
	pageData += '</div>';
	pageData += '				<div class="inline-block" style="width: '+(Math.ceil(shipInfo['midSlots']/2)*32)+'px;">';
					for(var x=0;x < shipInfo['midSlots'];x++) {	
						pageData += '<image class="slot-icon" src="'+BASEURL+'/images/Icons/items/8_64_10.png" />';
					}
	pageData += '</div>';
	pageData += '				<div class="inline-block" style="width: '+(Math.ceil(shipInfo['loSlots']/2)*32)+'px;">';
					for(var x=0;x < shipInfo['loSlots'];x++) {	
						pageData += '<image class="slot-icon" src="'+BASEURL+'/images/Icons/items/8_64_9.png" />';
					}
	pageData += '</div>';
	pageData += '				<div class="inline-block"  style="width: '+(Math.ceil(shipInfo['rigSlots']/2)*32)+'px;">';
					for(var x=0;x < shipInfo['rigSlots'];x++) {	
						pageData += '<image class="slot-icon" src="'+BASEURL+'/images/Icons/items/68_64_1.png" />';
					}
	pageData += '</div>';

	pageData += '<div class="inline-block bonus-text pull-right">';
		var skillName = "";
		for(var x=0; x < shipTraits.length; x++) {
			if(shipTraits[x]['skillName'] != skillName) {
				if(shipTraits[x]['skillName'] == null) {
					pageData += 'Role Bonus:';
				} else {
					pageData += shipTraits[x]['skillName']+' bonuses (per skill level):';
				}
				skillName = shipTraits[x]['skillName'];
			}
			pageData += '				<div class="block indent">';
			if(shipTraits[x]['bonus'] != null) {
				pageData += shipTraits[x]['bonus']+shipTraits[x]['unitName']+' ';
			}
			pageData += strip_tags(shipTraits[x]['bonusText']);
			pageData += '				</div>';
		}
	pageData += '			</div>';

	pageData += '			</div>';
	pageData += '		</div>';
	$( '#shipList').append(pageData);
}

function addShip(typeID) {
	teamList.push(getShip(typeID));
	checkShips();
}

function checkShips() {
	var logiFrigs = 0;
	var marauderCount = 0;
	pointsUsed = 0;
	selectedShips = {};
	//canMarauders = true;
	canLogistics = true;
	canLogisticsFrigates = true;

	for(var id in teamList) {
		if(teamList.hasOwnProperty(id)) {
			//if(teamList[id]['groupName'] == 'Marauder') {
				//canMarauders = false;
			//	marauderCount++;
			//}
			if(teamList[id]['groupName'] == 'Logistics' || teamList[id]['typeName'] == 'Augoror' || teamList[id]['typeName'] == 'Scythe' || teamList[id]['typeName'] == 'Exequror' || teamList[id]['typeName'] == 'Osprey') {
				canLogistics = false;
				canLogisticsFrigates = false;
			}
			if(teamList[id]['typeName'] == 'Inquisitor' || teamList[id]['typeName'] == 'Bantam' || teamList[id]['typeName'] == 'Navitas' || teamList[id]['typeName'] == 'Burst') {
				logiFrigs++;
			}
			pointsUsed += parseInt(teamList[id]['points']);
			if(selectedShips[teamList[id]['typeID']] === undefined) {
				selectedShips[teamList[id]['typeID']] = 1;
			} else {
				selectedShips[teamList[id]['typeID']] = selectedShips[teamList[id]['typeID']]+1;
			}
		}
	}

	if(logiFrigs >= 2) {
		canLogistics = false;
		canLogisticsFrigates = false;
	}

	//if(marauderCount >= 2) {
	//	canMarauders = false;
	//}

	if(teamList.length >= 12 || (points-pointsUsed) < 1) {
		$('#ship-selector').addClass('invisible');
	} else if($('#ship-selector').hasClass('invisible')) {
		$('#ship-selector').removeClass('invisible');
	}

	$('#ships-number').html(teamList.length);
	$('#points-number').html(pointsUsed);

	if(teamList.length == 0) {
		window.history.pushState("object or string", "Title", BASEURL+'/teambuilder');
	} else {
		var urlString = BASEURL+'/teambuilder?ships=';
		var idArray = [];
		for(var teamID in teamList) {
			if(teamList.hasOwnProperty(teamID)){
				idArray.push(teamList[teamID]['typeID']);
			}
		}
		urlString += idArray.join(',')
		window.history.pushState("object or string", "Tournament Team Builder", urlString);
	}
	createMenuList();
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)  {
            return sParameterName[1];
        }
    }
}

function strip_tags(input, allowed) {
	allowed = (((allowed || '') + '')
    	.toLowerCase()
    	.match(/<[a-z][a-z0-9]*>/g) || [])
    	.join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
	var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
		commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
	return input.replace(commentsAndPhpTags, '')
		.replace(tags, function($0, $1) {
			return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}


</script>
@stop