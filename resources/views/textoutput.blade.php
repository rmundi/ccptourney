@extends('layouts.main')

@section('page_title')Pandemic Legion @stop

@section('body_class')body_class @stop

@section('content')
<div class="container-fluid">
	@if(isset($TEXT))
		{{ $TEXT }}
		<br /><br />
	@endif
</div>
@stop
