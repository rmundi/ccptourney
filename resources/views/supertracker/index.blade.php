@extends('layouts.main')

@section('load_additional_js')
    <script src="{{ GetURLPath('') }}/js/bootstrap.datepicker.min.js?v={{{config('tools.AppVer')}}}"></script>
@stop

@section('page_title')Pandemic Legion - Super Trawler @stop

@section('body_class')body_class @stop

@section('content')

<div class="page">
	<div id="contents">
		<div class="row text-center">
			
			
			<button id="excelExport" class="btn excel right"type="button"></button>
			
			<h1>Super Tracker</h1>
			<p>Bring on the Wrecking Machine</p>
			 
            <div class="inline-left">
                
                <a button id="addApiKey" class="btn btn-primary btn-xs" type="button" href="{{{GetURLPath('supercapapi')}}}">Add API</a>
            </div>
			
			<div class="inline-block">
				<div class="input-group date date-box" id="dp1" data-date="{{date('Y-m-d',strtotime('-3 Months'))}}" data-date-format="yyyy-mm-dd">
					<span class="input-group-addon btn btn-black"><span class="glyphicon glyphicon-calendar"></span></span>
		    		<input id="startdate" class="span2 form-control" size="16" value="{{date('Y-m-d',strtotime('-3 Months'))}}" type="text" placeholder="From Date" readonly>
		    	</div>
		    </div>
		    
		    <span class="add-on">to</span>
		 	
		    <div class="inline-block">
				<div class="input-group date date-box" id="dp2" data-date="" data-date-format="yyyy-mm-dd">
		    		<input id="enddate" class="span2 form-control" size="16" value="" type="text" placeholder="End Date" readonly>
		    		<span class="input-group-addon btn btn-black"><span class="glyphicon glyphicon-calendar"></span></span>
		    	</div>
		    </div>
			
			<table id="supercaptable" class="table table-striped table-condensed-padding dataTable no-footer" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		            	<th></th>
		                <th>Character</th>
		                <th>Corporation</th>
		                <th>Alliance</th>
		                <th>Faction</th>
		                <th>Kills</th>
		                <th>Ship Type</th>
		                <th>Ship Class</th>
		                <th id="lastSeenCol">Last Seen</th>
		                <th>Last System</th>
		                <th>Last Region</th>
		                <th></th>
		            </tr>
		        </thead>
		    </table>
			
		</div>
	</div>
	<div style="clear:both;">&nbsp;</div>
</div>
<!-- Update Location Modal -->
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Update Location" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Update Location</h4>
            </div>
            <div class="modal-body">
            	Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="preparing-file-modal" title="Download Excel Sheet" style="display: none;">
    Producing the Excel file, please wait...
 
    <!--Throw what you'd like for a progress indicator below-->
    <div class="ui-progressbar-value ui-corner-left ui-corner-right" style="width: 100%; height:22px; margin-top: 20px;"></div>
</div>
 
<div id="error-modal" title="Error" style="display: none;">
    There was a problem generating the Excel document, you can try again if you like but I bet it still won't work. #AGMARMONEYTEAM
</div>
@stop
@section('additional_js')
<script>
function format ( d ) {
    var notes;
    if(d.notes !== null)
    	notes = d.notes;
    else
    	notes = '';
    return 	'<div class="row subtablebox">'+
    			'<div class="col-lg-3">'+
    				'<label>Notes:</label>'+
    				'<textarea class="eveSuperNotes" data-text="'+escape(notes)+'" data-charid="'+d.characterID+'">'+notes+'</textarea>'+
    				'<button type="button" class="btn btn-primary saveNotes right" disabled>Save</button>'+
    			'</div>'+
    			'<div class="col-lg-3">'+
    				'<div> TODO: Associated Chars and modal goes here.</div>'+
    			'</div>'+
    			'<div class="col-lg-3">'+
                    (d.apiID === null ? '<div>There is no API Data for this Character</div>' : '<div><label>Position:<br/><div><span class="text-normal-weight" id="'+d.characterID+'_position">'+d.shipClosestCelestial+
                        '</span></div></label></div><div><label>Distance:<br /><div><span class="text-normal-weight" id="'+d.characterID+'_distance">'+(d.shipClosestCelestialDistance/1000).toFixed(1)+' km'+
                        '</span></div></label></div><label>Update Position using Chars API:</label>'+
                        '<div><button data-charID="'+d.characterID+'" type="button" class="btn btn-primary updateAPILocation">Update Location</button></div>')+
                '</div>'+
    			'<div class="col-lg-3">'+
    				'<label>Update Position with Agent Mail or System:</label>'+
    				'<div><button id="'+d.characterID+'_agentbtn" data-toggle="modal" href="ajaxs/supercapupdate/'+d.characterID+'" data-target="#Modal" type="button" class="btn btn-primary updateLoc">Update Location</button></div>'+
    				'<label>Update Char info with Public API:<br /><span class="text-normal-weight">(Corporation/Alliance/Faction)</span></label>'+
    				'<div><button data-charID="'+d.characterID+'" type="button" class="btn btn-primary updateInfo">Update Character</button></div>'+
    			'</div>'+
    		'</div>';
};

$('#Modal').on('show.bs.modal', function (e) {
	$('#Modal .modal-body').html('Loading...');
    $('#Modal').removeData('bs.modal');
})

$(document).ready(function() {
	$.fn.dataTable.ext.errMode = '{{{config("tools.DT_ERRORTYPE")}}}';
	//$.fn.dataTableExt.oStdClasses["sFilter"] = 'clearable';
	$.extend( $.fn.dataTableExt.oStdClasses, {
	    "sFilterInput": "clearable"
	});

	$('#dp1').datepicker();
	$('#dp2').datepicker();

    var table = $('#supercaptable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "supercaptable",
            "type": "POST",
            "data": function ( d ) {
                d.filterStartDate = $('#startdate').val();
                d.filterEndDate = $('#enddate').val();
            },
            "beforeSend": function (request) {
                request.setRequestHeader( "X-CSRF-TOKEN",  $('meta[name="csrf-token"]').attr('content'));
            }
        },
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                 "searchable":       false,
                "data":           null,
                "defaultContent": '',
            },
            { "data": "characterName" },
            { "data": "corporationName" },
            { "data": "allianceName" },
            { "data": "factionName" },
            { "data": "totalKills" },
            { "data": "shipTypeName" },
            { 
                "data": "shipClass",
                "searchable":       false,
            },
            { "data": "lastSeen" },
            { "data": "lastSeenSystemName" },
            { "data": "lastSeenRegionName" },
            {
                "className":        'icon-images',
                "orderable":        false,
                "searchable":       false, 
                "data":             null,
                "defaultContent":   '',
            },
        ],
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                	var value = '';
                	if(row['notes'] !== null)
                		value += '<img src="{{ GetURLPath('/images/WindowIcons/chatchannel.png') }}">';
                	if(row['associatedChars'] !== null)
                		value += '<img src="{{ GetURLPath('/images/WindowIcons/contacts.png') }}">';
                	if(row['apiID'] !== null && row['apiKey'] !== null)
                		value += '<img src="{{ GetURLPath('/images/WindowIcons/contracts.png') }}">';
                	return value;
                },
                "targets": 11
            },
            {
                "render": function ( data, type, row ) {
                	if(row['characterStanding'] == null)
                		return '<a href="https://zkillboard.com/character/' + row['characterID'] + '/" target="_blank">' + row['characterName'] + '</a>';
                	
                	if(row['characterStanding'] < 0) {
                		var iconType = "minus-symbol";
                		if(row['characterStanding'] < -5)
                			var iconType2 = 'terrible-standing';
                		else
                			var iconType2 = 'bad-standing';
                	} else {
                		var iconType = "plus-symbol";
                		if(row['characterStanding'] > 5)
                			var iconType2 = 'excellent-standing';
                		else
                			var iconType2 = 'good-standing';
                	}

                	return '<div class="' + iconType +' ' + iconType2 + '"></div><a href="https://zkillboard.com/character/' + row['characterID'] + '/" target="_blank">&nbsp;' + row['characterName'] + '</a>';
                },
                "targets": 1
            },
            {
                "render": function ( data, type, row ) {
                	if(row['corporationStanding'] == null)
                		return '<a href="http://evemaps.dotlan.net/corp/' + row['corporationName'] + '" target="_blank">' + row['corporationName'] + '</a>';

                	if(row['corporationStanding'] < 0) {
                		var iconType = "minus-symbol";
                		if(row['corporationStanding'] < -5)
                			var iconType2 = 'terrible-standing';
                		else
                			var iconType2 = 'bad-standing';
                	} else {
                		var iconType = "plus-symbol";
                		if(row['corporationStanding'] > 5)
                			var iconType2 = 'excellent-standing';
                		else
                			var iconType2 = 'good-standing';
                	}

                	return '<div class="' + iconType +' ' + iconType2 + '"></div><a href="http://evemaps.dotlan.net/corp/' + row['corporationName'] + '" target="_blank">&nbsp;' + row['corporationName'] + '</a>';
                },
                "targets": 2
            },
            {
                "render": function ( data, type, row ) {

                	if(row['allianceStanding'] == null)
                		return '<a href="http://evemaps.dotlan.net/alliance/' + row['allianceName'] + '" target="_blank">' + row['allianceName'] + '</a>';
        	
                	if(row['allianceStanding'] < 0) {
                		var iconType = "minus-symbol";
                		if(row['allianceStanding'] < -5)
                			var iconType2 = 'terrible-standing';
                		else
                			var iconType2 = 'bad-standing';
                	} else {
                		var iconType = "plus-symbol";
                		if(row['allianceStanding'] > 5)
                			var iconType2 = 'excellent-standing';
                		else
                			var iconType2 = 'good-standing';
                	}

                	return '<div class="' + iconType +' ' + iconType2 + '"></div><a href="http://evemaps.dotlan.net/alliance/' + row['allianceName'] + '" target="_blank">&nbsp;' + row['allianceName'] + '</a>';
                },
                "targets": 3
            },
            {
                "render": function ( data, type, row ) {
                    if(row['lastKillID'] !== null)
                	   return '<a href="https://zkillboard.com/kill/' + row['lastKillID'] + '/" target="_blank">' + row['lastSeen'] + '</a>';
                    else
                        return row['lastSeen'];
                },
                "targets": 8
            },
            {
                "render": function ( data, type, row ) {
                	return '<a href="http://evemaps.dotlan.net/map/' + row['lastSeenRegionName'].replace(/\ /g, '_') + '/' + row['lastSeenSystemName'].replace(/\ /g, '_') + '" target="_blank">' + row['lastSeenSystemName'] + '</a>';
                },
                "targets": 9
            },
            {
                "render": function ( data, type, row ) {
                	return '<a href="http://evemaps.dotlan.net/map/' + row['lastSeenRegionName'].replace(/\ /g, '_') + '" target="_blank">' + row['lastSeenRegionName'] + '</a>';
                },
                "targets": 10
            },
            
        ],
        "createdRow": function ( row, data, index ) {
            if ( data['isDestroyed'] == 'true' ) {
                $(row).addClass('sc-dead');
            }
        },
        "drawCallback": function ( settings ) {
            /* Does the Date Row thing */
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(8, {page:'current'} ).data().each( function ( group, i ) {
            	if($('#lastSeenCol').hasClass('sorting_desc') || $('#lastSeenCol').hasClass('sorting_asc')) {
	                if ( last !== group.substr(0, 10) ) {
	                    $(rows).eq( i ).before(
	                        '<tr class="group"><td></td><td colspan="11">'+group.substr(0, 10)+'</td></tr>'
	                    );
	 
	                    last = group.substr(0, 10);
	                }
	            }
            } );
        },
        "stateSave": false,
        "deferRender": true,
        "info": true,
        "ordering": true,
        "paging": true,
        "searching": true,
        "autoWidth": true,
        "pageLength": 25,

        //"order": [[ 7, 'desc' ]],
        "order": [[ 8, 'desc' ]],
        
    } );
	
	$('#dp1, #dp2').datepicker().on('changeDate', function(ev){
		// update datatable
		table.draw();
	});

	$('#filteradmin').on('click', function () {
		// update datatable
		table.draw();
	});

    $('#supercaptable tbody').on('click', 'button.updateAPILocation', function () {
        var btn = $(this);
        var charID = btn.data('charid');

        var jqxhr = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: BASEURL+"/supercapapilocupdate",
                data: { charID: charID },
                dataType: "json"
            })
            .done(function(retData) {
                if(retData.errors == false) {
                    btn.closest('tr').prev('tr').children('td').each( function ( index ) {
                        if(index == 1) {
                            $(this).empty();
                            $(this).append('<a href="https://zkillboard.com/character/' + charID + '/" target="_blank">' + retData.characterName + '</a>');
                        }
                        if(index == 2) {
                            $(this).empty();
                            $(this).append('<a href="http://evemaps.dotlan.net/corp/' + retData.corporationName + '" target="_blank">' + retData.corporationName + '</a>');
                        }
                        if(index == 3) {
                            $(this).empty();
                            $(this).append('<a href="http://evemaps.dotlan.net/alliance/' + retData.allianceName + '" target="_blank">' + retData.allianceName + '</a>');
                        }
                        if(index == 4) {
                            $(this).empty();
                            $(this).append(retData.factionName);
                        }
                        if(index == 6) {
                            $(this).empty();
                            $(this).append(retData.shipTypeName);
                        }
                        if(index == 8) {
                            $(this).empty();
                            $(this).append(retData.lastSeen);
                        }
                        if(index == 9) {
                            $(this).empty();
                            $(this).append('<a href="http://evemaps.dotlan.net/map/' + retData.lastSeenRegionName + '/' + retData.lastSeenLocationName + '" target="_blank">' + retData.lastSeenLocationName + '</a>');
                        }
                        if(index == 10) {
                            $(this).empty();
                            $(this).append('<a href="http://evemaps.dotlan.net/map/' + retData.lastSeenRegionName + '" target="_blank">' + retData.lastSeenRegionName + '</a>');
                        }
                    });

                    $( '#'+charID+'_position').html(retData.closestName);
                    $( '#'+charID+'_distance').html((retData.closestDistance/1000).toFixed(1)+' km');

                    addAlert('success', retData.characterName+' location info updated from API', true);
                } else {
                    if(retData.errorMessage) {
                        addAlert('warning', retData.errorMessage, true);
                    } else {
                        addAlert('warning', 'Unspecified problem updating Location', true);
                    }
                }
                //disable button
                //btn.attr('disabled','disabled');
            })
            .fail(function() {
                //console.log( "error" );
                addAlert('danger', 'Pilot not updated, there was an error', true);
            })
            .always(function() {
                //console.log( "complete" );
            });
    });

	$('#supercaptable tbody').on('click', 'button.updateInfo', function () {
		var btn = $(this);
		var charID = btn.data('charid');

		var jqxhr = $.ajax({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                method: "POST",
                url: "ajaxs/supercapinfoupdate",
                data: { charID: charID },
                dataType: "json"
			})
			.done(function(retData) {
				if(retData != false) {
					btn.closest('tr').prev('tr').children('td').each( function ( index ) {
						if(index == 1) {
							$(this).empty();
							$(this).append('<a href="https://zkillboard.com/character/' + charID + '/" target="_blank">' + retData.characterName + '</a>');
						}
						if(index == 2) {
							$(this).empty();
							$(this).append('<a href="http://evemaps.dotlan.net/corp/' + retData.corporationName + '" target="_blank">' + retData.corporationName + '</a>');
						}
						if(index == 3) {
							$(this).empty();
							$(this).append('<a href="http://evemaps.dotlan.net/alliance/' + retData.allianceName + '" target="_blank">' + retData.allianceName + '</a>');
						}
						if(index == 4) {
							$(this).empty();
							$(this).append(retData.factionName);
						}
					});

                    addAlert('success', retData.characterName+' public info updated', true);
				} else {
                    addAlert('warning', 'Unspecified problem updating Info', true);
                }
				//disable button
				btn.attr('disabled','disabled');
			})
			.fail(function() {
		    	//console.log( "error" );
                addAlert('danger','Pilot not updated, there was an error', true);
			})
			.always(function() {
				//console.log( "complete" );
			});
	});
    
	$('#supercaptable tbody').on('change keyup paste', 'textarea.eveSuperNotes', function() {
		var btn = $(this).next('button');

	    if($(this).val() == unescape($(this).data('text'))) {
	    	btn.attr('disabled','disabled');
	        return; //check to prevent multiple simultaneous triggers
	    }

	    btn.removeAttr('disabled');
	});

    $('.modal').on('click', 'button#save-location', function () {
        $('#agentMail').bind('input propertychange', function() {
              $("#agentMail").removeClass('form-error');
        });

        var mailBody = $('#agentMail').val();
        var charID = $('#agentMail').data('charid');
        var btn = $('#'+charID+'_agentbtn');

        if(mailBody == '') {
            $('#agentMail').addClass('form-error');
        } else {
            var jqxhr = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "ajaxs/supercapagentlocate",
                data: { charID: charID, text: mailBody },
                dataType: "json"
            }).done(function(retData) {
                if(retData != false) {
                    btn.closest('tr').prev('tr').children('td').each( function ( index ) {
                        if(index == 8) {
                            $(this).empty();
                            $(this).append(retData.lastSeen);
                        }
                        if(index == 9) {
                            $(this).empty();
                            $(this).append('<a href="http://evemaps.dotlan.net/map/' + retData.lastSeenRegionName + '/' + retData.lastSeenLocationName + '" target="_blank">' + retData.lastSeenLocationName + '</a>');
                        }
                        if(index == 10) {
                            $(this).empty();
                            $(this).append('<a href="http://evemaps.dotlan.net/map/' + retData.lastSeenRegionName + '" target="_blank">' + retData.lastSeenRegionName + '</a>');
                        }
                    });
                    addAlert('success', 'Location Updated', true);
                    //$('.modal').modal('hide'); <--- doesn't work, but it should
                    $('#Modal').hide();
                    $('.modal-backdrop').hide();
                }  else {
                    //addAlert('danger','Mail rejected', true);
                    $('#agentMail').addClass('form-error');
                }  
            }).fail(function() {
                addAlert('danger', 'Unspecified Error', true);
            }).always(function() {
                //console.log( "complete" );
            });
        }
    });

    $('.modal').on('click', 'button#save-addsuper', function () {
        $('#pilotNameInput').bind('input propertychange', function() {
              $("#pilotNameInput").removeClass('form-error');
        });
        $('#locationNameInput').bind('input propertychange', function() {
              $("#locationNameInput").removeClass('form-error');
        });

        var errors = false;

        var charName = $('#pilotNameInput').val();
        var shipType = $( "#shipTypeSelect option:selected" ).text();
        var locationName = $('#locationNameInput').val();

        if(charName == "" || charName === null) {
            $('#pilotNameInput').addClass('form-error');
            errors = true;
        }

        if(locationName == "" || locationName === null) {
            $('#locationNameInput').addClass('form-error');
            errors = true;
        }

        if(!errors) {
            var jqxhr = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "ajaxs/supercapaddpilot",
                data: { charName: charName, shipType: shipType, locationName: locationName },
                dataType: "json"
            }).done(function(retData) {
                if(retData.errors == false) {
                    table.draw();
                    addAlert('success', 'New Pilot '+charName+' Added', true);
                    //$('.modal').modal('hide'); <--- doesn't work, but it should
                    $('#Modal').hide();
                    $('.modal-backdrop').hide();
                }  else {
                    //addAlert('danger','Mail rejected', true);
                    if(retData.errorMessage == '404 Character not found')
                        $('#pilotNameInput').addClass('form-error');
                    if(retData.errorMessage == '404 System not found')
                        $('#locationNameInput').addClass('form-error');

                    if(retData.errorMessage == 'Pilot already exists') {
                        addAlert('danger',retData.errorMessage, true);
                        $('#Modal').hide();
                        $('.modal-backdrop').hide();
                    }
                }  
            }).fail(function() {
                addAlert('danger', 'Unspecified Error', true);
            }).always(function() {
                //console.log( "complete" );
            });
        }
        
    });

	$('#supercaptable tbody').on('click', 'button.saveNotes', function () {
		var btn = $(this);
        var ele = $(this).prev('textarea');
        var charID = ele.data('charid');
        var txt = ele.val();

        var jqxhr = $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: BASEURL+"/supercapnotesupdate",
            data: { charID: charID, text: txt },
            dataType: "text"
		}).done(function(msg) {
			if(msg != "false") {
				ele.attr('data-text', escape(txt));
				ele.data('text', escape(txt));
				btn.attr('disabled','disabled');
				// add/remove icon
				td = ele.closest('tr').prev('tr').find('td.icon-images');
				var found = false
				td.children('img').each( function ( index ) {
					var src = $(this).attr('src');
					if(src.substr(-15) == 'chatchannel.png') {
						found = true;
						if(txt == '') {
							$(this).remove();
						}
					}
				});

				if(txt != '' && found == false) {
					td.prepend('<img src="{{ GetURLPath('/images/WindowIcons/chatchannel.png') }}">');
				}
                addAlert('success','Notes Saved', true);
			}	
		}).fail(function() {
	    	addAlert('danger', 'Unspecified Error, notes not saved', true);
		}).always(function() {
			//console.log( "complete" );
		});
    } );

    $('#supercaptable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        } else {

            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

	$('div.dataTables_filter input').attr('placeholder', 'Search');

} );

$('#excelExport').on('click', function() {
	var $preparingFileModal = $("#preparing-file-modal");
    
    var sortColumn, sortDirection;

    $('#supercaptable thead tr th').each( function ( index ) {
    	if($(this).hasClass('sorting_desc')) {
    		sortColumn = index;
    		sortDirection = "DESC";
    	}else if($(this).hasClass('sorting_asc')) {
    		sortColumn = index;
    		sortDirection = "ASC";
    	}
	});

    $preparingFileModal.dialog({ modal: true });
    $.fileDownload('supertracker/exceldownload', {
        successCallback: function(url) {
            $preparingFileModal.dialog('close');
        },
        failCallback: function(responseHtml, url) {
            $preparingFileModal.dialog('close');
            $("#error-modal").dialog({ modal: true });
        },
        httpMethod: "POST",
        data: { 
        	filterFromDate: $('#startdate').val(), 
        	filterToDate: $('#enddate').val(),
        	searchFilter: $('#supercaptable_filter input').val(),
        	sortColumn: sortColumn,
        	sortDirection: sortDirection
        },
	});

    return false;
});
</script>
@stop

