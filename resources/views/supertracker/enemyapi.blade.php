@extends('layouts.main')

@section('page_title')Pandemic Legion @stop

@section('body_class')body_class @stop

@section('content')
<div class="page">
    <div id="contents">
        <ol class="breadcrumb">
            <li><a href="{{GetURLPath('/supertracker/')}}">Super Tracker</a></li>
            <li class="active">API</li>
        </ol>
        <h1 class="text-center">
            <div class="inline">
                Pandemic Legion Enemy Super API Analysis Tool
            </div>
        </h1>
        <p>
            <b>API Key Analysis Tool:</b>
            <br />
            This tool will analyse API Keys and pull information related to supercaps. It accepts CAKs, either character, account or corporation.
            <ul>
                <li>Corporation Key<br />
                    Will pull the memberlist API and check which members own supercaps.<br />
                    Will fetch all corporation POS's and highlight systems where a supercap is known to be.<br />
                    Will retrieve bookmarks set to corporation highlighting ones which are in systems with supercaps.<br />
                </il>
                <li>Character/Account Keys<br />
                    Will pull characters from the key and check to see if any are in a Supercap.<br />
                    Will fetch assets and location API to determine the location of the ship within a system, listing the closest celestial and distance from it.<br />
                    Will retrieve character bookmarks highlighting ones which are in systems with the supercap.<br />
                </il>
            </ul>
        </p>
        <p>API Information will update Supertracker Information, Character Keys will be stored with the Character and can be used to update information from there.</p>
        <div class="row">
            <div id="KeysContainer" class="col-md-12 in">
                <div class="col-md-3"></div>
                <form method="post">
                    <div class="col-md-6">
                        <h3>Enter CAK:</h3>
                        <div class="row">
                            <div class="col-md-3 no-pad">
                                <input id="keyid" class="form-control" type="text" Placeholder="Key ID" name="keyid" />
                            </div>
                            <input id="addkey" class="btn btn-black right" type="submit" value="Check" name="submit" />
                        </div>
                        <input id="vcode" class="form-control" type="text" Placeholder="Verification Code" name="vcode" />
                    </div>
                </form>
                <div class="col-md-3"></div>
            </div>
        </div>
        <p></p>
        @if(isset($KeySupport))
        <div class="row">
            <h3>API Key Support List:</h3>
            <div class="col-md-12 in">
            @foreach($KeySupport as $key=>$value)
                <div class="col-md-3 no-pad">{{$key}}</div>
                <div class="col-md-3 no-pad">{{($value ? '<span class="text-green">True' : '<span class="text-red">False')}}</span></div>
            @endforeach
            </div>
        </div>
        @endif
        <p></p>
        @if(isset($CorpChars) && count($CorpChars) > 0)
        <div class="row">
            <h3 class="lineheader pl-icon">{{$CorpName}} Characters with SuperCaps</h3>
            <button type="button" class="btn btn-black btn-sm pull-right toggle" data-toggle="collapse" data-target="#CharsContainer">
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <div id="CharsContainer" class="col-md-12 in">
                <table id="charactersstable" class="table table-striped table-condensed-padding">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Ship</th>
                            <th>Location</th>
                            <th>Join Date</th>
                            <th>Last Logon Date</th>
                            <th>Last Logoff Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($CorpChars AS $char)
                        <tr>
                            <td>{{$char['name']}}</td>
                            <td>{{$char['title']}}</td>
                            <td>{{$char['shipType']}}</td>
                            <td>{{$char['location']}}</td>
                            <td>{{$char['startDateTime']}}</td>
                            <td>{{$char['logonDateTime']}}</td>
                            <td>{{$char['logoffDateTime']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
               
            </div>
        </div>
        @endif

        @if(isset($CorpPos) && count($CorpPos) > 0)
        <div class="row">
            <h3 class="lineheader pl-icon">{{$CorpName}} POS Locations (ones where Super is in system are highlighted)</h3>
            <button type="button" class="btn btn-black btn-sm pull-right toggle" data-toggle="collapse" data-target="#POSContainer">
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <div id="POSContainer" class="col-md-12 in">
                <table id="postable" class="table table-striped table-condensed-padding">
                    <thead>
                        <tr>
                            <th>Moon</th>
                            <th>Onlined</th>
                            <th>State</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($CorpPos AS $pos)
                        @if($pos['relevent'] == 1)
                        <tr class="table-row-highlight">
                        @else
                        <tr>
                        @endif
                            <td>{{$pos['moonName']}}</td>
                            <td>{{$pos['onlineTimestamp']}}</td>
                            <td>{{$pos['stateName']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
               
            </div>
        </div>
        @endif

        @if(isset($CorpBMs) && count($CorpBMs) > 0)
        <div class="row">
            <h3 class="lineheader pl-icon">{{$CorpName}} BookMarks (ones where Super is in system are highlighted)</h3>
            <button type="button" class="btn btn-black btn-sm pull-right toggle" data-toggle="collapse" data-target="#BMsContainer">
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <div id="BMsContainer" class="col-md-12 in">
                <table id="bmstable" class="table table-striped table-condensed-padding">
                    <thead>
                        <tr>
                            <th>Folder Name</th>
                            <th>System</th>
                            <th>Closest Celestial</th>
                            <th>Distance</th>
                            <th>Memo</th>
                            <th>Note</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($CorpBMs AS $bm)
                        @if($bm['relevent'] == 1)
                        <tr class="table-row-highlight">
                        @else
                        <tr>
                        @endif
                            <td>{{$bm['folderName']}}</td>
                            <td>{{$bm['systemName']}}</td>
                            <td>{{$bm['closestName']}}</td>
                            <td>{{number_format($bm['closestDistance']/1000,1)}} km</td>
                            <td>{{$bm['memo']}}</td>
                            <td>{{$bm['note']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
               
            </div>
        </div>
        @endif

        @if(isset($Chars) && count($Chars) > 0)
        <div class="row">
            <h3 class="lineheader pl-icon">Characters with SuperCaps</h3>
            <button type="button" class="btn btn-black btn-sm pull-right toggle" data-toggle="collapse" data-target="#CharactersContainer">
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <div id="CharactersContainer" class="col-md-12 in">
                <table id="charactersstable" class="table table-striped table-condensed-padding">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Ship Name</th>
                            <th>Ship</th>
                            <th>Location</th>
                            <th>Corporation</th>
                            <th>Alliance</th>
                            <th>Join Date</th>
                            <th>Closest Celestial</th>
                            <th>Distance</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($Chars AS $char)
                        <tr>
                            <td>{{$char['characterName']}}</td>
                            <td>{{$char['shipName']}}</td>
                            <td>{{$char['shipTypeName']}}</td>
                            <td>{{$char['lastKnownLocation']}}</td>
                            <td>{{$char['corporation']}}</td>
                            <td>{{$char['alliance']}}</td>
                            <td>{{$char['corporationDate']}}</td>
                            <td>{{$char['closestName']}}</td>
                            <td>{{number_format($char['closestDistance']/1000,1)}} km</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
               
            </div>
        </div>
        @endif

        @if(isset($CharBMs) && count($CharBMs) > 0)
        <div class="row">
            <h3 class="lineheader pl-icon">{{$CorpName}} BookMarks (ones where Super is in system are highlighted)</h3>
            <button type="button" class="btn btn-black btn-sm pull-right toggle" data-toggle="collapse" data-target="#CharBMsContainer">
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <div id="CharBMsContainer" class="col-md-12 in">
                <table id="bmstable" class="table table-striped table-condensed-padding">
                    <thead>
                        <tr>
                            <th>Folder Name</th>
                            <th>System</th>
                            <th>Closest Celestial</th>
                            <th>Distance</th>
                            <th>Memo</th>
                            <th>Note</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($CharBMs AS $bm)
                        @if($bm['relevent'] == 1)
                        <tr class="table-row-highlight">
                        @else
                        <tr>
                        @endif
                            <td>{{$bm['folderName']}}</td>
                            <td>{{$bm['systemName']}}</td>
                            <td>{{$bm['closestName']}}</td>
                            <td>{{number_format($bm['closestDistance']/1000,1)}} km</td>
                            <td>{{$bm['memo']}}</td>
                            <td>{{$bm['note']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>      
            </div>
        </div>
        @endif
    </div>
</div>
@stop
@section('additional_js')
<script>
$(function () {
    $('#addkey').click(function(e) {
        var val = $('#keyid').val();
        if(val == '') {
            e.preventDefault();
            $('#keyid').addClass('form-error');
        }
        var val = $('#vcode').val();
        if(val == '') {
            e.preventDefault();
            $('#vcode').addClass('form-error');
        }
    });

    $('#keyid').focus(function(e) {
        if($('#keyid').hasClass('form-error')) {
            $('#keyid').removeClass('form-error');
        }
    });
    $('#vcode').focus(function(e) {
        if($('#vcode').hasClass('form-error')) {
            $('#vcode').removeClass('form-error');
        }
    });

});
</script>
@stop