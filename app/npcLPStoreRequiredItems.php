<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class npcLPStoreRequiredItems extends Model
{
    public $primaryKey = 'storeItemID';
    public $timestamps = false;
    protected $table = 'npcLPStoreRequiredItems';
    protected $fillable = [ 'storeItemID', 
    						'typeID', 
    						'typeName',
    						'quantity'];
}
