<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@getIndex');
Route::get('legal', 'MainController@legal');
Route::get('ccpcopyright', 'MainController@ccpcopyright');


Route::get('teambuilder', 'TournyController@getShipbuilder');
Route::get('tournaments', 'TournyController@getIndex');
Route::get('tournaments/{id}', 'TournyController@getTournament');
Route::get('tournaments/{id}/teams/{teamid}', 'TournyController@getTeam');
Route::get('tournaments/teams/{teamid}', 'TournyController@getTeaminfo');
Route::get('tournaments/{id}/series', 'TournyController@getSeries');
Route::get('tournaments/teams/{teamid}/members', 'TournyController@getMembers');
Route::get('tournaments/{id}/series/{seriesID}/matches/{matchID}', 'TournyController@getMatch');



Route::get('auth', 'AuthController@getAuth');
Route::get('logout', 'AuthController@logout');

/* TESTS */
// Routes that need to have EveLogin checks (redirects to login if not logged in)
Route::get('ssotest', [
    'middleware' => 'ssoauth',
    'uses' => 'TestController@getSSOIndex'
]);

Route::get('test', 'TestController@getIndex');
//Route::get('crest/{name1}/{name2?}', 'CrestController@getCrest');
Route::get('character/{urlID?}', 'CrestController@getCharacter');
//Route::post('loyaltypointstore', 'CrestController@postViewlpstore');
//Route::resource('crest', 'CrestController');
/*Route::resource('supertracker', 'SuperTrackerController', ['only' => [
    'index', 'show', 'store'
]]);/**/
//Route::controller('supertracker', 'SupertrawlerController');
Route::get('supertracker', 'SuperTrackerController@getIndex');
Route::get('supercapapi', 'SuperTrackerController@getSuperapi');

//Route::group(['middleware' => ['web']], function () {
    Route::post('ajax', 'TournyController@postTournament');
	Route::post('shipdata', 'TournyController@postShipdata');
	Route::post('loyaltypointstore', 'CrestController@postViewlpstore');
	Route::post('agentfinder', 'CrestController@postAgentfinder');
	Route::post('waypoint', 'CrestController@postWaypoint');
	Route::post('supercaptable', 'SuperTrackerController@postSupercaptable');
	Route::post('supercapnotesupdate', 'SuperTrackerController@postSupercapnotesupdate');
	Route::post('supercapapi', 'SuperTrackerController@postSuperapi');
//});

