<?php

namespace Thunk\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Session;
use Thunk\Classes\Crest;

class EveSSOAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // check if Session has SSO Login info
        if(session()->has('sso_characterid') && session()->has('sso_expires')) {
            return $next($request);
        }

        //return redirect('/');
        // Must be Authenticated and isn't, so redirect to EVE Login
        $url = config('tools.sso-auth-url').'/?response_type=code&redirect_uri='.env('CREST_APP_URL').'&client_id='.env('CREST_APP_ID').'&scope='.Crest::setSSOScope().'&state='.Crest::setSSOState();

        return redirect($url);
    }
}
