<?php

namespace Thunk\Http\Middleware;

//use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Session;
use Thunk\Character;
use Thunk\Classes\Crest;
use Thunk\Classes\pageLogger;
use Illuminate\Support\Facades\Request;

class EveSSOLocation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if logged in, fetch location
        if(session()->has('sso_characterid')) {
            Crest::returnToRoot();
            Crest::walk('decode');
            Crest::walk('character');
            Crest::walk('location');
            $result = Crest::get();

            if($result) {
                $location = $result;

                if($location !== null && $location != '' && (is_object($location) && (count(get_object_vars($location)) > 0))) {
                    // logged in
                    session(['sso_isloggedin' => true]);
                    if(isset($location->solarSystem)) {
                        session(['sso_solarsystemid' => $location->solarSystem->id]);
                        session(['sso_solarsystemname' => $location->solarSystem->name]);
                        $lastLocationName = $location->solarSystem->name;
                        $lastLocationID = $location->solarSystem->id;
                    } else {
                        session()->forget('sso_solarsystemid');
                        session()->forget('sso_solarsystemname');
                    }

                    if(isset($location->station)) {
                        session(['sso_stationid' => $location->station->id]);
                        session(['sso_stationname' => $location->station->name]);
                        $lastLocationName = $location->station->name;
                        $lastLocationID = $location->station->id;
                    } else {
                        session()->forget('sso_stationid');
                        session()->forget('sso_stationname');
                    }

                    // update crestCharacter
                    $char = Character::find(session('sso_characterid'));

                    if(!$char) {
                        $char = new Character;
                        $char->characterID = session('sso_characterid');
                        $char->characterName = session('sso_charactername');
                    }

                    $char->lastLocationName = $lastLocationName;
                    $char->lastLocationID = $lastLocationID;

                    $char->save();
                    
                } else {
                    // logged out
                    session(['sso_isloggedin' => false]);
                    session()->forget('sso_solarsystemid');
                    session()->forget('sso_solarsystemname');
                    session()->forget('sso_stationid');
                    session()->forget('sso_stationname');
                }
            }
        }

        // log page being viewed
        if(!Request::ajax()) {
            if(session()->has('sso_characterid') && session()->has('sso_charactername')) {
                PageLogger::log('info', session('sso_charactername').' - '.url()->full());
            } else {
                PageLogger::log('info', 'Anonymous - '.url()->full());
            }
        }

        return $next($request);
    }
}
