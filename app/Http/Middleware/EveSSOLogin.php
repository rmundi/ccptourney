<?php

namespace Thunk\Http\Middleware;

use Thunk\User;
use Thunk\Classes\Crest;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Session;

class EveSSOLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // check if Session has SSO Login info
        if(session()->has('sso_characterid') && session()->has('sso_expires')) {
            $expires = session('sso_expires');

            // catch incase expires is still in timestring format, it shouldn't be
            if(strtotime($expires) !== false)
                $expires = strtotime($expires);

            if($expires-time() < 0) {
                if(session()->has('sso_refreshtoken')) {
                    // refresh the session - maintains session when the auth has a scope request
                    $refresh = Crest::refreshSSOSession(session('sso_refreshtoken'));

                    $response = json_decode($refresh['data']);
                    $authToken = $response->access_token;
                    $refreshToken = $response->refresh_token;

                    session(['sso_accesstoken' => $authToken]);
                    session(['sso_expires' => time()+$response->expires_in]);
                    if(isset($refreshToken) && $refreshToken !== null)
                        session(['sso_refreshtoken' => $refreshToken]);
                    
                    // x-real-ip
                    $ip = $request->header('x-real-ip');
                    if(!isset($ip))
                        $ip = $request->getClientIp();

                    // update model
/*                    $user = User::where('characterID', session('sso_characterid'))
                                ->where('characterHash', session('sso_characterhash'))
                                ->first();

                    if($user !== null) {
                        $user->characterID = session('sso_characterid');
                        $user->characterHash = session('sso_characterhash');
                        $user->characterName = session('sso_charactername');
                        $user->accessToken = $authToken;
                        $user->refreshToken = $refreshToken;
                        $user->expires = time()+$response->expires_in;
                        $user->ip = $ip;
                        $user->userAgent = $request->header('user-agent');
                    } else {
                        User::where('characterID', session('sso_characterid'))
                              ->where('characterHash', session('sso_characterhash'))
                              ->update(['accessToken' => $authToken, 'refreshToken' => $refreshToken, 'expires' => time()+$response->expires_in, 'ip' => $ip, 'userAgent' => $request->header('user-agent')]);
                    } 
                    
                    $user->save();
                    /**/

                    User::where('characterID', session('sso_characterid'))
                              ->where('characterHash', session('sso_characterhash'))
                              ->update(['accessToken' => $authToken, 'refreshToken' => $refreshToken, 'expires' => time()+$response->expires_in, 'ip' => $ip, 'userAgent' => $request->header('user-agent')]);

                    //session(['infomessage' => 'Refreshed Token']);

                } else {
                    // Session has expired and not able to refresh, so clear session and send home
                    Crest::SSOLogout();

                    return redirect('/');
                }
            }
        }

        return $next($request);
    }
}
