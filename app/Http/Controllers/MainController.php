<?php

namespace Thunk\Http\Controllers;

use Illuminate\Http\Request;

use Thunk\Http\Requests;

class MainController extends Controller
{
    public function getIndex() {
    	
		return View('index');

	}

	public function legal()
	{
		return View('textoutput', array('TEXT' => nl2br(file_get_contents(base_path().'/legal.txt'))));
	}

	public function ccpcopyright()
	{
		return View('textoutput', array('TEXT' => nl2br(file_get_contents(base_path().'/ccpcopyright.txt'))));
	}
}
