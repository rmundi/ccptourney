<?php

namespace Thunk\Http\Controllers;

use Illuminate\Http\Request;
use Thunk\Classes\SuperTracker;
use Thunk\Http\Requests;

class SuperTrackerController extends Controller
{
	public function __construct() {
		$this->middleware('ssoauth');
		/*$this->middleware('ssoauth', ['except' => [
            '',
            '',
            '',
            
        ]]);*/
	}

    public function getIndex() {
		return View('supertracker.index');
	}

	public function getSuperapi() {
		return View('supertracker.enemyapi');
	}

	public function postSuperapi(Request $request) {
	
		// if not logged in, redirect index
		//if(!$PLAuth['ye_old_fishermen_admin'] == 1) {
		//	Session::flash('errormessage', 'You do not have the required permissions.');
		//	return Redirect::to('/');
		//}
		
		//echo "adding of the key";
		$keyID = $request->Input('keyid');
        $vCode = $request->Input('vcode');

		// Key Checks
		$xml = api::fetchXML("account/APIKeyInfo.xml.aspx?keyID=".$keyID."&vCode=".$vCode, true);
	
		$myAccountChars = api::AccountCAKProcessXML($xml);
	
		if(api::getErrorCode()) {
			Session::flash('errormessage', api::getError());
			return Redirect::to(Request::url());
		}

		$keyType = $myAccountChars['type'];
		if($keyType == 'Account')
			$keyType = 'Character';

		$keyAbilities = api::checkAccessMask($keyType, $myAccountChars['accessMask']);

		// check isn't PL and log to security
		$isPL = false;
		foreach($myAccountChars as $account) {
			if(isset($account['allianceName'])) 
				if($account['allianceName'] == "Pandemic Legion" || $account['allianceName'] == "WAFFLES." || $account['allianceName'] == "Pandemic Horde")
					$isPL = true;
		}
			
		if($isPL && $PLAuth['logged_kindergarten'] == 0) {
			securityLog::log('info', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to Check a PL CAK (denied) '.Request::url());
			Session::flash('errormessage', 'That isn\'t going to happen');
			return Redirect::to(Request::url());
		} elseif($isPL) {
			securityLog::log('info', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to Check a PL CAK (permitted) '.Request::url());
		}

		$supersArray = array("Aeon", "Wyvern", "Nyx", "Hel", "Avatar", "Leviathan", "Erebus", "Ragnarok", "Revenant");

		$charList = array();
		$posList = array();
		$bmList = array();
		$corpName = null;
		$charNameArray = array();

		if($myAccountChars['type'] == "Corporation") {
			// Corporation Key

			// get memberlist. Show Supers
			
			$xml = api::fetchXML("Corp/MemberTracking.xml.aspx?keyID=".$keyID."&vCode=".$vCode."&characterID=".$myAccountChars[0]['characterID'], true);
			$data = api::MemberTrackingProcessXML($xml, false);
			if($data) {
				if(!api::getError()) {
					$corpName = $myAccountChars[0]['corporationName'];
					foreach($data as $row) {
						if(in_array((string)$row['shipType'],$supersArray)) {
							$charList[] = $row;

							// add/update supercaps info
							if(!$isPL) {
								corekb::setSuperCapPilotInfoFromCorpAPI((integer)$row["characterID"], (string)$row["name"], (integer)$row["shipTypeID"], (string)$row["shipType"], 
									(integer)$row["locationID"], (string)$row["location"], $myAccountChars[0]['corporationID'], $myAccountChars[0]['corporationName'], 
									$myAccountChars[0]['allianceID'], $myAccountChars[0]['allianceName'], $myAccountChars[0]['factionID'], $myAccountChars[0]['factionName'], (string)$row["logoffDateTime"]);
								Session::flash('successmessage', 'Members with Supercaps added to SuperTracker');
							} else {
								Session::flash('errormessage', 'Members not added to SuperTracker: PL Members');
							}
						}
					}
				}
			}

			if(count($charList) == 0) {
				Session::flash('infomessage', 'This Corp has no Super Capital Ships');
				return Redirect::to(Request::url());
			}

			// get pos's
			$xml = api::fetchXML("Corp/StarbaseList.xml.aspx?keyID=".$keyID."&vCode=".$vCode."&characterID=".$myAccountChars[0]['characterID'], true);
			$data = api::StarbaseListProcessXML($xml, false);
			if($data) {
				foreach($data as $row) {
					$relevent = false;
					foreach($charList as $char) {
						if((string)$row['locationID'] == (string)$char['locationID']) {
							$relevent = true;
						}
					}
					$row['relevent'] = $relevent;
					$row['moonName'] = invNamesCCP::getName((integer)$row['moonID']);
					if((string)$row['state'] == 0) {
						$row['stateName'] = "Unanchored";
					} elseif((string)$row['state'] == 1) {
						$row['stateName'] = "Anchored / Offline ";
					} elseif((string)$row['state'] == 2) {
						$row['stateName'] = "Onlining";
					} elseif((string)$row['state'] == 3) {
						$row['stateName'] = "Reinforced";
					} elseif((string)$row['state'] == 4) {
						$row['stateName'] = "Online";
					}
					$posList[] = $row;
				}
			}

			// get bm's
			$xml = api::fetchXML("Corp/Bookmarks.xml.aspx?keyID=".$keyID."&vCode=".$vCode."&characterID=".$myAccountChars[0]['characterID'], true);
			$data = api::BookmarksProcessXML($xml, false);
			if($data) {
				foreach($data as $row) {
					$relevent = false;
					foreach($charList as $char) {
						if((string)$row['locationID'] == (string)$char['locationID']) {
							$relevent = true;
						}
					}
					$row['systemName'] = invNamesCCP::getName((integer)$row['locationID']);
					$row['relevent'] = $relevent;
					$bmList[] = (array)$row;
				}
			}
			// what does the key support
			$support['MemberTrackingLimited'] = $keyAbilities['MemberTrackingLimited'];
			$support['MemberTrackingExtended'] = $keyAbilities['MemberTrackingExtended'];
			$support['StarbaseList'] = $keyAbilities['StarbaseList'];
			$support['Bookmarks'] = $keyAbilities['Bookmarks'];
		} else {
			// Character or Account
			// get assets and location
			//dd($myAccountChars);
			$charArray = array();
			if(isset($myAccountChars[0]['characterID']))
				$charArray[] = (integer)$myAccountChars[0]['characterID'];
			if(isset($myAccountChars[1]['characterID']))
				$charArray[] = (integer)$myAccountChars[1]['characterID'];
			if(isset($myAccountChars[2]['characterID']))
				$charArray[] = (integer)$myAccountChars[2]['characterID'];

			if(count($charArray) == 0) {
				// this should never happen
				Session::flash('errormessage', 'No Characters');
				return Redirect::to(Request::url());
			}

			// do char sheets
			$charsList = array();
			$charbmList = array();

			foreach($charArray as $char) {
				$xml = api::fetchXML("Eve/CharacterInfo.xml.aspx?keyID=".$keyID."&vCode=".$vCode."&characterID=".$char, true);
				$data = api::CharacterInfoProcessXML($xml, false);

				if($data) {
					if(in_array((string)$data['shipTypeName'],$supersArray)) {
						// get assets for location
						$xml = api::fetchXML("Char/AssetList.xml.aspx?keyID=".$keyID."&vCode=".$vCode."&characterID=".$char, true);
						$assets = api::AssetListProcessXML($xml, false);
						if($assets) {
							foreach($assets as $asset) {
								$locationName = invNamesCCP::getName((integer)$asset['locationID']);
								if($asset['typeID'] == (integer)$data['shipTypeID'] && $locationName == (string)$data['lastKnownLocation']) {
									//var_dump($asset['itemID']);
									// get location
									$xml = api::fetchXML("Char/Locations.xml.aspx?keyID=".$keyID."&vCode=".$vCode."&characterID=".$char."&ids=".(string)$asset['itemID'], true);
									$data['itemID'] = (string)$asset['itemID'];
									$data['x'] = (string)$xml->result->rowset->row['x'];
									$data['x'] = (string)$xml->result->rowset->row['x'];
									$data['y'] = (string)$xml->result->rowset->row['y'];
									$data['z'] = (string)$xml->result->rowset->row['z'];
									
									$closest = mapSolarSystemCCP::getNearestCelestialObject((integer)$asset['locationID'], (string)$data['x'], (string)$data['y'], (string)$data['z']);
									$data['closestName'] = $closest['name'];
									$data['closestDistance'] = $closest['distance'];
								}
							}
							if(!$isPL) {
								corekb::setSuperCapPilotInfoFromCharAPI($data["characterID"], $data["characterName"], $data["shipTypeID"], $data["shipTypeName"], $data['lastKnownLocation'],
									$data['corporationID'], $data['corporation'], $data['allianceID'], $data['alliance'], $myAccountChars[0]['factionID'], $myAccountChars[0]['factionName'], 
									$keyID, $vCode, $data['itemID'], $data['x'], $data['y'], $data['z'], $data['closestName'], $data['closestDistance'], date("Y-m-d H:i:s", time()), $data['shipName']);
								Session::flash('successmessage', 'Key added to SuperTracker');
							} else {
								Session::flash('errormessage', 'Key not added to SuperTracker: PL Key');
							}
						} else {
							Session::flash('errormessage', 'Key not added to SuperTracker: No Assets access');
						}

						$charsList[] = $data;

						// add/update supercaps info
						
						// get bms
						$xml = api::fetchXML("Char/Bookmarks.xml.aspx?keyID=".$keyID."&vCode=".$vCode."&characterID=".$char, true);
						$bmdata = api::BookmarksProcessXML($xml, false);
						if($bmdata) {
							foreach($bmdata as $row) {
								$relevent = false;
								foreach($charArray as $character) {
									$systemName = invNamesCCP::getName((integer)$row['locationID']);
									if($systemName == (string)$data['lastKnownLocation']) {
										$relevent = true;
									}
								}
								$row['systemName'] = invNamesCCP::getName((integer)$row['locationID']);
								$row['relevent'] = $relevent;
								$charbmList[] = $row;
							}
						}
					}
				}
			}

			if(count($charsList) == 0) {
				Session::flash('infomessage', 'There are no Characters with Super Caps');
				return Redirect::to(Request::url());
			}

			// what does the key support
			$support['CharacterInfoExt'] = $keyAbilities['CharacterInfoExt'];
			$support['AssetList'] = $keyAbilities['AssetList'];
			$support['Locations'] = $keyAbilities['Locations'];
			$support['Bookmarks'] = $keyAbilities['Bookmarks'];
		}
//dd($charbmList);
		return View::make('supertrawler.enemyapi', array('PLAuth' => $PLAuth, 'CorpName' => $corpName, 'CorpChars' => $charList, 'CorpPos' => $posList, 'CorpBMs' => $bmList, 'Chars' => $charsList, 'CharBMs' => $charbmList, 'KeySupport' => $support));
	}

	public function postSupercaptable(Request $request) {
		
		$filterStartDate = $request->Input('filterStartDate');
        $filterEndDate = $request->Input('filterEndDate');

		if (!$request->ajax()) {
			// log to security as some form of attack
			//securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to access Supercap table page: '.Request::url().' using a non-Ajax method');

			return 'Denied.';
		}

		return SuperTracker::getEveSuperPilotTableData($filterStartDate, $filterEndDate);
	}

	public function postSupercapnotesupdate(Request $request) {
	
		if (!$request->ajax()) {
			// log to security as some form of attack
			//securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to access Supercap Update Location page: '.Request::url().' using a non-Ajax method');

			return 'Denied.';
		}

		$charID = $request->Input('charID');
        $text = $request->Input('text');

		$scUser = SuperTracker::setSuperCapPilotNotes($charID, $text);
		//securityLog::log('info', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Updated SuperTracker Notes for: '.$charID);
		return "true";
	
		// else report to security log
		//securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to access: '.Request::url());
		//return "false";
		
	}

	public function postSupercapapilocupdate(Request $request) {
		
		if (!Request::ajax()) {
			// log to security as some form of attack
			//securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to access Supercap Update location from API: '.Request::url().' using a non-Ajax method');

			return 'Denied.';
		}

		
		$charID = $request->Input('charID');

		return SuperTracker::UpdateCharWithAPI($charID);
		
			// else report to security log
		//	securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to access: '.Request::url());
		//	return "false";
		
	}
}
