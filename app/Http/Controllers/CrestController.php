<?php

namespace Thunk\Http\Controllers;

use Illuminate\Http\Request;
use Thunk\CharacterInfo;
use Thunk\EmploymentHistory;
use Thunk\CharacterCloneImplants;
use Thunk\CharacterClones;
use Thunk\Character;
use Thunk\CharacterSkills;
use Thunk\SkillQueue;
use Thunk\eveSkillTree;
use Thunk\apiMedals;
use Thunk\eveRequiredSkills;
use Thunk\crestLoyaltyPoints;
use Thunk\npcLPStore;
use Thunk\apiAccountStatus;
use Thunk\apiStanding;
use Thunk\agtAgents;
use Thunk\Http\Requests;
use Thunk\Classes\Crest;

class CrestController extends Controller
{
	private static $root;

	public function __construct() {
		//$this->middleware('ssoauth');
		$this->middleware('ssoauth', ['except' => [
            'getCharacter',
            'postViewlpstore',
            'postAgentfinder',
            
        ]]);

		static::$root = self::crestRoot();
	}
/*
	public function getCrest($name1, $name2 = null) {

		//$root = self::crestRoot();
		//dd(static::$root->motd->dust->href);
		//dd(self::crestRoot());
 		//self::crestPage(array('motd', 'eve'));
 		
 		//dd(self::crestPage('tournaments'));

		if($name2 !== null)
			$json = self::crestPage(array($name1, $name2));
		else 
			$json = self::crestPage($name1);

 		dd($json);
	}
*/
	public function getCharacter($urlID = null) {
		
		if($urlID !== null && !is_numeric($urlID)) {
			// $id is a string - convert it to a id
			$name = urldecode($urlID);

			$res = apiResolveName($name);
//dd($res);
			$id = $res[$name];

			if($id == false)
				$id = null;

		} elseif(is_numeric($urlID)) {
			$id = $urlID;
		}
//dd($id);
		if(($id == session('sso_characterid') || $id === null) && session()->has('sso_characterid')) {
			$id = session('sso_characterid');

			Crest::returnToRoot();
			Crest::walk('decode');
			Crest::walk('character');
			$character = Crest::get();

			//$character = getCrestPage(config('tools.root').'characters/'.session('sso_characterid').'/');
			$charInfo = Crest::getAPIPage('eve', 'CharacterInfo', array('k' => 'accessToken', 'v' => session('sso_accesstoken')));
			$charClones = Crest::getAPIPage('char', 'Clones', array('k' => 'accessToken', 'v' => session('sso_accesstoken')));
			$charSkills = Crest::getAPIPage('char', 'Skills', array('k' => 'accessToken', 'v' => session('sso_accesstoken')));
			$charSkillQueue = Crest::getAPIPage('char', 'SkillQueue', array('k' => 'accessToken', 'v' => session('sso_accesstoken')));
			$charBalance = Crest::getAPIPage('char', 'AccountBalance', array('k' => 'accessToken', 'v' => session('sso_accesstoken')));
			$charMedals = Crest::getAPIPage('char', 'Medals', array('k' => 'accessToken', 'v' => session('sso_accesstoken')));

			Crest::returnToRoot();
			Crest::walk('decode');
			Crest::walk('character');
			Crest::walk('loyaltyPoints');
			$loyaltyPoints = Crest::get();

			//$loyaltyPoints = getCrestPage(config('tools.root').'characters/'.session('sso_characterid').'/loyaltypoints/');
			$accountStatus = Crest::getAPIPage('account', 'AccountStatus', array('k' => 'accessToken', 'v' => session('sso_accesstoken')));
			$standings = Crest::getAPIPage('char', 'Standings', array('k' => 'accessToken', 'v' => session('sso_accesstoken')));

			/* AccountStatus
			"paidUntil" => "2016-06-14 17:39:00"
	        "createDate" => "2006-03-05 12:27:00"
	        "logonCount" => "5612"
	        "logonMinutes" => "584328"
	        "multiCharacterTraining" => RowSet {#242 ▶}
	        "Offers" => RowSet {#237 ▶}
	        */
			// TODO EVESTATS
			// TODO determine which is most update $character/$charInfo and use that for things like corp/alliance

			// get skill points total
			$skillPoints = 0;
			$freeSkillPoints = $charSkills->freeSkillPoints;

			foreach($charSkills->skills as $skill) {
				$skillPoints += $skill->skillpoints;
			}

			// update CharacterInfo model
			$info = CharacterInfo::find($charInfo->characterID);

			if(!$info)
				$info = new CharacterInfo;

			$info->characterID = $charInfo->characterID;
			$info->characterName = $charInfo->characterName;
			$info->race = $charInfo->race;
			$info->bloodLineID = $charInfo->bloodlineID;
			$info->bloodLine = $charInfo->bloodline;
			$info->ancestryID = $charInfo->ancestryID;
			$info->ancestry = $charInfo->ancestry;
			$info->accountBalance = $charBalance->accounts[0]->balance;
			//$info->skillPoints = $skillPoints;
			//$info->shipName = $charInfo->shipName;
			//$info->shipTypeID = $charInfo->shipTypeID;
			//$info->shipTypeName = $charInfo->shipTypeName;
			$info->corporationID = $charInfo->corporationID;
			$info->corporationName = $charInfo->corporation;
			$info->corporationDate = $charInfo->corporationDate;
			$info->allianceID = $charInfo->allianceID;
			$info->allianceName = $charInfo->alliance;
			$info->allianceDate = $charInfo->allianceDate;
			//$info->lastKnownLocation = $charInfo->lastKnownLocation;
			//$info->lastKnownLocationID = $charInfo->lastKnownLocationID;
			$info->securityStatus = $charInfo->securityStatus;

			$info->save();

			// delete all character employment history
			$deletedRows = EmploymentHistory::where('characterID', $charInfo->characterID)->delete();

			// add employment history
			foreach($charInfo->employmentHistory as $row) {
				$employment = EmploymentHistory::create(['characterID' => $charInfo->characterID, 'recordID' => $row->recordID, 'corporationID' => $row->corporationID, 'corporationName' => $row->corporationName, 'startDate' => $row->startDate ]);
			}

			// update CREST Character model
			$char = Character::find($charInfo->characterID);

			if(!$char)
				$char = new Character;

			$char->characterID = $charInfo->characterID;
			$char->characterName = $charInfo->characterName;
			$char->DoB = $charClones->DoB;
			$char->race = $charClones->race;
			$char->bloodLineID = $charClones->bloodLineID;
			$char->bloodLine = $charClones->bloodLine;
			$char->ancestryID = $charClones->ancestryID;
			$char->ancestry = $charClones->ancestry;
			$char->gender = $charClones->gender;
			$char->skillPoints = $skillPoints;
			$char->freeRespecs = $charClones->freeRespecs;
			$char->freeSkillPoints = $charSkills->freeSkillPoints;
			$char->cloneJumpDate = $charClones->cloneJumpDate;
			$char->lastRespecDate = $charClones->lastRespecDate;
			$char->lastTimedRespec = $charClones->lastTimedRespec;
			$char->remoteStationDate = $charClones->remoteStationDate;
			$char->corporationID = $charInfo->corporationID;
			$char->corporationName = $charInfo->corporation;
			$char->corporationDate = $charInfo->corporationDate;
			$char->allianceID = $charInfo->allianceID;
			$char->allianceName = $charInfo->alliance;
			$char->allianceDate = $charInfo->allianceDate;
			$char->charisma = $charClones->attributes->charisma;
			$char->intelligence = $charClones->attributes->intelligence;
			$char->memory = $charClones->attributes->memory;
			$char->perception = $charClones->attributes->perception;
			$char->willpower = $charClones->attributes->willpower;
			$char->jumpActivation = $charClones->jumpActivation;
			$char->jumpFatigue = $charClones->jumpFatigue;
			$char->jumpLastUpdate = $charClones->jumpLastUpdate;
			//$char->lastLocationName = $charClones->lastKnownLocation;
			//$char->lastLocationID = $charClones->lastKnownLocationID;
			$char->description = $character->description;

			$char->save();

			// update apiAccountStatus model
			$account = apiAccountStatus::find($charInfo->characterID);

			if(!$account)
				$account = new apiAccountStatus;

			$account->characterID = $charInfo->characterID;
			$account->paidUntil = $accountStatus->paidUntil;
			$account->createDate = $accountStatus->createDate;
			$account->logonCount = $accountStatus->logonCount;
			$account->logonMinutes = $accountStatus->logonMinutes;

			$account->save();

			// delete all jump clone implants
			$deletedRows = CharacterCloneImplants::where('characterID', $charInfo->characterID)->delete();

			// add current clone implants
			foreach($charClones->implants as $row) {
				$implants = CharacterCloneImplants::create(['characterID' => $charInfo->characterID, 'jumpCloneID' => null, 'typeID' => $row->typeID, 'typeName' => $row->typeName]);
			}

			// add other clone implants
			foreach($charClones->jumpCloneImplants as $row) {
				$jcimplants = CharacterCloneImplants::create(['characterID' => $charInfo->characterID, 'jumpCloneID' => $row->jumpCloneID, 'typeID' => $row->typeID, 'typeName' => $row->typeName]);
			}

			// delete all jump clones
			$deletedRows = CharacterClones::where('characterID', $charInfo->characterID)->delete();

			// add current clones
			foreach($charClones->jumpClones as $row) {
				$clones = CharacterClones::create(['characterID' => $charInfo->characterID, 'jumpCloneID' => $row->jumpCloneID, 'typeID' => $row->typeID, 'locationID' => $row->locationID, 'cloneName' => $row->cloneName]);
			}

			// delete all char skills
			$deletedRows = CharacterSkills::where('characterID', $charInfo->characterID)->delete();

			// add current skills
			foreach($charSkills->skills as $row) {
				$skill = CharacterSkills::create(['characterID' => $charInfo->characterID, 'typeID' => $row->typeID, 'typeName' => $row->typeName, 'skillPoints' => $row->skillpoints, 'level' => $row->level, 'published' => $row->published]);
			}

			// delete SkillQueue
			$deletedRows = SkillQueue::where('characterID', $charInfo->characterID)->delete();

			// add Skill Queue
			foreach($charSkillQueue->skillqueue as $row) {
				$queue = SkillQueue::create([	'characterID' => $charInfo->characterID, 
												'queuePosition' => $row->queuePosition, 
												'typeID' => $row->typeID, 
												'level' => $row->level, 
												'startSP' => $row->startSP, 
												'endSP' => $row->endSP, 
												'startTime' => $row->startTime, 
												'endTime' => $row->endTime]);
			}


			// delete all medals
			$deletedRows = apiMedals::where('characterID', $charInfo->characterID)->delete();


			// get ID List and resolve
			$idList = array();

			foreach($charMedals->currentCorporation as $row) {
				$idList[$row->issuerID] = $row->issuerID;
				$idList[$row->corporationID] = $row->corporationID;
			}

			foreach($charMedals->otherCorporations as $row) {
				$idList[$row->issuerID] = $row->issuerID;
				$idList[$row->corporationID] = $row->corporationID;
			}

			$resolvedIDs = apiResolveID($idList);

			foreach($charMedals->currentCorporation as $row) {
				$medal = apiMedals::create([	'characterID' => $charInfo->characterID, 
												'medalID' => $row->medalID, 
							    				'reason' => $row->reason,
					    						'status' => $row->status, 
					    						'issuerID' => $row->issuerID, 
					    						'issuerName' => $resolvedIDs[$row->issuerID], 
					    						'issued' => $row->issued, 
					    						'corporationID' => $row->corporationID, 
					    						'corporationName' => $resolvedIDs[$row->corporationID], 
					    						'title' => $row->title, 
					    						'description' => $row->description]);
			}

			foreach($charMedals->otherCorporations as $row) {
				$medal = apiMedals::create([	'characterID' => $charInfo->characterID, 
												'medalID' => $row->medalID, 
							    				'reason' => $row->reason,
					    						'status' => $row->status, 
					    						'issuerID' => $row->issuerID, 
					    						'issuerName' => $resolvedIDs[$row->issuerID], 
					    						'issued' => $row->issued, 
					    						'corporationID' => $row->corporationID, 
					    						'corporationName' => $resolvedIDs[$row->corporationID], 
					    						'title' => $row->title, 
					    						'description' => $row->description]);
			}

			// delete all loyalty points
			$deletedRows = crestLoyaltyPoints::where('characterID', $charInfo->characterID)->delete();

			// add Loyalty Points 
			foreach($loyaltyPoints->items as $row) {
				//dd($row);
				$lp = crestLoyaltyPoints::create([	'characterID' => $charInfo->characterID, 
													'corporationID' => $row->corporation->id, 
													'corporationName' => $row->corporation->name, 
													'quantity' => $row->quantity]);
			}

			// delete all standings
			$deletedRows = apiStanding::where('ownerID', $charInfo->characterID)->delete();

			// Agents Standings
			foreach($standings->characterNPCStandings->agents as $row) {
				//dd($row);
				$st = apiStanding::create([	'ownerID' => $charInfo->characterID, 
											'type' => 'agents', 
											'fromID' => $row->fromID, 
											'fromName' => $row->fromName,
											'standing' => $row->standing]);
			}
 
			// NPC Corp Standings
            foreach($standings->characterNPCStandings->NPCCorporations as $row) {
				$st = apiStanding::create([	'ownerID' => $charInfo->characterID, 
											'type' => 'NPCCorporations', 
											'fromID' => $row->fromID, 
											'fromName' => $row->fromName,
											'standing' => $row->standing]);
			}

			// Faction Standings
			foreach($standings->characterNPCStandings->factions as $row) {
				$st = apiStanding::create([	'ownerID' => $charInfo->characterID, 
											'type' => 'factions', 
											'fromID' => $row->fromID, 
											'fromName' => $row->fromName,
											'standing' => $row->standing]);
			}
			/**/
		} elseif($id !== null) {
			// TODO: check $id is allowed/public

			// load from db
			//$info = CharacterInfo::find($id);
			//$char = Character::find($id);
		} else {
			// Not logged in and no $id provided, so need to redirect to Home
			return redirect('/');
		}
/*
		++$data['character'] = apiDB::getCharacter($ownerTypeID);
		++$data['characterInfo'] = apiDB::getCharacterInfo($ownerTypeID);
		//$data['medals'] = apiDB::getMedals($ownerTypeID);
		++$data['skillQueue'] = apiDB::getSkillQueue($ownerTypeID);
		+$skills = apiDB::getSkills($ownerTypeID);
		+$data['skillTree'] = apiDB::getSkillTree();
		-$data['cacheTime'] = ApiCurrent::get($ownerTypeID . "_char_charactersheet.xml");
// TODO fallback on cacheTime, use db lastUpdated entry
		+$data['corpHistory'] = apiDB::getEmploymentHistory($ownerTypeID);

		// alter skills with expired items from skillQueue to show them as completed
		$finishedSkills = apiDB::getTrainedSkillsFromSkillQueue($ownerTypeID);
		if($finishedSkills) {
			foreach($finishedSkills as $trainedSkill) {
				for($x = 0; $x < count($skills); $x++) {
					if($skills[$x]->typeID == $trainedSkill->typeID) {
						$skills[$x]->skillpoints = $trainedSkill->endSP;
						$skills[$x]->level = $trainedSkill->level;
					}
				}
			}
		}
		$data['skills'] = $skills;
		$data['clones'] = apiDB::getCharacterJumpClones($ownerTypeID);
//*/


		$data['characterInfo'] = CharacterInfo::find($id);
		$data['character'] = Character::find($id);
		$data['accountStatus'] = apiAccountStatus::find($id);
		$data['skillQueue'] = SkillQueue::getSkillQueue($id);
		$skills = eveSkillTree::getSkills($id);
		
		$data['corpHistory'] = EmploymentHistory::where('characterID',$id)->orderBy('recordID','DESC')->get();
		$data['requiredSkills'] = eveRequiredSkills::all();

		// TODO alter skills with expired items from skillQueue to show them as completed
/*		$finishedSkills = apiDB::getTrainedSkillsFromSkillQueue($ownerTypeID);
		if($finishedSkills) {
			foreach($finishedSkills as $trainedSkill) {
				for($x = 0; $x < count($skills); $x++) {
					if($skills[$x]->typeID == $trainedSkill->typeID) {
						$skills[$x]->skillpoints = $trainedSkill->endSP;
						$skills[$x]->level = $trainedSkill->level;
					}
				}
			}
		}
		/**/
		$data['skills'] = $skills;
		$data['clones'] = CharacterClones::getClones($id);
		$data['cloneImplants'] = CharacterCloneImplants::getCloneImplants($id);
		$data['medals'] = apiMedals::where('characterID',$id)->get();
		$data['lp'] = crestLoyaltyPoints::where('characterID',$id)->orderBy('quantity','DESC')->get();
		$data['standings']['agents'] = apiStanding::getAgentsStandings($id);
		$data['standings']['NPCCorporations'] = apiStanding::getNPCCorporationStandings($id);
		$data['standings']['factions'] = apiStanding::getFactionStandings($id);

		$apiPage = strtolower(str_replace(' ', '', 'Character Sheet'));

 		return View('api.view', array('APIPAGE' => $apiPage, 'DATA' => $data));
	}

	public function postViewlpstore(Request $request) {
    	/*
    	$method = $request->method();

		if ($request->isMethod('post')) {
		    //
		}
		*/

		// called by Character Sheet.
		
		if (!$request->ajax()) {
			// log to security as some form of attack
			//securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to add a Supercap: '.Request::url().' using a non-Ajax method');

			return 'Denied.';
		}

		$corpID = $request->Input('corpID');
		$corpName = $request->Input('corpName');
		$lp = $request->Input('lp');

		//$lpStore = getCrestPage(config('tools.root').'corporations/'.$corpID.'/loyaltystore/');
		$lpStore = npcLPStore::getStore($corpID);

		if($lpStore->count() > 0) {	
			$ret['data'] = $lpStore->toArray();

			return $ret;
		} else {
			return "No Loyalty Store found";
		}
	}

	public function postAgentfinder(Request $request) {
    	/*
    	$method = $request->method();

		if ($request->isMethod('post')) {
		    //
		}
		*/

		// called by Character Sheet.
		
		if (!$request->ajax()) {
			// log to security as some form of attack
			//securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to add a Supercap: '.Request::url().' using a non-Ajax method');

			return 'Denied.';
		}

		$corpID = $request->Input('corpID');
		$corpName = $request->Input('corpName');
		$facID = $request->Input('facID');
		$facName = $request->Input('facName');
		$charID = $request->Input('charID');
		$filterAgents = $request->Input('filterAgents');
		
		if($facID === null)
			$af = agtAgents::getCorporationAgents($corpID, $charID, $filterAgents);
		else 
			$af = agtAgents::getFactionAgents($facID, $charID, $filterAgents);

		if($af->count() > 0) {	
			//$ret['data'] = $af->toArray();

			return $af->make();
		} else {
			return "No Agents Found";
		}
	}
	public function postWaypoint(Request $request) {
		if (!$request->ajax()) {
			// log to security as some form of attack
			//securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to add a Supercap: '.Request::url().' using a non-Ajax method');

			return 'Denied.';
		}

		$solarSystemID = (int)$request->Input('solarSystemID');
		$solarSystemName = (string)$request->Input('solarSystemName');
		$locationID = (int)$request->Input('locationID');
		$locationName = (string)$request->Input('locationName');
		//$solarSystemID = 30000003;
		$wp['clearOtherWaypoints'] = false;
		$wp['first'] = false;
		$wp['solarSystem']['href'] = config('tools.root')."solarsystems/".$solarSystemID."/";
		$wp['solarSystem']['id'] = $solarSystemID;
		//$wp['station']['href'] = config('tools.root')."stations/".$locationID."/";
		//$wp['station']['id'] = $locationID;
		Crest::returnToRoot();
		Crest::walk('decode');
		Crest::walk('character');
		Crest::walk(array('ui','setWaypoints'));
		Crest::post($wp);

		//return "Autopilot set to ".$locationName;
		return "Autopilot set to ".$solarSystemName;
	}
/*
	public function crestPage($page) {
		// can be array or string, arrays are nested calls.
		if(is_array($page)) {
			$pointer = static::$root;

			foreach($page as $part) {
				if(isset($pointer->$part))
					$pointer = $pointer->$part;
				else
					return false;
			} 
			$href = $pointer->href;
		} else {
			if(isset(static::$root->$page->href))
				$href = static::$root->$page->href;
			else
				return false;
		}

		return getCrestPage($href);
	}
*/
	protected function crestRoot() {
		return getCrest(config('tools.root'), true, true);
	}
}
