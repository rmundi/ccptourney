<?php
// logging

namespace Thunk\Http\Controllers;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

class CalcLogger {
	private static $monolog;

	public static function log($level, $message) {
		if(empty(static::$monolog)) {
			$logFile = 'calclog-'.gmdate("Y-m").'.txt';
			$handler = new StreamHandler(storage_path().'/crest/debug/'.$logFile);
			$handler->setFormatter(new LineFormatter("[%datetime%] %channel%.%level_name%: %message%\n"));
			static::$monolog = new Logger('CalcLogger');	
			static::$monolog->pushHandler($handler, Logger::DEBUG);
		}

		static::$monolog->$level($message);
	}
}