<?php

namespace Thunk\Http\Controllers;

use Thunk\User;
use Illuminate\Http\Request;
use Thunk\Http\Requests;
use Thunk\Classes\Crest;


class AuthController extends Controller
{
    // Handles CREST Authing stuff - callback url is /auth

    public function getAuth(Request $request) {
    	$SSOCode = $request->Input('code');
    	$SSOState = $request->Input('state');
		
		$state = explode('*', $SSOState);

		$redirectURL = $state[0];
		$token = $state[1];

		if($token != csrf_token()) {
			session(['errormessage' => 'Token Expired. Try Logging in Again.']);

			// redirect somewhere
			return redirect('/');
		}

	    // get authorization code
	    $auth = Crest::getAuthorizationCode($SSOCode);

	    if($auth['success'] === false) {
	    	session(['errormessage' => 'Error: '.$auth['errorCode']]);

	    	// redirect somewhere
	    	return redirect('/');
	    }

	    $response = json_decode($auth['data']);
	    $authToken = $response->access_token;
	    $refreshToken = $response->refresh_token;

	    $verify = Crest::getSSOVerification($authToken);
	    
	    if($verify['success'] === false) {
	    	session(['errormessage' => 'Error: '.$verify['errorCode']]);

	    	// redirect somewhere
	    	return redirect('/');
	    }

	    $response = json_decode($verify['data']);

		if (!isset($response->CharacterID)) {
	    	session(['errormessage' => 'No character ID returned']);

	    	// redirect somewhere
	    	return redirect('/');
		}

		// Do Session stuff (use Laravel Session, not Cookies)
		session(['sso_characterid' => $response->CharacterID]);
		session(['sso_charactername' => $response->CharacterName]);
		session(['sso_characterhash' => $response->CharacterOwnerHash]);
		session(['sso_accesstoken' => $authToken]);
		session(['sso_expires' => $response->ExpiresOn]);
		if(isset($refreshToken) && $refreshToken !== null)
			session(['sso_refreshtoken' => $refreshToken]);


		if(strtotime($response->ExpiresOn) !== false)
            $expires = strtotime($response->ExpiresOn);

        // x-real-ip (old habits die hard)
        $ip = $request->header('x-real-ip');
		if(!isset($ip))
			$ip = $request->getClientIp();

		// update model
		$user = User::where('characterID', $response->CharacterID)
          			->where('characterHash', $response->CharacterOwnerHash)
          			->first();

        if($user === null)
        	$user = new User;

		$user->characterID = $response->CharacterID;
		$user->characterHash = $response->CharacterOwnerHash;
		$user->characterName = $response->CharacterName;
		$user->accessToken = $authToken;
		$user->refreshToken = $refreshToken;
		$user->expires = $expires;
		$user->ip = $ip;
		$user->userAgent = $request->header('user-agent');

		$user->save();

		return redirect($redirectURL);
	}

	public function logout() {
		Crest::SSOLogout();
		return redirect('/');
	}
}


/*
[12:07:25] <+Captain_Thunk> btw is CREST meant to return the Auth refresh_token as null?
[12:07:54] <LuciaDenniard> if you request no scopes, there is no refresh token
[12:07:58] <LuciaDenniard> if you've requested scopes you'll get one

$response =

{#179 ▼
  +"CharacterID": 629288508
  +"CharacterName": "Opus Dai"
  +"ExpiresOn": "2016-05-20T11:25:48"
  +"Scopes": ""
  +"TokenType": "Character"
  +"CharacterOwnerHash": "f85fj/MDTO1Fc5fNRw7rWqkhZsw="
  +"IntellectualProperty": "EVE"
}

$auth =

array:2 [▼
  "success" => true
  "data" => "{"access_token":"MDmwtV8BDT1o25wA4hNI75WGWKLSiyF8xNuUzdp5JUp3tQW6XnVnOCq3s-avxqX9yKJVZM21zgUTtjO8dBSN5g2","token_type":"Bearer","expires_in":1200,"refresh_token":null}"
]
*/