<?php

namespace Thunk\Http\Controllers;

use Cache;
use Illuminate\Http\Request;
use Thunk\Http\Requests;
use Thunk\invType;
use Thunk\invTrait;
use Thunk\dgmTypeAttribute;

class TournyController extends Controller
{
    public function getIndex() {

		$json = getCrest(config('tools.root').'tournaments/');

		$urlInfo = array();
		$details = array();

		if($json) {
			foreach($json->items as $item) {
				$url = self::fixUrl($item->href->href);
				$name = $item->href->name;

				$urlInfo[] = array('href' => $url, 'name' => $name);
			}

			$details["totalCount_str"] = $json->totalCount_str;
			$details["pageCount"] = $json->pageCount;
			$details["totalCount"] = $json->totalCount;

		} else {
			//session(['errormessage' => 'Problem getting CREST data']);
		}

		return View('tournament.index', array('DETAILS' => $details, 'INFO' => $urlInfo));

	}

	public function getTournament($id) {

		$json = getCrest(config('tools.root').'tournaments/'.$id.'/');

		// get details
		$details = array();
		$details['series'] = self::fixUrl($json->series->href);
		$details['membershipCutoff'] = str_replace('T', ' ', $json->membershipCutoff);
		$details['type'] = $json->type;
		$details['name'] = $json->name;

		// get teams
		$teamInfo = array();
		foreach($json->entries as $item) {
			$url = self::fixUrl($item->href);
			$name = $item->name;
			$teamStats = self::fixUrl($item->teamStats->href);

			$teamInfo[] = array('href' => $url, 'name' => $name, 'teamStats' => $teamStats);
		}

		return View('tournament.tournament', array('DETAILS' => $details, 'TEAMS' => $teamInfo));
	}

	public function getTeam($id, $teamid) {
	
		$json = getCrest(config('tools.root').'tournaments/'.$id.'/teams/'.$teamid.'/');

		// get details
		$details = array();
		$details['captain'] = array('href' => self::fixUrl($json->captain->href), 'name' => $json->captain->name, 'icon' => self::fixIcon($json->captain->icon->href));
		$details['name'] = $json->name;
		$details['iskKilled'] = $json->iskKilled;
		$details['iskKilled_str'] = $json->iskKilled_str;
		$details['shipsKilled_str'] = $json->shipsKilled_str;
		$details['shipsKilled'] = $json->shipsKilled;
		$details['flagshipType'] = array('href' => self::fixUrl($json->flagshipType->href), 'name' => $json->flagshipType->name, 'icon' => self::fixIcon($json->flagshipType->icon->href));

		// get Members information
		$membersJson = getCrest($json->members->href);
		$teamMembers = array();
		foreach($membersJson->items as $item) {
			$url = $item->self->href;
			$allianceUrl = $item->alliance->href;
			$name = $item->name;
			$icon = self::fixIcon($item->icon->href);

			$teamMembers[] = array('url' => $url, 'allianceUrl' => $allianceUrl, 'name' => $name, 'icon' => $icon);
		}

		// get series data
		$seriesData = getCrest(config('tools.root').'tournaments/'.$id.'/series/');

		$matches = array();
		foreach($json->matches as $item) {
			$seriesID = self::getSeriesID($id, $item->href);
			$matchData = getCrest(config('tools.root').'tournaments/'.$id.'/series/'.$seriesID['series'].'/matches/');
			$matchID = $seriesID['match'];

			$matches[] = array( 'url' => self::fixUrl($item->href), 'seriesData' => $seriesData->items[$seriesID['series']], 'matchData' => $matchData->items[$matchID]);
		}

		// get bans
		$bans = array();
		foreach($json->banFrequency as $item) {
			$numBans = $item->numBans;
			$numBans_str = $item->numBans_str;
			$shipType = array('href' => self::fixUrl($item->shipType->href), 'name' => $item->shipType->name, 'icon' => self::fixIcon($item->shipType->icon->href));

			$bans[] = array('numBans' => $numBans, 'numBans_str' => $numBans_str, 'shipType' => $shipType);
		}

		$bansAgainst = array();
		foreach($json->banFrequencyAgainst as $item) {
			$numBans = $item->numBans;
			$numBans_str = $item->numBans_str;
			$shipType = array('href' => self::fixUrl($item->shipType->href), 'name' => $item->shipType->name, 'icon' => self::fixIcon($item->shipType->icon->href));

			$bansAgainst[] = array('numBans' => $numBans, 'numBans_str' => $numBans_str, 'shipType' => $shipType);
		}

		// get pilots
		$pilots = array();
		foreach($json->pilots as $item) {
			$url = self::fixUrl($item->href);
			$name = $item->name;
			$icon = self::fixIcon($item->icon->href);

			$pilots[] = array('href' => $url, 'name' => $name, 'icon' => $icon);
		}

		return View('tournament.teamstats', array('DETAILS' => $details, 'BANS' => $bans, 'BANSAGAINST' => $bansAgainst, 'PILOTS' => $pilots, 'TEAMMEMBERS' => $teamMembers, 'MATCHES' => $matches));
	}

	public function getTeaminfo($teamid) {

		$json = getCrest(config('tools.root').'tournaments/teams/'.$teamid.'/');

		// get details
		$details = array();
		$details['captain'] = array('href' => self::fixUrl($json->captain->href), 'name' => $json->captain->name, 'icon' => self::fixIcon($json->captain->icon->href));
		$details['name'] = $json->name;
		$details['iskKilled'] = $json->iskKilled;
		$details['iskKilled_str'] = $json->iskKilled_str;
		$details['shipsKilled_str'] = $json->shipsKilled_str;
		$details['shipsKilled'] = $json->shipsKilled;

		// get Members information
		$membersJson = getCrest($json->members->href);
		$teamMembers = array();
		foreach($membersJson->items as $item) {
			$url = $item->self->href;
			$allianceUrl = $item->alliance->href;
			$name = $item->name;
			$icon = self::fixIcon($item->icon->href);

			$teamMembers[] = array('url' => $url, 'allianceUrl' => $allianceUrl, 'name' => $name, 'icon' => $icon);
		}

		// get bans
		$bans = array();
		foreach($json->banFrequency as $item) {
			$numBans = $item->numBans;
			$numBans_str = $item->numBans_str;
			$shipType = array('href' => self::fixUrl($item->shipType->href), 'name' => $item->shipType->name, 'icon' => self::fixIcon($item->shipType->icon->href));

			$bans[] = array('numBans' => $numBans, 'numBans_str' => $numBans_str, 'shipType' => $shipType);
		}

		$bansAgainst = array();
		foreach($json->banFrequencyAgainst as $item) {
			$numBans = $item->numBans;
			$numBans_str = $item->numBans_str;
			$shipType = array('href' => self::fixUrl($item->shipType->href), 'name' => $item->shipType->name, 'icon' => self::fixIcon($item->shipType->icon->href));

			$bansAgainst[] = array('numBans' => $numBans, 'numBans_str' => $numBans_str, 'shipType' => $shipType);
		}

		// get pilots
		$pilots = array();
		foreach($json->pilots as $item) {
			$url = self::fixUrl($item->href);
			$name = $item->name;
			$icon = self::fixIcon($item->icon->href);

			$pilots[] = array('href' => $url, 'name' => $name, 'icon' => $icon);
		}

		return View('tournament.teaminfo', array('DETAILS' => $details, 'BANS' => $bans, 'BANSAGAINST' => $bansAgainst, 'PILOTS' => $pilots, 'TEAMMEMBERS' => $teamMembers));
	}

	public function getShipbuilder() {
		return View('tournament.teambuilder');
	}

	public function getMembers($teamid) {
		$json = getCrest(config('tools.root').'tournaments/teams/'.$teamid.'/members/');
		dd($json);
	}

	public function getMatch($id, $seriesID, $matchID) {
		
		$json = getCrest(config('tools.root').'tournaments/'.$id.'/series/'.$seriesID.'/matches/'.$matchID.'/');

		if($json->finalized == true) {
			$redScore = $json->score->redTeam;
			$blueScore = $json->score->blueTeam;
		} else {
			$redScore = false;
			$blueScore = false;
		}

		$shipData = Cache::remember('getTournamentXIIIShips', config('tools.db-cache-time'), function() {
		    return invType::getTournamentXIIIShips();
		});
		$shipList = $shipData['ships'];

		// get Static Scene data
		$sceneJson = getCrest($json->staticSceneData->href);

		// get first frame - needed to tell which person is on which team
		$frameJson = getCrest(config('tools.root').'tournaments/'.$id.'/series/'.$seriesID.'/matches/'.$matchID.'/realtime/0/');

		// $sceneJson->globalObjects contains an array of celestial objects (planets, moons, belts, beacons etc...) - not needed for our purposes
		// $sceneJson->ships contains an array of the ships, it does not identify which pilot is on which team
		$teampoints = 0;
		$teamships = 0;
		$currentteam = "blue";
		$redTeamData = array();
		$blueTeamData = array();

		if($frameJson->redTeamShipData) {
			foreach($frameJson->redTeamShipData as $shipItem) {
				$redTeamData[] = self::getItemID($shipItem->itemRef->href);
			}
			foreach($frameJson->blueTeamShipData as $shipItem) {
				$blueTeamData[] = self::getItemID($shipItem->itemRef->href);
			}
		}

		if($sceneJson != false && $frameJson != false) {
			$ships = array();
			foreach($sceneJson->ships as $item){
				// get ShipSlots
				$shipSlots = Cache::remember('getSlotLayout_'.snake_case($item->type->name), config('tools.db-cache-time'), function() use($item) {
				    return invType::getSlotLayout($item->type->name);
				});

				$shipROFBonus = invTrait::getShipTraitsTourney($item->type->name);
			

				if($shipROFBonus === false || is_object($shipROFBonus))
					$shipROFBonus = 0;

				// get ship points from shipList
				$pts = 0;
				foreach($shipList as $shipPts) {
					if($item->type->name == $shipPts->typeName) {
						$pts = $shipPts->points;
					}
				}

				$hiSlots = 0;
				$medSlots = 0;
				$loSlots = 0;
				$rigs = 0;
				$subs = 0;

				if(isset($shipSlots[12]))
					$loSlots = $shipSlots[12];
				if(isset($shipSlots[13]))
					$medSlots = $shipSlots[13];
				if(isset($shipSlots[14]))
					$hiSlots = $shipSlots[14];
				if(isset($shipSlots[1137]))
					$rigs = $shipSlots[1137];
				if(isset($shipSlots[1367]))
					$subs = $shipSlots[1367];

				$turrets = array();
				$hiSlot = array();
				$subSystems = array();
				foreach($item->turrets as $turret) {
					$turretTypeData = Cache::remember('getGraphicName_'.snake_case($turret->graphicResource->href).'_one', config('tools.db-cache-time'), function() use($turret) {
					    return invType::getGraphicName(self::getGraphicID($turret->graphicResource->href), false);
					});

					$rofArray = array();

					if(count($turretTypeData) > 1) {
							$turretType = $turretTypeData[0];
					} else {
						$turretType = $turretTypeData[0];
					}
					
					for($x=0; $x < (count($turretTypeData)); $x++) {
						$turretTypeData[$x]->baseROF = dgmTypeAttribute::getItemAttribute($turretTypeData[$x]->typeID, 51);
					}

					if($turretType->categoryName == 'Subsystem') {
						$slotArray = Cache::remember('getT3SlotModifier_'.snake_case($turretType->typeName), config('tools.db-cache-time'), function() use($turretType) {
						    return invType::getT3SlotModifier($turretType->typeName);
						});
						
						if(isset($slotArray['1374']))
							$hiSlots += $slotArray['1374'];
						if(isset($slotArray['1375']))
							$medSlots += $slotArray['1375'];
						if(isset($slotArray['1376']))
							$loSlots += $slotArray['1376'];

						$subSystems[] = array('id' => self::getItemID($turret->href), 'graphicID' => $turretType->iconFile, 'name' => $turretType->typeName, 'typeID' => $turretType->typeID);
					} else {
						$turrets[] = array('id' => self::getItemID($turret->href), 'graphicID' => $turretType->iconFile, 'name' => $turretType->typeName, 'typeID' => $turretType->typeID, 'turretVariants' => $turretTypeData, 'groupName' => $turretType->groupName);
						// assign hiSlot too
						$hiSlot[] = array('id' => self::getItemID($turret->href), 'graphicID' => $turretType->iconFile, 'name' => $turretType->typeName, 'typeID' => $turretType->typeID, 'turretVariants' => $turretTypeData, 'groupName' => $turretType->groupName, 'calculatedROF' => false);
					}
				}

				// assign to team
				foreach($redTeamData as $pilot) {
					if($pilot == self::getItemID($item->item->href)) {
						$redShips[] = array('id' => self::getItemID($item->item->href), 'pilotName' => $item->character->name, 'pilotIcon' => self::fixIcon($item->character->icon->href),
									'type' => $item->type->name, 'icon' => self::fixIcon($item->type->icon->href), 'points' => $item->points, 'turrets' => $turrets, 'loSlots' => $loSlots, 
									'medSlots' => $medSlots, 'hiSlots' => $hiSlots, 'rigs' => $rigs, 'subs' => $subs, 'subSystems' => $subSystems, 'hiSlot' => $hiSlot, 'shipROFBonus' => $shipROFBonus, 'points' => $pts);
					}
				}
				
				foreach($blueTeamData as $pilot) {
					if($pilot == self::getItemID($item->item->href)) {
						$blueShips[] = array('id' => self::getItemID($item->item->href), 'pilotName' => $item->character->name, 'pilotIcon' => self::fixIcon($item->character->icon->href),
									'type' => $item->type->name, 'icon' => self::fixIcon($item->type->icon->href), 'points' => $item->points, 'turrets' => $turrets, 'loSlots' => $loSlots, 
									'medSlots' => $medSlots, 'hiSlots' => $hiSlots, 'rigs' => $rigs, 'subs' => $subs, 'subSystems' => $subSystems, 'hiSlot' => $hiSlot, 'shipROFBonus' => $shipROFBonus, 'points' => $pts);
					}
				}
			}
		} else {
			Session::flush();
		}

		self::aasort($blueShips,"points");
		self::aasort($redShips,"points");
// remove *pilots and declaration
//		$redPilots = array();
//		$pilots = array();
//		$teamMembers = array();
//		$bluePilots = array();	
		$redTeam = array('name' => $json->redTeam->teamName, 'data' => $redTeamData, 'ships' => $redShips, 'teamURL' => self::fixUrl($json->redTeam->href), 'score' => $redScore);
		$blueTeam = array('name' => $json->blueTeam->teamName,'data' => $blueTeamData, 'ships' => $blueShips, 'teamURL' => self::fixUrl($json->blueTeam->href), 'score' => $blueScore);

//		$redTeam = array('name' => $json->redTeam->teamName, 'pilots' => $redPilots, 'data' => $redTeamData, 'ships' => $redShips, 'teamURL' => self::fixUrl($json->redTeam->href), 'score' => $redScore);
//		$blueTeam = array('name' => $json->blueTeam->teamName, 'pilots' => $bluePilots, 'data' => $blueTeamData, 'ships' => $blueShips, 'teamURL' => self::fixUrl($json->blueTeam->href), 'score' => $blueScore);

		// banned from using
		$bans = array();
		foreach($json->bans->redTeam as $teamItem) {
			foreach($teamItem->typeBans as $item) {
				$bans[] = array('url' => $item->href, 'name' => $item->name, 'icon' => self::fixIcon($item->icon->href), 'bannedBy' => $json->blueTeam->teamName);
			}
		}
		foreach($json->bans->blueTeam as $teamItem) {
			foreach($teamItem->typeBans as $item) {
				$bans[] = array('url' => $item->href, 'name' => $item->name, 'icon' => self::fixIcon($item->icon->href), 'bannedBy' => $json->redTeam->teamName);
			}
		}
//dd($redTeam);
//		return View('tournament.match', array('REDTEAM' => $redTeam, 'BLUETEAM' => $blueTeam, 'BANS' => $bans, 'FRAMEURL' => config('tools.root').'tournaments/'.$id.'/series/'.$seriesID.'/matches/'.$matchID.'/realtime/', 
//			'MAXDEFENSE' => 1000000, 'MAXATTACK' => 5000, 'MAXCONTROL' => 100));

		return View('tournament.match', array('REDTEAM' => json_encode($redTeam), 'BLUETEAM' => json_encode($blueTeam), 'BANS' => $bans, 'FRAMEURL' => config('tools.root').'tournaments/'.$id.'/series/'.$seriesID.'/matches/'.$matchID.'/realtime/', 
			'MAXDEFENSE' => 1000000, 'MAXATTACK' => 5000, 'MAXCONTROL' => 100));

//		return View('tournament.match', array('REDTEAM' => $redTeam, 'BLUETEAM' => $blueTeam, 'BANS' => $bans, 'PILOTS' => $pilots, 'TEAMMEMBERS' => $teamMembers,
//							'FRAMEURL' => config('tools.root').'tournaments/'.$id.'/series/'.$seriesID.'/matches/'.$matchID.'/realtime/', 'MAXDEFENSE' => 1000000, 'MAXATTACK' => 5000, 'MAXCONTROL' => 100));
	}

	public function getSeries($id) {
		$json = getCrest(config('tools.root').'tournaments/'.$id.'/series/');
		dd($json);
	}


	public function postShipdata() {
		$json = Cache::remember('getTournamentXIIIShips', config('tools.db-cache-time'), function() {
		    return invType::getTournamentXIIIShips();
		});
		return json_encode($json);
	}

	public function postTournament(Request $request) {
		// need to sort out the information and send as json

		$ships = array();
		$count = 1;
		$jsonData = array();
		$json = getCrest( $request->Input('url'),true,true);
		$calcChecks = json_decode($request->Input('ROFchecks'),false);
		
		do {
			if(isset($json->redTeamShipData)) {
				foreach($json->redTeamShipData as $shipData) {

					// reset saved calcs array
					$savedCalcs = array();

					$id = getCrestID($shipData->itemRef->href);
					$ships[$id]['shield'] = $shipData->shield;
					$ships[$id]['armor'] = $shipData->armor;
					$ships[$id]['structure'] = $shipData->structure;

					if(isset($shipData->physicsData)) {
						$ships[$id]['physicsData']['x'] = $shipData->physicsData->x;
						$ships[$id]['physicsData']['y'] = $shipData->physicsData->y;
						$ships[$id]['physicsData']['z'] = $shipData->physicsData->z;
						$ships[$id]['physicsData']['vx'] = $shipData->physicsData->vx;
						$ships[$id]['physicsData']['vy'] = $shipData->physicsData->vy;
						$ships[$id]['physicsData']['vz'] = $shipData->physicsData->vz;
					}

					if(isset($shipData->effects)) {
						foreach($shipData->effects as $effect) {
							$modCount = 0;
							foreach($effect->modules as $mods) {
								$missiles = null;
								if(isset($shipData->missiles))
									$missiles = $shipData->missiles;

								$data = self::processEffect($effect, $modCount, $missiles);
								$ships[$id][$data['id']] = $data;
								$modCount++;

								// if High Slot and we don't already have ROF checks done, do the check
								//$data['slot'] = "high";
								$index = 0;
								foreach($calcChecks as $shipIDX => $checkData) {
									if($data['slot'] == "high" && $id == $shipIDX && $checkData->ROFChecked === false && isset($checkData->hiSlot)) {
										foreach($checkData->hiSlot as $moduleHi) {
											if($data['id'] == $moduleHi->id && isset($moduleHi->turretVariants) && (isset($moduleHi->calculatedROF) && $moduleHi->calculatedROF !== true)) {

												$roleBonus = 1;
			    								if($checkData->shipType == 'Machariel' || $checkData->shipType == 'Cynabal')
			    									$roleBonus = 0.75;
			    								if($checkData->shipType == 'Vangel')
			    									$roleBonus = 0.5;
//dd($checkData);
			    								unset($testrof);
			    								$testrof = null;

			    								if(isset($savedCalcs[$moduleHi->groupName][$data['duration']])) {
			    									$testrof = $savedCalcs[$moduleHi->groupName][$data['duration']];
			    									CalcLogger::log('info', 'TESTING, USING EXISTING TURRET');
			    								} else {
													$testrof = self::calculateROF($moduleHi->turretVariants, $moduleHi->groupName, $data['duration'], $roleBonus, $checkData->shipROFBonus, $shipIDX);

													// save Object so future similar turrets can use it again without recalcing
													$savedCalcs[$moduleHi->groupName][$data['duration']] = $testrof;
												}

												if($testrof !== null && is_array($testrof)) {
			    									$ships[$id][$moduleHi->id]['calculatedROF'] = true;
			    									$ships[$id][$moduleHi->id]['info'] = $testrof;

			    									// set stuff from ['variant']
			    									$ships[$id][$moduleHi->id]['name'] = $testrof['variant']->typeName;
			    									$ships[$id][$moduleHi->id]['meta'] = $testrof['variant']->meta;
			    									$ships[$id][$moduleHi->id]['typeID'] = $testrof['variant']->typeID;
			    									//$('#'+ships[shipID]['hiSlot'][index]['id']).attr('src', 'https://image.eveonline.com/Type/'+testrof['variant']['typeID']+'_32.png')
			    								} else {
			    									$ships[$id][$moduleHi->id]['calculatedROF'] = $testrof;
			    								}
											}
										}
										
									}

									$index++;
								}
							}
						}
					}

					if(isset($shipData->missiles)) {
						foreach($shipData->missiles as $missile) {
							
						}
					}

					if(isset($shipData->drones)) {
						//return "FALSE3";
						foreach($shipData->drones as $drone) {
							$data['itemID'] = $drone->itemID_str;
							$data['slot'] = "dronebay";
							$data['href'] = $drone->type->href;
							$data['name'] = $drone->type->name;
							if($drone->physicsData) {
								$data['physicsData']['x'] = $drone->physicsData->x;
								$data['physicsData']['y'] = $drone->physicsData->y;
								$data['physicsData']['z'] = $drone->physicsData->z;
								$data['physicsData']['vx'] = $drone->physicsData->vx;
								$data['physicsData']['vy'] = $drone->physicsData->vy;
								$data['physicsData']['vz'] = $drone->physicsData->vz;
							}
							$typeData = explode('/', $data['href']);
							$data['typeID'] = $typeData[(count($typeData)-2)];

					       	$data['icon'] = "https://image.eveonline.com/Type/".$data['typeID']."_32.png";
					       	$ships[$id]['drones'][$data['itemID']] = $data;
							
						}
					}
				}
			}

			if(isset($json->blueTeamShipData)) {
				foreach($json->blueTeamShipData as $shipData) {

					// reset saved calcs array
					$savedCalcs = array();

					$id = getCrestID($shipData->itemRef->href);
					$ships[$id]['shield'] = $shipData->shield;
					$ships[$id]['armor'] = $shipData->armor;
					$ships[$id]['structure'] = $shipData->structure;

					if(isset($shipData->physicsData)) {
						$ships[$id]['physicsData']['x'] = $shipData->physicsData->x;
						$ships[$id]['physicsData']['y'] = $shipData->physicsData->y;
						$ships[$id]['physicsData']['z'] = $shipData->physicsData->z;
						$ships[$id]['physicsData']['vx'] = $shipData->physicsData->vx;
						$ships[$id]['physicsData']['vy'] = $shipData->physicsData->vy;
						$ships[$id]['physicsData']['vz'] = $shipData->physicsData->vz;
					}

					if(isset($shipData->effects)) {
						foreach($shipData->effects as $effect) {
							$modCount = 0;
							foreach($effect->modules as $mods) {
								$missiles = null;
								if(isset($shipData->missiles))
									$missiles = $shipData->missiles;

								$data = self::processEffect($effect, $modCount, $missiles);
								$ships[$id][$data['id']] = $data;
								$modCount++;

								// if High Slot and we don't already have ROF checks done, do the check
								//$data['slot'] = "high";
								$index = 0;
								foreach($calcChecks as $shipIDX => $checkData) {
									//dd($shipIDX.' - ROFChecked: '.$checkData->ROFChecked);
									if($data['slot'] == "high" && $id == $shipIDX && $checkData->ROFChecked === false && isset($checkData->hiSlot)) {
										foreach($checkData->hiSlot as $moduleHi) {
											if($data['id'] == $moduleHi->id && $moduleHi->calculatedROF == false) {

												$roleBonus = 1;
			    								if($checkData->shipType == 'Machariel' || $checkData->shipType == 'Cynabal')
			    									$roleBonus = 0.75;
			    								if($checkData->shipType == 'Vangel')
			    									$roleBonus = 0.5;
//dd($checkData);

			    								unset($testrof);
			    								$testrof = null;

			    								if(isset($savedCalcs[$moduleHi->groupName][$data['duration']])) {
			    									$testrof = $savedCalcs[$moduleHi->groupName][$data['duration']];
			    									CalcLogger::log('info', 'TESTING, USING EXISTING TURRET');
			    								} else {
// TESTING TEMP
//$checkData->shipROFBonus = 10; // temp fix for fleet issue hurricane for testing - frame 7 match 50 Ash Refine Hurricane Fleet Issue
													$testrof = self::calculateROF($moduleHi->turretVariants, $moduleHi->groupName, $data['duration'], $roleBonus, $checkData->shipROFBonus, $shipIDX);

													// save Object so future similar turrets can use it again without recalcing
													$savedCalcs[$moduleHi->groupName][$data['duration']] = $testrof;
												}

												//$testrof = self::calculateROF($moduleHi->turretVariants, $moduleHi->groupName, $data['duration'], $roleBonus, $checkData->shipROFBonus, $id);

												if($testrof !== null && is_array($testrof)) {
			    									$ships[$id]['hiSlot'][$index]['calculatedROF'] = true;
			    									//ships[shipID]['hiSlot'][index].push({info:testrof});
			    									$ships[$id]['hiSlot'][$index]['info'] = $testrof;

			    									// set stuff from ['variant']
			    									$ships[$id]['hiSlot'][$index]['name'] = $testrof['variant']->typeName;
			    									$ships[$id]['hiSlot'][$index]['meta'] = $testrof['variant']->meta;
			    									$ships[$id]['hiSlot'][$index]['typeID'] = $testrof['variant']->typeID;
			    									//$('#'+ships[shipID]['hiSlot'][index]['id']).attr('src', 'https://image.eveonline.com/Type/'+testrof['variant']['typeID']+'_32.png')
			    								} else {
			    									$ships[$id]['hiSlot'][$index]['calculatedROF'] = $testrof;
			    								}
											}
										}
										
									}

									$index++;
								}
							}
						}
					}

					if(isset($shipData->missiles)) {
						foreach($shipData->missiles as $missile) {
							
						}
					}

					if(isset($shipData->drones)) {
						foreach($shipData->drones as $drone) {
							$data['itemID'] = $drone->itemID_str;
							$data['slot'] = "dronebay";
							$data['href'] = $drone->type->href;
							$data['name'] = $drone->type->name;
							if($drone->physicsData) {
								$data['physicsData']['x'] = $drone->physicsData->x;
								$data['physicsData']['y'] = $drone->physicsData->y;
								$data['physicsData']['z'] = $drone->physicsData->z;
								$data['physicsData']['vx'] = $drone->physicsData->vx;
								$data['physicsData']['vy'] = $drone->physicsData->vy;
								$data['physicsData']['vz'] = $drone->physicsData->vz;
							}

							$data['typeID'] = getCrestID($data['href']);
 					       	$data['icon'] = "https://image.eveonline.com/Type/".$data['typeID']."_32.png";
					       	$ships[$id]['drones'][$data['itemID']] = $data;
							
						}
					}

					// do checks
					foreach($calcChecks as $shipIDX => $checkData) {
						//dd($shipIDX.' - ROFChecked: '.$checkData->ROFChecked);
						if($id == $shipIDX && $checkData->ROFChecked === false) {

						}
					}
				}
			}
	
			if(isset($json->nextFrame->href)) {
				$ships['nextFrame'] = $json->nextFrame->href;
				$ships['currentFrame'] = getCrestID($json->nextFrame->href);
			} else {
				unset($ships['nextFrame']);
			}

			$ships['framesProcessed'] = $count;

			$ships['tidiFactor'] = $json->tidiFactor;
			$ships['isCached'] = $json->isCached;

			if(isset($json->blueTeamFleetAttributes))
				$ships['blueTeamFleetAttributes'] = $json->blueTeamFleetAttributes;

			if(isset($json->redTeamFleetAttributes))
				$ships['redTeamFleetAttributes'] = $json->redTeamFleetAttributes;

			return json_encode($ships);


			if(isset($json->nextFrame))
				$json = getCrest($json->nextFrame->href,true,true);
			else
				$json = false;

			if($json->key == "noSuchFrame") {
				$json = false;
				$unset($json->nextFrame);
			}

			if($json != false) {
				$frame++;
				$count++;
			}
	
		} while($json != false || isset($json->nextFrame));

		if(isset($json->nextFrame->href)) {
			$ships['nextFrame'] = $json->nextFrame->href;
		} else {
			unset($ships['nextFrame']);
		}
		$ships['framesProcessed'] = $count;
		$ships['currentFrame'] = $frame;
		$ships['tidiFactor'] = $json->tidiFactor;
		$ships['isCached'] = $json->$isCached;

		return json_encode($ships);
	}

	protected function aasort (&$array, $key) {
	    $sorter=array();
	    $ret=array();
	    reset($array);
	    foreach ($array as $ii => $va) {
	        $sorter[$ii]=$va[$key];
	    }
	    arsort($sorter);
	    foreach ($sorter as $ii => $va) {
	        $ret[$ii]=$array[$ii];
	    }
	    $array=$ret;
	}

	protected function fixUrl($url) {
		return str_replace(config('tools.root'), GetURLPath('/').'/',substr($url,0,-1));
	}

	protected function fixIcon($url) {
		return str_replace(config('tools.cdn-ccp-image-url'), config('tools.ccp-image-url'),$url);
	}

	protected function getSeriesID($id, $url) {
		$str = explode('/', str_replace(config('tools.root').'tournaments/'.$id.'/series/','', $url), 2);
		$series = $str[0];
		$str = explode('/', str_replace(config('tools.root').'tournaments/'.$id.'/series/'.$series.'/matches/','', $url), 2);
		$match = $str[0];

		return array('series' => $series, 'match' => $match);
	}

	protected function getItemID($string) {
		return str_replace('/', '',str_replace(config('tools.root').'items/', '', $string));
	}

	protected function getGraphicID($string) {
		return str_replace('/', '',str_replace(config('tools.root').'eve/graphics/', '', $string));
	}

	protected function formatSeries($raw) {

		$data = array();

		// general
		$data['totalCount_str'] = $raw->totalCount_str;
		$data['totalCount'] = $raw->totalCount;
		$data['pageCount_str'] = $raw->pageCount_str;
		$data['pageCount'] = $raw->pageCount;

		// items
		foreach($raw->items as $item) {
			$redTeam = array();
			$blueTeam = array();
			
			// Red 
			//$redTeam['teamName'] = $item->redTeam->team->

			// Blue

			// general
		}
		dd($raw);
	}

	protected function processEffect($effect, $val = 0, $missiles = null) {
		$data = array();
		switch ($effect->guid) {
			case "effects.Afterburner": // AB
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Afterburner";
		       	$data['icon'] = "https://image.eveonline.com/Type/12056_32.png";
		        break;
			case "effects.ArmorHardening": // reactive hardeners / armour hardeners
				$data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "low";
		       	if($effect->duration == 20000) {
		       		$data['name'] = "Armor Hardener";
		       		$data['icon'] = "https://image.eveonline.com/Type/11303_32.png";
		       	} else {
		       		$data['name'] = "Reactive Hardener";
		       		$data['level'] = (((1-($effect->duration/10000))*100)/10);
		       		$data['skill'] = "Armor Resistance Phasing";
		       		$data['icon'] = "https://image.eveonline.com/Type/4403_32.png";
		       	}
		        break;
		    case "effects.ArmorRepair": // local armour reps
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "low";
				$data['name'] = "Armour Repairer";
		       	$data['icon'] = "https://image.eveonline.com/Type/3538_32.png";
		        break;
		    case "effects.ElectronicAttributeModifyActivate":		// gang links / sensor boosters
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Sensor Booster";
		       	$data['icon'] = "https://image.eveonline.com/Type/1973_32.png";
		        break;
		    case "effects.ECMBurst": // ECM Burst
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "ECM Burst";
		       	$data['icon'] = "https://image.eveonline.com/Type/580_32.png";
		        break;
		    case "effects.ElectronicAttributeModifyTarget": // rem damp/track disrupt/ecm
		    	$data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
		       	if(isset($effect->effectName) && $effect->effectName == "ewRemoteSensorDamp") {
		       		$data['name'] = "Remote Sensor Dampener";
		       		$data['icon'] = "https://image.eveonline.com/Type/1968_32.png";
		       	}
		       	if(isset($effect->effectName) && $effect->effectName == "electronic") {
		       		$data['name'] = "ECM";
		       		$data['icon'] = "https://image.eveonline.com/Type/1957_32.png";
		       	}
		       	if(isset($effect->effectName) && $effect->effectName == "ewTrackingDisrupt") {
		       		$data['name'] = "Tracking Disruptor";
		       		$data['icon'] = "https://image.eveonline.com/Type/2108_32.png";
		       	}
		       	if(!isset($effect->effectName)) {
		       		$data['name'] = "Remote Sensor Booster";
		       		$data['icon'] = "https://image.eveonline.com/Type/1963_32.png";
		       	}
		       	if(!isset($data['name'])) {
		       		$data['name'] = "Unknown";
			       	//$data['icon'] = "https://image.eveonline.com/Type/1973_32.png";
			       	if(isset($effect->effectName))
			       		$data['effectName'] = $effect->effectName;
			       	else
			       		$data['effectName'] = "";
		       	}
		       		if(isset($effect->effectName))
			       		$data['effectName'] = $effect->effectName;
			       	else
			       		$data['effectName'] = "";
		       	// ??? Remote Sensor Booster ???
		        break;
		    case "effects.EMPWave": // smartbombs
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "high";
				$data['name'] = "SmartBomb";
		       	$data['icon'] = "https://image.eveonline.com/Type/3977_32.png";
		        break;
		    case "effects.EnergyDestabilization": /// neuts
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "high";
				if($effect->duration == 6000)
					$data['name'] = "Small Neutralizer";
				elseif($effect->duration == 12000)
					$data['name'] = "Medium Neutralizer";
				elseif($effect->duration == 24000)
					$data['name'] = "Heavy Neutralizer";
				else
					$data['name'] = "Neutralizer";
		       	$data['icon'] = "https://image.eveonline.com/Type/12269_32.png";
		        break;
		    case "effects.EnergyTransfer": // remote energy transfer
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "high";
				$data['name'] = "Remote Capacitor Transmitter";
		       	$data['icon'] = "https://image.eveonline.com/Type/12225_32.png";
		        break;
		    case "effects.EnergyVampire": // nos
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "high";
				if($effect->duration == 2500)
					$data['name'] = "Small Nosferatu";
				elseif($effect->duration == 5000)
					$data['name'] = "Medium Nosferatu";
				elseif($effect->duration == 10000)
					$data['name'] = "Heavy Nosferatu";
				else
					$data['name'] = "Nosferatu";
	
		       	$data['icon'] = "https://image.eveonline.com/Type/12261_32.png";
		        break;
		    case "effects.MicroJumpDriveEngage": // MJD
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Microwarp Drive";
		       	$data['icon'] = "https://image.eveonline.com/Type/12052_32.png";
		        break;
		    case "effects.MicroWarpDrive": // MWD
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Microwarp Drive";
		       	$data['icon'] = "https://image.eveonline.com/Type/12052_32.png";
		        break;
		    case "effects.ModifyShieldResonance": // Shield hardeners
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Shield Hardener";
		       	$data['icon'] = "https://image.eveonline.com/Type/2289_32.png";
		        break;
		    case "effects.ModifyTargetSpeed": // webs
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Stasis Webifier";
		       	$data['icon'] = "https://image.eveonline.com/Type/526_32.png";
		        break;
		    case "effects.RemoteArmourRepair": // rem armour rep
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "high";
				$data['name'] = "Remote Armour Repairer";
		       	$data['icon'] = "https://image.eveonline.com/Type/11359_32.png";
		        break;
		    case "effects.ScanStrengthBonusActivate": // eccm
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "ECCM";
		       	$data['icon'] = "https://image.eveonline.com/Type/2005_32.png";
		        break;
		    case "effects.ShieldBoosting": // Shield Booster
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Shield Booster";
		       	$data['icon'] = "https://image.eveonline.com/Type/10836_32.png";
		        break;
		    case "effects.ShieldTransfer": // remote shield booster
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "high";
				$data['name'] = "Remote Shield Booster";
		       	$data['icon'] = "https://image.eveonline.com/Type/3606_32.png";
		        break;
		    case "effects.SiegeMode": // bastion module
		    	$data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "high";
				$data['name'] = "Bastion Module";
		       	$data['icon'] = "https://image.eveonline.com/Type/33400_32.png";
		        break;
		    case "effects.TargetPaint": // target painter
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Target Painter";
		       	$data['icon'] = "https://image.eveonline.com/Type/12709_32.png";
		        break;
		    case "effects.TurretWeaponRangeTrackingSpeedMultiplyTarget": // rem tracking computer
		    	$data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Remote Tracking Computer";
		       	$data['icon'] = "https://image.eveonline.com/Type/1977_32.png";
		        break;	
		    case "effects.TurretWeaponRangeTrackingSpeedMultiplyActivate": // tracking computer
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Tracking Computer";
		       	$data['icon'] = "https://image.eveonline.com/Type/1977_32.png";
		        break;
		    case "effects.WarpScramble": // scramblers
		        $data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				$data['slot'] = "mid";
				$data['name'] = "Warp Scrambler";
		       	$data['icon'] = "https://image.eveonline.com/Type/447_32.png";
		        break;	    
		    
		    case "effects.Laser": // turrets
		    	$data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				$data['slot'] = "high";
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				if(isset($effect->ammoGraphicResource)) {
					if(isset($effect->ammoGraphicResource->color->r))
						$data['ammoGraphicResource']['r'] = round(($effect->ammoGraphicResource->color->r*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->g))
						$data['ammoGraphicResource']['g'] = round(($effect->ammoGraphicResource->color->g*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->b))
						$data['ammoGraphicResource']['b'] = round(($effect->ammoGraphicResource->color->b*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->a))
						$data['ammoGraphicResource']['a'] = $effect->ammoGraphicResource->color->a;
					$data['ammoGraphicResource']['href'] = $effect->ammoGraphicResource->href;
					$data['ammoGraphicResource']['typeID'] = getCrestID($data['ammoGraphicResource']['href']);
					$ammoType = array();
					$ammoType = Cache::remember('getGraphicName_'.snake_case($data['ammoGraphicResource']['href']), config('tools.db-cache-time'), function() use($data) {
					    return invType::getGraphicName(getCrestID($data['ammoGraphicResource']['href']));
					});

//var_dump($ammoType);
					if(is_array($ammoType))
						$data['ammoType'] = $ammoType[0];
					else
						$data['ammoType'] = $ammoType;
					if(isset($data['ammoType']->iconFile)) {
						$data['ammoType']->iconFile = getIcon($data['ammoType']->iconFile);
					}

					// remove _S, _M, _L, XL
					if(isset($data['ammoType']->typeName)) {
						if(substr($data['ammoType']->typeName, -2) == ' S' || substr($data['ammoType']->typeName, -2) == ' M' || substr($data['ammoType']->typeName, -2) == ' L') {
							$data['ammoType']->typeName = substr($data['ammoType']->typeName, 0, -2);
						} elseif (substr($data['ammoType']->typeName, -3) == ' XL') {
							$data['ammoType']->typeName = substr($data['ammoType']->typeName, 0, -3);
						}
					}
				}

		        break;
		    case "effects.ProjectileFired": // turrets
		    	$data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				$data['slot'] = "high";
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				if(isset($effect->ammoGraphicResource)) {
					if(isset($effect->ammoGraphicResource->color->r))
						$data['ammoGraphicResource']['r'] = round(($effect->ammoGraphicResource->color->r*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->g))
						$data['ammoGraphicResource']['g'] = round(($effect->ammoGraphicResource->color->g*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->b))
						$data['ammoGraphicResource']['b'] = round(($effect->ammoGraphicResource->color->b*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->a))
						$data['ammoGraphicResource']['a'] = $effect->ammoGraphicResource->color->a;
					$data['ammoGraphicResource']['href'] = $effect->ammoGraphicResource->href;
					$data['ammoGraphicResource']['typeID'] = getCrestID($data['ammoGraphicResource']['href']);
					$ammoType = array();
					$ammoType = Cache::remember('getGraphicName_'.snake_case($data['ammoGraphicResource']['href']), config('tools.db-cache-time'), function() use($data) {
					    return invType::getGraphicName(getCrestID($data['ammoGraphicResource']['href']));
					});

//var_dump($ammoType);
					if(is_array($ammoType))
						$data['ammoType'] = $ammoType[0];
					else
						$data['ammoType'] = $ammoType;
					if(isset($data['ammoType']->iconFile)) {
						$data['ammoType']->iconFile = getIcon($data['ammoType']->iconFile);
					}

					// remove _S, _M, _L, XL
					if(isset($data['ammoType']->typeName)) {
						if(substr($data['ammoType']->typeName, -2) == ' S' || substr($data['ammoType']->typeName, -2) == ' M' || substr($data['ammoType']->typeName, -2) == ' L') {
							$data['ammoType']->typeName = substr($data['ammoType']->typeName, 0, -2);
						} elseif (substr($data['ammoType']->typeName, -3) == ' XL') {
							$data['ammoType']->typeName = substr($data['ammoType']->typeName, 0, -3);
						}
					}
				}

		        break;
		    case "effects.MissileDeployment": // launchers
		   	 	$data['id'] = $effect->modules[$val]->moduleID_str;
				$data['duration'] = $effect->duration;
				$data['guid'] = $effect->guid;
				$data['slot'] = "high";
				if(isset($effect->targetID_str)) {
					$data['targetID'] = $effect->targetID_str;
				}
				if(isset($effect->ammoGraphicResource)) {
					if(isset($effect->ammoGraphicResource->color->r))
						$data['ammoGraphicResource']['r'] = round(($effect->ammoGraphicResource->color->r*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->g))
						$data['ammoGraphicResource']['g'] = round(($effect->ammoGraphicResource->color->g*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->b))
						$data['ammoGraphicResource']['b'] = round(($effect->ammoGraphicResource->color->b*100)*2.56);
					if(isset($effect->ammoGraphicResource->color->a))
						$data['ammoGraphicResource']['a'] = $effect->ammoGraphicResource->color->a;
					$data['ammoGraphicResource']['href'] = $effect->ammoGraphicResource->href;
					$data['ammoGraphicResource']['typeID'] = getCrestID($data['ammoGraphicResource']['href']);
					$ammoType = Cache::remember('getGraphicName_'.snake_case($data['ammoGraphicResource']['href']).'_one', config('tools.db-cache-time'), function() use($data) {
					    return invType::getGraphicName(getCrestID($data['ammoGraphicResource']['href']), false);
					});
					if(is_array($ammoType))
						$data['ammoType'] = $ammoType[0];
					else
						$data['ammoType'] = $ammoType;
					if(isset($data['ammoType']->iconFile)) {
						$data['ammoType']->iconFile = getIcon($data['ammoType']->iconFile);
					}

					// overide ammo details with missiles in air if available
					if(isset($missiles)) {
						foreach($missiles as $missile) {
							$graphicIDData = Cache::remember('getGraphicIDfromItemName_'.snake_case($missile->type->name), config('tools.db-cache-time'), function() use($missile) {
							    return invType::getGraphicIDfromItemName(getCrestID($missile->type->name));
							});
							if($data['ammoGraphicResource']['typeID'] == $graphicIDData) {
								$data['ammoType']->typeName = $missile->type->name;
								$data['ammoType']->typeID = getCrestID($missile->type->href);
								break;
							}
							
						}
					}
				}

		        break;
		}


		if(count($data) == 0) {
			$data['id'] = $effect->modules[$val]->moduleID_str;
			$data['duration'] = $effect->duration;
			$data['guid'] = $effect->guid;
			if(isset($effect->targetID_str)) {
				$data['targetID'] = $effect->targetID_str;
			}
			$data['name'] = "Unknown";
	       	//$data['icon'] = "https://image.eveonline.com/Type/1973_32.png";
	       	if(isset($effect->effectName))
	       		$data['effectName'] = $effect->effectName;
	       	else
	       		$data['effectName'] = "";
		}

		return $data;
	}

	protected function calculateROF($turretVariants, $weaponType, $duration, $roleBonus, $shipROFBonus, $shipID) {

		$precisionCheck = 8;

		$maxShipSkills = 0;

		if($shipROFBonus != 0) 
			$maxShipSkills = 5;

		// also rolebonus of 25 and 50 (don't forget them (Large Proj, Med Proj and Missiles))
		// and overheat 15%
		// rigs upto 3 10% (because no T2 allowed)
		$gunneryBonus = 2;
		$rapidFireBonus = 4;

		$missilesBonus = 2;
		$rapidLaunchBonus = 3;
		$missileSpecBonus = 2;

		$damageTurretModBonus = array(10.5, 9.75, 9, 8.25, 7.5, 0); // highly unlikely to be 10 unless Missile - will be rig if turret based (so removed)
		$damageMissileModBonus = array(10.5, 9.75, 9, 8.25, 7.5, 0); 

		$dmNum1 = 0;
		$dmNum2 = 0;
		$dmNum3 = 0;
		$dmNum4 = 0;
		$rigNum = 0;
		$rigs = 0;
		$gSkill = 0;
		$rfSkill = 0;
		$spSkill = 0;
		$shSkill = 0;
		$isMatch = false;
		$numMatches = 0;
		$tempBonus = 0;
		$myMatches = array();
		$maxStackingBonus = 8;

		switch($weaponType) {
		    case 'Energy Weapon':
		    case 'Projectile Weapon':
		    case 'Hybrid Weapon':
		    	CalcLogger::log('info', 'Target: '.$duration.' - ShipID:'.$shipID);
		    	for ($baseRofCount = 0; $baseRofCount < count($turretVariants); ++$baseRofCount) {
		    		$baseRof = $turretVariants[$baseRofCount]->baseROF;
			        for ($dmNum1 = 0; $dmNum1 < count($damageTurretModBonus); ++$dmNum1) {
			    		$dmBonus1 = $damageTurretModBonus[$dmNum1];
			    		for ($dmNum2 = 0; $dmNum2 < count($damageTurretModBonus); ++$dmNum2) {
			    			$dmBonus2 = $damageTurretModBonus[$dmNum2];
			    			for ($dmNum3 = 0; $dmNum3 < count($damageTurretModBonus); ++$dmNum3) {
			    				$dmBonus3 = $damageTurretModBonus[$dmNum3];
							    for($rigNum = 0; $rigNum < 4; ++$rigNum) {
								    if($dmNum1 >= $dmNum2 && $dmNum2 >= $dmNum3) {
									    $bonuses = array();

									    if($dmBonus1 != 0) {
									    	$bonuses[] = $dmBonus1;
									    }
									    if($dmBonus2 != 0) {
									    	$bonuses[] = $dmBonus2;
									    }
									    if($dmBonus3 != 0) {
									    	$bonuses[] = $dmBonus3;
									    }
									    
								    	for ($rigs = 0; $rigs <= $rigNum; $rigs++) {
								    		if($rigs != 0) {
								    			$bonuses[] = 10.5;
								    		}
								    	}

								    	// sort bonuses into highest order TODO: NEEDS PHP CONVERSION FROM JS
								    	//bonuses.sort(function(a, b){return b-a});
								    	sort($bonuses);

								    	$stackingBonus = 1;

								    	if(count($bonuses) > 0)
								    		$stackingBonus = calculateStackingBonus($bonuses);  	
									
										$shipSkillStart = 0;
										if($shipROFBonus > 0)
								  			$shipSkillStart = 1;
								  
								    	for($gSkill = 1; $gSkill < 6; $gSkill++) {
								    		for($rfSkill = 1; $rfSkill < 6; $rfSkill++) {
								    			for($shSkill = $shipSkillStart; $shSkill <= $maxShipSkills; $shSkill++) {
									    			if(!($gSkill < 3 && $rfSkill > 0) && ($gSkill >= $rfSkill)) {
										    			$tempBonus = $baseRof;
										    			$tempBonus *= 1-(($gSkill * $gunneryBonus)/100);
										    			$tempBonus *= 1-(($rfSkill * $rapidFireBonus)/100);
										    			$tempBonus *= 1-(($shSkill * $shipROFBonus)/100);
										    			if($weaponType == 'Projectile Weapon') {
										    				$tempBonus *= $roleBonus;
										    			}
										    			$tempBonus *= $stackingBonus;
										    			CalcLogger::log('info', $tempBonus);
										    			if(floatCompare($tempBonus, $duration)) {
										    				$isMatch = true;
										    				$numMatches++;

										    				$myObject = array();
										    				$myObject['gunneryLvl'] = $gSkill;
										    				$myObject['rfLvl'] = $rfSkill;
										    				$myObject['shSkill'] = null;
										    				if($shSkill > 0)
												    			$myObject['shSkill'] = $shSkill;
										    				$myObject['mod1'] = $dmBonus1;
										    				$myObject['mod2'] = $dmBonus2;
										    				$myObject['mod3'] = $dmBonus3;
										    				$myObject['rigs'] = $rigs-1;
										    				$myObject['overheated'] = false;
										    				$myObject['implant'] = false;
										 					$myObject['normalDamage'] = $tempBonus;
										 					$myObject['variant'] = $turretVariants[$baseRofCount];

										 					$myMatches[] = $myObject;
										    				//console.log(myObject);
										    			}

										    			// implant
									    				$tempBonus2 = $tempBonus * 0.97;
									    				CalcLogger::log('info', $tempBonus2);
									    				if(floatCompare($tempBonus2, $duration)) { 
										    				$isMatch = true;
										    				$numMatches++;

										    				$myObject = array();
										    				$myObject['gunneryLvl'] = $gSkill;
										    				$myObject['rfLvl'] = $rfSkill;
										    				$myObject['shSkill'] = null;
										    				if($shSkill > 0)
											    				$myObject['shSkill'] = $shSkill;
										    				$myObject['mod1'] = $dmBonus1;
										    				$myObject['mod2'] = $dmBonus2;
										    				$myObject['mod3'] = $dmBonus3;
										    				$myObject['rigs'] = $rigs-1;
										 					$myObject['implant'] = '3%';
										 					$myObject['overheated'] = false;
										 					$myObject['normalDamage'] = $tempBonus2;
										 					$myObject['variant'] = $turretVariants[$baseRofCount];

										 					$myMatches[] = $myObject;
										    				//console.log(myObject);
											    		}

											    		// overheating
									    				$tempBonus2 = $tempBonus * 0.85;
									    				CalcLogger::log('info', $tempBonus2);
									    				if(floatCompare($tempBonus2, $duration)) {
										    				$isMatch = true;
										    				$numMatches++;

										    				$myObject = array();
										    				$myObject['gunneryLvl'] = $gSkill;
										    				$myObject['rfLvl'] = $rfSkill;
										    				$myObject['shSkill'] = null;
										    				if($shSkill > 0)
										    					$myObject['shSkill'] = $shSkill;
										    				$myObject['mod1'] = $dmBonus1;
										    				$myObject['mod2'] = $dmBonus2;
										    				$myObject['mod3'] = $dmBonus3;
										    				$myObject['rigs'] = $rigs-1;
										    				$myObject['implant'] = false;
										    				$myObject['overheated'] = true;
										 					$myObject['normalDamage'] = $tempBonus;
										 					$myObject['variant'] = $turretVariants[$baseRofCount];

										 					$myMatches[] = $myObject;
										    				//console.log(myObject);
										    			}

										    			// overheating ++ implant
									    				$tempBonus2 = ($tempBonus * 0.85) * 0.97; // overheating ++ implant
									    				CalcLogger::log('info', $tempBonus2);
									    				if(floatCompare($tempBonus2, $duration)) {
										    				$isMatch = true;
										    				$numMatches++;

										    				$myObject = array();
										    				$myObject['gunneryLvl'] = $gSkill;
										    				$myObject['rfLvl'] = $rfSkill;
										    				$myObject['shSkill'] = null;
										    				if($shSkill > 0)
									    						$myObject['shSkill'] = $shSkill;
										    				$myObject['mod1'] = $dmBonus1;
										    				$myObject['mod2'] = $dmBonus2;
										    				$myObject['mod3'] = $dmBonus3;
										    				$myObject['rigs'] = $rigs-1;
										    				$myObject['overheated'] = true;
										    				$myObject['implant'] = '3%';
										 					$myObject['normalDamage'] = ($tempBonus*0.97);
										 					$myObject['variant'] = $turretVariants[$baseRofCount];

										 					$myMatches[] = $myObject;
										    				//console.log(myObject);
										    			}
										    		}
										    	}
								    		}
								    	}
									}
								}
							}
						}
					}
				}

				if($isMatch == true && $numMatches == 1) {
					//dd($myObject);
					return $myObject;
				}

				if($numMatches > 1) {
					$misL = $myMatches[0]['gunneryLvl'];
					$rlL = $myMatches[0]['rfLvl'];
					//var spL = myMatches[0]['spLvl'];
					
					$myDam1 = $myMatches[0]['mod1'];
					$myDam2 = $myMatches[0]['mod2'];
					$myDam3 = $myMatches[0]['mod3'];
					$rg = $myMatches[0]['rigs'];
					$oh = $myMatches[0]['overheated'];
					$imp = $myMatches[0]['implant'];
					$nd = $myMatches[0]['normalDamage'];
					$shL = null; //var shL = myMatches[0][shSkill];

					$variants = [];
					$isVariant = true;
					if($myMatches[0]['shSkill'] !== null) {
						$shL = $myMatches[0]['shSkill'];
					}

//	myObject['variant'] = turretVariants[baseRofCount];

					for ($objCount = 1; $objCount < count($myMatches); ++$objCount) {
			    		$obj = $myMatches[$objCount];

			    		if( $misL != $myMatches[$objCount]['gunneryLvl'] ||
			    			$rlL != $myMatches[$objCount]['rfLvl'] ||
			    			//spL != myMatches[objCount]['spLvl'] ||
			    			$myDam1 != $myMatches[$objCount]['mod1'] ||
			    			$myDam2 != $myMatches[$objCount]['mod2'] ||
			    			$myDam3 != $myMatches[$objCount]['mod3'] ||
			    			$rg != $myMatches[$objCount]['rigs'] ||
			    			$oh != $myMatches[$objCount]['overheated'] ||
			    			$imp != $myMatches[$objCount]['implant'] ||
			    			$nd != $myMatches[$objCount]['normalDamage']) {

			    			$isVariant = false;
			    		}

			    		if($myMatches[$objCount]['shSkill'] !== null) {
			    			if($shL != $myMatches[$objCount]['shSkill'])
			    				$isVariant = false;
			    		}
			    	}

			    	// if it's just a variant
			    	if($isVariant == true) {
			    		$myMatches[count($myMatches)-1]['variant']['typeName'] += '\r\n(or Variant with same ROF)';
			    		return $myMatches[count($myMatches)-1];
			    	} else {
						//console.log('Multi-Matches ('+shipID+') : '+numMatches);
						//console.log(myMatches);
						//console.log(shipROFBonus);
						//console.log(maxShipSkills);
						return true;
					}
				}

		        return false;
		        break;

		    case 'Missile Launcher Torpedo':
		    case 'Missile Launcher Heavy':
		    case 'Missile Launcher Rapid Heavy':
		    case 'Missile Launcher Cruise':
		    case 'Missile Launcher Rocket':
		    case 'Missile Launcher Light':
		    case 'Missile Launcher Rapid Light':
		    case 'Missile Launcher Defender':
		    case 'Missile Launcher Citadel':
		    case 'Missile Launcher Heavy Assault':
		    	for($baseRofCount = 0; $baseRofCount < count($turretVariants); ++$baseRofCount) {
		    		//dd($turretVariants[$baseRofCount]);
		    		$baseRof = $turretVariants[$baseRofCount]->baseROF;
			    	for($dmNum1 = 0; $dmNum1 < count($damageMissileModBonus); ++$dmNum1) {
			    		$dmBonus1 = $damageMissileModBonus[$dmNum1];
			    		for($dmNum2 = 0; $dmNum2 < count($damageMissileModBonus); ++$dmNum2) {
			    			$dmBonus2 = $damageMissileModBonus[$dmNum2];
			    			for($dmNum3 = 0; $dmNum3 < count($damageMissileModBonus); ++$dmNum3) {
			    				$dmBonus3 = $damageMissileModBonus[$dmNum3];
			    				for($dmNum4 = 0; $dmNum4 < count($damageMissileModBonus); ++$dmNum4) {
				    				$dmBonus4 = $damageMissileModBonus[$dmNum4];
								    for($rigNum = 0; $rigNum < 4; ++$rigNum) {
									    if($dmNum1 >= $dmNum2 && $dmNum2 >= $dmNum3 && $dmNum3 >= $dmNum4) {
										    $bonuses = [];

										    if($dmBonus1 != 0) {
										    	$bonuses[] = $dmBonus1;
										    }
										    if($dmBonus2 != 0) {
										    	$bonuses[] = $dmBonus2;
										    }
										    if($dmBonus3 != 0) {
										    	$bonuses[] = $dmBonus3;
										    }
										    if($dmBonus4 != 0) {
										    	$bonuses[] = $dmBonus4;
										    }

									    	for ($rigs = 0; $rigs <= $rigNum; $rigs++) {
									    		if($rigs != 0) {
									    			$bonuses[] = 10;
									    		}
									    	}
									    	// sort bonuses into highest order TODO: NEEDS PHP CONVERSION FROM JS
									    	//$bonuses.sort(function(a, b){return b-a});
									    	sort($bonuses);

									    	$stackingBonus = 1;

									    	if(count($bonuses) > 0)
									    		$stackingBonus = calculateStackingBonus($bonuses);  	
										
											$shipSkillStart = 0;
											if($shipROFBonus > 0)
									  			$shipSkillStart = 1;

									    	for($gSkill = 5; $gSkill < 6; $gSkill++) {
									    		for($rfSkill = 1; $rfSkill < 6; $rfSkill++) {
									    			for($spSkill = 0; $spSkill < 6; $spSkill++) {
									    				for($shSkill = $shipSkillStart; $shSkill <= $maxShipSkills; $shSkill++) {
									    					if(!($gSkill < 3 && $rfSkill > 0) && ($gSkill >= $spSkill) && ($rfSkill >= $spSkill) && count($bonuses) <= $maxStackingBonus && !($rfSkill > 0 && $rfSkill < 3) && !($gSkill > 0 && $gSkill < 3)) {
										    				//if(!(gSkill < 3 && rfSkill > 0) && (gSkill >= spSkill) && (rfSkill >= spSkill) && bonuses.length < 4) {

										    					//shipBonusModifier = 1-((shSkill * shipROFBonus))
												    			$tempBonus = $baseRof;
												    			$tempBonus *= 1-(($gSkill * $missilesBonus)/100);
												    			$tempBonus *= 1-(($rfSkill * $rapidLaunchBonus)/100);
												    			$tempBonus *= 1-(($spSkill * $missileSpecBonus)/100);
												    			$tempBonus *= 1-(($shSkill * $shipROFBonus)/100);
												    			//if(weaponType == 'Projectile Weapon') {  // VANGEL
												    			//	tempBonus *= roleBonus;
												    			//}

												    			$tempBonus *= $stackingBonus;
												    			CalcLogger::log('info', $tempBonus);
												    			if(floatCompare($tempBonus, $duration)) {
												    				$isMatch = true;
												    				$numMatches++;

												    				$myObject = array();
												    				$myObject['misLvl'] = $gSkill;
												    				$myObject['rlLvl'] = $rfSkill;
												    				$myObject['spLvl'] = $spSkill;
												    				$myObject['shSkill'] = null;
												    				if($shSkill > 0)
												    					$myObject['shSkill'] = $shSkill;
												    				$myObject['mod1'] = $dmBonus1;
												    				$myObject['mod2'] = $dmBonus2;
												    				$myObject['mod3'] = $dmBonus3;
												    				$myObject['mod4'] = $dmBonus4;
												    				$myObject['rigs'] = $rigs-1;
												    				$myObject['implant'] = false;
												    				$myObject['overheated'] = false;
												 					$myObject['normalDamage'] = $tempBonus;
												 					$myObject['variant'] = $turretVariants[$baseRofCount];

												 					$myMatches[] = $myObject;
												    				//console.log(myObject);
												    			} 

											    				// implant
											    				$tempBonus2 = $tempBonus * 0.97; // implant
											    				CalcLogger::log('info', $tempBonus2);
											    				if(floatCompare($tempBonus2, $duration)) {
												    				$isMatch = true;
												    				$numMatches++;

												    				$myObject = array();
												    				$myObject['misLvl'] = $gSkill;
												    				$myObject['rlLvl'] = $rfSkill;
												    				$myObject['spLvl'] = $spSkill;
												    				$myObject['shSkill'] = null;
												    				if($shSkill > 0)
											    						$myObject['shSkill'] = $shSkill;
												    				$myObject['mod1'] = $dmBonus1;
												    				$myObject['mod2'] = $dmBonus2;
												    				$myObject['mod3'] = $dmBonus3;
												    				$myObject['mod4'] = $dmBonus4;
												    				$myObject['rigs'] = $rigs-1;
												 					$myObject['implant'] = '3%';
												 					$myObject['overheated'] = false;
												 					$myObject['normalDamage'] = $tempBonus2;
												 					$myObject['variant'] = $turretVariants[$baseRofCount];

												 					$myMatches[] = $myObject;
												    				//console.log(myObject);
												    			} 

											    				// overheating
											    				$tempBonus2 = $tempBonus * 0.85; // implant
											    				CalcLogger::log('info', $tempBonus2);
											    				if(floatCompare($tempBonus2, $duration)) {
												    				$isMatch = true;
												    				$numMatches++;

												    				$myObject = array();
												    				$myObject['misLvl'] = $gSkill;
												    				$myObject['rlLvl'] = $rfSkill;
												    				$myObject['spLvl'] = $spSkill;
												    				$myObject['shSkill'] = null;
												    				if($shSkill > 0)
										    							$myObject['shSkill'] = $shSkill;
												    				$myObject['mod1'] = $dmBonus1;
												    				$myObject['mod2'] = $dmBonus2;
												    				$myObject['mod3'] = $dmBonus3;
												    				$myObject['mod4'] = $dmBonus4;
												    				$myObject['rigs'] = $rigs-1;
												    				$myObject['implant'] = false;
												    				$myObject['overheated'] = true;
												 					$myObject['normalDamage'] = $tempBonus;
												 					$myObject['variant'] = $turretVariants[$baseRofCount];

												 					$myMatches[] = $myObject;
												    				//console.log(myObject);
												    			} 

												    			// overheating ++ implant
											    				$tempBonus2 = ($tempBonus * 0.85) * 0.97; // implant
											    				CalcLogger::log('info', $tempBonus2);
											    				if(floatCompare($tempBonus2, $duration)) {
												    				$isMatch = true;
												    				$numMatches++;

												    				$myObject = array();
												    				$myObject['misLvl'] = $gSkill;
												    				$myObject['rlLvl'] = $rfSkill;
												    				$myObject['spLvl'] = $spSkill;
												    				$myObject['shSkill'] = null;
												    				if($shSkill > 0)
									    								$myObject['shSkill'] = $shSkill;
												    				$myObject['mod1'] = $dmBonus1;
												    				$myObject['mod2'] = $dmBonus2;
												    				$myObject['mod3'] = $dmBonus3;
												    				$myObject['mod4'] = $dmBonus4;
												    				$myObject['rigs'] = $rigs-1;
												    				$myObject['overheated'] = true;
												    				$myObject['implant'] = '3%';
												 					$myObject['normalDamage'] = $tempBonus * 0.97;
												 					$myObject['variant'] = $turretVariants[$baseRofCount];

												 					$myMatches[] = $myObject;
												    				//console.log(myObject);
												    			}
												    		}
												    	}
										    		}
									    		}
									    	}
									    }
									}
								}
							}
						}
					}
				}
//if($shipID == 1018941037107) {
	//dd($myObject);
//}
				if($isMatch == true && $numMatches == 1) {
					//dd($myObject);
					return $myObject;
				}

				if($numMatches > 1) {
					$misL = $myMatches[0]['misLvl'];
					$rlL = $myMatches[0]['rlLvl'];
					$spL = $myMatches[0]['spLvl'];
					
					$myDam1 = $myMatches[0]['mod1'];
					$myDam2 = $myMatches[0]['mod2'];
					$myDam3 = $myMatches[0]['mod3'];
					$myDam4 = $myMatches[0]['mod4'];
					$rg = $myMatches[0]['rigs'];
					$oh = $myMatches[0]['overheated'];
					$imp = $myMatches[0]['implant'];
					$nd = $myMatches[0]['normalDamage'];
					$shL = null; //var shL = myMatches[0][shSkill];

					$variants = [];
					$isVariant = true;
					if($myMatches[0]['shSkill'] !== null) {
						$shL = $myMatches[0]['shSkill'];
					}

//	myObject['variant'] = turretVariants[baseRofCount];

					for ($objCount = 1; $objCount < count($myMatches); ++$objCount) {
			    		$obj = $myMatches[$objCount];

			    		if( $misL != $myMatches[$objCount]['misLvl'] ||
			    			$rlL != $myMatches[$objCount]['rlLvl'] ||
			    			$spL != $myMatches[$objCount]['spLvl'] ||
			    			$myDam1 != $myMatches[$objCount]['mod1'] ||
			    			$myDam2 != $myMatches[$objCount]['mod2'] ||
			    			$myDam3 != $myMatches[$objCount]['mod3'] ||
			    			$myDam4 != $myMatches[$objCount]['mod4'] ||
			    			$rg != $myMatches[$objCount]['rigs'] ||
			    			$oh != $myMatches[$objCount]['overheated'] ||
			    			$imp != $myMatches[$objCount]['implant'] ||
			    			$nd != $myMatches[$objCount]['normalDamage']) {

			    			$isVariant = false;
			    		}

			    		if($myMatches[$objCount]['shSkill'] !== null) {
			    			if($shL != $myMatches[$objCount]['shSkill'])
			    				$isVariant = false;
			    		}
			    	}

			    	// if it's just a variant
			    	if($isVariant == true) {
			    		$myMatches[count($myMatches)-1]['variant']['typeName'] += '\r\n(or Variant with same ROF)';
			    		return $myMatches[count($myMatches)-1];
			    	} else {
						//console.log('Multi-Matches ('+shipID+') : '+numMatches);
						//console.log(myMatches);
						//console.log(shipROFBonus);
						//console.log(maxShipSkills);
						return true;
					}
				} 
		        return false;
		        break;

		    case 'Mining Laser':
		    case 'Festival Launcher':
		    case 'Missile Launcher Bomb':
		    	return true;
		    	break;
		    default:
		        //console.log(weaponType+' is not recognized');
		        return false; 
		}
	}
}

/*

"{"
	1018941009192":{
		"ROFChecked":false,
		"hiSlot":
			[
				{
					"id":"1018941014658",
					"graphicID":"res:/ui/texture/icons/109_64_1.png",
					"name":"Rapid+Heavy+Missile+Launcher+II",
					"typeID":"33450",
					"groupName":"Missile+Launcher+Rapid+Heavy",
					"turretVariants":
					[
						{
							"typeName":"Rapid+Heavy+Missile+Launcher+II",
							"typeID":33450,"baseROF":5185,"meta":5}],
							"calculatedROF":false
						},
						{
							"id":"1018941014653",
							"graphicID":"res:/ui/texture/icons/109_64_1.png",
							"name":"Rapid+Heavy+Missile+Launcher+II",
							"typeID":"33450",
							"groupName":"Missile+Launcher+Rapid+Heavy",
							"turretVariants":
							[
								{
									"typeName":"Rapid+Heavy+Missile+Launcher+II",
									"typeID":33450,
									"baseROF":5185,
									"meta":5
								}
							],
							"calculatedROF":false
						},
						{
							"id":"1018941014627",
							"graphicID":"res:/ui/texture/icons/109_64_1.png",
							"name":"Rapid+Heavy+Missile+Launcher+II",
							"typeID":"33450",
							"groupName":"Missile+Launcher+Rapid+Heavy",
							"turretVariants":
							[
								{
									"typeName":"Rapid+Heavy+Missile+Launcher+II",
									"typeID":33450,
									"baseROF":5185,
									"meta":5
								}
							],
							"calculatedROF":false
						},
						{
							"id":"1018941014638",
							"graphicID":"res:/ui/texture/icons/109_64_1.png",
							"name":"Rapid+Heavy+Missile+Launcher+II",
							"typeID":"33450",
							"groupName":"Missile+Launcher+Rapid+Heavy",
							"turretVariants":
							[
								{
									"typeName":"Rapid+Heavy+Missile+Launcher+II",
									"typeID":33450,
									"baseROF":5185,
									"meta":5
								}
							],
							"calculatedROF":false
						},
						{
							"id":"1018941014623",
							"graphicID":"res:/ui/texture/icons/109_64_1.png",
							"name":"Rapid+Heavy+Missile+Launcher+II",
							"typeID":"33450",
							"groupName":"Missile+Launcher+Rapid+Heavy",
							"turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018941014681","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false}],"shipROFBonus":5,"shipType":"Scorpion+Navy+Issue"},
	"1018941037107":
	{
		"ROFChecked":false,
		"hiSlot":
		[
			{
				"id":"1018941047548",
				"graphicID":"45_11",
				"name":"Cruise+Missile+Launcher+II",
				"typeID":"19739",
				"groupName":"Missile+Launcher+Cruise",
				"turretVariants":
				[
					{
						"typeName":"Cruise+Missile+Launcher+II",
						"typeID":19739,
						"baseROF":16540,
						"meta":5
					}
				],
				"calculatedROF":false
			},
			{
				"id":"1018941047540",
				"graphicID":"45_11",
				"name":"Cruise+Missile+Launcher+II",
				"typeID":"19739",
				"groupName":"Missile+Launcher+Cruise",
				"turretVariants":
				[
					{
						"typeName":"Cruise+Missile+Launcher+II",
						"typeID":19739,
						"baseROF":16540,
						"meta":5
					}
				],
				"calculatedROF":false
			},
			{
				"id":"1018941047551",
				"graphicID":"45_11",
				"name":"Cruise+Missile+Launcher+II",
				"typeID":"19739",
				"groupName":"Missile+Launcher+Cruise",
				"turretVariants":
				[
					{
						"typeName":"Cruise+Missile+Launcher+II",
						"typeID":19739,
						"baseROF":16540,
						"meta":5
					}
				],
				"calculatedROF":false
			},{"id":"1018941047524","graphicID":"45_11","name":"Cruise+Missile+Launcher+II","typeID":"19739","groupName":"Missile+Launcher+Cruise","turretVariants":[{"typeName":"Cruise+Missile+Launcher+II","typeID":19739,"baseROF":16540,"meta":5}],"calculatedROF":false},{"id":"1018941047533","graphicID":"45_11","name":"Cruise+Missile+Launcher+II","typeID":"19739","groupName":"Missile+Launcher+Cruise","turretVariants":[{"typeName":"Cruise+Missile+Launcher+II","typeID":19739,"baseROF":16540,"meta":5}],"calculatedROF":false},{"id":"1018941047552","graphicID":"45_11","name":"Cruise+Missile+Launcher+II","typeID":"19739","groupName":"Missile+Launcher+Cruise","turretVariants":[{"typeName":"Cruise+Missile+Launcher+II","typeID":19739,"baseROF":16540,"meta":5}],"calculatedROF":false},{"id":"1018941047554","graphicID":"45_11","name":"Cruise+Missile+Launcher+II","typeID":"19739","groupName":"Missile+Launcher+Cruise","turretVariants":[{"typeName":"Cruise+Missile+Launcher+II","typeID":19739,"baseROF":16540,"meta":5}],"calculatedROF":false}],"shipROFBonus":0,"shipType":"Raven+Navy+Issue"},"1018940344738":{"ROFChecked":false,"hiSlot":[{"id":"1018940513083","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018941024204","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018941024211","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018941024197","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018941024194","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018941024199","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false}],"shipROFBonus":5,"shipType":"Scorpion+Navy+Issue"},"1018940342236":{"ROFChecked":true},"1018932038185":{"ROFChecked":true},"1018940383205":{"ROFChecked":true},"1018938157877":{"ROFChecked":false,"hiSlot":[{"id":"1018938245122","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018938245854","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018938245993","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018938245927","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018938245775","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false}],"shipROFBonus":5,"shipType":"Widow"},"1018938138634":{"ROFChecked":false,"hiSlot":[{"id":"1018938152070","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018938152568","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018938152648","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018938152811","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false},{"id":"1018938151822","graphicID":"res:/ui/texture/icons/109_64_1.png","name":"Rapid+Heavy+Missile+Launcher+II","typeID":"33450","groupName":"Missile+Launcher+Rapid+Heavy","turretVariants":[{"typeName":"Rapid+Heavy+Missile+Launcher+II","typeID":33450,"baseROF":5185,"meta":5}],"calculatedROF":false}],"shipROFBonus":5,"shipType":"Widow"},"1018941637174":{"ROFChecked":false,"hiSlot":[{"id":"1018941648123","graphicID":"21_12","name":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":"8101","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":8101,"baseROF":14250,"meta":1},{"typeName":"Advanced+&#039;Limos&#039;+Heavy+Missile+Bay+I","typeID":8103,"baseROF":13500,"meta":2},{"typeName":"XR-3200+Heavy+Missile+Bay","typeID":7997,"baseROF":12750,"meta":3},{"typeName":"&#039;Arbalest&#039;+Heavy+Missile+Launcher","typeID":8105,"baseROF":12000,"meta":4},{"typeName":"&#039;Undertaker&#039;+Heavy+Missile+Launcher","typeID":20599,"baseROF":13500,"meta":6}],"calculatedROF":false},{"id":"1018941646083","graphicID":"21_12","name":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":"8101","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":8101,"baseROF":14250,"meta":1},{"typeName":"Advanced+&#039;Limos&#039;+Heavy+Missile+Bay+I","typeID":8103,"baseROF":13500,"meta":2},{"typeName":"XR-3200+Heavy+Missile+Bay","typeID":7997,"baseROF":12750,"meta":3},{"typeName":"&#039;Arbalest&#039;+Heavy+Missile+Launcher","typeID":8105,"baseROF":12000,"meta":4},{"typeName":"&#039;Undertaker&#039;+Heavy+Missile+Launcher","typeID":20599,"baseROF":13500,"meta":6}],"calculatedROF":false},{"id":"1018941644862","graphicID":"21_12","name":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":"8101","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":8101,"baseROF":14250,"meta":1},{"typeName":"Advanced+&#039;Limos&#039;+Heavy+Missile+Bay+I","typeID":8103,"baseROF":13500,"meta":2},{"typeName":"XR-3200+Heavy+Missile+Bay","typeID":7997,"baseROF":12750,"meta":3},{"typeName":"&#039;Arbalest&#039;+Heavy+Missile+Launcher","typeID":8105,"baseROF":12000,"meta":4},{"typeName":"&#039;Undertaker&#039;+Heavy+Missile+Launcher","typeID":20599,"baseROF":13500,"meta":6}],"calculatedROF":false},{"id":"1018938471862","graphicID":"21_12","name":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":"8101","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":8101,"baseROF":14250,"meta":1},{"typeName":"Advanced+&#039;Limos&#039;+Heavy+Missile+Bay+I","typeID":8103,"baseROF":13500,"meta":2},{"typeName":"XR-3200+Heavy+Missile+Bay","typeID":7997,"baseROF":12750,"meta":3},{"typeName":"&#039;Arbalest&#039;+Heavy+Missile+Launcher","typeID":8105,"baseROF":12000,"meta":4},{"typeName":"&#039;Undertaker&#039;+Heavy+Missile+Launcher","typeID":20599,"baseROF":13500,"meta":6}],"calculatedROF":false},{"id":"1018941647888","graphicID":"21_12","name":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":"8101","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"&#039;Malkuth&#039;+Heavy+Missile+Launcher+I","typeID":8101,"baseROF":14250,"meta":1},{"typeName":"Advanced+&#039;Limos&#039;+Heavy+Missile+Bay+I","typeID":8103,"baseROF":13500,"meta":2},{"typeName":"XR-3200+Heavy+Missile+Bay","typeID":7997,"baseROF":12750,"meta":3},{"typeName":"&#039;Arbalest&#039;+Heavy+Missile+Launcher","typeID":8105,"baseROF":12000,"meta":4},{"typeName":"&#039;Undertaker&#039;+Heavy+Missile+Launcher","typeID":20599,"baseROF":13500,"meta":6}],"calculatedROF":false}],"shipROFBonus":0,"shipType":"Rook"},"1018938256213":{"ROFChecked":false,"hiSlot":[{"id":"1018938351940","graphicID":"21_12","name":"Heavy+Missile+Launcher+II","typeID":"2410","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"Heavy+Missile+Launcher+II","typeID":2410,"baseROF":12000,"meta":5}],"calculatedROF":false},{"id":"1018938309289","graphicID":"21_12","name":"Heavy+Missile+Launcher+II","typeID":"2410","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"Heavy+Missile+Launcher+II","typeID":2410,"baseROF":12000,"meta":5}],"calculatedROF":false},{"id":"1018938351867","graphicID":"21_12","name":"Heavy+Missile+Launcher+II","typeID":"2410","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"Heavy+Missile+Launcher+II","typeID":2410,"baseROF":12000,"meta":5}],"calculatedROF":false},{"id":"1018938351670","graphicID":"21_12","name":"Heavy+Missile+Launcher+II","typeID":"2410","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"Heavy+Missile+Launcher+II","typeID":2410,"baseROF":12000,"meta":5}],"calculatedROF":false},{"id":"1018938351777","graphicID":"21_12","name":"Heavy+Missile+Launcher+II","typeID":"2410","groupName":"Missile+Launcher+Heavy","turretVariants":[{"typeName":"Heavy+Missile+Launcher+II","typeID":2410,"baseROF":12000,"meta":5}],"calculatedROF":false}],"shipROFBonus":0,"shipType":"Rook"},"1018938488338":{"ROFChecked":false,"hiSlot":[{"id":"1018938513405","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":7433.1232523104},{"id":"1018938513633","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":7433.1232523104},{"id":"1018938489020","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":7433.1232523104},{"id":"1018938513526","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":7433.1232523104},{"id":"1018938513793","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":7433.1232523104},{"id":"1018938513255","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":7433.1232523104}],"shipROFBonus":0,"shipType":"Hurricane+Fleet+Issue"},"1018938269174":{"ROFChecked":false,"hiSlot":[{"id":"1018938386676","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":8325.0980425877},{"id":"1018938386977","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":8325.0980425877},{"id":"1018938387131","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":8325.0980425877},{"id":"1018938367533","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":8325.0980425877},{"id":"1018938387226","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":8325.0980425877},{"id":"1018938386415","graphicID":"12_14","name":"720mm+Howitzer+Artillery+II","typeID":"2969","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"720mm+Howitzer+Artillery+II","typeID":2969,"baseROF":18003,"meta":5}],"calculatedROF":false,"currentROF":8325.0980425877}],"shipROFBonus":0,"shipType":"Hurricane+Fleet+Issue"},"1018938081064":{"ROFChecked":false,"hiSlot":[{"id":"1018938534724","graphicID":"49_07","name":"Bomb+Launcher+I","typeID":"27914","groupName":"Missile+Launcher+Bomb","turretVariants":[{"typeName":"Bomb+Launcher+I","typeID":27914,"baseROF":10000,"meta":0}],"calculatedROF":true},{"id":"1018938531545","graphicID":"21_16","name":"Torpedo+Launcher+II","typeID":"2420","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Torpedo+Launcher+II","typeID":2420,"baseROF":14400,"meta":5},{"typeName":"Polarized+Torpedo+Launcher","typeID":34294,"baseROF":11520,"meta":9}],"calculatedROF":false,"currentROF":6093.6356143889},{"id":"1018938071318","graphicID":"21_16","name":"Torpedo+Launcher+II","typeID":"2420","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Torpedo+Launcher+II","typeID":2420,"baseROF":14400,"meta":5},{"typeName":"Polarized+Torpedo+Launcher","typeID":34294,"baseROF":11520,"meta":9}],"calculatedROF":false,"currentROF":6093.6356143889},{"id":"1018938531688","graphicID":"21_16","name":"Torpedo+Launcher+II","typeID":"2420","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Torpedo+Launcher+II","typeID":2420,"baseROF":14400,"meta":5},{"typeName":"Polarized+Torpedo+Launcher","typeID":34294,"baseROF":11520,"meta":9}],"calculatedROF":false,"currentROF":6093.6356143889}],"shipROFBonus":0,"shipType":"Manticore"},"1018941691314":{"ROFChecked":false,"hiSlot":[{"id":"1018941693451","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false},{"id":"1018941707703","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false},{"id":"1018941707706","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false},{"id":"1018941704343","graphicID":"49_07","name":"Bomb+Launcher+I","typeID":"27914","groupName":"Missile+Launcher+Bomb","turretVariants":[{"typeName":"Bomb+Launcher+I","typeID":27914,"baseROF":10000,"meta":0}],"calculatedROF":false}],"shipROFBonus":0,"shipType":"Hound"},"1018938513901":{"ROFChecked":false,"hiSlot":[{"id":"1018938524686","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false},{"id":"1018938519550","graphicID":"49_07","name":"Bomb+Launcher+I","typeID":"27914","groupName":"Missile+Launcher+Bomb","turretVariants":[{"typeName":"Bomb+Launcher+I","typeID":27914,"baseROF":10000,"meta":0}],"calculatedROF":false},{"id":"1018938524460","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false},{"id":"1018938524805","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false}],"shipROFBonus":0,"shipType":"Manticore"},"1018938073104":{"ROFChecked":false,"hiSlot":[{"id":"1018938091995","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false},{"id":"1018938089138","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false},{"id":"1018938091206","graphicID":"21_16","name":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":"8113","groupName":"Missile+Launcher+Torpedo","turretVariants":[{"typeName":"Upgraded+&#039;Malkuth&#039;+Torpedo+Launcher","typeID":8113,"baseROF":17100,"meta":1},{"typeName":"Limited+&#039;Limos&#039;+Torpedo+Launcher","typeID":8115,"baseROF":16200,"meta":2},{"typeName":"Experimental+ZW-4100+Torpedo+Launcher","typeID":8001,"baseROF":15300,"meta":3},{"typeName":"Prototype+&#039;Arbalest&#039;+Torpedo+Launcher","typeID":8117,"baseROF":14400,"meta":4},{"typeName":"&#039;Barrage&#039;+Torpedo+Launcher","typeID":20603,"baseROF":16200,"meta":6},{"typeName":"Mizuro&#039;s+Modified+Torpedo+Launcher","typeID":14524,"baseROF":13680,"meta":11},{"typeName":"Kaikka&#039;s+Modified+Torpedo+Launcher","typeID":14680,"baseROF":11970,"meta":11},{"typeName":"Hakim&#039;s+Modified+Torpedo+Launcher","typeID":14525,"baseROF":12960,"meta":12},{"typeName":"Vepas&#039;s+Modified+Torpedo+Launcher","typeID":14682,"baseROF":10710,"meta":13},{"typeName":"Gotan&#039;s+Modified+Torpedo+Launcher","typeID":14526,"baseROF":12240,"meta":13},{"typeName":"Estamel&#039;s+Modified+Torpedo+Launcher","typeID":14683,"baseROF":10080,"meta":14}],"calculatedROF":false},{"id":"1018938133489","graphicID":"49_07","name":"Bomb+Launcher+I","typeID":"27914","groupName":"Missile+Launcher+Bomb","turretVariants":[{"typeName":"Bomb+Launcher+I","typeID":27914,"baseROF":10000,"meta":0}],"calculatedROF":false}],"shipROFBonus":0,"shipType":"Nemesis"},"1018938359148":{"ROFChecked":false,"hiSlot":[{"id":"1018938401037","graphicID":"12_09","name":"200mm+AutoCannon+I","typeID":"486","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"200mm+AutoCannon+I","typeID":486,"baseROF":3750,"meta":0},{"typeName":"200mm+Light+Carbine+Repeating+Cannon+I","typeID":8865,"baseROF":3750,"meta":1},{"typeName":"200mm+Light+Gallium+Machine+Gun","typeID":8867,"baseROF":3750,"meta":2},{"typeName":"200mm+Light+Prototype+Automatic+Cannon","typeID":8869,"baseROF":3750,"meta":3},{"typeName":"200mm+Light+&#039;Scout&#039;+Autocannon+I","typeID":8863,"baseROF":3750,"meta":4}],"calculatedROF":false},{"id":"1018938401299","graphicID":"12_09","name":"200mm+AutoCannon+I","typeID":"486","groupName":"Projectile+Weapon","turretVariants":[{"typeName":"200mm+AutoCannon+I","typeID":486,"baseROF":3750,"meta":0},{"typeName":"200mm+Light+Carbine+Repeating+Cannon+I","typeID":8865,"baseROF":3750,"meta":1},{"typeName":"200mm+Light+Gallium+Machine+Gun","typeID":8867,"baseROF":3750,"meta":2},{"typeName":"200mm+Light+Prototype+Automatic+Cannon","typeID":8869,"baseROF":3750,"meta":3},{"typeName":"200mm+Light+&#039;Scout&#039;+Autocannon+I","typeID":8863,"baseROF":3750,"meta":4}],"calculatedROF":false}],"shipROFBonus":0,"shipType":"Tristan"},"1018941727320":{"ROFChecked":false,"hiSlot":[{"id":"1018941741758","graphicID":"13_05","name":"75mm+Gatling+Rail+II","typeID":"3098","groupName":"Hybrid+Weapon","turretVariants":[{"typeName":"75mm+Gatling+Rail+II","typeID":3098,"baseROF":2600,"meta":5}],"calculatedROF":false},{"id":"1018941741229","graphicID":"13_05","name":"75mm+Gatling+Rail+II","typeID":"3098","groupName":"Hybrid+Weapon","turretVariants":[{"typeName":"75mm+Gatling+Rail+II","typeID":3098,"baseROF":2600,"meta":5}],"calculatedROF":false}],"shipROFBonus":0,"shipType":"Tristan"}}"


			*/