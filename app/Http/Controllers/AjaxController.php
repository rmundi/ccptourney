<?php

namespace Thunk\Http\Controllers;

use Illuminate\Http\Request;

use Thunk\Http\Requests;

class AjaxController extends Controller
{
    public function postViewlpstore() {
    	/*
    	$method = $request->method();

		if ($request->isMethod('post')) {
		    //
		}
		*/

		// called by Character Sheet.
		
		//if (!$request->ajax()) {
			// log to security as some form of attack
			//securityLog::log('warn', $PLAuth['_username'].'['.$PLAuth['_userid'].'] ['.Request::getClientIp().'] Attempted to add a Supercap: '.Request::url().' using a non-Ajax method');

		//	return 'Denied.';
		//}

		return '<div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				     <h4 class="modal-title">Add New Pilot (There is no editing or deleting, so get this right)</h4>
				</div>
				<div class="modal-body">
					<div class="">
						<div>
							<label>Pilot Name:
							<input id="pilotNameInput" type="text" class="form-control" placeholder="Pilot Name"></label>
						</div>
						<div>
							<label>Ship:
							<select class="form-control" id="shipTypeSelect">
							  <option>Aeon</option>
							  <option>Hel</option>
							  <option>Nyx</option>
							  <option>Wyvern</option>
							  <option>Revenant</option>
							  <option>Avatar</option>
							  <option>Erebus</option>
							  <option>Leviathan</option>
							  <option>Ragnarok</option>
							</select></label>
						</div>
						<div>
							<label>Location:
							<input id="locationNameInput" type="text" class="form-control" placeholder="System Name"></label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
				    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				    <button id="update-api" type="button" class="btn btn-primary">Update API</button>
				</div>';
		
		
	}
}
