<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class npcLPStore extends Model
{
    public $primaryKey = 'corporationID';
    public $timestamps = false;
    protected $table = 'npcLPStore';
    protected $fillable = [ 'corporationID', 
    						'storeItemID'];


    /*
	SELECT s.storeItemID, i.quantity AS itemQuantity, i.iskCost, i.lpCost, i.typeName, i.typeID, r.typeID AS reqTypeID, r.typeName AS reqTypeName, r.quantity AS reqQuantity 
	FROM npcLPStore AS s
	JOIN npcLPStoreItems AS i ON s.storeItemID = i.storeItemID
	LEFT JOIN npcLPStoreRequiredItems AS r ON s.storeItemID = r.storeItemID 
	WHERE s.corporationID = 1000002

	SELECT s.storeItemID, i.quantity AS itemQuantity, i.iskCost, i.lpCost, i.typeName, i.typeID, 
	r.typeID AS reqTypeID, r.typeName AS reqTypeName, r.quantity AS reqQuantity,
	e.iconFile, g.categoryID, g.groupName
	FROM npcLPStore AS s
	JOIN npcLPStoreItems AS i ON s.storeItemID = i.storeItemID
	LEFT JOIN npcLPStoreRequiredItems AS r ON s.storeItemID = r.storeItemID
	# Icon
	LEFT JOIN invTypes AS t ON t.typeID = i.typeID
	LEFT JOIN eveIcons AS e ON e.iconID = t.iconID
	LEFT JOIN invGroups AS g ON g.groupID = t.groupID
	WHERE s.corporationID = 1000002
    */

    public function scopeGetStore($query, $corporationID = null) {
        if(!isset($corporationID))
            return false;

        $data = $query->select('npcLPStore.storeItemID', 'i.quantity AS itemQuantity', 'i.iskCost', 'i.lpCost', 'i.typeName', 'i.typeID', 'g.categoryID', 'g.groupName', 'c.categoryName',
        						\DB::raw('IF(r.typeID IS NULL,NULL, CONCAT(\'[\', 
        						GROUP_CONCAT(CONCAT(\'{ "reqTypeID":\',r.typeID,\',"reqTypeName":"\', REPLACE(r.typeName, \'"\', \'\\"\'),
          						 \'","reqQuantity":\',r.quantity, \' }\')), \']\')) AS reqItems'))
				->join('npcLPStoreItems AS i', 'npcLPStore.storeItemID', '=', 'i.storeItemID')
                ->leftjoin('npcLPStoreRequiredItems AS r', 'npcLPStore.storeItemID', '=', 'r.storeItemID')
                ->leftjoin('invTypes AS t', 't.typeID', '=', 'i.typeID')
                ->leftjoin('invGroups AS g', 'g.groupID', '=', 't.groupID')
                ->leftjoin('invCategories AS c', 'c.categoryID', '=', 'g.categoryID')
                ->where('npcLPStore.corporationID', '=', $corporationID)
                ->groupBy('npcLPStore.storeItemID')
                ->get();

        return $data;
    }
}
