<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class EmploymentHistory extends Model
{
    public $primaryKey = 'recordID';
    public $timestamps = false;
    protected $table = 'apiEmploymentHistory';
    protected $fillable = [ 'characterID', 
    						'recordID', 
    						'corporationID',
    						'corporationName', 
    						'startDate'];
}
