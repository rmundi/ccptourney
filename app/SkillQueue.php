<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class SkillQueue extends Model
{
    public $primaryKey = 'characterID';
    public $timestamps = false;
    protected $table = 'apiSkillQueue';
    protected $fillable = [ 'characterID', 
    						'queuePosition', 
    						'typeID',
    						'level', 
    						'startSP', 
    						'endSP', 
    						'startTime', 
    						'endTime'];

    public function scopeGetSkillQueue($query, $characterID = null) {
        if(!isset($characterID))
            return false;

        $data = $query->select('apiSkillQueue.*', 't.typeName')
                ->join('eveSkillTree AS t', 'apiSkillQueue.typeID', '=', 't.typeID')
                ->where('apiSkillQueue.characterID', '=', $characterID)
                ->whereRaw('UTC_TIMESTAMP() < apiSkillQueue.endTime')
                ->orderBy('apiSkillQueue.endTime', 'ASC')
                ->get();

        return $data;
    }
}
