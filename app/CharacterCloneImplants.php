<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class CharacterCloneImplants extends Model
{
    public $primaryKey = 'characterID';
    public $timestamps = false;
    protected $table = 'apiJumpCloneImplants';
    protected $fillable = [ 'characterID', 
    						'jumpCloneID', 
    						'typeID',
    						'typeName'];

    public function scopeGetCloneImplants($query, $characterID = null) {
        if(!isset($characterID))
            return false;

        $data = $query->select('apiJumpCloneImplants.*', \DB::raw('COALESCE(a.valueInt, a.valueFloat) AS slotNum'), \DB::raw('COALESCE(c.valueInt, c.valueFloat) AS chaBonus'), \DB::raw('COALESCE(w.valueInt, w.valueFloat) AS willBonus')
        	, \DB::raw('COALESCE(p.valueInt, p.valueFloat) AS percBonus'), \DB::raw('COALESCE(m.valueInt, m.valueFloat) AS memBonus'), \DB::raw('COALESCE(i.valueInt, i.valueFloat) AS intBonus'))
                ->leftjoin('dgmTypeAttributes AS a', function($join)
                {

                    $join->on('apiJumpCloneImplants.typeID', '=', 'a.typeID')
                         ->where('a.attributeID', '=', 331);
                })
                ->leftjoin('dgmTypeAttributes AS c', function($join)
                {

                    $join->on('apiJumpCloneImplants.typeID', '=', 'c.typeID')
                         ->where('c.attributeID', '=', 175);
                })
                ->leftjoin('dgmTypeAttributes AS i', function($join)
                {

                    $join->on('apiJumpCloneImplants.typeID', '=', 'i.typeID')
                         ->where('i.attributeID', '=', 176);
                })
                ->leftjoin('dgmTypeAttributes AS m', function($join)
                {

                    $join->on('apiJumpCloneImplants.typeID', '=', 'm.typeID')
                         ->where('m.attributeID', '=', 177);
                })
                ->leftjoin('dgmTypeAttributes AS p', function($join)
                {

                    $join->on('apiJumpCloneImplants.typeID', '=', 'p.typeID')
                         ->where('p.attributeID', '=', 178);
                })
                ->leftjoin('dgmTypeAttributes AS w', function($join)
                {

                    $join->on('apiJumpCloneImplants.typeID', '=', 'w.typeID')
                         ->where('w.attributeID', '=', 179);
                })
                ->where('apiJumpCloneImplants.characterID', '=', $characterID)
                ->orderBy('apiJumpCloneImplants.jumpCloneID', 'ASC')
                ->orderBy('slotNum', 'ASC')
                ->get();

        return $data;
    }

}
