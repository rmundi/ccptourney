<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class eveSupers extends Model
{
    public $primaryKey = 'characterID';
    public $timestamps = true;
    protected $table = 'eveSupercaps';
    protected $fillable = [ 'characterID',
    						'characterName', 
    						'shipTypeID', 
    						'shipTypeName',
    						'firstSeen',
    						'firstSeenSystemID',
    						'firstSeenSystemName',
    						'firstSeenRegionName',
    						'lastSeen',
    						'lastSeenSystemID',
    						'lastSeenSystemName',
    						'lastSeenRegionName',
    						'totalKills',
    						'corporationID',
    						'corporationName',
    						'allianceID',
    						'allianceName',
    						'factionID',
    						'factionName',
    						'lastMailX',
    						'lastMailY',
    						'lastMailZ',
    						'mailClosestCelestial',
    						'mailClosestCelestialDistance',
    						'isDestroyed',
    						'firstKillID',
    						'lastKillID'];

    public function scopeGetSuperCapTable($query, $ownerID, $filterStartDate, $filterEndDate) {

    	if($filterStartDate == '')
    		$filterStartDate = null;
    	if($filterEndDate == '')
    		$filterEndDate = null;
    	
        $data = $query->select('eveSupercaps.characterID', 'eveSupercaps.characterName', 'eveSupercaps.corporationName', 'eveSupercaps.allianceName', 'eveSupercaps.factionName', 'eveSupercaps.totalKills', 
        		'eveSupercaps.shipTypeName', \DB::raw('IF(eveSupercaps.shipTypeName = "Avatar" OR eveSupercaps.shipTypeName = "Erebus" OR eveSupercaps.shipTypeName = "Leviathan" OR eveSupercaps.shipTypeName = "Ragnarok", "T", "SC") AS shipClass'), 
                'eveSupercaps.lastSeen','eveSupercaps.lastSeenSystemName', 'eveSupercaps.lastSeenRegionName',
                'eveSupercaps.corporationID', 'eveSupercaps.allianceID', 'eveSupercaps.factionID',
                'u.notes', 'u.associatedChars', 'u.apiKey', 'u.apiID', 'eveSupercaps.mailClosestCelestial', 'eveSupercaps.mailClosestCelestialDistance',
                'eveSupercaps.isDestroyed', 'eveSupercaps.lastKillID')
                ->leftjoin('eveSupercapUsers AS u', function($join) use ($ownerID)
                {
                    $join->on('u.characterID', '=', 'eveSupercaps.characterID')->where('u.ownerID', '=', $ownerID);
                });
        
        if($filterStartDate)
            $data->where('eveSupercaps.lastseen', '>', $filterStartDate." 00:00:00");

        if($filterEndDate)
            $data->where('eveSupercaps.lastseen', '<', $filterEndDate." 23:59:59");    

        return $data;
    }
    /*
$superCaps = DB::connection(DB_PLKILLBOARD)
            ->table('eve_supercaps AS s')
            ->select(array('s.characterID', 's.characterName', 's.corporationName', 's.allianceName', 's.factionName', 's.totalKills', 's.shipTypeName', 
                DB::connection(DB_PLKILLBOARD)->raw('IF(s.shipTypeName = "Avatar" OR s.shipTypeName = "Erebus" OR s.shipTypeName = "Leviathan" OR s.shipTypeName = "Ragnarok", "T", "SC") AS shipClass'), 
                's.lastSeen','s.lastSeenSystemName', 's.lastSeenRegionName',
                's.corporationID', 's.allianceID', 's.factionID',
                's.notes', 's.associatedChars', 's.apiKey', 's.apiID', 's.shipClosestCelestial', 's.shipClosestCelestialDistance',
                's.isDestroyed', 's.lastKillID'));
*/
}
