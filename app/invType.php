<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class invType extends Model
{
    //
	protected $table = 'invTypes as t';
    public $timestamps = false;
    public $primaryKey = 'typeID';

    public function scopeGetTypeName($query, $typeID = null) {
		if(!isset($typeID))
			return false;

		$first = $query->select('t.typeName')
                ->where('t.typeID', '=', $typeID)
                ->first();

        if (isset($first)) {
			return $first->typeName;
		} else {
			return false; // doesn't have a match
		}
	}

    public function scopeGetT3SlotModifier($query, $shipType = null) {
		if(!isset($shipType))
			return false;

		$data = $query->select('a.attributeID', \DB::raw('coalesce(valueFloat,valueInt) AS value'))
                ->leftjoin('dgmTypeAttributes AS a', 'a.typeID', '=', 't.typeID')
                ->whereIn('a.attributeID', array(1374,1375,1376))
                ->where('t.typeName', '=', $shipType)
                //->remember(CPPDB_REMEMBER_TIME)
                ->get();

        $items = array();
		foreach($data as $row) {
			$items[$row->attributeID] = $row->value;
		}

		if(count($items) > 0)
			return $items;
		else
			return false;
	}

    public function scopeGetGraphicName($query, $graphicID, $one = true) {
		if(!intval($graphicID))
			return false;

		$first = $query->select('t.typeName', 't.typeID', 'i.iconFile', 'c.categoryName', 'g.groupName', \DB::raw('coalesce(a.valueFloat,a.valueInt) AS meta'))
                ->leftjoin('eveIcons AS i', 't.iconID', '=', 'i.iconID')
                ->leftjoin('invGroups AS g', 't.groupID', '=', 'g.groupID')
                ->leftjoin('invCategories AS c', 'c.categoryID', '=', 'g.categoryID')
                ->leftjoin('dgmTypeAttributes AS a', 'a.typeID', '=', 't.typeID')
                ->where('t.graphicID', '=', $graphicID)
                ->where('t.published', '=', 1)
                ->where('a.attributeID', '=', 633)
                //->where('b.attributeID', '=', 51)
                ->orderBy('meta', 'ASC')
                //->remember(CPPDB_REMEMBER_TIME)
                ->get();

        if(isset($first) && count($first) == 0)
        	$first = invType::getGraphicNameNoAttribs($graphicID, $one);

        if (isset($first)) {
        	if($one == true && is_object($first)) {
        		if(is_array($first))
        			return $first[0];
        		else
        			return $first;
        	} else {
				return $first;
			}
		} else {
			return false; // doesn't have a match
		}
	}

	public function scopeGetGraphicNameNoAttribs($query, $graphicID, $one = true) {
		$first = $query->select('t.typeName', 't.typeID', 'i.iconFile', 'c.categoryName', 'g.groupName')
                ->leftjoin('eveIcons AS i', 't.iconID', '=', 'i.iconID')
                ->leftjoin('invGroups AS g', 't.groupID', '=', 'g.groupID')
                ->leftjoin('invCategories AS c', 'c.categoryID', '=', 'g.categoryID')
                ->where('graphicID', '=', $graphicID)
                ->where('t.published', '=', 1)
                //->remember(CPPDB_REMEMBER_TIME)
                ->first();
        return $first;
	}
	
	public function scopeGetGraphicIDfromItemName($query, $itemName = null) {
		if(!isset($itemName))
			return false;

		$first = $query->select('t.graphicID')
                ->where('t.typeName', '=', $itemName)
                //->remember(CPPDB_REMEMBER_TIME)
                ->first();

        if (isset($first)) {
			return $first->graphicID;
		} else {
			return false; // doesn't have a match
		}
	}

    public function scopeGetSlotLayout($query, $shipType = null) {
		if(!isset($shipType))
			return false;

		$data = $query->select('a.attributeID', \DB::raw('coalesce(valueFloat,valueInt) AS value'))
                ->leftjoin('dgmTypeAttributes AS a', 'a.typeID', '=', 't.typeID')
                ->whereIn('a.attributeID', array(12,13,14,1137,1367))
                ->where('t.typeName', '=', $shipType)
                //->remember(CPPDB_REMEMBER_TIME)
                ->get();

        $items = array();
		foreach($data as $row) {
			$items[$row->attributeID] = $row->value;
		}

		if(count($items) > 0)
			return $items;
		else
			return false;
	}

    public function scopeGetTournamentXIIIShips($query)
    {
    	$shipGroups = array("Marauder", "Battleship", "Black Ops", "Command Ship", "Strategic Cruiser", "Force Recon ship", "Combat Recon Ship", "Combat Battlecruiser", "Attack Battlecruiser", 
					"Heavy Assault Cruiser", "Logistics", "Cruiser", "Heavy Interdiction Cruiser", "Electronic Attack Ship", "Frigate", "Assault Frigate", "Covert Ops", "Interdictor", "Stealth Bomber", 
					"Interceptor", "Destroyer", "Industrial", "Tactical Destroyer", "Rookie ship");

		$skinnedShips = array("abaddon kador edition", "abaddon tash-murkon edition", "hyperion aliastra edition", "hyperion innerzone shipping edition", "maelstrom krusual edition", "maelstrom nefantar edition",
					"rokh nugoeihuvi edition", "rokh wiyrkomi edition", "caracal nugoeihuvi edition", "omen kador edition", "stabber krusual edition", "thorax aliastra edition", "tayra wiyrkomi edition",
					"iteron inner zone shipping edition", "bestower tash-murkon edition", "mammoth nefantar edition", "incursus aliastra edition", "merlin nugoeihuvi edition", "punisher kador edition",
					"rifter krusual edition");

		/*
			I'm very proud of this query, so was my db expert friend who sadly passed away. I don't know what was going wrong in his life that caused him to throw himself out of a 44th floor window 
			screaming "Oh my God! No!", but I take great solace that I was able to show him this query 2 minutes before he died.
		*/

		$ships = $query->select('t.typeID', 't.typeName', 'g.groupName', 'm.marketGroupName', \DB::raw('CAST(
					IF((groupName="Battleship" AND marketGroupName="Pirate Faction") OR groupName="Marauder", 19,
					IF((groupName="Battleship" AND marketGroupName="Navy Faction") OR groupName="Black Ops", 17,
					IF(groupName="Battleship" OR groupName="Command Ship" OR groupName="Strategic Cruiser", 16,
					IF(groupName="Combat Recon ship" OR groupName="Force Recon ship" OR groupName="Logistics", 13,
					IF(groupName="Heavy Assault Cruiser" OR (groupName="Combat Battlecruiser" AND marketGroupName="Navy Faction") OR (groupName="Attack Battlecruiser" AND marketGroupName = "Navy Faction"), 12,
					IF((groupName="Cruiser" AND marketGroupName = "Pirate Faction") OR groupName="Heavy Interdiction Cruiser" OR groupName="Attack Battlecruiser" OR groupName="Combat Battlecruiser", 11,
					IF(groupName="Cruiser" AND (typeName="Augoror" OR typeName="Scythe" OR typeName="Exequror" OR typeName="Osprey"), 10,
					IF((groupName="Cruiser" AND marketGroupName = "Navy Faction"), 9,
					IF(groupName="Cruiser" OR groupName="Tactical Destroyer", 6,
					IF(groupName="Electronic Attack Ship", 5,
					IF((groupName="Frigate" AND marketGroupName = "Pirate Faction") OR groupName="Assault Frigate" OR groupName="Covert Ops" OR groupName="Interdictor", 4,
					IF((groupName="Frigate" AND marketGroupName = "Navy Faction") OR groupName="Interceptor" OR groupName="Destroyer" OR groupName="Stealth Bomber" OR (groupName="Frigate" AND (typeName="maulus" OR typeName="vigil" OR typeName="griffin" OR typeName="crucifier"))
						OR (groupName="Frigate" AND (typeName="Sukuuvestaa Heron" OR typeName="Inner Zone Shipping Imicus" OR typeName="Sarum Magnate" OR typeName="Vherokior Probe" OR typeName="Tash-Murkon Magnate" OR typeName="Gold Magnate" OR typeName="Silver Magnate")), 3,
					IF((groupName="Rookie ship" AND marketGroupName = "Special Edition Frigates") OR groupName="Frigate" OR groupName="Industrial", 2,
					IF(groupName="Rookie ship", 1,
					0)))))))))))))) AS UNSIGNED) AS points'),  
					\DB::raw('COALESCE(a1.valueInt,a1.valueFloat) AS hiSlots'), 
					\DB::raw('COALESCE(a2.valueInt,a2.valueFloat) AS midSlots'), 
					\DB::raw('COALESCE(a3.valueInt,a3.valueFloat) AS loSlots'), 
					\DB::raw('COALESCE(a4.valueInt,a4.valueFloat) AS rigSlots'), 
					\DB::raw('COALESCE(a5.valueInt,a5.valueFloat) AS subSlots'), 
					\DB::raw('COALESCE(a6.valueInt,a6.valueFloat) AS turretSlots'),
					\DB::raw('COALESCE(a7.valueInt,a7.valueFloat) AS launcherSlots'),
					\DB::raw('COALESCE(a8.valueInt,a8.valueFloat) AS droneBandwidth'),
					\DB::raw('COALESCE(a9.valueInt,a9.valueFloat) AS droneBay') )
				->leftjoin('invGroups AS g', 'g.groupID', '=', 't.groupID')
				->leftjoin('invMarketGroups AS m', 'm.marketGroupID', '=', 't.marketGroupID')
				->leftjoin('dgmTypeAttributes AS a1', function($join)
					{
						$join->on('a1.typeID', '=', 't.typeID')
						->where('a1.attributeID', '=', 14);
					})
				->leftjoin('dgmTypeAttributes AS a2', function($join)
					{
						$join->on('a2.typeID', '=', 't.typeID')
						->where('a2.attributeID', '=', 13);
					})
				->leftjoin('dgmTypeAttributes AS a3', function($join)
					{
						$join->on('a3.typeID', '=', 't.typeID')
						->where('a3.attributeID', '=', 12);
					})
				->leftjoin('dgmTypeAttributes AS a4', function($join)
					{
						$join->on('a4.typeID', '=', 't.typeID')
						->where('a4.attributeID', '=', 1137);
					})
				->leftjoin('dgmTypeAttributes AS a5', function($join)
					{
						$join->on('a5.typeID', '=', 't.typeID')
						->where('a5.attributeID', '=', 1367);
					})
				->leftjoin('dgmTypeAttributes AS a6', function($join)
					{
						$join->on('a6.typeID', '=', 't.typeID')
						->where('a6.attributeID', '=', 102);
					})
				->leftjoin('dgmTypeAttributes AS a7', function($join)
					{
						$join->on('a7.typeID', '=', 't.typeID')
						->where('a7.attributeID', '=', 101);
					})
				->leftjoin('dgmTypeAttributes AS a8', function($join)
					{
						$join->on('a8.typeID', '=', 't.typeID')
						->where('a8.attributeID', '=', 1271);
					})
				->leftjoin('dgmTypeAttributes AS a9', function($join)
					{
						$join->on('a9.typeID', '=', 't.typeID')
						->where('a9.attributeID', '=', 283);
					})
				->whereIn('g.groupName', $shipGroups)
				->whereNotIn('t.typeName', $skinnedShips)
				->whereRaw('t.typeName NOT LIKE "% Edition"')
				->whereRaw('t.typeName NOT LIKE "%YC117%"')
				->where('t.typeName', '!=', 'Nestor')
				->where('t.published', '=', 1)
				->orderBy('points', 'desc')
				->orderBy('g.groupName', 'ASC')
				->orderBy('m.marketGroupName', 'ASC')
				//->remember(CPPDB_REMEMBER_TIME)
				->get();

        $traits = invTrait::getTraits($shipGroups);

        // returns single object with all points data we need to manipulate on page
		return array('ships' => $ships, 'traits' => $traits);
    }
}
