<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class eveRequiredSkills extends Model
{
    public $primaryKey = 'typeID';
    public $timestamps = false;
    protected $table = 'eveRequiredSkills';
    protected $fillable = [ 'typeID', 
    						'requiredSkillID', 
    						'skillLevel'];
}
