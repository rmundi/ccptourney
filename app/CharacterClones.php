<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class CharacterClones extends Model
{
    public $primaryKey = 'characterID';
    public $timestamps = false;
    protected $table = 'apiJumpClones';
    protected $fillable = [ 'characterID', 
    						'jumpCloneID', 
    						'typeID',
    						'locationID',
    						'cloneName'];

    public function scopeGetClones($query, $characterID = null) {
        if(!isset($characterID))
            return false;

        $data = $query->select('apiJumpClones.*', \DB::raw('coalesce(n.itemName,c.stationName) AS locationName'))
                ->leftjoin('invNames AS n', 'apiJumpClones.locationID', '=', 'n.itemID')
                ->leftjoin('eveConquerableStations AS c', 'apiJumpClones.locationID', '=', 'c.stationID')
                ->where('apiJumpClones.characterID', '=', $characterID)
                ->get();

        return $data;
    }
}
