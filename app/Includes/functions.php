<?php

// Assorted Functions
use Pheal\Pheal;
use Pheal\Core\Config;
use Thunk\Classes\Crest;
/*
function getAPIPage($scope, $sheet, $val = null) {
	Config::getInstance()->cache = new \Pheal\Cache\FileStorage(storage_path('pheal/'));

	$pheal = new Pheal();

	$scope = $scope.'Scope';

	if($val !== null) {
		if($scope == 'eveScope' && $sheet == 'CharacterInfo') 
			$data = $pheal->$scope->$sheet(array($val['k'] => $val['v'], 'characterID' => session('sso_characterid')));
		else
			$data = $pheal->$scope->$sheet(array($val['k'] => $val['v']));
	} else {
		$data = $pheal->$scope->$sheet();
	}
	
	return $data;
}
*/


/*

function getCrestPage($url) {
	// maybe do crawling for ?page='s

/* *WORKING*
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/';  <---- CHAR INFO V.USEFUL LOTS OF ROUTES (update: not so useful, most do not work)
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/contacts/';
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/fittings/';
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/opportunities/';
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/location/';
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/loyaltypoints/';


	*UNKNOWN*
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/navigation/waypoints/'; (write only??)

	*BROKEN / FUTURE*
	$url = 'https://crest-tq.eveonline.com/standings/'.session('sso_characterid').'/';					401	*API
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/private/';  		403
	$url = 'https://crest-tq.eveonline.com/accounts/'.session('sso_characterid').'/';					403
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/chat/channels/';	403 *API
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/blocked/';			401
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/capsuleer/';		403
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/vivox/';			403
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/notifications/';	400 *API
	$url = 'https://crest-tq.eveonline.com/accounts/'.session('sso_characterid').'/1000/';				403

	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/capsuleer/skills/';	401	*API
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/capsuleer/skills/';	403

	* API accessToken *
	$url = 'https://api.eveonline.com/account/AccountStatus.xml.aspx?accessToken='.session('sso_accesstoken');
	
	$url = 'https://api.eveonline.com/char/AccountBalance.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/AssetList.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Blueprints.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Bookmarks.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/UpcomingCalendarEvents.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/CalendarEventAttendees.xml.aspx?accessToken='.session('sso_accesstoken'); 400 (should be available by API AccessToken)
	$url = 'https://api.eveonline.com/char/ChatChannels.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Clones.xml.aspx?accessToken='.session('sso_accesstoken');  (this is basically CharacterSheet minus skills and perhaps a little private info)
	$url = 'https://api.eveonline.com/char/ContactList.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/ContactNotifications.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Contracts.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/ContractBids.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/ContractItems.xml.aspx?accessToken='.session('sso_accesstoken'); // assumed
	$url = 'https://api.eveonline.com/char/FacWarStats.xml.aspx?accessToken='.session('sso_accesstoken');	400 (possibly because I'm not in FacWar - iirc API always returned not found or similar)
	$url = 'https://api.eveonline.com/char/IndustryJobs.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/IndustryJobsHistory.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/KillMails.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Locations.xml.aspx?accessToken='.session('sso_accesstoken'); // assumed
	$url = 'https://api.eveonline.com/char/MailMessages.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Mailinglists.xml.aspx?accessToken='.session('sso_accesstoken');	
	$url = 'https://api.eveonline.com/char/MailBodies.xml.aspx?accessToken='.session('sso_accesstoken').'&ids=356787445';	// ids= comma delimited list
	$url = 'https://api.eveonline.com/char/MarketOrders.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Medals.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Notifications.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/NotificationTexts.xml.aspx?accessToken='.session('sso_accesstoken'); // assumed
	$url = 'https://api.eveonline.com/char/PlanetaryColonies.xml.aspx?accessToken='.session('sso_accesstoken'); 
	$url = 'https://api.eveonline.com/char/PlanetaryLinks.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/PlanetaryPins.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/PlanetaryRoutes.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Research.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/SkillInTraining.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/SkillQueue.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Skills.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/Standings.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/WalletJournal.xml.aspx?accessToken='.session('sso_accesstoken');
	$url = 'https://api.eveonline.com/char/WalletTransactions.xml.aspx?accessToken='.session('sso_accesstoken');


	*Statistics (works for any charID I think)*
	$url = 'https://characterstats.tech.ccp.is/v1/'.session('sso_characterid').'/';

	* char stuff
	$url = 'https://crest-tq.eveonline.com/characters/'.session('sso_characterid').'/'; <-- has Bio
	$url = 'https://api.eveonline.com/char/Clones.xml.aspx?accessToken='.session('sso_accesstoken');  (this is basically CharacterSheet minus skills and perhaps a little private info)
	$url = 'https://api.eveonline.com/char/Skills.xml.aspx?accessToken='.session('sso_accesstoken');

*/
	//$x = getCharacterInfo(session('sso_characterid'));
	//dd($x);
/*
	//$url = 'https://api.eveonline.com/char/private.xml.aspx?accessToken='.session('sso_accesstoken');
	$http = new http($url);
	$http->set_timeout(30);
	
	if(session()->has('sso_accesstoken'))
		$http->set_header('Authorization: Bearer '.session('sso_accesstoken'));
	//$http->set_header('Accept: application/vnd.ccp.eve.CapsuleerSkillCollection-v1+json');
	//$http->set_header('Host: crest-tq.eveonline.com');
	//$http->set_post();

	$json = json_decode($http->get_content());

	if ($http->get_http_code() != 200) {
		$result['success'] = false;
		$result['errorCode'] = $http->get_http_code();
		$result['header'] = $http->get_header();
		return $result;
	}

	if(strpos($http->get_header(), "Content-Encoding: gzip") && gzinflate(substr($json, 10))) 
		$json = gzinflate(substr($json, 10));

	$result['success'] = true;
	$result['data'] = $json;

	return $result;
}

function setCrestPage($url, $jsonData = null) {
	$http = new http($url);
	$http->set_timeout(30);
	
	if(session()->has('sso_accesstoken'))
		$http->set_header('Authorization: Bearer '.session('sso_accesstoken'));
	//$http->set_header('Accept: application/vnd.ccp.eve.CapsuleerSkillCollection-v1+json');
	//$http->set_header('Host: crest-tq.eveonline.com');
	//$http->set_post();

	//$http->set_postform('', $postData);
	//foreach($postData as $name => $data) {
		//if(is_array($data)) {
		//	$data = json_encode($data, JSON_FORCE_OBJECT);
		//}
		//if(is_array($data)) {
		//	foreach($data as $name2 => $data2) {
		//		$http->set_postform($name2, $data2);
		//		echo $name2 .': '.$data2.'<br />';
		//	}
		//} else {
		//	$http->set_json($data);
		//	echo $name .': '.$data.'<br />';
		//}
	//}

	$http->set_json($jsonData);

	$json = json_decode($http->get_content());

	if ($http->get_http_code() != 200) {
		$result['success'] = false;
		$result['errorCode'] = $http->get_http_code();
		$result['header'] = $http->get_header();
		return $result;
	}

	if(strpos($http->get_header(), "Content-Encoding: gzip") && gzinflate(substr($json, 10))) 
		$json = gzinflate(substr($json, 10));

	$result['success'] = true;
	$result['data'] = $json;

	return $result;
}

/**/

function getSystemCSS($system) {
  return "system".str_replace('.', '-', number_format($system,1));
}

function getCrest($url, $decode = true, $canCache = false) {
	$hash = hash('md5', $url);

	// Check Crest Cache
	if ($canCache == true && is_file(config('tools.crest-cache-dir') . $hash)) {
			$json = @file_get_contents(config('tools.crest-cache-dir') . $hash);
			$isCached = true;
	} else {
		$isCached = false;

		$http = new http($url);
		$http->set_timeout(300);
		$http->set_header("Accept-Encoding: gzip");
		$json = $http->get_content();

		if ($http->get_http_code() != 200) {
			session(['errormessage' => 'Crest Error (HTTP: '.$http->get_http_code().')']);
			return false;
		}

		if(strpos($http->get_header(), "Content-Encoding: gzip") && gzinflate(substr($json, 10))) 
			$json = gzinflate(substr($json, 10));

		if($canCache == true) {
			$test = json_decode($json);
			if(empty($data->key) || $test->key != "noSuchFrame")
				file_put_contents( config('tools.crest-cache-dir') . $hash, $json );
		}
	}

	if($decode == true) {
    $data = json_decode($json);

    if(empty($data)) {
      session(['errormessage' => 'No Data']);
      return false;
    }

    if(!empty($data->key) && $data->key == "serviceUnavailable") {
      session(['errormessage' => $data->message]);
      return false;
    }

    $data->isCached = $isCached;

    return $data;
  } else {
    // doesn't say if isCached when json
    return $json;
  }
}

function getCrestID($string, $delimeter = "/") {
	$pieces = explode($delimeter, $string);
	foreach($pieces as $piece)
		if($piece != "")
			$lastPiece = $piece;

	return $lastPiece;
}

function GetURLPath($url = '') {
	if((!empty($_SERVER['HTTP_CF_VISITOR']) && $_SERVER['HTTP_CF_VISITOR'] == '{"scheme":"https"}') || (!empty($_SERVER['HTTPS'])  && $_SERVER['HTTPS'] != 'off')) {
		return secure_url($url);
	} else {
		return url($url);
	}
}

function GetAssetPath($url = '') {
	if((!empty($_SERVER['HTTP_CF_VISITOR']) && $_SERVER['HTTP_CF_VISITOR'] == '{"scheme":"https"}') || (!empty($_SERVER['HTTPS'])  && $_SERVER['HTTPS'] != 'off')) {
		return secure_asset($url);
	} else {
		return asset($url);
	}
}

function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	} else {
		// Return array
		return $d;
	}
}

function arrayToObject($d) {
	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return (object) array_map(__FUNCTION__, $d);
	} else {
		// Return object
		return $d;
	}
}

function getIcon($iconFile, $size = 32) {
  // icons available in 16,32,64,128
  // when image find fails, have it search for next best fit. If asked for small, start small. If asked for large, start with large
  if($size == 16 || $size == 32)
  {
    $wrongsize[0] = 16;
    $wrongsize[1] = 32;
    $wrongsize[2] = 64;
    $wrongsize[3] = 128;
  } else {
    $wrongsize[0] = 128;
    $wrongsize[1] = 64;
    $wrongsize[2] = 32;
    $wrongsize[3] = 16;
  }
  
  // split the icon
  $pieces = explode("_", $iconFile);
  if(isset($pieces[0]))
    $alpha = (integer)$pieces[0];
  else
    $alpha = 0;

  if(isset($pieces[1]))
    $beta = (integer)$pieces[1];
  else
    $beta = 0;

  $iconString = (string)$alpha."_".$size."_".(string)$beta;
  if(!file_exists(public_path("images/Icons/items/".$iconString.".png"))) {
    $iconString = (string)$alpha."_".$wrongsize[0]."_".(string)$beta;
    if(!file_exists(public_path("images/Icons/items/".$iconString.".png"))) {
      $iconString = (string)$alpha."_".$wrongsize[1]."_".(string)$beta;
      if(!file_exists(public_path("images/Icons/items/".$iconString.".png"))) {
        $iconString = (string)$alpha."_".$wrongsize[2]."_".(string)$beta;
        if(!file_exists(public_path("images/Icons/items/".$iconString.".png"))) {
          $iconString = (string)$alpha."_".$wrongsize[3]."_".(string)$beta;
          if(!file_exists(public_path("images/Icons/items/".$iconString.".png"))) {
            $iconString = "7_64_15";
          }
        }
      }
    }
  }
  return $iconString;
}

function calculateStackingBonus($bonuses) {
	$magicModifier = 1-($bonuses[0]/100);
	
	for ($index = 1; $index < count($bonuses); ++$index) {
	    $magicModifier *= 1-($bonuses[$index] * exp(-pow($index,2)/7.1289)/100);
	}

	return $magicModifier;
}

// floats are unreliable, especially PHP / JS mixes. This functions compares two floats to a certain precision ($epsilon) and returns true/false if equal
function floatCompare($first, $second, $epsilon = 0.00001) {
	if(abs($first-$second) < $epsilon) 
		return true;
	else 
		return false;
}

function rel_time($from, $to = null, $short = false, $skipSecs = false)
{
  $to = (($to === null) ? (time()) : ($to));
  $to = ((is_int($to)) ? ($to) : (strtotime($to)));
  $from = ((is_int($from)) ? ($from) : (strtotime($from)));

  
  if($short) {
    if($skipSecs) {
      $units = array (
        //"Y"    => 29030400, // seconds in a year   (12 months)
       // "M"     => 2419200,  // seconds in a month  (4 weeks)
        //"W"    => 604800,   // seconds in a week   (7 days)
        "D"     => 86400,    // seconds in a day    (24 hours)
        "H"    => 3600,     // seconds in an hour  (60 minutes)
        "M"   => 60      // seconds in a minute (60 seconds)
        //"S"   => 1         // 1 second
      );
    } else {
      $units = array (
        //"Y"    => 29030400, // seconds in a year   (12 months)
       // "M"     => 2419200,  // seconds in a month  (4 weeks)
        //"W"    => 604800,   // seconds in a week   (7 days)
        "D"     => 86400,    // seconds in a day    (24 hours)
        "H"    => 3600,     // seconds in an hour  (60 minutes)
        "M"   => 60,      // seconds in a minute (60 seconds)
        "S"   => 1         // 1 second
      );
    }
  } else {
    if($skipSecs) {
      $units = array (
        "year"   => 29030400, // seconds in a year   (12 months)
        "month"  => 2419200,  // seconds in a month  (4 weeks)
        "week"   => 604800,   // seconds in a week   (7 days)
        "day"    => 86400,    // seconds in a day    (24 hours)
        "hour"   => 3600,     // seconds in an hour  (60 minutes)
        "minute" => 60,       // seconds in a minute (60 seconds)
        //"second" => 1         // 1 second
      );
    } else {
      $units = array (
        "year"   => 29030400, // seconds in a year   (12 months)
        "month"  => 2419200,  // seconds in a month  (4 weeks)
        "week"   => 604800,   // seconds in a week   (7 days)
        "day"    => 86400,    // seconds in a day    (24 hours)
        "hour"   => 3600,     // seconds in an hour  (60 minutes)
        "minute" => 60,       // seconds in a minute (60 seconds)
        "second" => 1         // 1 second
      );
    }
  }

  $diff = abs($from - $to);
  $suffix = (($from > $to) ? ("") : ("-"));
  $output = '';

  foreach($units as $unit => $mult)
  {
    if($diff >= $mult)
    {
      $and = (($mult != 1) ? ("") : ("and "));
      if($short)
        $output .= " ".$and.intval($diff / $mult).$unit;
      else
        $output .= ", ".$and.intval($diff / $mult)." ".$unit.((intval($diff / $mult) == 1) ? ("") : ("s"));
      $diff -= intval($diff / $mult) * $mult;
    }
  }
  
  if($short)
    $output = substr($output, strlen(" "));
  else
    $output = substr($output, strlen(", "));
  $output = $suffix.$output;

  return $output;
}

function resolveID ($id) {
	// check invTypes / invNames / eve_conquerablestations
	if($id > 0 && $id < 500000) {
		$name = Thunk\invType::getTypeName($id);
		// add result to returnarray
		if($name) {
			$temp = null;
			$temp['id'] = $id;
			$temp['name'] = $name;
			$returnArray[] = $temp;
		}
	} elseif(($id >= 500000 && $id < 61000000) || ($id >= 62000000 && $id < 90000000)) {
		$name = Thunk\invName::getName($id);
		// add result to returnarray
		if($name) {
			$temp = null;
			$temp['id'] = $id;
			$temp['name'] = $name;
			$returnArray[] = $temp;
		}
	} elseif($id >= 61000000 && $id < 62000000) {   
		$name = eveConquerableStation::getName($id);
		// add result to returnarray
		if($name) {
			$temp = null;
			$temp['id'] = $id;
			$temp['name'] = $name;
			$returnArray[] = $temp;
		}
	} else {
		$temp = null;
		$temp['id'] = $id;
		$temp['name'] = null;
		$returnArray[] = $temp;
	}
}

function apiResolveID($id) {
	// TODO: Needs to check amount of $ids and do in batches if necessary
	if($id == null)
		return false;

	if(!is_array($id))
		$id = (array)$id;

	$idList = implode(',', $id);

	$data = Crest::getAPIPage('eve', 'CharacterName', array('k' => 'ids', 'v' => $idList));

	if(count($data) > 0) {
		$ret = array();
		foreach($data->characters as $dat) {
			$ret[$dat->characterID] = $dat->name;
		}
	} else {
		return false;
	}

	return $ret;
}

function apiResolveName($name) {
	// TODO: Needs to check amount of $ids and do in batches if necessary
	if($name == null)
		return false;

	if(!is_array($name))
		$name = (array)$name;

	$nameList = implode(',', $name);

	// doesn't work 'name' is urlencoded preventing the correct ID being discovered
	//$data = getAPIPage('eve', 'CharacterID', array('k' => 'names', 'v' => $nameList));

	$pheal = new Pheal();

	$scope = 'eveScope';
	$sheet = 'characterID';

	$data = $pheal->$scope->$sheet(array('names' => $nameList));

	if(count($data) > 0) {
		$ret = array();
		foreach($data->characters as $dat) {
			$ret[$dat->name] = $dat->characterID;
		}
	} else {
		return false;
	}

	return $ret;
}