<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class chrBloodline extends Model
{
    protected $table = 'chrBloodlines as b';
    public $timestamps = false;
    public $primaryKey = 'bloodlineID';

    public function scopeGetBloodlineImg($query, $bloodLineName) {

		$data = $query->select('e.iconFile')
                ->join('eveIcons AS e', 'e.iconID', '=', 'b.iconID')
                ->where('b.bloodlineName', '=', $bloodLineName)
                ->first();

		if( $data->iconFile ) 
			return $data->iconFile;
		else 
			return false;
	}
}
