<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class mapSolarSystem extends Model
{
	protected $table = 'mapSolarSystems';
    public $timestamps = false;
    public $primaryKey = 'solarSystemID';

    private static $regionFromSystemID;
    private static $solarSystemID;
    private static $solarSystemName;

    public function scopeGetSolarSystemName($query, $solarSystemID = null) {
		if(!isset($solarSystemID))
			return false;

		if(isset(static::$solarSystemName[$solarSystemID]))
			return static::$solarSystemName[$solarSystemID];

		$first = $query->select('solarSystemName', 'solarSystemID')
                ->where('solarSystemID', '=', $solarSystemID)
                ->first();

        if(isset($first)) {
        	static::$solarSystemID[$first->solarSystemName] = $first->solarSystemID;
			static::$solarSystemName[$first->solarSystemID] = $first->solarSystemName;

			return $first->solarSystemName;
		} else {
			return false; // doesn't have a match
		}
	}

	public function scopeGetRegionFromSystemID($query, $solarSystemID = null) {
		if(!isset($solarSystemID))
			return false;

		if(isset(static::$regionFromSystemID[$solarSystemID]))
			return static::$regionFromSystemID[$solarSystemID];

		$region = $query->select('r.regionName')
				->leftjoin('mapRegions AS r', 'r.regionID', '=', 'mapSolarSystems.regionID')
                ->where('mapSolarSystems.solarSystemID', '=', $solarSystemID)
                ->first();

        if(isset($region->regionName)) {
        	static::$regionFromSystemID[$solarSystemID] = $region->regionName;

			return $region->regionName;
		} else {
			return "Unknown";
		}
	}
}
