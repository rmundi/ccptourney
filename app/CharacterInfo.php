<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class CharacterInfo extends Model
{
	public $primaryKey = 'characterID';
    protected $table = 'apiCharacterInfo';
}
