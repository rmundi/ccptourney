<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class dgmTypeAttribute extends Model
{
    //
	protected $table = 'dgmTypeAttributes as ta';
    public $timestamps = false;
    public $primaryKey = 'typeID'; // actually a composite key: ['typeID', 'attributeID'] but eloquent does not support composite keys

    private static $itemAttributes;

    public function scopeGetItemAttributes($query, $itemID = null) {
        if(!intval($itemID))
            return null;

        if(isset(static::$itemAttributes[$itemID]))
            return static::$itemAttributes[$itemID];

        $attributesRaw = $query->select('ta.typeID', 'ta.attributeID', \DB::raw('coalesce(valueFloat,valueInt) AS value'), 'dgmAttributeTypes.*', 'dgmAttributeCategories.categoryName', 'eveUnits.displayName as unit', 'eveIcons.iconFile')
                        ->join('dgmAttributeTypes', 'dgmAttributeTypes.attributeID', '=', 'ta.attributeID')
                        ->leftjoin('eveUnits', 'dgmAttributeTypes.unitID', '=', 'eveUnits.unitID')
                        ->leftjoin('eveIcons', 'dgmAttributeTypes.iconID', '=', 'eveIcons.iconID')
                        ->leftjoin('dgmAttributeCategories', 'dgmAttributeCategories.categoryID', '=', 'dgmAttributeTypes.categoryID')
                        ->where('ta.typeID', '=', $itemID)
                        //->remember(CPPDB_REMEMBER_TIME)
                        ->get();

        if($attributesRaw) {
            $attributes = array();
            foreach($attributesRaw as $row) {
                if($row->displayName == '') {
                    $row->displayName = $row->attributeName;
                }
                $attributes[$row->attributeName] = $row;
            }
            static::$itemAttributes[$itemID] = $attributes;
            return $attributes;
        }
        return null;
    }
    public function scopeGetItemAttribute($query, $typeID = null, $attributeID = null) {
    	if(!intval($typeID) || !intval($attributeID))
    		return false;

    	$row = $query->select(\DB::raw('coalesce(valueFloat,valueInt) AS value'))
                ->where('ta.typeID', '=', intval($typeID))
                ->where('ta.attributeID', '=', intval($attributeID))
                //->remember(CPPDB_REMEMBER_TIME)
                ->first();

        if(isset($row))
        	return $row->value;
        else
        	return false;
    }
}
