<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class invTrait extends Model
{
    //
	protected $table = 'invTraits as i';
    public $timestamps = false;
    public $primaryKey = 'traitID';

    public function scopeGetTraits($query, $shipGroups)
    {
    	return $query->select('i.*', 't.typeName AS skillName', 'u.displayName AS unitName', \DB::raw('IF(LOCATE("<a href=showinfo:",i.bonusText), 
                	SUBSTRING(i.bonusText,LOCATE("<a href=showinfo:",i.bonusText)+17,LOCATE(">",i.bonusText)-(LOCATE("<a href=showinfo:",i.bonusText)+17)),NULL) AS bonusID'), 'b.typeName', 'g.groupName',
                	'm.marketGroupName')
                ->leftjoin('invTypes AS t', 'i.skillID', '=', 't.typeID')
                ->leftjoin('eveUnits AS u', 'u.unitID', '=', 'i.unitID')
                ->leftjoin('invTypes AS b', 'b.typeID', '=', \DB::raw('IF(LOCATE("<a href=showinfo:",i.bonusText), 
                	SUBSTRING(i.bonusText,LOCATE("<a href=showinfo:",i.bonusText)+17,LOCATE(">",i.bonusText)-(LOCATE("<a href=showinfo:",i.bonusText)+17)),NULL)'))
                ->leftjoin('invGroups AS g', 'g.groupID', '=', 'b.groupID')
                ->leftjoin('invMarketGroups AS m', 'm.marketGroupID', '=', 'b.marketGroupID')
                ->whereIn('i.typeID', function($subquery) use($shipGroups)
				    {
				        $subquery->select('t.typeID')
				              ->from('invTypes AS t')
				              ->leftjoin('invGroups AS g', 'g.groupID', '=', 't.groupID')
				              ->whereIn('g.groupName', $shipGroups)
				              ->where('t.published', '=', 1);
				    })
                //->remember(CPPDB_REMEMBER_TIME)
                ->get();


    }

    public function scopeGetShipTraitsTourney($query, $typeName = null) {
		if($typeName == null)
    		return false;
		
		$traits = $query->select('i.*')
	                ->leftjoin('invTypes as t', 'i.typeID', '=', 't.typeID')
	                ->where('t.typeName', '=', $typeName)
	                ->whereRaw('i.bonusText LIKE "%rate of fire%"')
	                ->where('i.skillID', '!=', -1)
	                //->remember(CPPDB_REMEMBER_TIME)
	                ->first();
	    
	    if (isset($traits)) {
			return $traits->bonus;
		} else {
			return false; // doesn't have a match
		}
	}
}
