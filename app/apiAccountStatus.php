<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class apiAccountStatus extends Model
{
    public $primaryKey = 'characterID';
    public $timestamps = false;
    protected $table = 'apiAccountStatus';
    protected $fillable = [ 'characterID',  
    						'paidUntil', 
    						'createDate', 
    						'logonCount', 
    						'logonMinutes'];
}
