<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class crestLoyaltyPoints extends Model
{
    public $primaryKey = 'characterID';
    public $timestamps = false;
    protected $table = 'crestLoyaltyPoints';
    protected $fillable = [ 'characterID',  
    						'corporationID', 
    						'corporationName', 
    						'quantity'];
}
