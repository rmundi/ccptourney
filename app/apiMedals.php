<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class apiMedals extends Model
{
    public $primaryKey = 'characterID';
    public $timestamps = false;
    protected $table = 'apiMedals';
    protected $fillable = [ 'characterID', 
    						'medalID', 
    						'reason',
    						'status', 
    						'issuerID', 
    						'issuerName', 
    						'issued', 
    						'corporationID', 
    						'corporationName', 
    						'title', 
    						'description'];
}
