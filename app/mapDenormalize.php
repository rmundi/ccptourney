<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class mapDenormalize extends Model
{
	protected $table = 'mapDenormalize';
    public $timestamps = false;
    public $primaryKey = 'solarSystemID';

    public function scopeGetNearestCelestialObject($query, $solarSystemID, $x, $y, $z)
	{

		$closestDistance = -1;
		$closestObject = "";
	
		$rows = $query->select('*')
				->leftjoin('mapPlanetPyRnd AS p', 'p.planetID', '=', 'mapDenormalize.itemID')
                ->where('mapDenormalize.solarSystemID', '=', $solarSystemID)
                ->get();
			
		foreach($rows as $row) {

			// if Sun, Planet or Moon get warpin point
			if($row->pythonRnd !== null) {
				/* The warp-in point of a planet is determined by the planet's ID, its location, and radius.
				Let x, y, and z represent the planet's coordinates. Let r be the planet's radius.
				The planet's warp-in point is the vector (x+dsinθ,y+12rsinj,z−dcosθ) where:
				d=r(s+1)+1000000
				θ=sin−1(x|x|⋅zx2+z2−−−−−−√)+j
				s|0.5≤s≤10.5=20(140(10log10(r106)−39))20+12
				Now, j is a special snowflake. Its value is the Python equivalent of (random.Random(planetID).random() - 1.0) / 3.0.
				*/

				$cr = $row->radius;

				$cx = $row->x;
				$cy = $row->y;
				$cz = $row->z;

				
				$pr = (float)$row->pythonRnd;

				$j = ($row->pythonRnd - 1.0) / 3.0;
		    	$t = asin($cx/abs($cx) * ($cz/sqrt(pow($cx,2) + pow($cz,2)))) + $j;
		    	
		    	$s = 20 * pow(((1/40) * ((10 * log10($cr/1000000)) - 39)),20) + 0.5;
		    	$s = max(0.5, min($s, 10.5));
		    	$d = $cr*($s + 1) + 1000000;

		    	$wx = $cx + $d * sin($t);
		    	$wy = $cy + 0.5 * $cr * sin($j);
		    	$wz = $cz - $d * cos($t);

				$rowX = $wx;
				$rowY = $wy;
				$rowZ = $wz;

				// find the distance between this celestial object and the co-ords given
				//$distance = sqrt(pow(($rowX-$x),2) + pow(($rowY-$y),2) + pow(($rowZ-$z),2)) - $cr;
				$distance = sqrt(pow(($rowX-$x),2) + pow(($rowY-$y),2) + pow(($rowZ-$z),2));
			} elseif($row->radius >= 90000) {
				// Large Objects ( > 90km )
				/* Let x, y, and z represent the object's coordinates. Let r be the object's radius.
					The object's warp-in point is the vector (x+(r+5000000)cos r,y+1.3r−7500,z−(r+5000000)sin r).
				*/
				$cr = $row->radius;

				$cx = $row->x;
				$cy = $row->y;
				$cz = $row->z;

				$wx = $cx + ($cr+5000000) * cos($cr);
				$wy = $cy + (1.3 * $cr) - 7500;
				$wz = $cz - ($cr+5000000) * sin($cr);

				$rowX = $wx;
				$rowY = $wy;
				$rowZ = $wz;

				// find the distance between this celestial object and the co-ords given
				$distance = sqrt(pow(($rowX-$x),2) + pow(($rowY-$y),2) + pow(($rowZ-$z),2));
			} else {
				$rowX = $row->x;
				$rowY = $row->y;
				$rowZ = $row->z;

				// find the distance between this celestial object and the co-ords given
				$distance = sqrt(pow(($rowX-$x),2) + pow(($rowY-$y),2) + pow(($rowZ-$z),2));
			}
			
			
			if($closestDistance == -1) {
				$closestDistance = $distance;
				$closestObject = $row->itemName;
				$closestItemID = $row->itemID;
			} else {
				if($distance < $closestDistance) {
					$closestDistance = $distance;
					$closestObject = $row->itemName;
					$closestItemID = $row->itemID;
				}
			}
		}
		if($closestObject == NULL || $closestObject == "") {
			// resolve ID
			$closestObject = invName::getName($closestItemID);
		}
		
		$closest['name'] = $closestObject;
		$closest['distance'] = $closestDistance;
		
		return $closest;
	}

// NOT CONVERTED, USED FOR REFERENCE ***** WILL NOT WORK AS STANDS ****
	public function getWarpInPoint($query, $celestialID, $range = 1) {
		$celestial = $query->select('*')
						->leftjoin('mapPlanetPyRnd AS p', 'p.planetID', '=', 'm.itemID')
						->where('m.itemID', '=', $celestialID)
						->first();

		if(!$celestial)
			return false;

		$r = $celestial->radius;

		$x = $celestial->x;
		$y = $celestial->y;
		$z = $celestial->z;

		if($celestial->groupID != 7 && $r > 90000) {
			// Large Objects
			/* Let x, y, and z represent the object's coordinates. Let r be the object's radius.
				The object's warp-in point is the vector (x+(r+5000000)cos r,y+1.3r−7500,z−(r+5000000)sin r).
			*/
			$wx = $x + ($r+5000000) * cos($r);
			$wy = $y + (1.3 * $r) - 7500;
			$wz = $z - ($r+5000000) * sin($r);	

			$distance = sqrt(pow(($wx-$x),2) + pow(($wy-$y),2) + pow(($wz-$z),2));

			return(array('x'=>$wx, 'y'=>$wy, 'z'=>$wz, 'distance' => $distance));
		}

		if($celestial->groupID == 7) {
			/* The warp-in point of a planet is determined by the planet's ID, its location, and radius.
			Let x, y, and z represent the planet's coordinates. Let r be the planet's radius.
			The planet's warp-in point is the vector (x+dsinθ,y+12rsinj,z−dcosθ) where:
			d=r(s+1)+1000000
			θ=sin−1(x|x|⋅zx2+z2−−−−−−√)+j
			s|0.5≤s≤10.5=20(140(10log10(r106)−39))20+12
			Now, j is a special snowflake. Its value is the Python equivalent of (random.Random(planetID).random() - 1.0) / 3.0.
			*/
			$pr = (float)$celestial->pythonRnd;

			$j = ($celestial->pythonRnd - 1.0) / 3.0;
	    	$t = asin($x/abs($x) * ($z/sqrt(pow($x,2) + pow($z,2)))) + $j;
	    	
	    	$s = 20 * pow(((1/40) * ((10 * log10($r/1000000)) - 39)),20) + 0.5;
	    	$s = max(0.5, min($s, 10.5));
	    	$d = $r*($s + 1) + 1000000;

	    	$wx = $x + $d * sin($t);
	    	$wy = $y + 0.5 * $r * sin($j);
	    	$wz = $z - $d * cos($t);

			$distance = sqrt(pow(($wx-$x),2) + pow(($wy-$y),2) + pow(($wz-$z),2));

			return(array('x'=>$wx, 'y'=>$wy, 'z'=>$wz, 'distance' => $distance - $r));
		}

		/* An ordinary object is any object that does not fall withing any of the other categories described below.
		Let the 3D vectors pd and ps represent the object's position and the warp's origin, respectively; and v⃗  the directional vector from ps to pd. Let r be the object's radius.
		The object's warp-in point is the vector ps+v⃗ −rv^.
		*/

		// without the origin (ps) then there's little point to this, so lets justs send back x,y,z and pretend we bothered.

		return(array('x'=>$x, 'y'=>$y, 'z'=>$z));
	}
}
