<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'appUsers';
    //public $timestamps = false;
    public $primaryKey = 'characterID';
}
