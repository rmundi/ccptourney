<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class eveSupercapUsers extends Model
{
    public $primaryKey = 'ownerID';
    public $timestamps = true;
    protected $table = 'eveSupercapUsers';
    protected $fillable = [ 'ownerID', 
    						'characterID', 
    						'notes',
    						'associatedChars',
    						'apiKey',
    						'apiID',
    						'shipUniqueID',
    						'shipPositionX',
    						'shipPositionY',
    						'shipPositionZ',
    						'shipClosestCelestial',
    						'shipClosestCelestialDistance'];

}
