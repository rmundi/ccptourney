<?php

namespace Thunk\Console\Commands;

use Illuminate\Console\Command;
use Thunk\Classes\cronLogger;

class CronDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command that calls commands on an Daily basis by Cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->callSilent to suppress output
        CronLogger::log('info', '*** DAILY CRONJOB STARTED ***');
        $this->call('prepare:conqstations');
        $this->call('prepare:skilltree');
        //$this->call('prepare:lpstore');
        CronLogger::log('info', '*** DAILY CRONJOB FINISHED ***');
    }
}
