<?php

namespace Thunk\Console\Commands;

use Illuminate\Console\Command;
use Thunk\Classes\SuperTracker;
use Thunk\Classes\zKillboardAPI;
use Thunk\Classes\cronLogger;

class UpdateSupers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:supers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Super Caps based on new mails found in last 2 days from zKillboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $zkbErrors = 0;

        $endTime = gmdate("Y-m-d"). ' 23:59';
        $startTime = gmdate("Y-m-d"). ' 00:00';

        //calculate days between the two dates
        $start = strtotime($startTime)-86400;
        $end = strtotime($endTime);

        $days = floor(abs($end - $start) / 86400);

        $this->info('Days: '.($days+1));
        CronLogger::log('info', 'Days: '.($days+1));
        //cronLog::log('info', 'Supercap Update from ZKB started... - Days: '.($days+1));
        
        for($x=0; $x<=$days; $x++) {
            $dbData = array();

            //$this->info('Day: '.$x.' of '. $days.' days');
            $checkTime = $start + ($x * 86400);

            zKillboardAPI::reset();
            zKillboardAPI::setOrderDirection('asc');
            // can add upto 10 shiptypes (the groupID doesn't seem to work)
            foreach(SuperTracker::getShipTypes() as $shipID)
                zKillboardAPI::setShipTypeID($shipID);
            //zKillboardAPI::setShipTypeID(3628);   // Nation
            zKillboardAPI::setNoItems(true);
            zKillboardAPI::setStartTime(gmdate('Y-m-d', $checkTime));
            zKillboardAPI::setEndTime(gmdate('Y-m-d', ($checkTime+86400))); // 23:59:59

            //$this->info(gmdate('Y-m-d H:i', $checkTime));
            //$this->info(gmdate('Y-m-d H:i', $checkTime+86399));
            $pageNum = 0;
            $zKillData = array();
            $rows = 0;

            do {
                
                zKillboardAPI::setPage($pageNum);
                $zKillData = zKillboardAPI::get();
                $pageNum++;

                // to prevent zkb getting unhappy, sleep for 1hr / maxRequestPerHour + 1 second
                //if(\Thunk\zKillboardAPI::getXBinMaxRequests())
                //   sleep((3600 / \Thunk\zKillboardAPI::getXBinMaxRequests())+1);

                if(count($zKillData) > 0 && $zKillData[0] && $zKillData[0]->killTime) {
                    $rows += count($zKillData);
                    foreach($zKillData AS $kill) {
                        // system and other basic information
                        $killID = $kill->killID;
                        $solarSystemID = $kill->solarSystemID;
                        $killTime = $kill->killTime;
                        $moonID = $kill->moonID;

                        if(isset($kill->position) && isset($kill->position->x) && $kill->position->x != null) {
                            $lastMailX = $kill->position->x;
                            $lastMailY = $kill->position->y;
                            $lastMailZ = $kill->position->z;

                            $closest = \Thunk\mapDenormalize::getNearestCelestialObject($kill->solarSystemID, $kill->position->x, $kill->position->y, $kill->position->z);
                            $mailClosestCelestial = $closest['name'];
                            $mailClosestCelestialDistance = $closest['distance'];

                        } else {
                            $lastMailX = null;
                            $lastMailY = null;
                            $lastMailZ = null;
                            $mailClosestCelestial = null;
                            $mailClosestCelestialDistance = null;
                        }

                        // Victim information
                        if(in_array($kill->victim->shipTypeID,SuperTracker::getShipTypes())) {

                            // ship destroyed - update      
                            SuperTracker::updateEveSuperPilot($kill->victim->characterID, $kill->victim->characterName, $kill->victim->shipTypeID, $kill->killTime, $kill->solarSystemID, 
                            $kill->victim->corporationID, $kill->victim->corporationName, $kill->victim->allianceID, $kill->victim->allianceName, $kill->victim->factionID, $kill->victim->factionName,
                             $kill->killID, 'true', $lastMailX, $lastMailY, $lastMailZ, $mailClosestCelestial, $mailClosestCelestialDistance);
                        }
        
                        if($kill->attackers ) {
                            foreach($kill->attackers as $attacker) {
                                if(in_array($attacker->shipTypeID,SuperTracker::getShipTypes())) {

                                    // update   
                                    SuperTracker::updateEveSuperPilot($attacker->characterID, $attacker->characterName, $attacker->shipTypeID, $kill->killTime, $kill->solarSystemID, 
                                        $attacker->corporationID, $attacker->corporationName, $attacker->allianceID, $attacker->allianceName, $attacker->factionID, $attacker->factionName,
                                         $kill->killID, 'false', $lastMailX, $lastMailY, $lastMailZ, $mailClosestCelestial, $mailClosestCelestialDistance);
                                }
                            }
                        }
                    }
                }
            } while (count($zKillData) == 200 && $pageNum <= 10);
            $this->line('Day: '.$x.' of '. $days.' days, Rows: '.$rows. ', Dates: '.gmdate('Y-m-d', $checkTime).' to '.gmdate('Y-m-d', ($checkTime+86400)));
            CronLogger::log('info', 'Day: '.$x.' of '. $days.' days, Rows: '.$rows. ', Dates: '.gmdate('Y-m-d', $checkTime).' to '.gmdate('Y-m-d', ($checkTime+86400)));
            //$this->info('Day: '.$x.' of '. $days.' days, Rows: '.$rows. ', ZKB Request: '.\Thunk\zKillboardAPI::getXBinRequestCount().' / '.\Thunk\zKillboardAPI::getXBinMaxRequests().', Dates: '.gmdate('Y-m-d', $checkTime).' to '.gmdate('Y-m-d', ($checkTime+86400)));
            //cronLog::log('info', 'Day: '.$x.' of '. $days.' days, Rows: '.$rows. ', ZKB Request: '.zKillboardAPI::getXBinRequestCount().' / '.zKillboardAPI::getXBinMaxRequests().', Dates: '.gmdate('Y-m-d', $checkTime).' to '.gmdate('Y-m-d', ($checkTime+86400)));
        }
        //cronLog::log('info', 'Supercap Update from ZKB completed');
        $this->info('Supercap Update from ZKB completed');
        CronLogger::log('info', 'Supercap Update from ZKB completed');
    }   
}
