<?php

namespace Thunk\Console\Commands;

use Illuminate\Console\Command;
use Thunk\eveSkillTree;
use Thunk\eveRequiredSkills;
use Thunk\Classes\Crest;
use Thunk\Classes\cronLogger;

class CreateSkillTree extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prepare:skilltree';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wipes then creates a fresh SkillTree using API data. Only needs to be run when the skilltree changes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get SkillTreeXML
        $this->line('Downloading SkillTree XML');
        CronLogger::log('info', 'Downloading SkillTree XML');
        $data = Crest::getAPIPage('eve', 'SkillTree');

        // truncate eveSkillTree
        $this->line('Truncating eveSkillTree Table');
        CronLogger::log('info', 'Truncating eveSkillTree Table');
        $deletedRows = eveSkillTree::getQuery()->delete();

        // truncate eveRequiredSkills
        $this->line('Truncating eveRequiredSkills Table');
        CronLogger::log('info', 'Truncating eveRequiredSkills Table');
        $deletedRows = eveRequiredSkills::getQuery()->delete();
                
        $this->line('Writing XML to Database');
        CronLogger::log('info', 'Writing XML to Database');
        foreach($data->skillGroups as $group) {
            foreach($group->skills as $skill) {
                $bonusVal = 0;
                if( isset($skill->skillBonusCollection[0]) && $skill->skillBonusCollection[0]->bonusType == "canNotBeTrainedOnTrial")
                    $bonusVal = $skill->skillBonusCollection[0]->bonusValue;

                if($skill->requiredAttributes->_value != "") {
                    $primaryAttribute = $skill->requiredAttributes->primaryAttribute;
                    $secondaryAttribute = $skill->requiredAttributes->secondaryAttribute;
                } else {
                    $primaryAttribute = null;
                    $secondaryAttribute = null;
                }
                $typeID = $skill->typeID;
                $skillInsert = eveSkillTree::create([   'typeID' => $skill->typeID, 
                                                        'typeName' => $skill->typeName, 
                                                        'groupID' => $group->groupID, 
                                                        'groupName' => $group->groupName, 
                                                        'description' => $skill->description->_value, 
                                                        'rank' => $skill->rank->_value, 
                                                        'primaryAttribute' => $primaryAttribute, 
                                                        'secondaryAttribute' => $secondaryAttribute, 
                                                        'canNotBeTrainedOnTrial' => $bonusVal, 
                                                        'published' => $skill->published]);
                
                // add required Skills
                foreach($skill->requiredSkills as $requiredSkills)
                    $skillInsert = eveRequiredSkills::create(['typeID' => $typeID, 'requiredSkillID' => $requiredSkills->typeID, 'skillLevel' => $requiredSkills->skillLevel]);
            }
        }

        $this->error('eveSkillTree: '.eveSkillTree::count().' rows written');
        CronLogger::log('info', 'eveSkillTree: '.eveSkillTree::count().' rows written');
        $this->error('eveRequiredSkills: '.eveRequiredSkills::count().' rows written');
        CronLogger::log('info', 'eveRequiredSkills: '.eveRequiredSkills::count().' rows written');
        $this->info('Task completed!');
        CronLogger::log('info', 'Task completed!');
    }
}
