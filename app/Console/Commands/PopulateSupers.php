<?php

namespace Thunk\Console\Commands;

use Illuminate\Console\Command;
use Thunk\Classes\SuperTracker;
use Thunk\Classes\zKillboardAPI;
use Thunk\Classes\cronLogger;

define('STARTPERIOD','2008-01-01 00:00');

class PopulateSupers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prepare:supers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncates Supercap table and repopulates from zKillboard (from 2008)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit','2164M');
        $zkbErrors = 0;

        $this->line('Truncating Table: eveSupercaps');
        $deletedRows = \Thunk\eveSupers::getQuery()->delete();
        
        $endTime = gmdate("Y-m-d"). ' 23:59';
        $startTime = STARTPERIOD;
        //$startTime = '2015-04-05 00:00';

        //calculate days between the two dates
        $start = strtotime($startTime);
        $end = strtotime($endTime);

        $days = floor(abs($end - $start) / 86400);

        //$this->info('Days:'.$days);

        for($x=0; $x<=$days; $x++) {
            $dbData = array();

            //$this->line('Day: '.$x.' of '. $days.' days');
            $checkTime = $start + ($x * 86400);

            zKillboardAPI::reset();
            zKillboardAPI::setOrderDirection('asc');
            foreach(SuperTracker::getShipTypes() as $shipID)
                zKillboardAPI::setShipTypeID($shipID);
            //zKillboardAPI::setShipTypeID(3628);   // Nation
            zKillboardAPI::setNoItems(true);
            zKillboardAPI::setStartTime(gmdate('Y-m-d', $checkTime));
            zKillboardAPI::setEndTime(gmdate('Y-m-d', ($checkTime+86400)));

            $pageNum = 0;
            $zKillData = array();
            $rows = 0;

            do {
                zKillboardAPI::setPage($pageNum);
                $zKillData = zKillboardAPI::get();
                $pageNum++;

                // to prevent zkb getting unhappy, sleep for 1hr / maxRequestPerHour + 1 second (looks like zkb removed these headers)
                //if(zKillboardAPI::getXBinMaxRequests())
                    //sleep((3600 / zKillboardAPI::getXBinMaxRequests())+1); 

                if(isset($zKillData) && count($zKillData) > 0 && isset($zKillData[0]) && $zKillData[0]->killTime) {
                    $rows += count($zKillData);
                    foreach($zKillData AS $kill) {
                        // system and other basic information
                        $killID = $kill->killID;
                        $solarSystemID = $kill->solarSystemID;
                        $killTime = $kill->killTime;
                        $moonID = $kill->moonID;

                        // Victim information
                        if(in_array($kill->victim->shipTypeID,SuperTracker::getShipTypes())) {
                            // ship destroyed - update
                            if(isset($dbData[$kill->victim->characterID])) { // already exists in search
                                $dbData[$kill->victim->characterID]['shipTypeID'] = $kill->victim->shipTypeID;
                                $dbData[$kill->victim->characterID]['characterName'] = $kill->victim->characterName;
                                $dbData[$kill->victim->characterID]['corporationID'] = $kill->victim->corporationID;
                                $dbData[$kill->victim->characterID]['corporationName'] = $kill->victim->corporationName;
                                $dbData[$kill->victim->characterID]['allianceID'] = $kill->victim->allianceID;
                                $dbData[$kill->victim->characterID]['allianceName'] = $kill->victim->allianceName;
                                $dbData[$kill->victim->characterID]['factionID'] = $kill->victim->factionID;
                                $dbData[$kill->victim->characterID]['factionName'] = $kill->victim->factionName;
                                $dbData[$kill->victim->characterID]['lastKillID'] = $kill->killID;
                                $dbData[$kill->victim->characterID]['lastSeen'] = $kill->killTime;
                                $dbData[$kill->victim->characterID]['lastSeenSystemID'] = $kill->solarSystemID;
                                // if(!isset($dbData[$attacker->characterID]['totalKills']))
                                //    $dbData[$attacker->characterID]['totalKills'] = 0;
                                //else
                                    $dbData[$kill->victim->characterID]['totalKills']++;
                                $dbData[$kill->victim->characterID]['isDestroyed'] = 'true';
                            } else {
                                $dbData[$kill->victim->characterID]['shipTypeID'] = $kill->victim->shipTypeID;
                                $dbData[$kill->victim->characterID]['characterName'] = $kill->victim->characterName;
                                $dbData[$kill->victim->characterID]['corporationID'] = $kill->victim->corporationID;
                                $dbData[$kill->victim->characterID]['corporationName'] = $kill->victim->corporationName;
                                $dbData[$kill->victim->characterID]['allianceID'] = $kill->victim->allianceID;
                                $dbData[$kill->victim->characterID]['allianceName'] = $kill->victim->allianceName;
                                $dbData[$kill->victim->characterID]['factionID'] = $kill->victim->factionID;
                                $dbData[$kill->victim->characterID]['factionName'] = $kill->victim->factionName;
                                $dbData[$kill->victim->characterID]['firstKillID'] = $kill->killID;
                                $dbData[$kill->victim->characterID]['firstSeen'] = $kill->killTime;
                                $dbData[$kill->victim->characterID]['firstSeenSystemID'] = $kill->solarSystemID;
                                $dbData[$kill->victim->characterID]['lastKillID'] = $kill->killID;
                                $dbData[$kill->victim->characterID]['lastSeen'] = $kill->killTime;
                                $dbData[$kill->victim->characterID]['lastSeenSystemID'] = $kill->solarSystemID;
                                //if(!isset($dbData[$kill->victim->characterID]['totalKills']))
                                    $dbData[$kill->victim->characterID]['totalKills'] = 0;
                                //else
                                //    $dbData[$kill->victim->characterID]['totalKills']++;
                                $dbData[$kill->victim->characterID]['isDestroyed'] = 'true';
                            }
                            // position
                            if(isset($kill->position) && isset($kill->position->x) && $kill->position->x != null) {
                                $dbData[$kill->victim->characterID]['lastMailX'] = $kill->position->x;
                                $dbData[$kill->victim->characterID]['lastMailY'] = $kill->position->y;
                                $dbData[$kill->victim->characterID]['lastMailZ'] = $kill->position->z;

                                $closest = \Thunk\mapDenormalize::getNearestCelestialObject($kill->solarSystemID, $kill->position->x, $kill->position->y, $kill->position->z);
                                $dbData[$kill->victim->characterID]['mailClosestCelestial'] = $closest['name'];
                                $dbData[$kill->victim->characterID]['mailClosestCelestialDistance'] = $closest['distance'];
                            } else {
                                $dbData[$kill->victim->characterID]['lastMailX'] = null;
                                $dbData[$kill->victim->characterID]['lastMailY'] = null;
                                $dbData[$kill->victim->characterID]['lastMailZ'] = null;
                                $dbData[$kill->victim->characterID]['mailClosestCelestial'] = null;
                                $dbData[$kill->victim->characterID]['mailClosestCelestialDistance'] = null;
                            }
                        }

                        if($kill->attackers ) {
                            foreach($kill->attackers as $attacker) {
                                if(in_array($attacker->shipTypeID,SuperTracker::getShipTypes())) {
                                    // update
                                    if(isset($dbData[$attacker->characterID])) {
                                        $dbData[$attacker->characterID]['shipTypeID'] = $attacker->shipTypeID;
                                        $dbData[$attacker->characterID]['characterName'] = $attacker->characterName;
                                        $dbData[$attacker->characterID]['corporationID'] = $attacker->corporationID;
                                        $dbData[$attacker->characterID]['corporationName'] = $attacker->corporationName;
                                        $dbData[$attacker->characterID]['allianceID'] = $attacker->allianceID;
                                        $dbData[$attacker->characterID]['allianceName'] = $attacker->allianceName;
                                        $dbData[$attacker->characterID]['factionID'] = $attacker->factionID;
                                        $dbData[$attacker->characterID]['factionName'] = $attacker->factionName;
                                        $dbData[$attacker->characterID]['lastKillID'] = $kill->killID;
                                        $dbData[$attacker->characterID]['lastSeen'] = $kill->killTime;
                                        $dbData[$attacker->characterID]['lastSeenSystemID'] = $kill->solarSystemID;
                                        //if(!isset($dbData[$attacker->characterID]['totalKills']))
                                        //    $dbData[$attacker->characterID]['totalKills'] = 1;
                                        //else
                                            $dbData[$attacker->characterID]['totalKills']++;
                                        if(!isset($dbData[$attacker->characterID]['isDestroyed']) || $dbData[$attacker->characterID]['isDestroyed'] != 'true')
                                            $dbData[$attacker->characterID]['isDestroyed'] = 'false';
                                    } else {
                                        $dbData[$attacker->characterID]['shipTypeID'] = $attacker->shipTypeID;
                                        $dbData[$attacker->characterID]['characterName'] = $attacker->characterName;
                                        $dbData[$attacker->characterID]['corporationID'] = $attacker->corporationID;
                                        $dbData[$attacker->characterID]['corporationName'] = $attacker->corporationName;
                                        $dbData[$attacker->characterID]['allianceID'] = $attacker->allianceID;
                                        $dbData[$attacker->characterID]['allianceName'] = $attacker->allianceName;
                                        $dbData[$attacker->characterID]['factionID'] = $attacker->factionID;
                                        $dbData[$attacker->characterID]['factionName'] = $attacker->factionName;
                                        $dbData[$attacker->characterID]['firstKillID'] = $kill->killID;
                                        $dbData[$attacker->characterID]['firstSeen'] = $kill->killTime;
                                        $dbData[$attacker->characterID]['firstSeenSystemID'] = $kill->solarSystemID;
                                        $dbData[$attacker->characterID]['lastKillID'] = $kill->killID;
                                        $dbData[$attacker->characterID]['lastSeen'] = $kill->killTime;
                                        $dbData[$attacker->characterID]['lastSeenSystemID'] = $kill->solarSystemID;
                                        //if(!isset($dbData[$attacker->characterID]['totalKills']))
                                            $dbData[$attacker->characterID]['totalKills'] = 1;
                                        //else
                                        //    $dbData[$attacker->characterID]['totalKills']++;
                                        if(!isset($dbData[$attacker->characterID]['isDestroyed']) || $dbData[$attacker->characterID]['isDestroyed'] != 'true')
                                            $dbData[$attacker->characterID]['isDestroyed'] = 'false';
                                    }
                                    // position
                                    if(isset($kill->position) && isset($kill->position->x) && $kill->position->x != null) {
                                        $dbData[$attacker->characterID]['lastMailX'] = $kill->position->x;
                                        $dbData[$attacker->characterID]['lastMailY'] = $kill->position->y;
                                        $dbData[$attacker->characterID]['lastMailZ'] = $kill->position->z;

                                        $closest = \Thunk\mapDenormalize::getNearestCelestialObject($kill->solarSystemID, $kill->position->x, $kill->position->y, $kill->position->z);
                                        $dbData[$attacker->characterID]['mailClosestCelestial'] = $closest['name'];
                                        $dbData[$attacker->characterID]['mailClosestCelestialDistance'] = $closest['distance'];
                                    } else {
                                        $dbData[$attacker->characterID]['lastMailX'] = null;
                                        $dbData[$attacker->characterID]['lastMailY'] = null;
                                        $dbData[$attacker->characterID]['lastMailZ'] = null;
                                        $dbData[$attacker->characterID]['mailClosestCelestial'] = null;
                                        $dbData[$attacker->characterID]['mailClosestCelestialDistance'] = null;
                                    }
                                }
                            }
                        }
                    }
                    foreach($dbData as $characterID => $data) {
                        SuperTracker::saveSupercap($characterID, $data['characterName'], $data['shipTypeID'], $data['firstSeen'], $data['firstSeenSystemID'],$data['lastSeen'], $data['lastSeenSystemID'], 
                            $data['corporationID'], $data['corporationName'], $data['allianceID'], $data['allianceName'], $data['factionID'], $data['factionName'], $data['firstKillID'], $data['lastKillID'], 
                            $data['totalKills'], $data['isDestroyed'], $data['lastMailX'], $data['lastMailY'], $data['lastMailZ'], $data['mailClosestCelestial'], $data['mailClosestCelestialDistance'] );
                    }
                } else {
                    /* This Outputs the headers when there's no Kills - unnecessary
                    $headers = zKillboardAPI::getZKBHeader();
                    foreach($headers as $header) {
                        $this->error(implode(',',$header));
                    }
                    
                    $zkbErrors++;
                    */
                    $this->error('No Kills found');
                    $zkbErrors++;
                }
            } while (count($zKillData) == 200 && $pageNum <= 10);
            // ZKB Requests do not return anything (and they're arrays)
            //$this->info('Day: '.$x.' of '. $days.' days, Rows: '.$rows. ', ZKB Request: '.zKillboardAPI::getXBinRequestCount().' / '.zKillboardAPI::getXBinMaxRequests().', Dates: '.gmdate('Y-m-d', $checkTime).' to '.gmdate('Y-m-d', ($checkTime+86400)));
            $this->line('Day: '.$x.' of '. $days.' days, Rows: '.$rows. ', Dates: '.gmdate('Y-m-d', $checkTime).' to '.gmdate('Y-m-d', ($checkTime+86400)));
        }
        $this->info('Script complete. Errors: '.$zkbErrors);
    }
}
