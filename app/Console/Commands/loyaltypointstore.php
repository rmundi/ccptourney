<?php

namespace Thunk\Console\Commands;

use Illuminate\Console\Command;
use Thunk\npcLPStore;
use Thunk\npcLPStoreItems;
use Thunk\npcLPStoreRequiredItems;
use Thunk\Classes\Crest;
use Thunk\Classes\cronLogger;

class loyaltypointstore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prepare:lpstore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads data for LP Stores and saves to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $errors = 0;

        // Need to create the CrestController class so the CREST functions can be accessed
        //$controller = app()->make('Thunk\Http\Controllers\CrestController');
        //$data = app()->call([$controller, 'crestPage'], ['npcCorporations']);

        Crest::returnToRoot();
        Crest::walk('npcCorporations');
        $data = Crest::get();

        if($data) {

            $this->line('Truncating npcLPStore Table');
            CronLogger::log('info', 'Truncating npcLPStore Table');
            $deletedRows = npcLPStore::getQuery()->delete();
            $this->line('Truncating npcLPStoreItems Table');
            CronLogger::log('info', 'Truncating npcLPStoreItems Table');
            $deletedRows = npcLPStoreItems::getQuery()->delete();
            $this->line('Truncating npcLPStoreRequiredItems Table');
            CronLogger::log('info', 'Truncating npcLPStoreRequiredItems Table');
            $deletedRows = npcLPStoreRequiredItems::getQuery()->delete();

            foreach($data->items as $item) {
                $storeURI  = $item->loyaltyStore->href;
                $corpID = $item->id;

                $this->line('Reading LP Store data for '.$item->name);
                //CronLogger::log('info', 'Reading LP Store data for '.$item->name); // too spammy
                Crest::setURL($storeURI);
                $storeInfo = Crest::get();
                //$storeInfo = getCrestPage($storeURI);
                
                if($storeInfo) {
                    foreach($storeInfo->items as $itemRow) {
                        // write Item to npcLPStore
                        $storeInsert = npcLPStore::create([  'corporationID' => $corpID, 
                                                            'storeItemID' => $itemRow->id]);

                        // write Item to npcLPStoreItems
                        $itemInsert = npcLPStoreItems::firstOrCreate([  'storeItemID' => $itemRow->id, 
                                                                        'quantity' => $itemRow->quantity, 
                                                                        'iskCost' => $itemRow->iskCost, 
                                                                        'lpCost' => $itemRow->lpCost, 
                                                                        'typeID' => $itemRow->item->id, 
                                                                        'typeName' => $itemRow->item->name]);

                        if(isset($itemRow->requiredItems) && count($itemRow->requiredItems) > 0) {
                            foreach($itemRow->requiredItems as $reqRow) {
                                // write Item to npcLPStoreRequiredItems (if not exist)
                                $newItem = npcLPStoreRequiredItems::where('storeItemID',  $itemRow->id)->where('typeID', $reqRow->item->id);
//var_dump($newItem);
                                if($newItem->count() == 0) {
                                    $newItem = new npcLPStoreRequiredItems;

                                    $newItem->storeItemID = $itemRow->id;
                                    $newItem->typeID = $reqRow->item->id;
                                    $newItem->typeName = $reqRow->item->name;
                                    $newItem->quantity = $reqRow->quantity;
                                    
                                    $newItem->save();
                                }

                                //$reqItemInsert = npcLPStoreRequiredItems::create([  'storeItemID' => $itemRow->id, 
                                //                                           'typeID' => $reqRow->item->id, 
                                //                                            'typeName' => $reqRow->item->name, 
                                //                                            'quantity' => $reqRow->quantity]);
                            }
                        }
                    }
                } else {
                    $errors++;

                    $this->error('Failed to acquire LP Store data for '.$item->name.' @ '. $storeURI);
                    CronLogger::log('error', 'Failed to acquire LP Store data for '.$item->name.' @ '. $storeURI);
                }
            }

            if($errors > 0) {
                $this->error('Task Completed with '.$errors.' errors');
                CronLogger::log('error', 'Task Completed with '.$errors.' errors');
            } else {
                $this->info('Task completed!');
                CronLogger::log('info', 'Task completed!');
            }
        } else {
            $this->error('Failed to acquire NPC Corporation data');
            CronLogger::log('error', 'Failed to acquire NPC Corporation data');
        }
        
    }
}
