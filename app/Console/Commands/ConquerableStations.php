<?php

namespace Thunk\Console\Commands;

use Illuminate\Console\Command;
use Thunk\eveConquerableStation;
use Thunk\Classes\Crest;
use Thunk\Classes\cronLogger;

class ConquerableStations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prepare:conqstations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates list of Conquerable Stations from API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get ConquerableStationListXML
        $this->line('Downloading Conquerable Stations XML');
        CronLogger::log('info', 'Downloading Conquerable Stations XML');
        $data = Crest::getAPIPage('eve', 'ConquerableStationList');

        // truncate eveConquerableStations
        $this->line('Truncating eveConquerableStations Table');
        CronLogger::log('info', 'Truncating eveConquerableStations Table');
        $deletedRows = eveConquerableStation::getQuery()->delete();

        $this->line('Writing XML to Database');
        CronLogger::log('info', 'Writing XML to Database');

        foreach($data->outposts as $row) {
            $conqInsert = eveConquerableStation::create([   'stationID' => $row->stationID, 
                                                            'stationName' => $row->stationName, 
                                                            'stationTypeID' => $row->stationTypeID, 
                                                            'solarSystemID' => $row->solarSystemID, 
                                                            'corporationID' => $row->corporationID, 
                                                            'corporationName' => $row->corporationName, 
                                                            'x' => $row->x, 
                                                            'y' => $row->y, 
                                                            'z' => $row->z]);
        }

        $this->error('eveConquerableStations: '.eveConquerableStation::count().' rows written');
        CronLogger::log('info', 'eveConquerableStations: '.eveConquerableStation::count().' rows written');
        $this->info('Task completed!');
        CronLogger::log('info', 'Task completed!');
    }
}
