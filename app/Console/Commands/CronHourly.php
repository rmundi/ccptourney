<?php

namespace Thunk\Console\Commands;

use Illuminate\Console\Command;
use Thunk\Classes\cronLogger;

class CronHourly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command that calls commands on an hourly basis by Cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->callSilent to suppress output
        CronLogger::log('info', '*** HOURLY CRONJOB STARTED ***');
        $this->call('update:supers');
        CronLogger::log('info', '*** HOURLY CRONJOB FINISHED ***');
    }
}
