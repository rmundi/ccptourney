<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Facades\Datatables;

class agtAgents extends Model
{
	private static $outlawCorps = array("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha\'s Nation","Serpentis");

    public $primaryKey = 'agentID';
    public $timestamps = false;
    protected $table = 'agtAgents';
    
	/* basic query to get all agents to corporation
    SELECT n.itemName, a.*, s.*, d.divisionName, t.agentType, 
IF(sta.stationName IS NULL, sys.solarSystemName, sta.stationName) AS locationName,
IF(sta.stationName IS NULL, sys.solarSystemID, sta.solarSystemID) AS systemID
FROM agtAgents AS a
LEFT JOIN invNames AS n ON n.itemID = a.agentID
LEFT JOIN agtAgentTypes AS t ON t.agentTypeID = a.agentTypeID
LEFT JOIN crpNPCDivisions AS d ON d.divisionID = a.divisionID
LEFT JOIN staStations AS sta ON sta.stationID = a.locationID
LEFT JOIN mapSolarSystems AS sys ON sys.solarSystemID = a.locationID
LEFT JOIN apiStandings AS s ON s.fromID = a.agentID AND s.ownerID = 900571518
WHERE a.corporationID = 1000121

# Corp Agents + related Faction Standings - NEEDS to pick highest standing Faction > Corp > Agent (actually do this in PHP I think)
# Guessed that -1 is the lowest a level 1 agent will accept
SELECT n.itemName AS agentName, agtAgents.level, agtAgents.isLocator, s.standing, d.divisionName, t.agentType, 
IF(sta.stationName IS NULL, sys.solarSystemName, sta.stationName) AS locationName,
IF(sta.stationName IS NULL, sys.solarSystemID, sta.solarSystemID) AS systemID,
IF(sta.stationName IS NULL, sys.solarSystemName, stasys.solarSystemName) AS systemName,
IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*con.level),2)),
	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*cri.level),2))
) AS adjStanding,
IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(s.standing < 0, "Diplomacy", "Connections"),IF(s.standing < 0, "Diplomacy", "Criminal Connections")
) AS adjSkill,
IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(s.standing < 0, dip.level, con.level),IF(s.standing < 0, dip.level, cri.level)
) AS adjSkillLevel,
c.factionID,
fs.standing AS facStanding,
IF(fs.fromName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*con.level),2)),
	IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*cri.level),2))
) AS facAdjStanding,
IF(fs.fromName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(fs.standing < 0, "Diplomacy", "Connections"),IF(fs.standing < 0, "Diplomacy", "Criminal Connections")
) AS facAdjSkill,
IF(fs.fromName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(fs.standing < 0, dip.level, con.level),IF(fs.standing < 0, dip.level, cri.level)
) AS facAdjSkillLevel,

cs.standing AS crpStanding,
IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*con.level),2)),
	IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*cri.level),2))
) AS crpAdjStanding,
IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(cs.standing < 0, "Diplomacy", "Connections"),IF(cs.standing < 0, "Diplomacy", "Criminal Connections")
) AS crpAdjSkill,
IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
	IF(cs.standing < 0, dip.level, con.level),IF(cs.standing < 0, dip.level, cri.level)
) AS crpAdjSkillLevel,
IF(agtAgents.level > 1,((agtAgents.level-1)*2)-1, -1) AS reqStanding
FROM agtAgents
LEFT JOIN invNames AS n ON n.itemID = agtAgents.agentID
LEFT JOIN agtAgentTypes AS t ON t.agentTypeID = agtAgents.agentTypeID
LEFT JOIN crpNPCDivisions AS d ON d.divisionID = agtAgents.divisionID
LEFT JOIN crpNPCCorporations AS c ON c.corporationID = agtAgents.corporationID
LEFT JOIN chrFactions AS f ON f.factionID = c.factionID
LEFT JOIN staStations AS sta ON sta.stationID = agtAgents.locationID
LEFT JOIN mapSolarSystems AS sys ON sys.solarSystemID = agtAgents.locationID
LEFT JOIN mapSolarSystems AS stasys ON stasys.solarSystemID = sta.solarSystemID
LEFT JOIN apiStandings AS s ON s.fromID = agtAgents.agentID AND s.ownerID = 900571518
LEFT JOIN apiSkills AS con ON con.characterID = 900571518 AND con.typeID = 3359
LEFT JOIN apiSkills AS cri ON cri.characterID = 900571518 AND cri.typeID = 3361
LEFT JOIN apiSkills AS dip ON dip.characterID = 900571518 AND dip.typeID = 3357
LEFT JOIN apiStandings AS fs ON fs.fromID = c.factionID AND fs.ownerID = 900571518
LEFT JOIN apiStandings AS cs ON cs.fromID = c.corporationID AND cs.ownerID = 900571518
WHERE agtAgents.corporationID = 1000121


*/

    public function scopeGetCorporationAgents($query, $corpID, $ownerID = null, $filterAgents = false) {
        if(!isset($corpID))
            return false;

        $data = $query->select('n.itemName AS agentName', 't.agentType', 'd.divisionName', 'agtAgents.level',
        			\DB::raw('IF(sta.stationName IS NULL, sys.solarSystemName, sta.stationName) as locationName'),
        			\DB::raw('IF(sta.stationName IS NULL, sys.security, stasys.security) as security'),
        			\DB::raw('IF(GREATEST(IF(s.standing IS NULL, -20, s.standing), IF(cs.standing IS NULL, -20, cs.standing), IF(fs.standing IS NULL, -20, fs.standing)) = -20, 0.00, GREATEST(IF(s.standing IS NULL, -20, s.standing), IF(cs.standing IS NULL, -20, cs.standing), IF(fs.standing IS NULL, -20, fs.standing))) as highStanding'),
        			'sys.solarSystemName', 'sta.stationName', 
                    's.standing as standing',
                    //\DB::raw('IF(s.standing IS NULL, 0.00, s.standing) AS standing'),
                    \DB::raw('IF(sta.stationName IS NULL, sys.solarSystemName, stasys.solarSystemName) as systemName'),
                    \DB::raw('IF(s.standing IS NOT NULL,IF(
                    			IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*con.level),2)),
                                	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*cri.level),2))
                                ) IS NULL, 0.00, IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*con.level),2)),	
                                	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*cri.level),2))
                                )),s.standing) AS adjStanding'),
                    \DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), 
                    	IF(s.standing < 0, "Diplomacy", "Connections"),
                    	IF(s.standing < 0, "Diplomacy", "Criminal Connections")) AS adjSkill'),
                    \DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), 
                    	IF(s.standing < 0, dip.level, con.level),
                    	IF(s.standing < 0, dip.level, cri.level)) AS adjSkillLevel'),
                    'c.factionID', 'fs.standing AS facStanding',
                    \DB::raw('IF(IF(fs.fromName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                	IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*con.level),2)),
                                	IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*cri.level),2))
                                ) IS NULL, 0.00, IF(fs.fromName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*con.level),2))
                                ,IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*cri.level),2))
                                )) AS facAdjStanding'),
                    \DB::raw('IF(fs.fromName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(fs.standing < 0, "Diplomacy", "Connections"),IF(fs.standing < 0, "Diplomacy", "Criminal Connections")) AS facAdjSkill'),
                    \DB::raw('IF(fs.fromName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(fs.standing < 0, dip.level, con.level),IF(fs.standing < 0, dip.level, cri.level)) AS facAdjSkillLevel'),
                    'cs.standing AS crpStanding',
                    \DB::raw('IF(IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                	IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*con.level),2)),
                                	IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*cri.level),2))
                                ) IS NULL, 0.00, IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*con.level),2))
                                ,IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*cri.level),2))
                                )) AS crpAdjStanding'),
                    \DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(cs.standing < 0, "Diplomacy", "Connections"),IF(cs.standing < 0, "Diplomacy", "Criminal Connections")) AS crpAdjSkill'),
                    \DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(cs.standing < 0, dip.level, con.level),IF(cs.standing < 0, dip.level, cri.level)) AS crpAdjSkillLevel'),
                    \DB::raw('IF(agtAgents.level > 1,((agtAgents.level-1)*2)-1, -1) AS reqStanding'),
                    'agtAgents.isLocator', 'agtAgents.locationID',
                    \DB::raw('IF(sta.stationName IS NULL, sys.solarSystemID, sta.solarSystemID) as systemID')
                    )
				->leftjoin('invNames AS n', 'n.itemID', '=', 'agtAgents.agentID')
                ->leftjoin('agtAgentTypes AS t', 't.agentTypeID', '=', 'agtAgents.agentTypeID')
                ->leftjoin('crpNPCDivisions AS d', 'd.divisionID', '=', 'agtAgents.divisionID')
                ->leftjoin('crpNPCCorporations AS c', 'c.corporationID', '=', 'agtAgents.corporationID')
                ->leftjoin('chrFactions AS f', 'f.factionID', '=', 'c.factionID')
                ->leftjoin('staStations AS sta', 'sta.stationID', '=', 'agtAgents.locationID')
                ->leftjoin('mapSolarSystems AS sys', 'sys.solarSystemID', '=', 'agtAgents.locationID')
                ->leftjoin('mapSolarSystems AS stasys', 'stasys.solarSystemID', '=', 'sta.solarSystemID')
                ->leftjoin('apiStandings AS s', function($join) use($ownerID)
                {
                    $join->on('s.fromID', '=', 'agtAgents.agentID')->where('s.ownerID', '=', $ownerID);
                })
                ->leftjoin('apiSkills AS dip', function($join) use($ownerID)
                {
                    $join->on('dip.characterID', '=', 'dip.characterID')->where('dip.typeID', '=', 3357)->where('dip.characterID', '=', $ownerID);
                })
                ->leftjoin('apiSkills AS con', function($join) use($ownerID) 
                {
                    $join->on('con.characterID', '=', 'con.characterID')->where('con.typeID', '=', 3359)->where('con.characterID', '=', $ownerID);
                })
                ->leftjoin('apiSkills AS cri', function($join) use($ownerID) 
                {
                    $join->on('cri.characterID', '=', 'cri.characterID')->where('cri.typeID', '=', 3361)->where('cri.characterID', '=', $ownerID);
                })
                ->leftjoin('apiStandings AS fs', function($join) use($ownerID)
                {
                    $join->on('fs.fromID', '=', 'c.factionID')->where('fs.ownerID', '=', $ownerID);
                })
                ->leftjoin('apiStandings AS cs', function($join) use($ownerID)
                {
                    $join->on('cs.fromID', '=', 'c.corporationID')->where('cs.ownerID', '=', $ownerID);
                })
                ->where('agtAgents.corporationID', '=', $corpID);
                //->orderBy('agtAgents.level', 'DESC'); // NO ORDER BY IN DATATABLES
                //->get();

        if($filterAgents == "true")
            $data->havingRaw('adjStanding >= reqStanding OR crpAdjStanding >= reqStanding OR facAdjStanding >= reqStanding');

        return Datatables::of($data);
    }

    public function scopeGetFactionAgents($query, $facID, $ownerID = null, $filterAgents = false) {
        if(!isset($facID))
            return false;

        $data = $query->select('n.itemName AS agentName', 'cn.itemName AS corporationName', 't.agentType', 'd.divisionName', 'agtAgents.level',
        			\DB::raw('IF(sta.stationName IS NULL, sys.solarSystemName, sta.stationName) as locationName'), 
        			\DB::raw('IF(sta.stationName IS NULL, sys.security, stasys.security) as security'),
                    \DB::raw('IF(GREATEST(IF(s.standing IS NULL, -20, s.standing), IF(cs.standing IS NULL, -20, cs.standing), IF(fs.standing IS NULL, -20, fs.standing)) = -20, 0.00, GREATEST(IF(s.standing IS NULL, -20, s.standing), IF(cs.standing IS NULL, -20, cs.standing), IF(fs.standing IS NULL, -20, fs.standing))) as highStanding'),
        			'sys.solarSystemName', 
        			'sta.stationName',
                    's.standing as standing',
                    //\DB::raw('IF(s.standing IS NULL, 0.00, s.standing) AS standing'), 
                    \DB::raw('IF(sta.stationName IS NULL, sys.solarSystemName, stasys.solarSystemName) as systemName'),
                    \DB::raw('IF(s.standing IS NOT NULL,IF(
                    			IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*con.level),2)),
                                	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*cri.level),2))
                                ) IS NULL, 0.00, IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*con.level),2)),	
                                	IF(s.standing < 0, TRUNCATE(s.standing+((10-s.standing)*0.04*dip.level),2), TRUNCATE(s.standing+((10-s.standing)*0.04*cri.level),2))
                                )),s.standing) AS adjStanding'),
                    \DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), 
                    	IF(s.standing < 0, "Diplomacy", "Connections"),
                    	IF(s.standing < 0, "Diplomacy", "Criminal Connections")) AS adjSkill'),
                    \DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), 
                    	IF(s.standing < 0, dip.level, con.level),
                    	IF(s.standing < 0, dip.level, cri.level)) AS adjSkillLevel'),
                    'c.factionID', 'fs.standing AS facStanding',
                    \DB::raw('IF(IF(fs.fromName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                	IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*con.level),2)),
                                	IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*cri.level),2))
                                ) IS NULL, 0.00, IF(fs.fromName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*con.level),2))
                                ,IF(fs.standing < 0, TRUNCATE(fs.standing+((10-fs.standing)*0.04*dip.level),2), TRUNCATE(fs.standing+((10-fs.standing)*0.04*cri.level),2))
                                )) AS facAdjStanding'),
                    \DB::raw('IF(fs.fromName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(fs.standing < 0, "Diplomacy", "Connections"),IF(fs.standing < 0, "Diplomacy", "Criminal Connections")) AS facAdjSkill'),
                    \DB::raw('IF(fs.fromName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(fs.standing < 0, dip.level, con.level),IF(fs.standing < 0, dip.level, cri.level)) AS facAdjSkillLevel'),
                    'cs.standing AS crpStanding',
                    \DB::raw('IF(IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                	IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*con.level),2)),
                                	IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*cri.level),2))
                                ) IS NULL, 0.00, IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
                                IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*con.level),2))
                                ,IF(cs.standing < 0, TRUNCATE(cs.standing+((10-cs.standing)*0.04*dip.level),2), TRUNCATE(cs.standing+((10-cs.standing)*0.04*cri.level),2))
                                )) AS crpAdjStanding'),
                    \DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(cs.standing < 0, "Diplomacy", "Connections"),IF(cs.standing < 0, "Diplomacy", "Criminal Connections")) AS crpAdjSkill'),
                    \DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(cs.standing < 0, dip.level, con.level),IF(cs.standing < 0, dip.level, cri.level)) AS crpAdjSkillLevel'),
                    \DB::raw('IF(agtAgents.level > 1,((agtAgents.level-1)*2)-1, -1) AS reqStanding'),
                    'agtAgents.isLocator', 'agtAgents.locationID',
                    \DB::raw('IF(sta.stationName IS NULL, sys.solarSystemID, sta.solarSystemID) as systemID')
                    )
				->leftjoin('invNames AS n', 'n.itemID', '=', 'agtAgents.agentID')
                ->leftjoin('agtAgentTypes AS t', 't.agentTypeID', '=', 'agtAgents.agentTypeID')
                ->leftjoin('crpNPCDivisions AS d', 'd.divisionID', '=', 'agtAgents.divisionID')
                ->leftjoin('crpNPCCorporations AS c', 'c.corporationID', '=', 'agtAgents.corporationID')
                ->leftjoin('invNames AS cn', 'cn.itemID', '=', 'c.corporationID')
                ->leftjoin('chrFactions AS f', 'f.factionID', '=', 'c.factionID')
                ->leftjoin('staStations AS sta', 'sta.stationID', '=', 'agtAgents.locationID')
                ->leftjoin('mapSolarSystems AS sys', 'sys.solarSystemID', '=', 'agtAgents.locationID')
                ->leftjoin('mapSolarSystems AS stasys', 'stasys.solarSystemID', '=', 'sta.solarSystemID')
                ->leftjoin('apiStandings AS s', function($join) use($ownerID)
                {
                    $join->on('s.fromID', '=', 'agtAgents.agentID')->where('s.ownerID', '=', $ownerID);
                })
                ->leftjoin('apiSkills AS dip', function($join) use($ownerID)
                {
                    $join->on('dip.characterID', '=', 'dip.characterID')->where('dip.typeID', '=', 3357)->where('dip.characterID', '=', $ownerID);
                })
                ->leftjoin('apiSkills AS con', function($join) use($ownerID) 
                {
                    $join->on('con.characterID', '=', 'con.characterID')->where('con.typeID', '=', 3359)->where('con.characterID', '=', $ownerID);
                })
                ->leftjoin('apiSkills AS cri', function($join) use($ownerID) 
                {
                    $join->on('cri.characterID', '=', 'cri.characterID')->where('cri.typeID', '=', 3361)->where('cri.characterID', '=', $ownerID);
                })
                ->leftjoin('apiStandings AS fs', function($join) use($ownerID)
                {
                    $join->on('fs.fromID', '=', 'c.factionID')->where('fs.ownerID', '=', $ownerID);
                })
                ->leftjoin('apiStandings AS cs', function($join) use($ownerID)
                {
                    $join->on('cs.fromID', '=', 'c.corporationID')->where('cs.ownerID', '=', $ownerID);
                })
                ->where('f.factionID', '=', $facID);
                //->orderBy('agtAgents.level', 'DESC'); // NO ORDER BY IN DATATABLES
                //->get();
        
        if($filterAgents == "true")
            $data->havingRaw('adjStanding >= reqStanding OR crpAdjStanding >= reqStanding OR facAdjStanding >= reqStanding');

        return Datatables::of($data);
    }
}
