<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class eveSkillTree extends Model
{
    public $primaryKey = 'typeID';
    public $timestamps = false;
    protected $table = 'eveSkillTree';
    protected $fillable = [ 'typeID', 
    						'typeName', 
    						'groupID',
    						'groupName',
    						'description',
    						'rank',
    						'primaryAttribute',
    						'secondaryAttribute',
    						'canNotBeTrainedOnTrial',
    						'published'];

    public function scopeGetSkills($query, $characterID = null) {
        if(!isset($characterID))
            return false;

        $data = $query->select('eveSkillTree.*', 's.skillPoints', 's.level')
                ->leftjoin('apiSkills AS s', function($join) use($characterID)
                {

                    $join->on('s.typeID', '=', 'eveSkillTree.typeID')
                         ->where('s.characterID', '=', $characterID);
                })
                ->where('eveSkillTree.published', '=', 1)
                ->orderBy('eveSkillTree.groupName', 'ASC')
                ->orderBy('eveSkillTree.typeName', 'ASC')
                ->get();

    /* Unified SkillTree/Skills/SkillQueue - could have problems with multiple entries in skillqueue
    $data = $query->select('eveSkillTree.*', 's.skillPoints', 's.level', 'q.level AS queueLevel', 'q.startSP', 'q.endSP', 'q.startTime', 'q.endTime')
                ->leftjoin('apiSkills AS s', function($join) use($characterID)
                {

                    $join->on('s.typeID', '=', 'eveSkillTree.typeID')
                         ->where('s.characterID', '=', $characterID);
                })
                ->leftjoin('apiSkillQueue AS q', function($join) use($characterID)
                {

                    $join->on('q.typeID', '=', 'eveSkillTree.typeID')
                         ->where('q.characterID', '=', $characterID);
                })
                ->where('eveSkillTree.published', '=', 1)
                ->orderBy('eveSkillTree.groupName', 'ASC')
                ->orderBy('eveSkillTree.typeName', 'ASC')
                ->get();
                /**/

        return $data;
    }
}
