<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class eveConquerableStation extends Model
{
    public $primaryKey = 'stationID';
    public $timestamps = false;
    protected $table = 'eveConquerableStations';
    protected $fillable = [ 'stationID', 
    						'stationName', 
    						'stationTypeID', 
    						'solarSystemID', 
    						'corporationID', 
    						'corporationName',  
    						'x', 
    						'y', 
    						'z'];

    public function scopeGetName($query, $stationID = null) {
		if(!isset($stationID))
			return false;

		$first = $query->select('eveConquerableStations.stationName')
                ->where('eveConquerableStations.stationID', '=', $stationID)
                ->first();

        if (isset($first)) {
			return $first->stationName;
		} else {
			return false; // doesn't have a match
		}
	}
}
