<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class npcLPStoreItems extends Model
{
    public $primaryKey = 'storeItemID';
    public $timestamps = false;
    protected $table = 'npcLPStoreItems';
    protected $fillable = [ 'storeItemID', 
    						'quantity',
    						'iskCost', 
    						'lpCost', 
    						'typeID', 
    						'typeName'];
}
