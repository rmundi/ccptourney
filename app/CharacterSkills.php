<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class CharacterSkills extends Model
{
    public $primaryKey = 'characterID';
    public $timestamps = false;
    protected $table = 'apiSkills';
    protected $fillable = [ 'characterID',  
    						'typeID',
    						'typeName',
    						'skillPoints',
    						'level',
    						'published'];

	public function scopeGetSkills($query, $characterID = null) {
        if(!isset($characterID))
            return false;

        $data = $query->select('apiSkills.*', 't.*')
                ->join('eveSkillTree AS t', 'apiSkills.typeID', '=', 't.typeID')
                ->where('apiSkills.characterID', '=', $characterID)
                ->where('t.published', '=', 1)
                ->orderBy('t.groupName', 'ASC')
                ->orderBy('t.typeName', 'ASC')
                ->get();

        return $data;
    }
}
