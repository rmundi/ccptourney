<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class chrRace extends Model
{
    protected $table = 'chrRaces as r';
    public $timestamps = false;
    public $primaryKey = 'raceID';

    public function scopeGetRaceImg($query, $raceName) {

		$data = $query->select('e.iconFile')
                ->join('eveIcons AS e', 'e.iconID', '=', 'r.iconID')
                ->where('r.raceName', '=', $raceName)
                ->first();

		if( $data->iconFile ) 
			return $data->iconFile;
		else 
			return false;
	}
}
