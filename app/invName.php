<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class invName extends Model
{
    protected $table = 'invNames as n';
    public $timestamps = false;
    public $primaryKey = 'itemID';

    public function scopeGetName($query, $typeID = null) {
		if(!isset($typeID))
			return false;

		$first = $query->select('n.itemName')
                ->where('n.itemID', '=', $typeID)
                ->first();

        if (isset($first)) {
			return $first->itemName;
		} else {
			return false; // doesn't have a match
		}
	}
}
