<?php

// Collection of Eve related Helpers

namespace Thunk\Classes;

class Eve
{
	public static function calculateStackingBonus($bonuses) {
		$magicModifier = 1-($bonuses[0]/100);
		
		for ($index = 1; $index < count($bonuses); ++$index) {
		    $magicModifier *= 1-($bonuses[$index] * exp(-pow($index,2)/7.1289)/100);
		}

		return $magicModifier;
	}

	public static function kmToAU($km, $decimalPlaces = 2) {
		return number_format(($km/149597870.7), $decimalPlaces, '.', ',');
	}

	public static function mToAU($m, $decimalPlaces = 2) {
		return number_format((($m/1000)/149597870.7), $decimalPlaces, '.', ',');
	}
}