<?php

namespace Thunk\Classes;

use GuzzleHttp\Client;
use Pheal\Pheal;
use Pheal\Core\Config;

class Crest
{
    protected static $isInit = false;
    protected static $currentEndpoint;
    protected static $currentURL;
    protected static $crestRoot;
    protected static $guzzle;
    protected static $pheal;

    public function __construct() {
        static::$crestRoot = config('tools.root');
        static::$currentURL = static::$crestRoot;
        static::$guzzle = new Client();
    }

    protected static function init() {
        // init because construct does not occur for static classes
        if(static::$isInit === false) {
            Config::getInstance()->cache = new \Pheal\Cache\FileStorage(storage_path('pheal/'));

            static::$pheal = new Pheal();
            static::$guzzle = new Client();
            static::$crestRoot = config('tools.root');
            static::$currentURL = static::$crestRoot;
            static::$isInit = true;
        }
    }

    public static function getAPIPage($scope, $sheet, $val = null) {
        self::init();

        $scope = $scope.'Scope';

        if($val !== null) {
            if($scope == 'eveScope' && $sheet == 'CharacterInfo') 
                $data = static::$pheal->$scope->$sheet(array($val['k'] => $val['v'], 'characterID' => session('sso_characterid')));
            else
                $data = static::$pheal->$scope->$sheet(array($val['k'] => $val['v']));
        } else {
            $data = static::$pheal->$scope->$sheet();
        }
        
        return $data;
    }

    protected static function makeBasicAuthHeader() {
        return 'Basic '.base64_encode(config('tools.sso-id').':'.config('tools.sso-key'));
    }

    public static function returnToRoot() {
        self::init();
        static::$currentEndpoint = 'root';
        static::$currentURL = static::$crestRoot;
        //return new static;
    }

    public static function walk($endpointName) {
        //self::init();
        $response = self::get();

        if(is_array($endpointName)) {
            foreach($endpointName as $part) {
                //var_dump($part.'<br/>');
                $response = $response->$part;
            }
//var_dump($response);
            static::$currentURL = $response->href;
            static::$currentEndpoint = $part;
        } else {
            
//dd($response);
            static::$currentURL = $response->$endpointName->href;
            static::$currentEndpoint = $endpointName;
//var_dump(static::$currentEndpoint.' - '.static::$currentURL);        
            //return new static;
        }
        //$response = self::get();
    }

    public static function setURL($url) {
        self::init();
        static::$currentURL = $url;
    }

    public static function getURL($url = null, $version = null, $headers = null) {
        self::init();

        if($url === null)
            $url = static::$currentURL;


        $options = [
            'headers' => [
                'User-Agent' => config('tools.user-agent').', hosted at '.config('app.url'),
                //'Authorization' => 'Bearer '.session('sso_accesstoken'),
            ],
        ];

        if(session()->has('sso_accesstoken'))
            $options['headers']['Authorization' ] = 'Bearer '.session('sso_accesstoken');
        if($headers) {
            foreach($headers as $key=>$val) {
                $options['headers'][$key] = $val;
            }
//dd($options);
        }
//dd($options);
        if($version) {
            $options['headers']['Accept'] = $version;
        }
        $response = static::$guzzle->get($url, $options);
        //dd(json_decode($response->getBody(),true));
        return json_decode($response->getBody());
    }

    // SSO commands
    public static function getAuthorizationCode($SSOCode) {
        self::init();
        //$guzzle = new Client();
        $options = [
            'headers' => [
                'User-Agent' => config('tools.user-agent').', hosted at '.config('app.url'),
                'Authorization' => self::makeBasicAuthHeader(),
            ],
            'form_params' => [
                'grant_type' => 'authorization_code',
                'code' => $SSOCode,
            ],
        ];

        $response = static::$guzzle->post(config('tools.sso-login-url'), $options);
        $json = $response->getBody();
        
        /*
        if ($http->get_http_code() != 200) {
            $result['success'] = false;
            $result['errorCode'] = $http->get_http_code();
            return $result;
        }

        if(strpos($http->get_header(), "Content-Encoding: gzip") && gzinflate(substr($json, 10))) 
            $json = gzinflate(substr($json, 10));
*/
        $result['success'] = true;
        $result['data'] = $json;

        return $result;
    }

    public static function getSSOVerification($authToken) {
        self::init();
        // Get the Character details from SSO
        //$guzzle = new Client();
        $options = [
            'headers' => [
                'User-Agent' => config('tools.user-agent').', hosted at '.config('app.url'),
                'Authorization' => 'Bearer '.$authToken,
            ],
        ];

        

        //if ($http->get_http_code() != 200) {
        //    $result['success'] = false;
        //    $result['errorCode'] = $http->get_http_code();
        //    return $result;
        //}

        $response = static::$guzzle->get(config('tools.sso-verify-url'), $options);
        $json = $response->getBody();
       
        $result['success'] = true;
        $result['data'] = $json;

        return $result;
    }

    public static function refreshSSOSession($refreshToken) {
        self::init();
        //$guzzle = new Client();
        $options = [
            'headers' => [
                'User-Agent' => config('tools.user-agent').', hosted at '.config('app.url'),
                'Authorization' => self::makeBasicAuthHeader(),
            ],
            'form_params' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $refreshToken,
            ],
        ];

        $response = static::$guzzle->post(config('tools.sso-login-url'), $options);
        $json = $response->getBody();
        
        /*
        if ($http->get_http_code() != 200) {
            $result['success'] = false;
            $result['errorCode'] = $http->get_http_code();
            return $result;
        }

        if(strpos($http->get_header(), "Content-Encoding: gzip") && gzinflate(substr($json, 10))) 
            $json = gzinflate(substr($json, 10));
*/
        $result['success'] = true;
        $result['data'] = $json;

        return $result;
    }

    public static function SSOLogout() {
        session()->forget('sso_characterid');
        session()->forget('sso_charactername');
        session()->forget('sso_characterhash');
        session()->forget('sso_expires');
        session()->forget('sso_accesstoken');
        if(session()->has('sso_refreshtoken'))
            session()->forget('sso_refreshtoken');
        session()->forget('sso_isloggedin');
        session()->forget('sso_solarsystemid');
        session()->forget('sso_solarsystemname');
        session()->forget('sso_stationid');
        session()->forget('sso_stationname');
    }

    public static function setSSOState() {
        // sets the State with redirecturl+crsftoken for redirect and validation
        $ret = rawurlencode(url()->full());
        $ret.= '*'.csrf_token();
        
        return $ret;
    }

    public static function setSSOScope() {
        return implode(' ', config('tools.sso-scope'));
    }

    // verbs

    public static function get($options = null, $parameters = null) {
        self::init();

        // Perform a GET request at the $currentURL
        $version = config('crest.versions.'.static::$currentEndpoint);
        $url = static::$currentURL;
        if ($parameters) {
            $url = $url.'?'.http_build_query($parameters);
        }
        $arrayResponse = self::getURL($url, $version, $options);
//dd($arrayResponse);
        /*
        if ($http->get_http_code() != 200) {
            $result['success'] = false;
            $result['errorCode'] = $http->get_http_code();
            $result['header'] = $http->get_header();
            return $result;
        }
        */

        // If resource is paginated, pull all pages and combine. THIS IS FOR ARRAY NEEDS TO BE FOR JSON OBJECT <-----------------------------------------------------------
        /*
        while (array_key_exists('items', $arrayResponse) && array_key_exists('next', $arrayResponse)) {
            $nextPage = self::getURL($arrayResponse['next']['href'], $version);
            $nextPage['items'] = array_merge($arrayResponse['items'], $nextPage['items']);
            $arrayResponse = $nextPage;
        }
*/
        //$result['success'] = true;
        //$result['data'] = $arrayResponse;
//dd($arrayResponse);
        return $arrayResponse;
    }
    
    public static function post($payloadArray) {
        self::init();
        $options = [
            'headers' => [
                'User-Agent' => config('tools.user-agent').', hosted at '.config('app.url'),
                'Authorization' => 'Bearer '.session('sso_accesstoken'),
            ],
            'json' => $payloadArray,
        ];
        return static::$guzzle->post(static::$currentURL, $options);
    }

    public static function delete($url) {
        self::init();
        $options = [
            'headers' => [
                'User-Agent' => config('tools.user-agent').', hosted at '.config('app.url'),
                'Authorization' => 'Bearer '.session('sso_accesstoken'),
            ],
        ];
        static::$guzzle->delete($url, $options);
    }
}