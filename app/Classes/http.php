<?php

/*
 * http_request class
 *
 */

class http
{
	private $follow = false;

	public function __construct($url = '', $method = 'GET') {
		if ($url)
		{
			$this->url = parse_url($url);
			if(empty($this->url["port"]) || !$this->url["port"])
				$this->url["realport"] = 80;
			else
				$this->url["realport"] = $this->url["port"];
		}
		$this->useragent = 'Tournament Analyser';
		$this->method = $method;
		$this->postform = array();
		$this->postdata = array();
		$this->headers = array();
		$this->jsonData = null;
		$this->cookiedata = array();
		$this->socket_timeout = 5;
		$this->fp = false;
		$this->requested = false;
    }

	// socket timeout is the amount of time in second which is waited
	// for incoming data, set higher if you request stuff from heavy-loaded
	// scripts or compressed data streams
	public function setSockettimeout($int) {
		$this->socket_timeout = $int;
	}

	public function set_timeout($int) {
		$this->socket_timeout = $int;
	}

	public function get_content() {
		$this->request();
		return $this->content;
	}

	public function get_header() {
		$this->request();
		return $this->header;
	}

	public function get_specific_header($header) {
		$this->request();

		$headers = explode("\n", $this->header);
	
		if(count($headers)) {
			foreach($headers as $row) {
				$parts = explode(": ", $row);
				if($parts[0] == $header)
					return trim($parts[1]);
			}
		} else {
			return false;
		}
	}

	public function get_sent() {
		return $this->sent;
	}

	public function get_recv() {
		return $this->recv;
	}

	public function get_http_code() {
		if(!$this->header) return false;
		$result = explode(" ", $this->header, 3);
		return $result[1];
	}

	public function connect() {
		if ($this->fp)
		{
			return true;
		}
			
		if($this->url['scheme'] == 'https')
				$this->fp = @fsockopen("ssl://".$this->url["host"], 443, $errno, $errstr, 5);
		else $this->fp = @fsockopen($this->url["host"], $this->url["realport"], $errno, $errstr, 5);

		if (!$this->fp)
		{
			$this->_errno = $errno;
			$this->_errstr = $errstr;
			return false;
		}
		return true;
	}

	public function getError() {
        return 'Error occured with fsockopen: '.$this->_errstr.' ('.$this->_errno.')<br/>'."\n";
	}

	public function getURI() {
		$str = $this->url['scheme']."://".$this->url['host'];
		
		if($this->url['port'])
			$str .= ":" . $this->url['port'];
			
		$str .= $this->url['path']."?".$this->url['query'];
		return $str;
	}

	public function request() {
		if ($this->requested)
		{
			return;
		}
		$this->connect();
		$fp = &$this->fp;

        if (!is_resource($fp))
		{
			$this->content = '';
			return false;
		}

		// define a linefeed (carriage return + newline)
		$lf = "\r\n";

		if(empty($this->url['query']))
			$this->url['query'] = '';
	
        $request_string = $this->method.' '.$this->url['path'].'?'.$this->url['query'].' HTTP/1.0'.$lf
                          .'Accept-Language: en'.$lf
                          .'User-Agent: '.$this->useragent.$lf
                          .'Host: '.$this->url['host'].$lf
                          .'Connection: close'.$lf;
		if (isset($this->url['user']) && isset($this->url['pass']))
		{
			$base64 = base64_encode($this->url['user'].':'.$this->url['pass']);
			$request_string .=	'Authorization: Basic '.$base64.$lf.$lf;
		}
		if (count($this->headers))
		{
            $request_string .= join($lf, $this->headers).$lf;
		}
		if (count($this->cookiedata))
		{
			$request_string .= 'Cookie: ';
			foreach ($this->cookiedata as $key => $value)
			{
                $request_string .= $key.'='.$value.'; ';
			}
			$request_string .= $lf;
		}
		if ($this->method == 'POST')
		{
            $boundary = substr(md5(rand(0,32000)),0,10);
            $data = '--'.$boundary.$lf;

			foreach ($this->postform as $name => $content_file)
			{
				$data .= 'Content-Disposition: form-data; name="'.$name.'"'.$lf.$lf;
				$data .= $content_file.$lf;
				$data .= '--'.$boundary.$lf;
			}

			foreach ($this->postdata as $name => $content_file)
			{
				$data .= 'Content-Disposition: form-data; name="'.$name.'"; filename="'.$name.'"'.$lf;
				$data .= 'Content-Type: text/plain'.$lf.$lf;
				$data .= $content_file.$lf;
				$data .= '--'.$boundary.$lf;
			}

			if($this->jsonData !== null) {
				$data .= 'json: '.json_encode($this->jsonData);
				$data .= 'Content-Type: application/json'.$lf.$lf;
				$data .= $this->jsonData.$lf;
				$data .= '--'.$boundary.$lf;
			}

			$request_string .= 'Content-Length: '.strlen($data).$lf;
			$request_string .= 'Content-Type: multipart/form-data, boundary='.$boundary.$lf;
		}
		else
		{
			$data = '';
		}
		$request_string .= $lf;

		fputs($fp, $request_string.$data);
		$this->sent = strlen($data);

		$header = 1;
		$http_header = '';
		$file = '';
		socket_set_timeout($fp, $this->socket_timeout);
		while ($line = @fgets($fp, 4096))
		{
			if ($header)
			{
				$http_header .= $line;
			}
			else
			{
				$file .= $line;
			}
			if ($header && $line == $lf)
			{
				$header = 0;
			}
		}
		$this->status = socket_get_status($fp);
		fclose($fp);
		$this->header = $http_header;
		if($this->follow)
		{
			$result = explode(" ", $this->header, 3);
			if($result[1] >= 300 && $result[1] < 400)
			{
				$start = strpos($this->header, "Location: ");
				$end = strpos($this->header, "\n", $start + 1);
				if($start && $end)
				{
					$location = trim(substr($this->header, $start + 10, $end));
					$this->url = parse_url($location);
					$this->follow = false;
					$this->fp = false;
					return $this->get_content();
				}
			}
		}
		$this->content = $file;
		$this->recv = strlen($http_header)+strlen($file);
		$this->requested = true;
	}

	public function url($url) {
		$this->url = parse_url($url);
		if(!$this->url["port"])
			$this->url["realport"] = 80;
		else
			$this->url["realport"] = $this->url["port"];
	}

	// this is to send file-data to be accessed with $_FILES[$name]
	public function set_postdata($name, $data) {
		$this->method = 'POST';
		$this->postdata[$name] = $data;
	}

	// this function sends form data objects like $_POST[$name] = $data
	public function set_postform($name, $data) {
		$this->method = 'POST';
		$this->postform[$name] = $data;
	}

	public function set_post() {
		$this->method = 'POST';
	}

	public function set_json($data) {
		$this->method = 'POST';
		$this->jsonData = $data;
	}
	public function set_cookie($name, $data) {
		$this->cookiedata[$name] = $data;
	}

	public function set_useragent($string) {
		$this->useragent = $string;
	}

	public function set_header($headerstring) {
		if (!strpos($headerstring, ':'))
		{
			return;
		}
		$this->headers[$headerstring] = $headerstring;
	}

	public function set_follow($follow = true) {
		$this->follow = $follow;
	}
}