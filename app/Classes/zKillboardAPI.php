<?php
namespace Thunk\Classes;

use GuzzleHttp\Client;

define('zKillURL', 'https://zkillboard.com/api/');

//
// Supers are populated using artisan command
// 
//
class zKillboardAPI {

	// zKillboard API functions --- https://neweden-dev.com/ZKillboard_API
	/*
		     Look at the X-Bin-Request-Count header and X-Bin-Max-Requests header for how many requests you've made, and how many you can make pr. hour.
		    You can do any amount of requests pr. second that you want.
		    All IDs used with the API are CCP IDs (Except killmail IDs, which can be internally set, but they are denoted with a - infront (negative numbers))
		    If you get an error 403, look at the Retry-After header.
		    The API will maximum deliver of 200 killmails.
		    If the /limit/ modifier is used, then /page/ is unavailable.
		    startTime and endTime is datetime timestamps, in the format YmdHi.. Example 2012-11-25 19:00 is written as 201211251900
		    /w-space/ and /solo/ can be combined with /kills/ and /losses/
		    To get combined /kills/ and /losses/, don't pass either /kills/ or /losses/
		    Up to 10 IDs can be fetched at the same time, by seperating them with a , (Comma)
		    pastSeconds returns only kills that have happened in the past x seconds.
		    pastSeconds can maximum go up to 7 days (604800 seconds)
		    Default return is JSON, XML is available with /xml/ parameter.
		    Remember to finish with a / (Forward slash)
		    All modifiers can be combined in any order
		    If you do not paass /killID/ then you must pass at least two of the following modifiers. /w-space/, /solo/ or any of the /xID/ ones. (characterID, allianceID, factionID etc.)
		    Negative killIDs denote manually posted killmails.
		    JSONP is available
		    The API has CORS set.
		    Page requests over 10 are only allowed for characterID, corporationID and allianceID 
    */

	// Page / Time / Limit modifiers 
	private static $limit;
	private static $page;
	private static $startTime;
	private static $endTime;
	private static $year;
	private static $month;
	private static $week;
	private static $beforeKillID;
	private static $afterKillID;
	private static $pastSeconds;
	private static $killID;
	
	// Order modifiers 
	private static $orderDirection;

	// Fetch modifiers 
	private static $characterID;
	private static $corporationID;
	private static $allianceID;
	private static $factionID;
	private static $shipTypeID;
	private static $groupID;
	private static $solarSystemID;
	private static $regionID;

	// Fetch Type modifiers 
	private static $kills;
	private static $losses;
	private static $wspace; 		// w-space
	private static $solo;

	// Information modifiers 
	private static $noitems;		// no-items
	private static $noattackers;	// no-attackers
	private static $apionly;		// api-only

	// Format modifiers 
	private static $xml;

	// data
	private static $zKillData;
	private static $XBinRequestCount;
	private static $XBinMaxRequests;
	private static $zKBHeader;

    public function __construct() {
    	self::reset();
		static::$orderDirection = 'desc';
    }

    public static function reset() {
    	static::$orderDirection = 'desc';

    	static::$zKillData = '';

    	static::$xml = false;

    	static::$apionly = false;
    	static::$noattackers = false;
    	static::$noitems = false;

    	static::$solo = false;
    	static::$wspace = false;
    	static::$losses = false;
    	static::$kills = false;

    	static::$characterID = false;
    	static::$corporationID = false;
    	static::$allianceID = false;
    	static::$factionID = false;
    	static::$shipTypeID = false;
    	static::$groupID = false;
    	static::$solarSystemID = false;
    	static::$regionID = false;

    	static::$limit = false;
    	static::$page = false;
    	static::$startTime = false;
    	static::$endTime = false;
    	static::$year = false;
    	static::$month = false;
    	static::$week = false;
    	static::$beforeKillID = false;
    	static::$afterKillID = false;
    	static::$pastSeconds = false;
    	static::$killID = false;
    }

    public static function get($fetch = true) {
    	if($fetch)
			self::fetch();

		return static::$zKillData;
    }

    public static function fetch() {
    	// assemble URL string and call

    	$url = zKillURL;

    	if(static::$xml == true)
    		$url .= 'xml/';
    	if(static::$apionly == true)
    		$url .= 'api-only/';
    	if(static::$noattackers == true)
    		$url .= 'no-attackers/';
    	if(static::$noitems == true)
    		$url .= 'no-items/';

    	if(static::$solo == true)
    		$url .= 'solo/';
    	if(static::$wspace == true)
    		$url .= 'w-space/';
    	if(static::$losses == true)
    		$url .= 'losses/';
    	if(static::$kills == true)
    		$url .= 'kills/';

    	if(static::$characterID == true)
    		$url .= 'characterID/'.implode(',',static::$characterID).'/';
    	if(static::$corporationID == true)
    		$url .= 'corporationID/'.implode(',',static::$corporationID).'/';
    	if(static::$allianceID == true)
    		$url .= 'allianceID/'.implode(',',static::$allianceID).'/';
    	if(static::$factionID == true)
    		$url .= 'factionID/'.implode(',',static::$factionID).'/';
    	if(static::$shipTypeID == true)
    		$url .= 'shipTypeID/'.implode(',',static::$shipTypeID).'/';
    	if(static::$groupID == true)
    		$url .= 'groupID/'.implode(',',static::$groupID).'/';
    	if(static::$solarSystemID == true)
    		$url .= 'solarSystemID/'.implode(',',static::$solarSystemID).'/';
    	if(static::$regionID == true)
    		$url .= 'regionID/'.implode(',',static::$regionID).'/';

    	if(static::$limit != false)
    		$url .= 'limit/'.static::$limit.'/';
    	if(static::$page != false)
    		$url .= 'page/'.static::$page.'/';
    	if(static::$startTime != false)
    		$url .= 'startTime/'.static::$startTime.'/';
    	if(static::$endTime != false)
    		$url .= 'endTime/'.static::$endTime.'/';
    	if(static::$year != false)
    		$url .= 'year/'.static::$year.'/';
    	if(static::$month != false)
    		$url .= 'month/'.static::$month.'/';
    	if(static::$week != false)
    		$url .= 'week/'.static::$week.'/';
    	if(static::$beforeKillID != false)
    		$url .= 'beforeKillID/'.static::$beforeKillID.'/';
    	if(static::$afterKillID != false)
    		$url .= 'afterKillID/'.static::$afterKillID.'/';
    	if(static::$pastSeconds != false)
    		$url .= 'pastSeconds/'.static::$pastSeconds.'/';
    	if(static::$killID != false)
    		$url .= 'killID/'.static::$killID.'/';

    	if(static::$orderDirection != false)
    		$url .= 'orderDirection/'.static::$orderDirection.'/';

        $guzzle = new Client(['http_errors' => false]);

        $options = [
            'headers' => [
                'User-Agent' => config('tools.user-agent').', hosted at '.config('app.url'),
                'Content-Encoding' => 'gzip',
            ],
            'decode_content' => 'gzip'
        ];
        $response = $guzzle->get($url, $options);
        $json = $response->getBody();
        $code = $response->getStatusCode();

        if ($code != 200 && $code != 403 && $code != 400) {
            session(['errormessage' => 'zKillboard Error: '.$code]);
            return false;
        }

        // if 502'd - wait 30 seconds then try again / bandaid fix
        if($code == 502) {
            sleep(30);

            $response = $guzzle->get($url, $options);
            $json = $response->getBody();
            $code = $response->getStatusCode();

            if ($code != 200 && $code != 403 && $code != 400) {
                session(['errormessage' => 'zKillboard Error: '.$code]);
                return false;
            }
        }
        static::$zKBHeader = $response->getHeaders();
        static::$XBinRequestCount = $response->getHeader('X-Bin-Request-Count');  // do these headers work still? command isn't showing any value
        static::$XBinMaxRequests =  $response->getHeader('X-Bin-Max-Requests');

	    if(static::$xml == true)
	    	static::$zKillData = simplexml_load_string($json);
	    else
	    	static::$zKillData = json_decode($json);
    }

    public static function setOrderDirection($direction) {
    	if(strtolower($direction) == 'asc') {
    		static::$orderDirection = 'asc';
    	} else {
    		static::$orderDirection = 'desc';
    	}
    }

    public static function setLimit($limit) {
    	
    	if(is_numeric($limit)) {
    		static::$page = false;

    		static::$limit = $limit;
    	}
    }

    public static function setPage($page) {
    	
    	if(is_numeric($page)) {
    		static::$limit = false;

    		static::$page = $page;
    	}
    }

    public static function setStartTime($startTime) {
    	// YmdHi
    	static::$startTime = self::cleanTime($startTime);
    }

    public static function setEndTime($endTime) {
    	// YmdHi
    	static::$endTime = self::cleanTime($endTime);
    }

    public static function setYear($year) {
    	// Y
    	if(strlen($year) == 4 && is_numeric($year))
    		static::$year = $year;
    }

    public static function setMonth($month) {
    	// m
    	if(strlen($month) == 2 && is_numeric($month) && (int)$month <= 12)
    		static::$month = $month;
    	elseif(strlen($month) == 1 && is_numeric($month) && (int)$month <= 12)
    		static::$month = '0'.$month;
    }

    public static function setWeek($week) {
    	// w
    	if(strlen($week) == 2 && is_numeric($week) && (int)$week <= 53)
    		static::$week = $week;
    	elseif(strlen($week) == 1 && is_numeric($week) && (int)$week <= 53)
    		static::$week = '0'.$week;
    }

    public static function setBeforeKillID($killID) {
    	
    	if(is_numeric($killID)) {
    		static::$afterKillID = false;
    		static::$killID = false;

    		static::$beforeKillID = $killID;
    	}
    }

    public static function setAfterKillID($killID) {
    	
    	if(is_numeric($killID)) {
    		static::$beforeKillID = false;
    		static::$killID = false;

    		static::$afterKillID = $killID;
    	}
    }

    public static function setKillID($killID) {
    	
    	if(is_numeric($killID)) {
    		static::$beforeKillID = false;
    		static::$afterKillID = false;

    		static::$killID = $killID;
    	}
    }

	public static function setPastSeconds($pastSeconds) {
    	
    	if(is_numeric($pastSeconds)) {
    		static::$pastSeconds = $pastSeconds;
    	}
    }

    public static function setXML($value) {
    	if($value === true) {
    		static::$xml = true;
    	} else {
    		static::$xml = false;
    	}
    }

    public static function setAPIOnly($value) {
    	if($value === true) {
    		static::$apionly = true;
    	} else {
    		static::$apionly = false;
    	}
    }

    public static function setNoAttackers($value) {
    	if($value === true) {
    		static::$noattackers = true;
    	} else {
    		static::$noattackers = false;
    	}
    }

    public static function setNoItems($value) {
    	if($value === true) {
    		static::$noitems = true;
    	} else {
    		static::$noitems = false;
    	}
    }

    public static function setSolo($value) {
    	if($value === true) {
    		static::$solo = true;
    	} else {
    		static::$solo = false;
    	}
    }

    public static function setWSpace($value) {
    	if($value === true) {
    		static::$wspace = true;
    	} else {
    		static::$wspace = false;
    	}
    }

    public static function setLosses($value) {
    	if($value === true) {
    		static::$losses = true;
    	} else {
    		static::$losses = false;
    	}
    }

    public static function setKills($value) {
    	if($value === true) {
    		static::$kills = true;
    	} else {
    		static::$kills = false;
    	}
    }

	public static function setCharacterID($value) {
    	
    	if(is_numeric($value)) {
    		if(count(static::$characterID) <= 10)
    			static::$characterID[] = $value;
    	}
    }

    public static function setCorporationID($value) {
    	
    	if(is_numeric($value)) {
    		if(count(static::$corporationID) <= 10)
    			static::$corporationID[] = $value;
    	}
    }

    public static function setAllianceID($value) {
    	
    	if(is_numeric($value)) {
    		if(count(static::$allianceID) <= 10)
    			static::$allianceID[] = $value;
    	}
    }

    public static function setFactionID($value) {
    	
    	if(is_numeric($value)) {
    		if(count(static::$factionID) <= 10)
    			static::$factionID[] = $value;
    	}
    }

    public static function setShipTypeID($value) {
    	
    	if(is_numeric($value)) {
    		if(count(static::$shipTypeID) <= 10)
    			static::$shipTypeID[] = $value;
    	}
    }

    public static function setGroupID($value) {
    	
    	if(is_numeric($value)) {
    		if(count(static::$groupID) <= 10)
    			static::$groupID[] = $value;
    	}
    }

    public static function setSolarSystemID($value) {
    	
    	if(is_numeric($value)) {
    		if(count(static::$solarSystemID) <= 10)
    			static::$solarSystemID[] = $value;
    	}
    }

    public static function setRegionID($value) {
    	
    	if(is_numeric($value)) {
    		if(count(static::$regionID) <= 10)
    			static::$regionID[] = $value;
    	}
    }

    public static function getXBinRequestCount() {
    	return static::$XBinRequestCount;
    }

    public static function getXBinMaxRequests() {
    	return static::$XBinMaxRequests;
    }

    public static function getZKBHeader() {
    	return static::$zKBHeader;
    }

    private static function cleanTime($timestamp) {
    	return str_replace(':','',str_replace('-','',str_replace(' ','',$timestamp)));
    }
}