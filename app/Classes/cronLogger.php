<?php
// logging

namespace Thunk\Classes;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

class cronLogger {
	private static $monolog;

	public static function log($level, $message) {
		if(empty(static::$monolog)) {
			$logFile = 'cronlog-'.gmdate("Y-m").'.txt';
			$handler = new StreamHandler(storage_path().'/logs/'.$logFile);
			$handler->setFormatter(new LineFormatter("[%datetime%] %channel%.%level_name%: %message%\n"));
			static::$monolog = new Logger('CronLogger');	
			static::$monolog->pushHandler($handler, Logger::DEBUG);
		}

		static::$monolog->$level($message);
	}
}