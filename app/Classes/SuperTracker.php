<?php

namespace Thunk\Classes;

use Thunk\Classes\zKillboardAPI;
use Thunk\eveSupers;
use Thunk\eveSupercapUsers;
use Thunk\mapSolarSystem;
use Thunk\invType;
use Yajra\Datatables\Facades\Datatables;

class SuperTracker
{
    private static $shipTypes = array(671,3514,3764,11567,22852,23773,23913,23917,23919); // Erebus, Revenant, Leviathon, Avatar, Hel, Ragnarok, Nyx, Wyvern, Aeon

    public function __construct() {

    }

    public static function getShipTypes() {
        return static::$shipTypes;
    }

    public static function saveSupercap($characterID, $characterName, $shipTypeID, $firstSeen, $firstSeenSystemID, $lastSeen, $lastSeenSystemID, $corporationID, $corporationName, $allianceID, $allianceName, 
                                        $factionID, $factionName, $firstKillID, $lastKillID, $totalKills, $isDestroyed, $lastMailX, $lastMailY, $lastMailZ, $mailClosestCelestial, $mailClosestCelestialDistance) {

        // get other db data
        $shipTypeName = invType::getTypeName($shipTypeID);
        $firstSeenSystemName = mapSolarSystem::getSolarSystemName($firstSeenSystemID);
        $lastSeenSystemName = mapSolarSystem::getSolarSystemName($lastSeenSystemID);
     
        $firstSeenRegionName = mapSolarSystem::getRegionFromSystemID($firstSeenSystemID);
        $lastSeenRegionName = mapSolarSystem::getRegionFromSystemID($lastSeenSystemID);

        $super = eveSupers::firstOrNew(['characterID' => $characterID]);
        if(isset($super->totalKills) && $super->firstKillID != null)
            $totalKills += $super->totalKills;
        
        $super->shipTypeID = $shipTypeID;
        $super->shipTypeName = $shipTypeName;
        $super->lastSeen = $lastSeen;
        $super->lastSeenSystemID = $lastSeenSystemID;
        $super->lastSeenSystemName = $lastSeenSystemName;
        $super->lastSeenRegionName = $lastSeenRegionName;
        $super->totalKills = $totalKills;
        $super->corporationID = $corporationID;
        $super->corporationName = $corporationName;
        $super->allianceID = $allianceID;
        $super->allianceName = $allianceName;
        $super->factionID = $factionID;
        $super->factionName = $factionName;
        if(!isset($super->firstKillID) || $super->firstKillID == null) {
            $super->characterName = $characterName;
            $super->firstSeen = $firstSeen;
            $super->firstSeenSystemID = $firstSeenSystemID;
            $super->firstSeenSystemName = $firstSeenSystemName;
            $super->firstSeenRegionName = $firstSeenRegionName;
            $super->firstKillID = $firstKillID;
        }
        $super->lastKillID = $lastKillID;
        $super->isDestroyed = $isDestroyed;
        $super->lastMailX = $lastMailX;
        $super->lastMailY = $lastMailY;
        $super->lastMailZ = $lastMailZ;
        $super->mailClosestCelestial = $mailClosestCelestial;
        $super->mailClosestCelestialDistance = $mailClosestCelestialDistance;

        $super->save();


/*
        DB::connection(DB_PLKILLBOARD)->insert('insert ignore into eve_supercaps (characterID, characterName, shipTypeID, shipTypeName, 
            firstSeen, firstSeenSystemID, firstSeenSystemName, firstSeenRegionName, lastSeen, lastSeenSystemID, lastSeenSystemName, lastSeenRegionName, totalKills, corporationID, corporationName, allianceID, allianceName,
            factionID, factionName, isDestroyed, firstKillID, lastKillID, lastupdated) 
                        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now()) 
                        ON DUPLICATE KEY UPDATE shipTypeID = ?, shipTypeName = ?, lastSeen = ?, lastSeenSystemID = ?, lastSeenSystemName = ?, lastSeenRegionName = ?,
                        totalKills = totalKills + ?, corporationID = ?, corporationName = ?, allianceID = ?, allianceName = ?, factionID = ?, factionName =?,
                        isDestroyed = ?, lastKillID = ?, lastupdated = now()', 
                        array($characterID, $characterName, $shipTypeID, $shipTypeName, $firstSeen, $firstSeenSystemID, $firstSeenSystemName, $firstSeenRegionName, $lastSeen, $lastSeenSystemID, $lastSeenSystemName, $lastSeenRegionName, 
                            $totalKills, $corporationID, $corporationName, $allianceID, $allianceName, $factionID, $factionName, $isDestroyed, $firstKillID, $lastKillID,
                            $shipTypeID, $shipTypeName, $lastSeen, $lastSeenSystemID, $lastSeenSystemName, $lastSeenRegionName, $totalKills, $corporationID, 
                            $corporationName, $allianceID, $allianceName, $factionID, $factionName, $isDestroyed, $lastKillID));
        */

        return;
        
    }

    public static function updateEveSuperPilot($characterID, $characterName, $shipTypeID, $lastSeen, $lastSeenSystemID, $corporationID, $corporationName, $allianceID, $allianceName, $factionID, $factionName,
                                                 $lastKillID, $isDestroyed, $lastMailX, $lastMailY, $lastMailZ, $mailClosestCelestial, $mailClosestCelestialDistance) {
        // get other db data
        $shipTypeName = invType::getTypeName($shipTypeID);
        $lastSeenSystemName = mapSolarSystem::getSolarSystemName($lastSeenSystemID);

        if($isDestroyed == 'true')
            $totalKills = 0;
        else
            $totalKills = 1;

        $lastSeenRegionName = mapSolarSystem::getRegionFromSystemID($lastSeenSystemID);

        $super = eveSupers::firstOrNew(['characterID' => $characterID]);

        // common to both
        $super->shipTypeID = $shipTypeID;
        $super->shipTypeName = $shipTypeName;
        $super->lastSeenSystemID = $lastSeenSystemID;
        $super->lastSeenSystemName = $lastSeenSystemName;
        $super->lastSeenRegionName = $lastSeenRegionName;
        $super->corporationID = $corporationID;
        $super->corporationName = $corporationName;
        $super->allianceID = $allianceID;
        $super->allianceName = $allianceName;
        $super->factionID = $factionID;
        $super->factionName = $factionName;
        $super->isDestroyed = $isDestroyed;
        $super->lastKillID = $lastKillID;
        $super->lastMailX = $lastMailX;
        $super->lastMailY = $lastMailY;
        $super->lastMailZ = $lastMailZ;
        $super->mailClosestCelestial = $mailClosestCelestial;
        $super->mailClosestCelestialDistance = $mailClosestCelestialDistance;
//echo "check\r\n";
        if(isset($super->characterName) && $super->characterName != null) {
            // update
            //echo $super->characterName.': '.$super->lastSeen.'('.strtotime($super->lastSeen).') - '.$lastSeen."(".strtotime($lastSeen).")\r\n";
            if(strtotime($super->lastSeen) < strtotime($lastSeen)) {
                $super->lastSeen = $lastSeen;
                $super->totalKills++;

                $super->save();
                //echo "update\r\n";
            }
        } else {
            // insert
            $super->characterID = $characterID;
            $super->characterName = $characterName;
            $super->firstSeen = $lastSeen;
            $super->firstSeenSystemID = $lastSeenSystemID;
            $super->firstSeenSystemName = $lastSeenSystemName;
            $super->firstSeenRegionName = $lastSeenRegionName;
            $super->lastSeen = $lastSeen;
            $super->totalKills = $totalKills;
            $super->firstKillID = $lastKillID;

            $super->save();
            //echo "insert/r/n";
        }

        
    }

     public static function getEveSuperPilotTableData($filterStartDate, $filterEndDate) {
        //$PLAuth = Config::get('plconfig');
        //$standings = apiDB::getContactList(828800677,"alliance"); // Snigg

          

        //DB::connection(DB_PLKILLBOARD)->flushQueryLog(); // clears QueryLog

        $ownerID = null;
        if(session()->has('sso_characterid'))
            $ownerID = session('sso_characterid');

        $superCaps = eveSupers::getSuperCapTable($ownerID, $filterStartDate, $filterEndDate);
 /*       
        $superCaps = DB::connection(DB_PLKILLBOARD)
            ->table('eve_supercaps AS s')
            ->select(array('s.characterID', 's.characterName', 's.corporationName', 's.allianceName', 's.factionName', 's.totalKills', 's.shipTypeName', 
                DB::connection(DB_PLKILLBOARD)->raw('IF(s.shipTypeName = "Avatar" OR s.shipTypeName = "Erebus" OR s.shipTypeName = "Leviathan" OR s.shipTypeName = "Ragnarok", "T", "SC") AS shipClass'), 
                's.lastSeen','s.lastSeenSystemName', 's.lastSeenRegionName',
                's.corporationID', 's.allianceID', 's.factionID',
                's.notes', 's.associatedChars', 's.apiKey', 's.apiID', 's.shipClosestCelestial', 's.shipClosestCelestialDistance',
                's.isDestroyed', 's.lastKillID'));
*/
/*
        if($filterStartDate)
            $superCaps->where('s.lastseen', '>', $filterStartDate." 00:00:00");

        if($filterEndDate)
            $superCaps->where('s.lastseen', '<', $filterEndDate." 23:59:59");
*/
        $retData = Datatables::of($superCaps)->make();

        //return $retData;
        $jdata = json_decode($retData->getContent());
        $data = $jdata->data;

        $newData = array();

        foreach($data as $ele) {
            $newEle = array();

            $newEle['characterID'] = $ele[0];
            $newEle['characterName'] = $ele[1];
            $newEle['corporationName'] = $ele[2];
            $newEle['allianceName'] = $ele[3];
            $newEle['factionName'] = $ele[4];
            $newEle['totalKills'] = $ele[5];
            $newEle['shipTypeName'] = $ele[6];
            $newEle['shipClass'] = $ele[7];
            $newEle['lastSeen'] = $ele[8];
            $newEle['lastSeenSystemName'] = $ele[9];
            $newEle['lastSeenRegionName'] = $ele[10];
            $newEle['corporationID'] = $ele[11];
            $newEle['allianceID'] = $ele[12];
            $newEle['factionID'] = $ele[13];
            $newEle['notes'] = $ele[14];
            $newEle['associatedChars'] = $ele[15];
            if($ele[16] === null)
                $newEle['apiKey'] = $ele[16];                                               // mask not needed
            else
                $newEle['apiKey'] = substr($ele[16],0,6) . str_repeat("*",4);               // mask part of the key
            $newEle['apiID'] = $ele[17];
            $newEle['shipClosestCelestial'] = $ele[18];
            $newEle['shipClosestCelestialDistance'] = $ele[19];
            $newEle['isDestroyed'] = $ele[20];
            $newEle['lastKillID'] = $ele[21];
            $newEle['characterStanding'] = null;
            $newEle['corporationStanding'] = null;
            $newEle['allianceStanding'] = null;
            $newEle['factionStanding'] = null;
            // standings
            if(isset($standings)) {
                foreach($standings as $standing) {
                    if($ele[0] == $standing->contactID && $standing->standing != 0)
                        $newEle['characterStanding'] = $standing->standing;
                        
                    if($ele[11] == $standing->contactID && $standing->standing != 0)
                        $newEle['corporationStanding'] = $standing->standing;                     

                    if($ele[12] == $standing->contactID && $standing->standing != 0)
                        $newEle['allianceStanding'] = $standing->standing;

                    if($ele[13] == $standing->contactID && $standing->standing != 0)
                        $newEle['factionStanding'] = $standing->standing;                  
                }
            }
            $newData[] = $newEle;
        }
        
        $jdata->data = $newData;
        return json_encode($jdata);
    }

    public static function setSuperCapPilotNotes($charID, $text) {
        if(!session()->has('sso_characterid'))
            return false;

        $ownerID = session('sso_characterid');

        //$data = eveSupercapUsers::setNote($ownerID, $charID, $text);
        $super = eveSupercapUsers::firstOrNew(['ownerID' => $ownerID, 'characterID' => $charID]);
        $super->notes = $text;
        $super->save();

        return $super;
    }

    public static function UpdateCharWithAPI($charID) {
        /*
        $isPL = false;
        $supersArray = array("Aeon", "Wyvern", "Nyx", "Hel", "Avatar", "Leviathan", "Erebus", "Ragnarok", "Revenant");

        // get api Info
        $info = corekb::getSuperCapPilot($charID);
        $affiliateInfo = api::getCharacterAffiliation($charID);

        if(!$info)
            return json_encode(array('errors' => true, 'errorMessage' => 'Could not find Pilot'));

        $xml = api::fetchXML("Eve/CharacterInfo.xml.aspx?keyID=".$info->apiID."&vCode=".$info->apiKey."&characterID=".$charID, true);
        $data = api::CharacterInfoProcessXML($xml, false);

        if($data) {
            if(in_array((string)$data['shipTypeName'],$supersArray)) {
                // get assets for location
                $xml = api::fetchXML("Char/AssetList.xml.aspx?keyID=".$info->apiID."&vCode=".$info->apiKey."&characterID=".$charID, true);
                $assets = api::AssetListProcessXML($xml, false);
                if($assets) {
                    foreach($assets as $asset) {
                        $locationName = invNamesCCP::getName((integer)$asset['locationID']);
                        if($asset['typeID'] == (integer)$data['shipTypeID'] && $locationName == (string)$data['lastKnownLocation']) {
                            //var_dump($asset['itemID']);
                            // get location
                            $xml = api::fetchXML("Char/Locations.xml.aspx?keyID=".$info->apiID."&vCode=".$info->apiKey."&characterID=".$charID."&ids=".(string)$asset['itemID'], true);
                            $data['itemID'] = (string)$asset['itemID'];
                            $data['x'] = (string)$xml->result->rowset->row['x'];
                            $data['x'] = (string)$xml->result->rowset->row['x'];
                            $data['y'] = (string)$xml->result->rowset->row['y'];
                            $data['z'] = (string)$xml->result->rowset->row['z'];
                            
                            $closest = mapSolarSystemCCP::getNearestCelestialObject((integer)$asset['locationID'], (string)$data['x'], (string)$data['y'], (string)$data['z']);
                            $data['closestName'] = $closest['name'];
                            $data['closestDistance'] = $closest['distance'];
                        }
                    }

                    if($data['alliance'] == "Pandemic Legion" || $data['alliance'] == "WAFFLES." || $data['alliance'] == "Pandemic Horde")
                        $isPL = true;
                
                    if(!$isPL) {
                        $locationID = mapSolarSystemCCP::getSolarSystemID($data['lastKnownLocation']);
                        $regionName = mapSolarSystemCCP::getRegionFromSystemID($locationID);

                        if($data['lastKnownLocation'] != $info->lastSeenSystemName || $data['x'] != $info->shipPositionX || $data['y'] != $info->shipPositionY || $data['z'] != $info->shipPositionZ) {
                            corekb::setSuperCapPilotInfoFromCharAPI($data["characterID"], $data["characterName"], $data["shipTypeID"], $data["shipTypeName"], $data['lastKnownLocation'],
                                $data['corporationID'], $data['corporation'], $data['allianceID'], $data['alliance'], $affiliateInfo[$charID]['factionID'], $affiliateInfo[$charID]['factionName'], 
                                $info->apiID, $info->apiKey, $data['itemID'], $data['x'], $data['y'], $data['z'], $data['closestName'], $data['closestDistance'], date("Y-m-d H:i:s", time()), $data['shipName']);
                            
                            return(json_encode(array('errors' => false, 'characterName' => $data["characterName"], 'corporationName' => $data['corporation'],
                                                    'allianceName' => $data['alliance'], 'factionName' => $affiliateInfo[$charID]['factionName'], 'shipTypeName' => $data['shipTypeName'],
                                                    'lastSeenLocationName' => $data['lastKnownLocation'], 'lastSeenRegionName'=>$regionName, 'lastSeen' => gmdate("Y-m-d H:i:s", time()),
                                                    'closestName' => $data['closestName'], 'closestDistance' => $data['closestDistance'])));
                        } else {
                            // hasn't moved
                            return json_encode(array('errors' => true, 'errorMessage' => 'Has not Moved'));
                        }
                    } else {
                        //Session::flash('errormessage', 'Key not added to SuperTracker: PL Key');
                        return json_encode(array('errors' => true, 'errorMessage' => 'Key not added to SuperTracker: PL Key'));
                    }
                } else {
                    //Session::flash('errormessage', 'Key not added to SuperTracker: No Assets access');
                    return json_encode(array('errors' => true, 'errorMessage' => 'Key not added to SuperTracker: No Assets access'));
                }
            } else {
                return json_encode(array('errors' => true, 'errorMessage' => 'Is not in a Supercap'));
            }
        }
        /**/
    }
}