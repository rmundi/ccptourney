<?php

namespace Thunk;

use Illuminate\Database\Eloquent\Model;

class apiStanding extends Model
{
	private static $outlawCorps = array("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha\'s Nation","Serpentis");

    public $primaryKey = 'ownerID';
    public $timestamps = false;
    protected $table = 'apiStandings';
    protected $fillable = [ 'ownerID',  
    						'type', 
    						'fromID', 
    						'fromName', 
    						'standing'];


    /*
		SELECT s.*, f.factionName, con.level, cri.level, dip.level,
		IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
		IF(s.standing < 0, s.standing+((10-s.standing)*0.04*dip.level), s.standing+((10-s.standing)*0.04*con.level))
		,IF(s.standing < 0, s.standing+((10-s.standing)*0.04*dip.level), s.standing+((10-s.standing)*0.04*cri.level))
		) AS adjStanding
		FROM apiStandings AS s
		LEFT JOIN crpNPCCorporations AS c ON s.fromID = c.corporationID
		LEFT JOIN chrFactions AS f ON f.factionID = c.factionID
		LEFT JOIN apiSkills AS con ON con.characterID = s.ownerID AND con.typeID = 3359
		LEFT JOIN apiSkills AS cri ON cri.characterID = s.ownerID AND cri.typeID = 3361
		LEFT JOIN apiSkills AS dip ON dip.characterID = s.ownerID AND dip.typeID = 3357
		WHERE s.type = "NPCCorporations"

		(same for factions)

		Improved
		SELECT s.*, f.factionName,
		IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
		IF(s.standing < 0, s.standing+((10-s.standing)*0.04*dip.level), s.standing+((10-s.standing)*0.04*con.level))
		,IF(s.standing < 0, s.standing+((10-s.standing)*0.04*dip.level), s.standing+((10-s.standing)*0.04*cri.level))
		) AS adjStanding,
		IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
		IF(s.standing < 0, "Diplomacy", "Connections")
		,IF(s.standing < 0, "Diplomacy", "Criminal Connections")
		) AS adjSkill,
		IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
		IF(s.standing < 0, dip.level, con.level)
		,IF(s.standing < 0, dip.level, cri.level)
		) AS adjSkillLevel
		FROM apiStandings AS s
		LEFT JOIN crpNPCCorporations AS c ON s.fromID = c.corporationID
		LEFT JOIN chrFactions AS f ON f.factionID = c.factionID
		LEFT JOIN apiSkills AS con ON con.characterID = s.ownerID AND con.typeID = 3359
		LEFT JOIN apiSkills AS cri ON cri.characterID = s.ownerID AND cri.typeID = 3361
		LEFT JOIN apiSkills AS dip ON dip.characterID = s.ownerID AND dip.typeID = 3357
		WHERE s.type = "NPCCorporations"
		ORDER BY adjStanding DESC

		Best
		SELECT apiStandings.*, f.factionName,
		IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
		IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*con.level),2))
		,IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*cri.level),2))
		) AS adjStanding,
		IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
		IF(apiStandings.standing < 0, "Diplomacy", "Connections"),IF(apiStandings.standing < 0, "Diplomacy", "Criminal Connections")) AS adjSkill,
		IF(f.factionName NOT IN("Guristas Pirates","Angel Cartel","Blood Raider Covenant","Sansha's Nation","Serpentis"),
		IF(apiStandings.standing < 0, dip.level, con.level),IF(apiStandings.standing < 0, dip.level, cri.level)) AS adjSkillLevel
		FROM apiStandings
		LEFT JOIN crpNPCCorporations AS c ON apiStandings.fromID = c.corporationID
		LEFT JOIN chrFactions AS f ON f.factionID = c.factionID
		LEFT JOIN apiSkills AS con ON con.characterID = apiStandings.ownerID AND con.typeID = 3359
		LEFT JOIN apiSkills AS cri ON cri.characterID = apiStandings.ownerID AND cri.typeID = 3361
		LEFT JOIN apiSkills AS dip ON dip.characterID = apiStandings.ownerID AND dip.typeID = 3357
		WHERE apiStandings.type = "NPCCorporations"
		ORDER BY adjStanding DESC
$data = $query->select('a.attributeID', \DB::raw('coalesce(valueFloat,valueInt) AS value'))
                ->leftjoin('dgmTypeAttributes AS a', 'a.typeID', '=', 't.typeID')
                ->whereIn('a.attributeID', array(1374,1375,1376))
                ->where('t.typeName', '=', $shipType)
                //->remember(CPPDB_REMEMBER_TIME)
                ->get();
    */

    public function scopeGetNPCCorporationStandings($query, $ownerID = null) {
        if(!isset($ownerID))
            return false;

        $data = $query->select('apiStandings.*', 'f.factionName', 'f.factionID',
        			\DB::raw('IF(IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
								IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*con.level),2))
								,IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*cri.level),2))
								) IS NULL, apiStandings.standing, IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
								IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*con.level),2))
								,IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*cri.level),2))
								)) AS adjStanding'),
        			\DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
						IF(apiStandings.standing < 0, "Diplomacy", "Connections"),IF(apiStandings.standing < 0, "Diplomacy", "Criminal Connections")) AS adjSkill'),
        			\DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
						IF(apiStandings.standing < 0, dip.level, con.level),IF(apiStandings.standing < 0, dip.level, cri.level)) AS adjSkillLevel'))
                ->leftjoin('crpNPCCorporations AS c', 'apiStandings.fromID', '=', 'c.corporationID')
                ->leftjoin('chrFactions AS f', 'f.factionID', '=', 'c.factionID')
                ->leftjoin('apiSkills AS dip', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'dip.characterID')->where('dip.typeID', '=', 3357);
                })
                ->leftjoin('apiSkills AS con', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'con.characterID')->where('con.typeID', '=', 3359);
                })
                ->leftjoin('apiSkills AS cri', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'cri.characterID')->where('cri.typeID', '=', 3361);
                })
                ->where('apiStandings.ownerID', '=', $ownerID)
                ->where('apiStandings.type', '=', "NPCCorporations")
                ->orderBy('adjStanding', 'DESC')
                ->get();

        return $data;
    }

    public function scopeGetFactionStandings($query, $ownerID = null) {
        if(!isset($ownerID))
            return false;

        $data = $query->select('apiStandings.*', 
        			\DB::raw('IF(IF(apiStandings.fromName NOT IN("'.implode('","', static::$outlawCorps).'"),
								IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*con.level),2))
								,IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*cri.level),2))
								) IS NULL, apiStandings.standing, IF(apiStandings.fromName NOT IN("'.implode('","', static::$outlawCorps).'"),
								IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*con.level),2))
								,IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*cri.level),2))
								)) AS adjStanding'),
        			\DB::raw('IF(apiStandings.fromName NOT IN("'.implode('","', static::$outlawCorps).'"),
						IF(apiStandings.standing < 0, "Diplomacy", "Connections"),IF(apiStandings.standing < 0, "Diplomacy", "Criminal Connections")) AS adjSkill'),
        			\DB::raw('IF(apiStandings.fromName NOT IN("'.implode('","', static::$outlawCorps).'"),
						IF(apiStandings.standing < 0, dip.level, con.level),IF(apiStandings.standing < 0, dip.level, cri.level)) AS adjSkillLevel'))
                ->leftjoin('crpNPCCorporations AS c', 'apiStandings.fromID', '=', 'c.corporationID')
                ->leftjoin('apiSkills AS dip', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'dip.characterID')->where('dip.typeID', '=', 3357);
                })
                ->leftjoin('apiSkills AS con', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'con.characterID')->where('con.typeID', '=', 3359);
                })
                ->leftjoin('apiSkills AS cri', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'cri.characterID')->where('cri.typeID', '=', 3361);
                })
                ->where('apiStandings.ownerID', '=', $ownerID)
                ->where('apiStandings.type', '=', "factions")
                ->orderBy('adjStanding', 'DESC')
                ->get();

        return $data;
    }

    public function scopeGetAgentsStandings($query, $ownerID = null) {
        if(!isset($ownerID))
            return false;

        $data = $query->select('apiStandings.*','a.level', 'a.isLocator', 't.agentType', 'd.divisionName', 'n.itemName', 'f.factionName', 
                    \DB::raw('IF(sta.stationName IS NULL, sys.solarSystemName, sta.stationName) as locationName'), 
                    \DB::raw('IF(sta.stationName IS NULL, sys.solarSystemID, sta.solarSystemID) as systemID'),
                    \DB::raw('IF(sta.stationName IS NULL, sys.solarSystemName, stasys.solarSystemName) as systemName'),
                    \DB::raw('IF(sta.stationName IS NULL, sys.security, stasys.security) as security'),
                    'a.locationID',
        			\DB::raw('IF(IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
								IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*con.level),2))
								,IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*cri.level),2))
								) IS NULL, apiStandings.standing, IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"),
								IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*con.level),2))
								,IF(apiStandings.standing < 0, TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*dip.level),2), TRUNCATE(apiStandings.standing+((10-apiStandings.standing)*0.04*cri.level),2))
								)) AS adjStanding'),
        			\DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(apiStandings.standing < 0, "Diplomacy", "Connections"),IF(apiStandings.standing < 0, "Diplomacy", "Criminal Connections")) AS adjSkill'),
        			\DB::raw('IF(f.factionName NOT IN("'.implode('","', static::$outlawCorps).'"), IF(apiStandings.standing < 0, dip.level, con.level),IF(apiStandings.standing < 0, dip.level, cri.level)) AS adjSkillLevel'))
                ->leftjoin('agtAgents AS a', 'apiStandings.fromID', '=', 'a.agentID')
                ->leftjoin('agtAgentTypes AS t', 't.agentTypeID', '=', 'a.agentTypeID')
                ->leftjoin('crpNPCDivisions AS d', 'd.divisionID', '=', 'a.divisionID')
                ->leftjoin('crpNPCCorporations AS c', 'a.corporationID', '=', 'c.corporationID')
                ->leftjoin('chrFactions AS f', 'f.factionID', '=', 'c.factionID')
                ->leftjoin('staStations AS sta', 'sta.stationID', '=', 'a.locationID')
                ->leftjoin('mapSolarSystems AS sys', 'sys.solarSystemID', '=', 'a.locationID')
                ->leftjoin('mapSolarSystems AS stasys', 'stasys.solarSystemID', '=', 'sta.solarSystemID')
                ->leftjoin('invNames AS n', 'n.itemID', '=', 'a.corporationID')
                ->leftjoin('apiSkills AS dip', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'dip.characterID')->where('dip.typeID', '=', 3357);
                })
                ->leftjoin('apiSkills AS con', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'con.characterID')->where('con.typeID', '=', 3359);
                })
                ->leftjoin('apiSkills AS cri', function($join) 
                {
                    $join->on('apiStandings.ownerID', '=', 'cri.characterID')->where('cri.typeID', '=', 3361);
                })
                ->where('apiStandings.ownerID', '=', $ownerID)
                ->where('apiStandings.type', '=', "agents")
                ->orderBy('adjStanding', 'DESC')
                ->get();

        return $data;
    }
}
