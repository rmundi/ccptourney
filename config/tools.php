<?php
//define('SSO_LOGIN_URL', 'https://login.eveonline.com/oauth/token');
//define('SSO_VERIFY_URL', 'https://login.eveonline.com/oauth/verify');
//define('SSO_AUTH_URL', 'https://login.eveonline.com/oauth/authorize');
return [
    'app-ver' => '0.016',
    'user-agent' => 'Captain Thunks Toolset',
    // will write calcs to file if enabled
    'calc-debug' => true,
    'DT_ERRORTYPE' => 'throw',      // throw / none
    'db-cache-time' => 1440,
    'crest-cache-dir' => __DIR__.'/../storage/crest/',
    'ccp-image-url' => 'https://image.eveonline.com/',
    'cdn-ccp-image-url' => 'https://images.cdn1.eveonline.com/',

    'root' => 'https://crest-tq.eveonline.com/',
    'sso-login-url' => 'https://login.eveonline.com/oauth/token',
    'sso-verify-url' => 'https://login.eveonline.com/oauth/verify',
    'sso-auth-url' => 'https://login.eveonline.com/oauth/authorize',
    'sso-id' => env('CREST_APP_ID'),
    'sso-key' => env('CREST_APP_KEY'),
    'sso-url' => env('CREST_APP_URL'),
    'sso-scope' => ['characterAccountRead',
                    'characterAssetsRead',
                    'characterBookmarksRead',
                    //'characterCalendarRead',
                    'characterChatChannelsRead',
                    'characterClonesRead',
                    'characterContactsRead',
                    'characterContactsWrite',
                    'characterContractsRead',
                    //'characterFactionalWarfareRead',
                    'characterFittingsRead',
                    'characterFittingsWrite',
                    'characterIndustryJobsRead',
                    'characterKillsRead',
                    'characterLocationRead',
                    'characterLoyaltyPointsRead',
                    'characterMailRead',
                    'characterMarketOrdersRead',
                    'characterMedalsRead',
                    'characterNavigationWrite',
                    'characterNotificationsRead',
                    'characterOpportunitiesRead',
                    'characterResearchRead',
                    'characterSkillsRead',
                    'characterStatsRead',
                    'characterWalletRead',
                    'fleetRead',
                    'fleetWrite',
                    'publicData',
                    'remoteClientUI',
                    'structureVulnUpdate'],
    
    'versions' => [
        //'root' => 'application/vnd.ccp.eve.Api-v3+json',
        //'decode' => 'application/vnd.ccp.eve.TokenDecode-v1+json',
        //'character' => 'application/vnd.ccp.eve.Character-v3+json',
        //'contacts' => 'application/vnd.ccp.eve.ContactCollection-v2+json',
    ],
];
// Some Globals
//define('AppVer', '0.012');
//define('CPPDB_REMEMBER_TIME', 1440); // Length of time used for standard DB queries
//define('T_URL', 'https://crest-tq.eveonline.com/');
//define('T_URL', 'https://public-crest.eveonline.com/');
//define('T_ICON', 'https://images.cdn1.eveonline.com/');
//define('F_ICON', 'https://image.eveonline.com/');

//define('LOGOPATH', F_ICON.'Corporation/698393874_64.png');

//define('CRESTCACHEDIR', base_path('storage/crest/'));
//define('CRESTCACHEDIR', __DIR__.'/../../storage/crest/');

//define('SSO_LOGIN_URL', 'https://login.eveonline.com/oauth/token');
//define('SSO_VERIFY_URL', 'https://login.eveonline.com/oauth/verify');
//define('SSO_AUTH_URL', 'https://login.eveonline.com/oauth/authorize');
/*define('SSO_SCOPE', 'characterAccountRead characterAssetsRead characterBookmarksRead characterCalendarRead characterChatChannelsRead characterClonesRead characterContactsRead characterContactsWrite characterContractsRead characterFactionalWarfareRead characterFittingsRead characterFittingsWrite characterIndustryJobsRead characterKillsRead characterLocationRead characterLoyaltyPointsRead characterMailRead characterMarketOrdersRead characterMedalsRead characterNavigationWrite characterNotificationsRead characterOpportunitiesRead characterResearchRead characterSkillsRead characterStatsRead characterWalletRead fleetRead fleetWrite publicData structureVulnUpdate'); // https://eveonline-third-party-documentation.readthedocs.io/en/latest/sso/authentication.html#implementing-the-sso
*/
// will write calcs to file if enabled
//define('CALC_DEBUG', true);

/* Scope List CREST
characterAccountRead: Read your account subscription status.
characterAssetsRead: Read your asset list.
characterBookmarksRead: List your bookmarks and their coordinates.
characterCalendarRead: Read your calendar events and attendees.
characterChatChannelsRead: List chat channels you own or operate.
characterClonesRead: List your jump clones, implants, attributes, and jump fatigue timer.
characterContactsRead: Allows access to reading your characters contacts.
characterContactsWrite: Allows applications to add, modify, and delete contacts for your character.
characterContractsRead: Read your contracts.
characterFactionalWarfareRead: Read your factional warfare statistics.
characterFittingsRead: Allows an application to view all of your character's saved fits.
characterFittingsWrite: Allows an application to create and delete the saved fits for your character.
characterIndustryJobsRead: List your industry jobs.
characterKillsRead: Read your kill mails.
characterLocationRead: Allows an application to read your characters real time location in EVE.
characterLoyaltyPointsRead: List loyalty points your character has for the different corporations.
characterMailRead: Read your EVE Mail.
characterMarketOrdersRead: Read your market orders.
characterMedalsRead: List your public and private medals.
characterNavigationWrite: Allows an application to set your ships autopilot destination.
characterNotificationsRead: Receive in-game notifications.
characterOpportunitiesRead: List the opportunities your character has completed.
characterResearchRead: List your research agents working for you and research progress.
characterSkillsRead: Read your skills and skill queue.
characterStatsRead: Yearly aggregated stats about your character.
characterWalletRead: Read your wallet status, transaction, and journal history.
corporationAssetRead: Read your corporation's asset list.
corporationBookmarksRead: List your corporation's bookmarks and their coordinates.
corporationContractsRead: List your corporation's contracts.
corporationFactionalWarfareRead: Read your corporation's factional warfare statistics.
corporationIndustryJobsRead: List your corporation's industry jobs.
corporationKillsRead: Read your corporation's kill mails.
corporationMarketOrdersRead: List your corporation's market orders.
corporationMedalsRead: List your corporation's issued medals.
corporationMembersRead: List your corporation's members, their titles, and roles.
corporationShareholdersRead: List your corporation's shareholders and their shares.
corporationStructuresRead: List your corporation's structures, outposts, and starbases.
corporationWalletRead: Read your corporation's wallet status, transaction, and journal history.
fleetRead: Allows real time reading of your fleet information (members, ship types, etc.) if you're the boss of the fleet.
fleetWrite: Allows the ability to invite, kick, and update fleet information if you're the boss of the fleet.
publicData: Allows access to public data.
structureVulnUpdate: Allows updating your structures' vulnerability timers.

*/